-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 04-01-2019 a las 13:41:20
-- Versión del servidor: 10.0.37-MariaDB-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `porvenir_prohunter`
--
CREATE DATABASE IF NOT EXISTS `porvenir_prohunter` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `porvenir_prohunter`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `Actualizar_Blog_Aviso`$$
$$

DROP PROCEDURE IF EXISTS `Cambiar_status_postulado`$$
$$

DROP PROCEDURE IF EXISTS `Eliminar_Candidato`$$
CREATE  PROCEDURE `Eliminar_Candidato` (`_id_Candidato` BIGINT)  BEGIN

SET SQL_SAFE_UPDATES = 0;
DELETE FROM candidates where id = _id_Candidato;
DELETE FROM candidate_vacancy where candidate_id = _id_Candidato;
DELETE FROM candidate_language where candidate_id = _id_Candidato;
DELETE FROM candidate_software where candidate_id = _id_Candidato;

END$$

DROP PROCEDURE IF EXISTS `Eliminar_Categoria`$$
CREATE  PROCEDURE `Eliminar_Categoria` (IN `_id` BIGINT)  BEGIN
    IF(EXISTS(select * from categories WHERE id = _id)) THEN 
          IF(EXISTS(select * from vacancies where category_id = _id)) THEN 
              SELECT 'Esta categoria tiene vacantes activas, primero elimina o modifica las vacantes 'as msg;
		  ELSE
                DELETE FROM categories WHERE id = _id;
                DELETE FROM category_preference WHERE category_id = _id;
                SELECT 'Eliminado correctamente' as msg;
	      END IF;
	ELSE
            SELECT 'No se encontro la categoria'as msg;
    END IF;
END$$

DROP PROCEDURE IF EXISTS `Eliminar_Usuarios`$$
CREATE  PROCEDURE `Eliminar_Usuarios` (`_id` BIGINT)  BEGIN

IF(EXISTS(SELECT * FROM users WHERE id = _id)) THEN 

DELETE FROM users WHERE id = _id;
SELECT 'ELIMINADO';
ELSE
SELECT 'NO SE ENCONTRO REGISTRO';
END IF;

END$$

DROP PROCEDURE IF EXISTS `Eliminar_Vacantes`$$
CREATE  PROCEDURE `Eliminar_Vacantes` (IN `_Id_Vacante` INT)  BEGIN

SET SQL_SAFE_UPDATES = 0;
DELETE FROM vacancies WHERE ID = _Id_Vacante;
DELETE FROM candidate_vacancy WHERE vacancy_id = _Id_Vacante;
DELETE FROM user_vacancy WHERE vacancy_id = _Id_Vacante;
SELECT 'Eliminado';
SET SQL_SAFE_UPDATES = 1;

END$$

DROP PROCEDURE IF EXISTS `Elimina_Localizacion`$$
$$

DROP PROCEDURE IF EXISTS `Elimina_Software`$$
$$

DROP PROCEDURE IF EXISTS `Estadistica_Candidatos_ultimos_30_dias`$$
$$

DROP PROCEDURE IF EXISTS `Ganacias_usuario`$$
$$

DROP PROCEDURE IF EXISTS `Grafica_Actividad_Usuarios`$$
$$

DROP PROCEDURE IF EXISTS `Insertar_Software_Categoria`$$
$$

DROP PROCEDURE IF EXISTS `sp_whachado`$$
$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `about`
--

DROP TABLE IF EXISTS `about`;
CREATE TABLE `about` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `about`
--

INSERT INTO `about` (`id`, `text`, `image_url`, `created_at`, `updated_at`) VALUES
(1, 'Somos una empresa comprometida con la calidad de nuestros productos para así incentivar el desarrollo tecnológico de nuestros trabajadores y del país.', NULL, '2018-06-27 21:32:25', '2018-08-28 03:33:55'),
(8, NULL, NULL, '2018-08-20 23:48:09', '2018-08-20 23:48:09'),
(9, NULL, NULL, '2018-08-20 23:48:19', '2018-08-20 23:48:19'),
(11, NULL, 'https://porvenirapps.com/prohunter/storage/app/about_images/jEx9wpctqqbr3bs7wWPpYMNGLGuZCkBBzITOKzYF.jpeg', '2018-08-21 00:30:20', '2018-08-21 00:30:20'),
(13, NULL, NULL, '2018-08-28 03:33:36', '2018-08-28 03:33:36'),
(14, NULL, NULL, '2018-08-28 03:33:40', '2018-08-28 03:33:40'),
(16, NULL, NULL, '2018-10-03 19:27:39', '2018-10-03 19:27:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrator_modules_assigned`
--

DROP TABLE IF EXISTS `administrator_modules_assigned`;
CREATE TABLE `administrator_modules_assigned` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `modules` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrator_modules_assigned`
--

INSERT INTO `administrator_modules_assigned` (`id`, `user_id`, `modules`, `created_at`, `updated_at`) VALUES
(1, 1, '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"12\",\"13\",\"14\",\"15\"]', '2018-10-31 16:55:56', '2018-10-31 20:55:56'),
(2, 66, '[\"1\",\"2\",\"3\",\"7\",\"9\",\"13\"]', '2018-10-30 20:02:00', '2018-10-31 00:02:00'),
(3, 68, '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"12\",\"13\",\"14\"]', '2018-10-31 00:02:35', '2018-10-31 00:02:35'),
(4, 87, '[\"1\",\"2\",\"3\",\"4\"]', '2018-10-31 00:21:31', '2018-10-31 00:21:31'),
(5, 90, '[\"1\",\"3\",\"5\"]', '2018-11-05 22:55:39', '2018-11-05 22:55:39'),
(6, 91, '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"12\",\"13\",\"14\",\"15\"]', '2018-11-06 21:32:19', '2018-11-06 21:32:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `announcements`
--

DROP TABLE IF EXISTS `announcements`;
CREATE TABLE `announcements` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `title` varchar(150) NOT NULL,
  `subtitle` varchar(150) DEFAULT NULL,
  `body` text,
  `message` text,
  `icon` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `watched` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `announcements`
--

INSERT INTO `announcements` (`id`, `user_id`, `title`, `subtitle`, `body`, `message`, `icon`, `created_at`, `updated_at`, `watched`) VALUES
(1, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Jorge Octavio González  Manzano en la vacante Desarrollador Android de la empresa Oomovil!', '', '8', '2018-11-26 18:12:32', '2018-11-26 17:05:26', 0),
(2, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante Desarrollador Android', '', '1', '2018-11-26 18:12:32', '2018-11-26 17:05:26', 0),
(3, 4, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Sandy Ramirez Magallanes en la vacante Desarrollador Android de la empresa Oomovil!', '', '8', '2018-09-19 18:17:02', '2018-09-19 18:17:02', 1),
(4, 2, '¡En entrevista!', '', '¡El postulado Jorge Octavio Manzano González  fue llamado a entrevista!', 'Entrevista a las 11:00 am', '3', '2018-11-26 18:12:32', '2018-11-26 17:05:26', 0),
(5, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Jorge Octavio Manzano González  ha sido revisado!', 'Todo párrafo se caracteriza por ser dueño de un conjunto de oraciones que concatenadas a través de la unidad y la coherencia, proyectan una idea sólida y consumada que cumple así la exigencia entre lo que pensamos, lo que queremos decir, y lo que realmente decimos', '2', '2018-11-26 18:12:32', '2018-11-26 17:05:26', 0),
(6, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Octavio  Nznx Kskd en la vacante Desarrollador Android de la empresa Oomovil!', '', '8', '2018-11-26 18:12:32', '2018-11-26 17:05:26', 0),
(7, 2, '¡Candidato postulado!', '', '¡Has postulado a Octavio  Kskd Nznx en la vacante Desarrollador Android', '', '1', '2018-11-26 18:12:32', '2018-11-26 17:05:26', 0),
(8, 2, '¡En entrevista!', '', '¡El postulado Jorge Octavio Manzano González  fue llamado a entrevista!', 'ASDAFF', '3', '2018-11-26 18:12:30', '2018-11-26 17:05:26', 0),
(9, 2, '¡Examenes psicométricos!', '', 'Tu postulado Jorge Octavio Manzano González  hara examenes psicométicos', 'Aefaef', '4', '2018-11-26 16:47:54', '2018-11-26 15:47:54', 1),
(10, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Jorge Octavio Manzano González  a sido contratado en la vacante Desarrollador Android!', 'AEFAEF', '5', '2018-11-26 16:47:54', '2018-11-26 15:47:54', 1),
(11, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Octavio  en la vacante Desarrollador Android ya ha sido cubierta!', '', '6', '2018-11-26 16:47:54', '2018-11-26 15:47:54', 1),
(12, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante Test 2', '', '1', '2018-11-26 16:47:54', '2018-11-26 15:47:54', 1),
(13, 2, '¡Candidato postulado!', '', '¡Has postulado a Octavio  Kskd Nznx en la vacante Test 2', '', '1', '2018-09-20 22:55:04', '2018-09-20 22:55:04', 1),
(14, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Jorge Octavio Manzano González  ha sido revisado!', '123', '2', '2018-09-20 22:59:05', '2018-09-20 22:59:05', 1),
(15, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Jorge Octavio Manzano González  ha sido rechazado', 'NOP', '7', '2018-09-20 23:00:32', '2018-09-20 23:00:32', 1),
(16, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Octavio  Kskd Nznx ha sido rechazado', 'malo', '7', '2018-09-20 23:13:50', '2018-09-20 23:13:50', 1),
(17, 2, '¡Candidato postulado!', '', '¡Has postulado a OCTAVIO 2 JDJDJF Jdjdjd en la vacante Desarrollador Android', '', '1', '2018-09-22 01:43:44', '2018-09-22 01:43:44', 1),
(18, 4, '¡Nueva vacante disponible!', '', 'Podrías postular al candidato Sandy Magallanes Ramirez en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-25 20:40:07', '2018-09-25 20:40:07', 1),
(19, 2, '¡Nueva vacante disponible!', '', 'Podrías postular al candidato OCTAVIO 2 JDJDJF Jdjdjd en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-25 20:40:07', '2018-09-25 20:40:07', 1),
(20, 2, '¡Nueva vacante disponible!', '', 'Podrías postular al candidato Jorge Octavio Manzano González  en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-25 20:40:07', '2018-09-25 20:40:07', 1),
(21, 2, '¡Nueva vacante disponible!', '', 'Podrías postular al candidato Octavio  Kskd Nznx en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-25 20:40:07', '2018-09-25 20:40:07', 1),
(22, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Vincent Moon Amos Pennington Yuri Reed en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 20:07:48', '2018-09-26 20:07:48', 1),
(23, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Hamilton Snow Rosalyn Norman Kai Roach en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 20:23:15', '2018-09-26 20:23:15', 1),
(24, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Hamilton Snow Rosalyn Norman Kai Roach en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 20:24:06', '2018-09-26 20:24:06', 1),
(25, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Jorge Gonzalez Manzano en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 20:43:54', '2018-09-26 20:43:54', 1),
(26, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Justin Hoover Nigel Stanton Castor Stout en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 21:00:01', '2018-09-26 21:00:01', 1),
(27, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Pascale Pace Rinah Finley Clarke Humphrey en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 22:43:40', '2018-09-26 22:43:40', 1),
(28, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Pascale Pace Rinah Finley Clarke Humphrey en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 22:46:44', '2018-09-26 22:46:44', 1),
(29, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Pascale Pace Rinah Finley Clarke Humphrey en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 22:47:18', '2018-09-26 22:47:18', 1),
(30, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato OCTAVIO 2 Jdjdjd JDJDJF en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 23:15:58', '2018-09-26 23:15:58', 1),
(31, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato OCTAVIO 2 Jdjdjd JDJDJF en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-26 23:21:51', '2018-09-26 23:21:51', 1),
(32, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Judah Delaney Cameron Simpson Cameron Hartman en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 00:06:46', '2018-09-27 00:06:46', 1),
(33, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Wanda Douglas Salvador Mcclain Lillith Flynn en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 00:26:42', '2018-09-27 00:26:42', 1),
(34, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Wanda Douglas Salvador Mcclain Lillith Flynn en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 00:27:04', '2018-09-27 00:27:04', 1),
(35, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Yuri Sawyer Kennedy Herman Ora Fleming en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 00:30:51', '2018-09-27 00:30:51', 1),
(36, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Yuri Sawyer Kennedy Herman Ora Fleming en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 00:33:11', '2018-09-27 00:33:11', 1),
(37, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Yuri Sawyer Kennedy Herman Ora Fleming en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 00:46:49', '2018-09-27 00:46:49', 1),
(38, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Jenette Moses Acton Adkins Dorothy Richards en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 00:50:02', '2018-09-27 00:50:02', 1),
(39, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Kylan Phelps Olympia Hensley Gareth Sargent en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 01:30:21', '2018-09-27 01:30:21', 1),
(40, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Nissim Figueroa Cynthia Fleming Jemima Cervantes en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 01:32:44', '2018-09-27 01:32:44', 1),
(41, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Erin Sutton Brenna Morse Myra Mccarthy en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 01:55:38', '2018-09-27 01:55:38', 1),
(42, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Hashim Rosa Michelle Malone Kyra Rocha en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 02:03:42', '2018-09-27 02:03:42', 1),
(43, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Portia Guthrie Macon Walker Laura Sims en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 17:46:08', '2018-09-27 17:46:08', 1),
(44, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato OCTAVIO 2 Jdjdjd JDJDJF en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 18:35:47', '2018-09-27 18:35:47', 1),
(45, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Galvin Pugh Doris Hickman Abigail Peck en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 18:38:52', '2018-09-27 18:38:52', 1),
(46, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Jorge Octavio González  Manzano en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 19:56:37', '2018-09-27 19:56:37', 1),
(47, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Jorge Octavio González  Manzano en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 20:00:19', '2018-09-27 20:00:19', 1),
(48, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Jorge Octavio González  Manzano en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-27 20:01:26', '2018-09-27 20:01:26', 1),
(49, 2, '¡Candidato postulado!', '', '¡Has postulado a OCTAVIO 2 JDJDJF Jdjdjd en la vacante tecnico de fallas', '', '1', '2018-09-28 01:17:54', '2018-09-28 01:17:54', 1),
(50, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante tecnico de fallas', '', '1', '2018-09-28 01:18:27', '2018-09-28 01:18:27', 1),
(51, 2, '¡Nuevo candidato registrado!', '', 'Podrías postular al candidato Porter Robbins Burke Lewis Eliana Noble en la vacante tecnico de fallas de la empresa Hochin camaras!', '', '8', '2018-09-28 01:21:55', '2018-09-28 01:21:55', 1),
(52, 2, '¡Candidato postulado!', '', '¡Has postulado a Porter Robbins Eliana Noble Burke Lewis en la vacante tecnico de fallas', '', '1', '2018-09-28 01:22:16', '2018-09-28 01:22:16', 1),
(53, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado OCTAVIO 2 JDJDJF Jdjdjd a sido contratado en la vacante tecnico de fallas!', 'El perfil de tu candidato cumplió los requisitos.\r\n¡Felicidades!', '5', '2018-09-28 02:59:18', '2018-09-28 02:59:18', 1),
(54, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante tecnico de fallas ya ha sido cubierta!', '', '6', '2018-09-28 02:59:18', '2018-09-28 02:59:18', 1),
(55, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante tecnico de fallas ya ha sido cubierta!', '', '6', '2018-09-28 02:59:18', '2018-09-28 02:59:18', 1),
(56, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Porter Robbins en la vacante tecnico de fallas ya ha sido cubierta!', '', '6', '2018-09-28 02:59:18', '2018-09-28 02:59:18', 1),
(57, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Porter Robbins en la vacante tecnico de fallas ya ha sido cubierta!', '', '6', '2018-09-28 02:59:18', '2018-09-28 02:59:18', 1),
(58, 2, '¡Candidato postulado!', '', '¡Has postulado a Porter Robbins Eliana Noble Burke Lewis en la vacante RECEPCIONISTA RECLUTAMIENTO', '', '1', '2018-09-28 20:29:09', '2018-09-28 20:29:09', 1),
(59, 2, '¡Candidato postulado!', '', '¡Has postulado a OCTAVIO 2 JDJDJF Jdjdjd en la vacante RECEPCIONISTA RECLUTAMIENTO', '', '1', '2018-09-28 20:58:10', '2018-09-28 20:58:10', 1),
(60, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado OCTAVIO 2 bdzfhdhfdhfarhsgav af gsdr gerahayety fy e e Jdjdjd ha sido rechazado', 'test', '7', '2018-09-28 23:08:19', '2018-09-28 23:08:19', 1),
(61, 2, '¡Candidato postulado!', '', '¡Has postulado a OCTAVIO 2 bdzfhdhfdhfarhsgav af gsdr gerahayety fy e e Jdjdjd en la vacante CAPTURISTA DE DATOS', '', '1', '2018-09-28 23:41:03', '2018-09-28 23:41:03', 1),
(62, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante CAPTURISTA DE DATOS', '', '1', '2018-09-28 23:41:19', '2018-09-28 23:41:19', 1),
(63, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante CAPTURISTA DE DATOS', '', '1', '2018-09-28 23:49:54', '2018-09-28 23:49:54', 1),
(64, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante AUXILIAR DE ACTIVOS', '', '1', '2018-09-28 23:53:29', '2018-09-28 23:53:29', 1),
(65, 2, '¡Candidato postulado!', '', '¡Has postulado a OCTAVIO 2 bdzfhdhfdhfarhsgav af gsdr gerahayety fy e e Jdjdjd en la vacante Jardinero', '', '1', '2018-09-28 23:53:57', '2018-09-28 23:53:57', 1),
(66, 2, '¡Candidato postulado!', '', '¡Has postulado a OCTAVIO 2 bdzfhdhfdhfarhsgav af gsdr gerahayety fy e e Jdjdjd en la vacante Auxiliar de limpieza mujer.', '', '1', '2018-09-28 23:55:28', '2018-09-28 23:55:28', 1),
(67, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante AUXILIAR DE ACTIVOS', '', '1', '2018-09-28 23:57:13', '2018-09-28 23:57:13', 1),
(68, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante Auxiliar de limpieza mujer.', '', '1', '2018-09-28 23:57:28', '2018-09-28 23:57:28', 1),
(69, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante DISEÑADOR GRÁFICO', '', '1', '2018-09-29 02:15:05', '2018-09-29 02:15:05', 1),
(70, 25, '¡En entrevista!', '', '¡El postulado alsd sas sas fue llamado a entrevista!', 'test', '3', '2018-09-29 02:21:50', '2018-09-29 02:21:50', 1),
(71, 25, '¡Examenes psicométricos!', '', 'Tu postulado alsd sas sas hara examenes psicométicos', 'test', '4', '2018-09-29 02:22:02', '2018-09-29 02:22:02', 1),
(72, 25, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado alsd sas sas a sido contratado en la vacante Auxiliar de limpieza mujer.!', 'test', '5', '2018-09-29 02:22:09', '2018-09-29 02:22:09', 1),
(73, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de OCTAVIO 2 en la vacante Auxiliar de limpieza mujer. ya ha sido cubierta!', '', '6', '2018-09-29 02:22:09', '2018-09-29 02:22:09', 1),
(74, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de OCTAVIO 2 en la vacante Auxiliar de limpieza mujer. ya ha sido cubierta!', '', '6', '2018-09-29 02:22:09', '2018-09-29 02:22:09', 1),
(75, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante Auxiliar de limpieza mujer. ya ha sido cubierta!', '', '6', '2018-09-29 02:22:09', '2018-09-29 02:22:09', 1),
(76, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante Auxiliar de limpieza mujer. ya ha sido cubierta!', '', '6', '2018-09-29 02:22:09', '2018-09-29 02:22:09', 1),
(77, 25, '¡El CV Visto!', '', '¡El curriculum de tu postulado alsd sas sas ha sido revisado!', 'test', '2', '2018-09-29 02:22:36', '2018-09-29 02:22:36', 1),
(78, 25, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado alsd sas sas ha sido rechazado', 'test', '7', '2018-09-29 02:22:43', '2018-09-29 02:22:43', 1),
(79, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante RECEPCIONISTA RECLUTAMIENTO', '', '1', '2018-10-01 17:34:37', '2018-10-01 17:34:37', 1),
(80, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Jorge Octavio Manzano González  ha sido revisado!', 'Visto ayer alas 3:00pm', '2', '2018-10-01 23:51:18', '2018-10-01 23:51:18', 1),
(81, 2, '¡En entrevista!', '', '¡El postulado Jorge Octavio Manzano González  fue llamado a entrevista!', 'En entrevista mañana', '3', '2018-10-01 23:51:38', '2018-10-01 23:51:38', 1),
(82, 21, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Adriana  Rosas  Andrade  a sido contratado en la vacante RECEPCIONISTA RECLUTAMIENTO!', 'felicidades', '5', '2018-10-02 02:27:26', '2018-10-02 02:27:26', 1),
(83, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Jorge Octavio Manzano González  a sido contratado en la vacante CAPTURISTA DE DATOS!', 'd', '5', '2018-10-02 02:27:46', '2018-10-02 02:27:46', 1),
(84, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante CAPTURISTA DE DATOS ya ha sido cubierta!', '', '6', '2018-10-02 02:27:46', '2018-10-02 02:27:46', 1),
(85, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Test Q Q a sido contratado en la vacante AUXILIAR DE ACTIVOS!', '¡Felicidades!', '5', '2018-10-02 02:29:21', '2018-10-02 02:29:21', 1),
(86, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-02 02:29:21', '2018-10-02 02:29:21', 1),
(87, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante CAPTURISTA DE DATOS', '', '1', '2018-10-02 02:29:35', '2018-10-02 02:29:35', 1),
(88, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante Jardinero', '', '1', '2018-10-02 02:29:58', '2018-10-02 02:29:58', 1),
(89, 32, '¡Candidato postulado!', '', '¡Has postulado a Mario Alberto Cuevas Venegas  en la vacante TECNICO DE SOPORTE', '', '1', '2018-10-02 02:37:27', '2018-10-02 02:37:27', 1),
(90, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante Auxiliar General', '', '1', '2018-10-02 17:39:38', '2018-10-02 17:39:38', 1),
(91, 35, '¡Candidato postulado!', '', '¡Has postulado a A A A en la vacante BECARIO', '', '1', '2018-10-02 23:50:59', '2018-10-02 23:50:59', 1),
(92, 33, '¡Candidato postulado!', '', '¡Has postulado a José Alberto Gaspar Baltazar en la vacante BECARIO ATENCIÓN AL CLIENTE RECLUTAMIENTO', '', '1', '2018-10-03 00:03:56', '2018-10-03 00:03:56', 1),
(93, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante BECARIO', '', '1', '2018-10-03 02:26:09', '2018-10-03 02:26:09', 1),
(94, 33, '¡El CV Visto!', '', '¡El curriculum de tu postulado José Alberto Gaspar Baltazar ha sido revisado!', '¡Hola! el CV de tu candidato fue visto, enviaremos el cv a revisión y seguiremos con el proceso.', '2', '2018-10-03 18:36:49', '2018-10-03 18:36:49', 1),
(95, 33, '¡El CV Visto!', '', '¡El curriculum de tu postulado José Alberto Gaspar Baltazar ha sido revisado!', '¡Hola! el CV de tu candidato fue visto, enviaremos el cv a revisión y seguiremos con el proceso.', '2', '2018-10-03 18:36:52', '2018-10-03 18:36:52', 1),
(96, 33, '¡En entrevista!', '', '¡El postulado José Alberto Gaspar Baltazar fue llamado a entrevista!', '¡Hola! Tu candidato fue seleccionado para un entrevista ¡Felicidades!', '3', '2018-10-03 18:42:46', '2018-10-03 18:42:46', 1),
(97, 33, '¡Examenes psicométricos!', '', 'Tu postulado José Alberto Gaspar Baltazar hara examenes psicométicos', '¡Hola! Tu candidato fue seleccionado, el siguiente paso serán los Psicométricos.', '4', '2018-10-03 18:43:49', '2018-10-03 18:43:49', 1),
(98, 2, '¡Candidato postulado!', '', '¡Has postulado a mario g g en la vacante Auxiliar General', '', '1', '2018-10-03 22:49:19', '2018-10-03 22:49:19', 1),
(99, 32, '¡Candidato postulado!', '', '¡Has postulado a Mario Alberto Cuevas Venegas  en la vacante DISEÑADOR GRÁFICO', '', '1', '2018-10-03 22:49:57', '2018-10-03 22:49:57', 1),
(100, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante Auxiliar de limpieza mujer.', '', '1', '2018-10-04 00:27:25', '2018-10-04 00:27:25', 1),
(101, 21, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Adriana  Rosas  Andrade  a sido contratado en la vacante CAPTURISTA DE DATOS!', 's', '5', '2018-10-04 00:27:47', '2018-10-04 00:27:47', 1),
(102, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante CAPTURISTA DE DATOS ya ha sido cubierta!', '', '6', '2018-10-04 00:27:48', '2018-10-04 00:27:48', 1),
(103, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante CAPTURISTA DE DATOS ya ha sido cubierta!', '', '6', '2018-10-04 00:27:48', '2018-10-04 00:27:48', 1),
(104, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante CAPTURISTA DE DATOS ya ha sido cubierta!', '', '6', '2018-10-04 00:27:48', '2018-10-04 00:27:48', 1),
(105, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante CAPTURISTA DE DATOS ya ha sido cubierta!', '', '6', '2018-10-04 00:27:48', '2018-10-04 00:27:48', 1),
(106, 21, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Adriana  Rosas  Andrade  a sido contratado en la vacante Jardinero!', 'd', '5', '2018-10-04 00:43:39', '2018-10-04 00:43:39', 1),
(107, 25, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de alsd en la vacante Jardinero ya ha sido cubierta!', '', '6', '2018-10-04 00:43:40', '2018-10-04 00:43:40', 1),
(108, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Jorge Octavio Manzano González  ha sido revisado!', 'Visto el jueves a las 11', '2', '2018-10-04 20:55:25', '2018-10-04 20:55:25', 1),
(109, 2, '¡En entrevista!', '', '¡El postulado Jorge Octavio Manzano González  fue llamado a entrevista!', 'En entrevista para el lunes alas 8:00', '3', '2018-10-04 21:00:59', '2018-10-04 21:00:59', 1),
(110, 2, '¡Examenes psicométricos!', '', 'Tu postulado Jorge Octavio Manzano González  hara examenes psicométicos', 'En Psicometricos para el lunes alas 8:00', '4', '2018-10-04 23:06:33', '2018-10-04 23:06:33', 1),
(111, 21, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Adriana  Rosas  Andrade  a sido contratado en la vacante Auxiliar General!', 'g', '5', '2018-10-04 23:37:39', '2018-10-04 23:37:39', 1),
(112, 25, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de alsd en la vacante Auxiliar General ya ha sido cubierta!', '', '6', '2018-10-04 23:37:39', '2018-10-04 23:37:39', 1),
(113, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de alsd en la vacante Auxiliar General ya ha sido cubierta!', '', '6', '2018-10-04 23:37:39', '2018-10-04 23:37:39', 1),
(114, 25, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de mario en la vacante Auxiliar General ya ha sido cubierta!', '', '6', '2018-10-04 23:37:39', '2018-10-04 23:37:39', 1),
(115, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de mario en la vacante Auxiliar General ya ha sido cubierta!', '', '6', '2018-10-04 23:37:39', '2018-10-04 23:37:39', 1),
(116, 35, '¡Candidato postulado!', '', '¡Has postulado a A A A en la vacante DISEÑADOR GRÁFICO', '', '1', '2018-10-05 01:10:49', '2018-10-05 01:10:49', 1),
(117, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante OPERADOR DIE CASTING', '', '1', '2018-10-05 23:56:40', '2018-10-05 23:56:40', 1),
(118, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante OPERADOR DIE CASTING', '', '1', '2018-10-05 23:57:25', '2018-10-05 23:57:25', 1),
(119, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante OP EQPO (MAQUINISTA)', '', '1', '2018-10-05 23:59:16', '2018-10-05 23:59:16', 1),
(120, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante PROMOTOR', '', '1', '2018-10-06 00:10:16', '2018-10-06 00:10:16', 1),
(121, 2, '¡Candidato postulado!', '', '¡Has postulado a mario g g en la vacante PROMOTOR', '', '1', '2018-10-06 00:10:38', '2018-10-06 00:10:38', 1),
(122, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante PROMOTOR', '', '1', '2018-10-06 00:11:24', '2018-10-06 00:11:24', 1),
(123, 33, '¡Candidato postulado!', '', '¡Has postulado a José Alberto Gaspar Baltazar en la vacante BECARIO', '', '1', '2018-10-06 02:52:35', '2018-10-06 02:52:35', 1),
(124, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Jorge Octavio Manzano González  ha sido revisado!', 'Tu curriculum fue visto', '2', '2018-10-08 18:16:32', '2018-10-08 18:16:32', 1),
(125, 2, '¡En entrevista!', '', '¡El postulado Jorge Octavio Manzano González  fue llamado a entrevista!', 'en entrevista', '3', '2018-10-08 18:16:41', '2018-10-08 18:16:41', 1),
(126, 2, '¡Examenes psicométricos!', '', 'Tu postulado Jorge Octavio Manzano González  hara examenes psicométicos', 'en psicmetricos', '4', '2018-10-08 18:16:49', '2018-10-08 18:16:49', 1),
(127, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Jorge Octavio Manzano González  a sido contratado en la vacante OPERADOR DIE CASTING!', 'Contrado empieza mañana', '5', '2018-10-08 18:18:59', '2018-10-08 18:18:59', 1),
(128, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante OPERADOR DIE CASTING ya ha sido cubierta!', '', '6', '2018-10-08 18:18:59', '2018-10-08 18:18:59', 1),
(129, 33, '¡El CV Visto!', '', '¡El curriculum de tu postulado José Alberto Gaspar Baltazar ha sido revisado!', '¡Alguien ha visto tu Curriculum! Estamos haciendo las valoraciones necesarias para gestionar tu curriculum. Sigue al pendiente de los resultados.', '2', '2018-10-08 18:57:16', '2018-10-08 18:57:16', 1),
(130, 33, '¡En entrevista!', '', '¡El postulado José Alberto Gaspar Baltazar fue llamado a entrevista!', 'Se ha programado una entrevista para la vacante. Pronto te estaremos informando del siguiente proceso.', '3', '2018-10-08 18:57:56', '2018-10-08 18:57:56', 1),
(131, 33, '¡Examenes psicométricos!', '', 'Tu postulado José Alberto Gaspar Baltazar hara examenes psicométicos', 'Lo mejor está por venir. Necesitamos hacer las últimas evaluaciones, antes de tomar la decisión final.', '4', '2018-10-08 18:58:22', '2018-10-08 18:58:22', 1),
(132, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante CAPTURISTA DE DATOS', '', '1', '2018-10-09 18:14:02', '2018-10-09 18:14:02', 1),
(133, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Jorge Octavio Manzano González  a sido contratado en la vacante OP EQPO (MAQUINISTA)!', 'Contratado Felicidades !!', '5', '2018-10-09 21:12:49', '2018-10-09 21:12:49', 1),
(134, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Jorge Octavio Manzano González  a sido contratado en la vacante PROMOTOR!', 'gg', '5', '2018-10-10 19:39:40', '2018-10-10 19:39:40', 1),
(135, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de mario en la vacante PROMOTOR ya ha sido cubierta!', '', '6', '2018-10-10 19:39:41', '2018-10-10 19:39:41', 1),
(136, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de mario en la vacante PROMOTOR ya ha sido cubierta!', '', '6', '2018-10-10 19:39:41', '2018-10-10 19:39:41', 1),
(137, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante PROMOTOR ya ha sido cubierta!', '', '6', '2018-10-10 19:39:41', '2018-10-10 19:39:41', 1),
(138, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante PROMOTOR ya ha sido cubierta!', '', '6', '2018-10-10 19:39:41', '2018-10-10 19:39:41', 1),
(139, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante INGENIERO DE VENTAS', '', '1', '2018-10-10 23:26:47', '2018-10-10 23:26:47', 1),
(140, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante INGENIERO DE VENTAS', '', '1', '2018-10-10 23:28:04', '2018-10-10 23:28:04', 1),
(141, 21, '¡Candidato postulado!', '', '¡Has postulado a Alejandra  Rosas Andrade  en la vacante MONITORISTA', '', '1', '2018-10-11 00:38:05', '2018-10-11 00:38:05', 1),
(142, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante ASESOR DE VENTAS', '', '1', '2018-10-11 00:38:14', '2018-10-11 00:38:14', 1),
(143, 21, '¡Candidato postulado!', '', '¡Has postulado a Ninfa  Nino Ortega en la vacante ASESOR DE VENTAS', '', '1', '2018-10-11 00:38:19', '2018-10-11 00:38:19', 1),
(144, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante OPERADOR DE CNC', '', '1', '2018-10-11 00:38:26', '2018-10-11 00:38:26', 1),
(145, 21, '¡Candidato postulado!', '', '¡Has postulado a Alejandra  Rosas Andrade  en la vacante OPERADOR DE CNC', '', '1', '2018-10-11 00:38:32', '2018-10-11 00:38:32', 1),
(146, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante MONITORISTA', '', '1', '2018-10-11 00:39:40', '2018-10-11 00:39:40', 1),
(147, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante OPERADOR DE CNC', '', '1', '2018-10-11 00:47:07', '2018-10-11 00:47:07', 1),
(148, 41, '¡Candidato postulado!', '', '¡Has postulado a María Gallegos Martínez  en la vacante OPERADOR DE CNC', '', '1', '2018-11-28 15:40:10', '2018-11-28 18:38:25', 1),
(149, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Jorge Octavio Manzano González  ha sido revisado!', 'Visto', '2', '2018-10-15 22:27:33', '2018-10-15 22:27:33', 1),
(150, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Test Q Q ha sido revisado!', 'cv visto', '2', '2018-10-16 04:25:07', '2018-10-16 04:25:07', 1),
(151, 2, '¡En entrevista!', '', '¡El postulado Test Q Q fue llamado a entrevista!', 'cv visto', '3', '2018-10-16 04:26:16', '2018-10-16 04:26:16', 1),
(152, 2, '¡Examenes psicométricos!', '', 'Tu postulado Test Q Q hara examenes psicométicos', 'cv visto', '4', '2018-10-16 04:27:35', '2018-10-16 04:27:35', 1),
(153, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Test Q Q ha sido rechazado', 'cv visto', '7', '2018-10-16 05:33:57', '2018-10-16 05:33:57', 1),
(154, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante Vacante de prueba', '', '1', '2018-10-16 05:36:08', '2018-10-16 05:36:08', 1),
(155, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Test Q Q ha sido revisado!', 'cv', '2', '2018-10-16 05:36:28', '2018-10-16 05:36:28', 1),
(156, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Test Q Q ha sido revisado!', 'cv', '2', '2018-10-16 05:36:47', '2018-10-16 05:36:47', 1),
(157, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Test Q Q ha sido revisado!', 'cv', '2', '2018-10-16 05:36:59', '2018-10-16 05:36:59', 1),
(158, 41, '¡Candidato postulado!', '', '¡Has postulado a Georgina Arias Meza en la vacante AUXILIAR DE ACTIVOS', '', '1', '2018-11-28 15:20:07', '2018-11-28 18:38:25', 1),
(159, 41, '¡Candidato postulado!', '', '¡Has postulado a José  Villa  Navarro  en la vacante AUXILIAR DE ACTIVOS', '', '1', '2018-11-28 15:20:07', '2018-11-28 18:38:25', 1),
(160, 41, '¡Candidato postulado!', '', '¡Has postulado a Georgina Arias Meza en la vacante VENDEDOR DE MOSTRADOR', '', '1', '2018-10-16 19:07:31', '2018-11-28 18:38:25', 1),
(161, 41, '¡Candidato postulado!', '', '¡Has postulado a Rodrigo  Valles Pérez  en la vacante CAJERO', '', '1', '2018-10-16 19:27:10', '2018-11-28 18:38:26', 1),
(162, 41, '¡Candidato postulado!', '', '¡Has postulado a María Gallegos Martínez  en la vacante VENDEDOR DE MOSTRADOR', '', '1', '2018-10-16 19:30:03', '2018-11-28 18:38:26', 1),
(163, 41, '¡Candidato postulado!', '', '¡Has postulado a José  Villa  Navarro  en la vacante ASESOR DE VENTAS', '', '1', '2018-10-16 19:30:48', '2018-11-28 18:27:06', 1),
(164, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Jorge Octavio Manzano González  ha sido revisado!', 'Visto', '2', '2018-10-16 20:07:11', '2018-10-16 20:07:11', 1),
(165, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Jorge Octavio Manzano González  a sido contratado en la vacante CAPTURISTA DE DATOS!', 'felcidades', '5', '2018-10-16 20:51:23', '2018-10-16 20:51:23', 1),
(166, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Jorge Octavio Manzano González  a sido contratado en la vacante INGENIERO DE VENTAS!', 'c', '5', '2018-10-16 20:58:32', '2018-10-16 20:58:32', 1),
(167, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante INGENIERO DE VENTAS ya ha sido cubierta!', '', '6', '2018-10-16 20:58:33', '2018-10-16 20:58:33', 1),
(168, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante OPERADOR DE CNC', '', '1', '2018-10-17 00:28:54', '2018-10-17 00:28:54', 1),
(169, 2, '¡Candidato postulado!', '', '¡Has postulado a Hilda Arvizu  Flores en la vacante Vacante de prueba', '', '1', '2018-10-17 22:20:43', '2018-10-17 22:20:43', 1),
(170, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano Modifi González  en la vacante AUXILIAR DE ACTIVOS', '', '1', '2018-10-17 22:40:02', '2018-10-17 22:40:02', 1),
(171, 2, '¡Candidato postulado!', '', '¡Has postulado a Kasper Aguilar Kirby Carr Emmanuel Brewer en la vacante OPERADOR DE CNC', '', '1', '2018-10-18 01:36:40', '2018-10-18 01:36:40', 1),
(172, 21, '¡Candidato postulado!', '', '¡Has postulado a Ninfa  Nino Ortega en la vacante Vacante de prueba', '', '1', '2018-10-18 20:04:22', '2018-10-18 20:04:22', 1),
(173, 2, '¡En entrevista!', '', '¡El postulado Jorge Octavio Manzano Modifi González  fue llamado a entrevista!', 'en entrevista', '3', '2018-10-18 21:19:38', '2018-10-18 21:19:38', 1),
(174, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante aaa', '', '1', '2018-10-18 22:55:29', '2018-10-18 22:55:29', 1),
(175, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano Modifi González  en la vacante aaa', '', '1', '2018-10-18 22:55:43', '2018-10-18 22:55:43', 1),
(176, 21, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Alejandra  Rosas Andrade  a sido contratado en la vacante MONITORISTA!', 'g', '5', '2018-10-18 23:04:06', '2018-10-18 23:04:06', 1),
(177, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Adriana  en la vacante MONITORISTA ya ha sido cubierta!', '', '6', '2018-10-18 23:04:07', '2018-10-18 23:04:07', 1),
(178, 21, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Adriana  Rosas  Andrade  a sido contratado en la vacante ASESOR DE VENTAS!', 'aaa', '5', '2018-10-18 23:07:33', '2018-10-18 23:07:33', 1),
(179, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Ninfa  en la vacante ASESOR DE VENTAS ya ha sido cubierta!', '', '6', '2018-10-18 23:07:33', '2018-10-18 23:07:33', 1),
(180, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Ninfa  en la vacante ASESOR DE VENTAS ya ha sido cubierta!', '', '6', '2018-10-18 23:07:33', '2018-11-28 18:27:06', 1),
(181, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de José  en la vacante ASESOR DE VENTAS ya ha sido cubierta!', '', '6', '2018-10-18 23:07:33', '2018-10-18 23:07:33', 1),
(182, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de José  en la vacante ASESOR DE VENTAS ya ha sido cubierta!', '', '6', '2018-10-18 23:07:33', '2018-10-18 23:07:33', 1),
(183, 21, '¡Candidato postulado!', '', '¡Has postulado a Alejandra  Rosas Andrade  en la vacante ASESOR DE VENTAS', '', '1', '2018-10-18 23:08:32', '2018-10-18 23:08:32', 1),
(184, 21, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Adriana  Rosas  Andrade  a sido contratado en la vacante OPERADOR DE CNC!', 'p', '5', '2018-10-18 23:08:47', '2018-10-18 23:08:47', 1),
(185, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:47', '2018-10-18 23:08:47', 1),
(186, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:47', '2018-10-18 23:08:47', 1),
(187, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:47', '2018-10-18 23:08:47', 1),
(188, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:47', '2018-10-18 23:08:47', 1),
(189, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:47', '2018-10-18 23:08:47', 1),
(190, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:48', '2018-10-18 23:08:48', 1),
(191, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:48', '2018-10-18 23:08:48', 1),
(192, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:48', '2018-10-18 23:08:48', 1),
(193, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:48', '2018-10-18 23:08:48', 1),
(194, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:48', '2018-10-18 23:08:48', 1),
(195, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Kasper Aguilar en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:49', '2018-10-18 23:08:49', 1),
(196, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Kasper Aguilar en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:49', '2018-10-18 23:08:49', 1),
(197, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Kasper Aguilar en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:49', '2018-10-18 23:08:49', 1),
(198, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Kasper Aguilar en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:49', '2018-10-18 23:08:49', 1),
(199, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Kasper Aguilar en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:49', '2018-10-18 23:08:49', 1),
(200, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:51', '2018-10-18 23:08:51', 1),
(201, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:51', '2018-10-18 23:08:51', 1),
(202, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:51', '2018-10-18 23:08:51', 1),
(203, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:51', '2018-10-18 23:08:51', 1),
(204, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:51', '2018-10-18 23:08:51', 1),
(205, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de María en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:52', '2018-10-18 23:08:52', 1),
(206, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de María en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:52', '2018-10-18 23:08:52', 1),
(207, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de María en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:52', '2018-10-18 23:08:52', 1),
(208, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de María en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:52', '2018-10-18 23:08:52', 1),
(209, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de María en la vacante OPERADOR DE CNC ya ha sido cubierta!', '', '6', '2018-10-18 23:08:52', '2018-10-18 23:08:52', 1),
(210, 21, '¡Candidato postulado!', '', '¡Has postulado a Ninfa  Nino Ortega en la vacante OPERADOR DE CNC', '', '1', '2018-10-18 23:09:13', '2018-10-18 23:09:13', 1),
(211, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante BECARIO', '', '1', '2018-10-19 20:03:58', '2018-10-19 20:03:58', 1),
(212, 21, '¡Candidato postulado!', '', '¡Has postulado a Ninfa  Nino Ortega en la vacante DISEÑADOR GRAFICO', '', '1', '2018-10-19 20:04:12', '2018-10-19 20:04:12', 1),
(213, 21, '¡Candidato postulado!', '', '¡Has postulado a Alejandra  Rosas Andrade  en la vacante DISEÑADOR GRAFICO', '', '1', '2018-10-19 20:04:17', '2018-10-19 20:04:17', 1),
(214, 21, '¡Candidato postulado!', '', '¡Has postulado a Adriana  Rosas  Andrade  en la vacante AUXILIAR DE ACTIVOS', '', '1', '2018-10-19 20:04:27', '2018-10-19 20:04:27', 1),
(215, 21, '¡Candidato postulado!', '', '¡Has postulado a Alejandra  Rosas Andrade  en la vacante AUXILIAR DE ACTIVOS', '', '1', '2018-10-19 20:06:27', '2018-10-19 20:06:27', 1),
(216, 2, '¡Candidato postulado!', '', '¡Has postulado a Duncan Andrews Channing Michael Guinevere David en la vacante VENDEDOR DE MOSTRADOR', '', '1', '2018-10-19 20:14:52', '2018-10-19 20:14:52', 1),
(217, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Octavio Manzano González  en la vacante Vacante de prueba', '', '1', '2018-10-20 05:05:12', '2018-10-20 05:05:12', 1),
(218, 69, '¡Candidato postulado!', '', '¡Has postulado a Alejandra Rosas Andrade en la vacante PROMOTOR', '', '1', '2018-10-22 18:54:21', '2018-10-22 18:54:21', 1),
(219, 69, '¡Candidato postulado!', '', '¡Has postulado a Juan Enrique González González en la vacante OP EQPO (MAQUINISTA)', '', '1', '2018-10-22 18:56:56', '2018-10-22 18:56:56', 1),
(220, 21, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Ninfa  Nino Ortega a sido contratado en la vacante Vacante de prueba!', '4', '5', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(221, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante Vacante de prueba ya ha sido cubierta!', '', '6', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(222, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante Vacante de prueba ya ha sido cubierta!', '', '6', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(223, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante Vacante de prueba ya ha sido cubierta!', '', '6', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(224, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Hilda en la vacante Vacante de prueba ya ha sido cubierta!', '', '6', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(225, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Hilda en la vacante Vacante de prueba ya ha sido cubierta!', '', '6', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(226, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Hilda en la vacante Vacante de prueba ya ha sido cubierta!', '', '6', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(227, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante Vacante de prueba ya ha sido cubierta!', '', '6', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(228, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante Vacante de prueba ya ha sido cubierta!', '', '6', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(229, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante Vacante de prueba ya ha sido cubierta!', '', '6', '2018-10-22 19:45:56', '2018-10-22 19:45:56', 1),
(230, 35, '¡Candidato postulado!', '', '¡Has postulado a A A A en la vacante aaa', '', '1', '2018-10-22 22:40:49', '2018-10-22 22:40:49', 1),
(231, 21, '¡Candidato postulado!', '', '¡Has postulado a Ninfa  Nino Ortega en la vacante RECEPCIONISTA RECLUTAMIENTO', '', '1', '2018-10-23 02:05:46', '2018-10-23 02:05:46', 1),
(232, 35, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Graciela Ortega Lopez a sido contratado en la vacante aaa!', 'kahnfk', '5', '2018-10-25 01:21:57', '2018-10-25 01:21:57', 1),
(233, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante aaa ya ha sido cubierta!', '', '6', '2018-10-25 01:21:57', '2018-10-25 01:21:57', 1),
(234, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante aaa ya ha sido cubierta!', '', '6', '2018-10-25 01:21:57', '2018-10-25 01:21:57', 1),
(235, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante aaa ya ha sido cubierta!', '', '6', '2018-10-25 01:21:57', '2018-10-25 01:21:57', 1),
(236, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Jorge Octavio en la vacante aaa ya ha sido cubierta!', '', '6', '2018-10-25 01:21:57', '2018-10-25 01:21:57', 1),
(237, 2, '¡Candidato postulado!', '', '¡Has postulado a Hilda Arvizu  Flores en la vacante AUXILIAR DE ACTIVOS', '', '1', '2018-10-26 20:02:40', '2018-10-26 20:02:40', 1),
(238, 2, '¡Candidato postulado!', '', '¡Has postulado a Moy Hernán Zamarrip en la vacante ASESOR DE VENTAS', '', '1', '2018-10-29 22:20:30', '2018-10-29 22:20:30', 1),
(239, 2, '¡En entrevista!', '', '¡El postulado Jorge Manzano Gonzalez fue llamado a entrevista!', 'sdv', '3', '2018-10-29 22:50:43', '2018-10-29 22:50:43', 1),
(240, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Jorge Manzano Gonzalez a sido contratado en la vacante AUXILIAR DE ACTIVOS!', 'contratado', '5', '2018-10-29 22:52:57', '2018-10-29 22:52:57', 1),
(241, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Hilda en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:52:58', '2018-10-29 22:52:58', 1),
(242, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Hilda en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:52:58', '2018-10-29 22:52:58', 1),
(243, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Hilda en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:52:58', '2018-10-29 22:52:58', 1),
(244, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Hilda en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-11-28 15:20:07', '2018-11-28 14:20:07', 1),
(245, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Hilda en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-11-28 15:20:07', '2018-11-28 14:20:07', 1),
(246, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Adriana  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:52:59', '2018-10-29 22:52:59', 1),
(247, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Adriana  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:52:59', '2018-10-29 22:52:59', 1),
(248, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Adriana  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:52:59', '2018-10-29 22:52:59', 1),
(249, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Adriana  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-11-28 15:20:08', '2018-11-28 14:20:07', 1),
(250, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Adriana  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:52:59', '2018-10-29 22:52:59', 1),
(251, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:00', '2018-10-29 22:53:00', 1),
(252, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:00', '2018-10-29 22:53:00', 1),
(253, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:00', '2018-10-29 22:53:00', 1),
(254, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:00', '2018-10-29 22:53:00', 1),
(255, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Alejandra  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:00', '2018-10-29 22:53:00', 1),
(256, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Georgina en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:01', '2018-10-29 22:53:01', 1),
(257, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Georgina en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:01', '2018-10-29 22:53:01', 1),
(258, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Georgina en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:01', '2018-10-29 22:53:01', 1),
(259, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Georgina en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:01', '2018-10-29 22:53:01', 1);
INSERT INTO `announcements` (`id`, `user_id`, `title`, `subtitle`, `body`, `message`, `icon`, `created_at`, `updated_at`, `watched`) VALUES
(260, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Georgina en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:01', '2018-10-29 22:53:01', 1),
(261, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de José  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:03', '2018-10-29 22:53:03', 1),
(262, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de José  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:03', '2018-10-29 22:53:03', 1),
(263, 21, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de José  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:03', '2018-10-29 22:53:03', 1),
(264, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de José  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:03', '2018-10-29 22:53:03', 1),
(265, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de José  en la vacante AUXILIAR DE ACTIVOS ya ha sido cubierta!', '', '6', '2018-10-29 22:53:03', '2018-10-29 22:53:03', 1),
(266, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante VENDEDOR DE MOSTRADOR', '', '1', '2018-10-29 23:48:17', '2018-10-29 23:48:17', 1),
(267, 2, '¡Candidato postulado!', '', '¡Has postulado a mario Hola ghola en la vacante VENDEDOR DE MOSTRADOR', '', '1', '2018-10-29 23:48:26', '2018-10-29 23:48:26', 1),
(268, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Lizet Blue chompi ha sido revisado!', 'visto', '2', '2018-10-29 23:50:36', '2018-10-29 23:50:36', 1),
(269, 2, '¡En entrevista!', '', '¡El postulado Lizet Blue chompi fue llamado a entrevista!', 'entrevista', '3', '2018-10-29 23:51:02', '2018-10-29 23:51:02', 1),
(270, 2, '¡Examenes psicométricos!', '', 'Tu postulado Lizet Blue chompi hara examenes psicométicos', 'psico', '4', '2018-10-29 23:51:17', '2018-10-29 23:51:17', 1),
(271, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Lizet Blue chompi a sido contratado en la vacante VENDEDOR DE MOSTRADOR!', 'ok', '5', '2018-10-29 23:51:27', '2018-10-29 23:51:27', 1),
(272, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de mario en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-10-29 23:51:27', '2018-10-29 23:51:27', 1),
(273, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de mario en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-10-29 23:51:27', '2018-10-29 23:51:27', 1),
(274, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de María en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-10-29 23:51:27', '2018-10-29 23:51:27', 1),
(275, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de María en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-10-29 23:51:27', '2018-10-29 23:51:27', 1),
(276, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante AUXILIAR DE ACTIVOS', '', '1', '2018-10-30 01:54:04', '2018-10-30 01:54:04', 1),
(277, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Manzano Gonzalez en la vacante BECARIO EN CONTABILIDAD', '', '1', '2018-10-30 19:14:12', '2018-10-30 19:14:12', 1),
(278, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante TECNICO DE SOPORTE', '', '1', '2018-10-30 19:31:22', '2018-10-30 19:31:22', 1),
(279, 2, '¡Candidato postulado!', '', '¡Has postulado a mario Hola ghola en la vacante TECNICO DE SOPORTE', '', '1', '2018-10-30 19:31:36', '2018-10-30 19:31:36', 1),
(280, 2, '¡Candidato postulado!', '', '¡Has postulado a User Jdjdja Ndnz en la vacante TECNICO DE SOPORTE', '', '1', '2018-10-30 19:31:54', '2018-10-30 19:31:54', 1),
(281, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Moy Hernán Zamarrip ha sido rechazado', '1', '7', '2018-10-30 19:32:35', '2018-10-30 19:32:35', 1),
(282, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Lizet Blue chompi ha sido rechazado', '2', '7', '2018-10-30 19:32:44', '2018-10-30 19:32:44', 1),
(283, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Jorge Manzano Gonzalez ha sido rechazado', '3', '7', '2018-10-30 19:32:56', '2018-10-30 19:32:56', 1),
(284, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Hilda Arvizu  Flores ha sido rechazado', '4', '7', '2018-10-30 19:33:05', '2018-10-30 19:33:05', 1),
(285, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Nuevo Nuevo Nuevo ha sido rechazado', '5', '7', '2018-10-30 19:33:15', '2018-10-30 19:33:15', 1),
(286, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Test Manzano Kdjx ha sido rechazado', '6', '7', '2018-10-30 19:33:27', '2018-10-30 19:33:27', 1),
(287, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Test Q Q ha sido rechazado', '6', '7', '2018-10-30 19:33:48', '2018-10-30 19:33:48', 1),
(288, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado mario Hola ghola ha sido rechazado', '7', '7', '2018-10-30 19:34:00', '2018-10-30 19:34:00', 1),
(289, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado User Jdjdja Ndnz ha sido rechazado', '8', '7', '2018-10-30 19:34:15', '2018-10-30 19:34:15', 1),
(290, 2, '¡Candidato postulado!', '', '¡Has postulado a Hilda Arvizu  Flores en la vacante RECEPCIONISTA RECLUTAMIENTO', '', '1', '2018-10-30 19:34:28', '2018-10-30 19:34:28', 1),
(291, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante RECEPCIONISTA RECLUTAMIENTO', '', '1', '2018-10-30 19:34:39', '2018-10-30 19:34:39', 1),
(292, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante DISEÑADOR GRAFICO', '', '1', '2018-10-30 19:35:51', '2018-10-30 19:35:51', 1),
(293, 2, '¡Candidato postulado!', '', '¡Has postulado a Hilda Arvizu  Flores en la vacante DISEÑADOR GRAFICO', '', '1', '2018-10-30 19:36:05', '2018-10-30 19:36:05', 1),
(294, 2, '¡Candidato postulado!', '', '¡Has postulado a Octavio  Backend Zjjs en la vacante DISEÑADOR GRAFICO', '', '1', '2018-10-30 19:36:12', '2018-10-30 19:36:12', 1),
(295, 2, '¡Candidato postulado!', '', '¡Has postulado a Nuevo Nuevo Nuevo en la vacante DISEÑADOR GRAFICO', '', '1', '2018-10-30 19:37:44', '2018-10-30 19:37:44', 1),
(296, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Manzano Gonzalez en la vacante RECEPCIONISTA RECLUTAMIENTO', '', '1', '2018-10-30 19:37:53', '2018-10-30 19:37:53', 1),
(297, 2, '¡Candidato postulado!', '', '¡Has postulado a mario Hola ghola en la vacante RECEPCIONISTA RECLUTAMIENTO', '', '1', '2018-10-30 19:38:12', '2018-10-30 19:38:12', 1),
(298, 2, '¡Candidato postulado!', '', '¡Has postulado a Moy Hernán Zamarrip en la vacante TECNICO DE SOPORTE', '', '1', '2018-10-30 19:46:20', '2018-10-30 19:46:20', 1),
(299, 2, '¡Candidato postulado!', '', '¡Has postulado a Moy Hernán Zamarrip en la vacante BECARIO', '', '1', '2018-10-30 19:46:23', '2018-10-30 19:46:23', 1),
(300, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante BECARIO', '', '1', '2018-10-30 19:46:29', '2018-10-30 19:46:29', 1),
(301, 2, '¡Candidato postulado!', '', '¡Has postulado a Jorge Manzano Gonzalez en la vacante Becario', '', '1', '2018-10-30 19:47:28', '2018-10-30 19:47:28', 1),
(302, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Hilda Arvizu  Flores ha sido rechazado', 'Rechazado por falta de xp', '7', '2018-11-01 19:32:01', '2018-11-01 19:32:01', 1),
(303, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Lizet Blue chompi ha sido rechazado', 'No  paso las pruebas bla bla bla,No  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla bla', '7', '2018-11-01 19:54:56', '2018-11-01 19:54:56', 1),
(304, 35, '¡Candidato postulado!', '', '¡Has postulado a Graciela Ortega Lopez en la vacante ASESOR DE VENTAS', '', '1', '2018-11-05 22:46:17', '2018-11-05 22:46:17', 1),
(305, 35, '¡Candidato postulado!', '', '¡Has postulado a Graciela Ortega Lopez en la vacante VENDEDOR DE MOSTRADOR', '', '1', '2018-11-05 22:51:54', '2018-11-05 22:51:54', 1),
(306, 33, '¡Examenes psicométricos!', '', 'Tu postulado José Alberto Gaspar Baltazar hara examenes psicométicos', 'Lo mejor está por venir. Necesitamos hacer las últimas evaluaciones, antes de tomar la decisión final.', '4', '2018-11-06 03:46:25', '2018-11-06 03:46:25', 1),
(307, 63, '¡Candidato postulado!', '', '¡Has postulado a Manzano DE CV DE CV en la vacante VENDEDOR DE MOSTRADOR', '', '1', '2018-11-06 19:49:43', '2018-11-06 19:49:43', 1),
(308, 2, '¡Candidato postulado!', '', '¡Has postulado a Gabo Sansón Rodea en la vacante CAJERO', '', '1', '2018-11-06 22:24:03', '2018-11-06 22:24:03', 1),
(309, 2, '¡Candidato postulado!', '', '¡Has postulado a User Jdjdja Ndnz en la vacante BECARIO EN CONTABILIDAD', '', '1', '2018-11-06 22:31:19', '2018-11-06 22:31:19', 1),
(310, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante VENDEDOR DE MOSTRADOR', '', '1', '2018-11-06 23:02:23', '2018-11-06 23:02:23', 1),
(311, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Manzano Kdjx en la vacante vacante prueba', '', '1', '2018-11-06 23:16:49', '2018-11-06 23:16:49', 1),
(312, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Nuevo Nuevo Nuevo ha sido rechazado', 'no', '7', '2018-11-06 23:21:10', '2018-11-06 23:21:10', 1),
(313, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado OctavioJorge Cruzzano Cruzalez ha sido rechazado', '123', '7', '2018-11-06 23:28:12', '2018-11-06 23:28:12', 1),
(314, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Moy Hernán Zamarrip ha sido rechazado', '123', '7', '2018-11-06 23:28:20', '2018-11-06 23:28:20', 1),
(315, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Lizet Blue chompi ha sido rechazado', '123', '7', '2018-11-06 23:28:31', '2018-11-06 23:28:31', 1),
(316, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Hilda Arvizu  Flores ha sido rechazado', '123', '7', '2018-11-06 23:28:39', '2018-11-06 23:28:39', 1),
(317, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado OctavioJorge Cruzzano Cruzalez ha sido rechazado', '123', '7', '2018-11-06 23:28:54', '2018-11-06 23:28:54', 1),
(318, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Gabo Sansón Rodea ha sido rechazado', 'no', '7', '2018-11-06 23:29:50', '2018-11-06 23:29:50', 1),
(319, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Octavio  Backend Zjjs ha sido rechazado', 'rgd', '7', '2018-11-06 23:30:00', '2018-11-06 23:30:00', 1),
(320, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado User Jdjdja Ndnz ha sido rechazado', 'rgt', '7', '2018-11-06 23:30:08', '2018-11-06 23:30:08', 1),
(321, 41, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Rodrigo  Valles Pérez  ha sido rechazado', 'sdv', '7', '2018-11-06 23:30:18', '2018-11-06 23:30:18', 1),
(322, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Lizet Blue chompi ha sido rechazado', 'wgr', '7', '2018-11-06 23:30:26', '2018-11-06 23:30:26', 1),
(323, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Moy Hernán Zamarrip ha sido rechazado', 'erg', '7', '2018-11-06 23:30:42', '2018-11-06 23:30:42', 1),
(324, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Test Q Q ha sido rechazado', 'erg', '7', '2018-11-06 23:30:51', '2018-11-06 23:30:51', 1),
(325, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Test Manzano Kdjx ha sido rechazado', 'erg', '7', '2018-11-06 23:31:01', '2018-11-06 23:31:01', 1),
(326, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado mario Hola ghola ha sido rechazado', 'erg', '7', '2018-11-06 23:31:10', '2018-11-06 23:31:10', 1),
(327, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante vacante prueba', '', '1', '2018-11-06 23:31:48', '2018-11-06 23:31:48', 1),
(328, 2, '¡Candidato postulado!', '', '¡Has postulado a OctavioJorge Cruzzano Cruzalez en la vacante TECNICO DE SOPORTE', '', '1', '2018-11-06 23:38:00', '2018-11-06 23:38:00', 1),
(329, 2, '¡Candidato postulado!', '', '¡Has postulado a Nuevo Nuevo Nuevo en la vacante vacante prueba', '', '1', '2018-11-06 23:42:12', '2018-11-06 23:42:12', 1),
(330, 2, '¡Candidato postulado!', '', '¡Has postulado a Hilda Arvizu  Flores en la vacante vacante prueba', '', '1', '2018-11-06 23:46:23', '2018-11-06 23:46:23', 1),
(331, 2, '¡Candidato postulado!', '', '¡Has postulado a mario Hola ghola en la vacante vacante prueba', '', '1', '2018-11-06 23:48:36', '2018-11-06 23:48:36', 1),
(332, 2, '¡Candidato postulado!', '', '¡Has postulado a Moy Hernán Zamarrip en la vacante BECARIO EN CONTABILIDAD', '', '1', '2018-11-06 23:50:29', '2018-11-06 23:50:29', 1),
(333, 2, '¡Candidato postulado!', '', '¡Has postulado a Gabo Sansón Rodea en la vacante vacante prueba', '', '1', '2018-11-06 23:52:28', '2018-11-06 23:52:28', 1),
(334, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante vacante prueba', '', '1', '2018-11-06 23:55:27', '2018-11-06 23:55:27', 1),
(335, 2, '¡Candidato postulado!', '', '¡Has postulado a User Jdjdja Ndnz en la vacante vacante prueba', '', '1', '2018-11-07 00:05:34', '2018-11-07 00:05:34', 1),
(336, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Manzano Kdjx en la vacante Vacante de prueba', '', '1', '2018-11-07 00:05:50', '2018-11-07 00:05:50', 1),
(337, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Lizet Blue chompi ha sido rechazado', 'n/a', '7', '2018-11-07 00:11:50', '2018-11-07 00:11:50', 1),
(338, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante Vacante de prueba', '', '1', '2018-11-07 00:11:57', '2018-11-07 00:11:57', 1),
(339, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado OctavioJorge Cruzzano Cruzalez ha sido rechazado', 'n/a', '7', '2018-11-07 00:12:26', '2018-11-07 00:12:26', 1),
(340, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Nuevo Nuevo Nuevo ha sido rechazado', 'n/a', '7', '2018-11-07 00:12:41', '2018-11-07 00:12:41', 1),
(341, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Moy Hernán Zamarrip ha sido rechazado', 'n/a', '7', '2018-11-07 00:12:49', '2018-11-07 00:12:49', 1),
(342, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Test Q Q ha sido rechazado', 'n/a', '7', '2018-11-07 00:12:56', '2018-11-07 00:12:56', 1),
(343, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado User Jdjdja Ndnz ha sido rechazado', 'n/a', '7', '2018-11-07 00:13:14', '2018-11-07 00:13:14', 1),
(344, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Test Manzano Kdjx ha sido rechazado', 'n/a', '7', '2018-11-07 00:13:25', '2018-11-07 00:13:25', 1),
(345, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado mario Hola ghola ha sido rechazado', 'n/a', '7', '2018-11-07 00:13:33', '2018-11-07 00:13:33', 1),
(346, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Hilda Arvizu  Flores ha sido rechazado', 'n/a', '7', '2018-11-07 00:13:45', '2018-11-07 00:13:45', 1),
(347, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Gabo Sansón Rodea ha sido rechazado', 'n/a', '7', '2018-11-07 00:13:53', '2018-11-07 00:13:53', 1),
(348, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Lizet Blue chompi ha sido rechazado', 'n/a', '7', '2018-11-07 00:14:04', '2018-11-07 00:14:04', 1),
(349, 2, '¡Candidato postulado!', '', '¡Has postulado a OctavioJorge Cruzzano Cruzalez en la vacante vacante prueba', '', '1', '2018-11-07 00:14:17', '2018-11-07 00:14:17', 1),
(350, 2, '¡Candidato postulado!', '', '¡Has postulado a Moy Hernán Zamarrip en la vacante vacante prueba', '', '1', '2018-11-07 00:14:54', '2018-11-07 00:14:54', 1),
(351, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado OctavioJorge Cruzzano Cruzalez ha sido rechazado', 'asd', '7', '2018-11-07 00:24:09', '2018-11-07 00:24:09', 1),
(352, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Moy Hernán Zamarrip ha sido rechazado', 'asd', '7', '2018-11-07 00:24:18', '2018-11-07 00:24:18', 1),
(353, 2, '¡Candidato postulado!', '', '¡Has postulado a Nuevo Nuevo Nuevo en la vacante vacante prueba', '', '1', '2018-11-07 00:28:16', '2018-11-07 00:28:16', 1),
(354, 2, '¡Candidato postulado!', '', '¡Has postulado a User Jdjdja Ndnz en la vacante vacante prueba', '', '1', '2018-11-07 00:34:23', '2018-11-07 00:34:23', 1),
(355, 2, '¡Candidato postulado!', '', '¡Has postulado a OctavioJorge Cruzzano Cruzalez en la vacante vacante prueba', '', '1', '2018-11-07 00:37:45', '2018-11-07 00:37:45', 1),
(356, 2, '¡Candidato postulado!', '', '¡Has postulado a Hilda Arvizu  Flores en la vacante vacante prueba', '', '1', '2018-11-07 00:38:26', '2018-11-07 00:38:26', 1),
(357, 2, '¡Candidato postulado!', '', '¡Has postulado a Hilda Arvizu  Flores en la vacante vacante prueba', '', '1', '2018-11-07 00:38:29', '2018-11-07 00:38:29', 1),
(358, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante vacante prueba', '', '1', '2018-11-07 00:40:07', '2018-11-07 00:40:07', 1),
(359, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante vacante prueba', '', '1', '2018-11-07 00:40:08', '2018-11-07 00:40:08', 1),
(360, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Q Q en la vacante vacante prueba', '', '1', '2018-11-07 00:43:20', '2018-11-07 00:43:20', 1),
(361, 2, '¡Candidato postulado!', '', '¡Has postulado a Gabo Sansón Rodea en la vacante vacante prueba', '', '1', '2018-11-07 00:45:04', '2018-11-07 00:45:04', 1),
(362, 2, '¡Candidato postulado!', '', '¡Has postulado a mario Hola ghola en la vacante vacante prueba', '', '1', '2018-11-07 00:47:50', '2018-11-07 00:47:50', 1),
(363, 2, '¡Candidato postulado!', '', '¡Has postulado a Test Manzano Kdjx en la vacante vacante prueba', '', '1', '2018-11-07 00:52:44', '2018-11-07 00:52:44', 1),
(364, 35, '¡Candidato postulado!', '', '¡Has postulado a Graciela Ortega Lopez en la vacante Aliquam esse omn', '', '1', '2018-11-14 03:49:32', '2018-11-14 03:49:32', 1),
(365, 73, '¡Candidato postulado!', '', '¡Has postulado a Kathya  Mercado  Partida en la vacante DISEÑADOR GRAFICO', '', '1', '2018-11-14 04:00:28', '2018-11-14 04:00:28', 1),
(366, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Lizet Blue chompi ha sido rechazado', 'gh', '7', '2018-11-14 20:40:57', '2018-11-14 20:40:57', 1),
(367, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Hilda Arvizu  Flores ha sido rechazado', 'hu', '7', '2018-11-14 20:41:14', '2018-11-14 20:41:14', 1),
(368, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Lizet Blue chompi ha sido revisado!', 'gh', '2', '2018-11-14 20:41:38', '2018-11-14 20:41:38', 1),
(369, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Gabo Sansón Rodea ha sido rechazado', 'bh', '7', '2018-11-14 21:14:56', '2018-11-14 21:14:56', 1),
(370, 2, '¡Candidato postulado!', '', '¡Has postulado a Gabo Sansón Rodea en la vacante OPERADOR DE CNC', '', '1', '2018-11-14 21:24:29', '2018-11-14 21:24:29', 1),
(371, 2, '¡Candidato postulado!', '', '¡Has postulado a Moy Hernán Zamarrip en la vacante VENDEDOR DE MOSTRADOR', '', '1', '2018-11-14 21:28:53', '2018-11-14 21:28:53', 1),
(372, 2, '¡El CV Visto!', '', '¡El curriculum de tu postulado Moy Hernán Zamarrip ha sido revisado!', 'ok', '2', '2018-11-14 21:29:16', '2018-11-14 21:29:16', 1),
(373, 2, '¡En entrevista!', '', '¡El postulado Moy Hernán Zamarrip fue llamado a entrevista!', 'ok', '3', '2018-11-14 21:29:22', '2018-11-14 21:29:22', 1),
(374, 2, '¡Examenes psicométricos!', '', 'Tu postulado Moy Hernán Zamarrip hara examenes psicométicos', 'ok', '4', '2018-11-14 21:29:25', '2018-11-14 21:29:25', 1),
(375, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Moy Hernán Zamarrip a sido contratado en la vacante VENDEDOR DE MOSTRADOR!', 'ok', '5', '2018-11-14 21:29:29', '2018-11-14 21:29:29', 1),
(376, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:29', '2018-11-14 21:29:29', 1),
(377, 35, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:29', '2018-11-14 21:29:29', 1),
(378, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:29', '2018-11-14 21:29:29', 1),
(379, 63, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Test en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:29', '2018-11-14 21:29:29', 1),
(380, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Graciela en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:30', '2018-11-14 21:29:30', 1),
(381, 35, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Graciela en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:30', '2018-11-14 21:29:30', 1),
(382, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Graciela en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:30', '2018-11-14 21:29:30', 1),
(383, 63, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Graciela en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:30', '2018-11-14 21:29:30', 1),
(384, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Georgina en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:31', '2018-11-14 21:29:31', 1),
(385, 35, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Georgina en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:31', '2018-11-14 21:29:31', 1),
(386, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Georgina en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:31', '2018-11-14 21:29:31', 1),
(387, 63, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Georgina en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:31', '2018-11-14 21:29:31', 1),
(388, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Manzano en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:32', '2018-11-14 21:29:32', 1),
(389, 35, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Manzano en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:32', '2018-11-14 21:29:32', 1),
(390, 41, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Manzano en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:32', '2018-11-14 21:29:32', 1),
(391, 63, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Manzano en la vacante VENDEDOR DE MOSTRADOR ya ha sido cubierta!', '', '6', '2018-11-14 21:29:32', '2018-11-14 21:29:32', 1),
(392, 2, '¡Candidato postulado!', '', '¡Has postulado a Gabo Sansón Rodea en la vacante Natus ex ips', '', '1', '2018-11-14 21:33:57', '2018-11-14 21:33:57', 1),
(393, 2, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Gabo Sansón Rodea a sido contratado en la vacante Natus ex ips!', 'cvcx', '5', '2018-11-23 20:13:33', '2018-11-23 19:13:33', 1),
(394, 2, '¡Candidato postulado!', '', '¡Has postulado a Gabo Sansón Rodea en la vacante Natus ex ips', '', '1', '2018-11-23 20:13:33', '2018-11-23 19:13:33', 1),
(395, 2, '¡Candidato postulado!', '', '¡Has postulado a Moy Hernán Zamarrip en la vacante Sit dolor vel accus', '', '1', '2018-11-23 20:13:33', '2018-11-23 19:13:33', 1),
(396, 35, '¡Candidato postulado!', '', '¡Has postulado a Ninfa Niño Ortega en la vacante Sit dolor vel accus', '', '1', '2018-11-15 04:58:57', '2018-11-15 04:58:57', 1),
(397, 35, 'Postulado contratado', '', '¡Felicidades! ¡Tu postulado Ninfa Niño Ortega a sido contratado en la vacante Sit dolor vel accus!', 'test', '5', '2018-11-15 05:01:23', '2018-11-15 05:01:23', 1),
(398, 2, 'Vacante cubierta', '', '¡Lo sentimos! ¡La postulacion de Moy en la vacante Sit dolor vel accus ya ha sido cubierta!', '', '6', '2018-11-23 20:13:33', '2018-11-23 19:13:33', 1),
(399, 35, '¡Candidato postulado!', '', '¡Has postulado a Graciela Ortega Lopez en la vacante Aliquam esse omn', '', '1', '2018-11-21 00:49:53', '2018-11-21 00:49:53', 1),
(400, 35, '¡Candidato postulado!', '', '¡Has postulado a Ninfa Niño Ortega en la vacante BECARIO', '', '1', '2018-11-21 22:56:15', '2018-11-21 22:56:15', 1),
(401, 35, '¡Candidato postulado!', '', '¡Has postulado a Ninfa Niño Ortega en la vacante BECARIO', '', '1', '2018-11-21 22:56:16', '2018-11-21 22:56:16', 1),
(402, 41, '¡Candidato postulado!', '', '¡Has postulado a Georgina Arias Meza en la vacante Becario', '', '1', '2018-11-23 21:01:56', '2018-11-23 21:01:56', 1),
(403, 41, '¡Candidato postulado!', '', '¡Has postulado a María Gallegos Martínez  en la vacante vacante prueba', '', '1', '2018-11-23 21:02:24', '2018-11-23 21:02:24', 1),
(404, 41, '¡Candidato postulado!', '', '¡Has postulado a José  Villa  Navarro  en la vacante Aliquam esse omn', '', '1', '2018-11-23 22:54:34', '2018-11-23 22:54:34', 1),
(405, 2, '¡Candidato postulado!', '', '¡Has postulado a Moy Hernán Zamarrip en la vacante Vacante de prueba', '', '1', '2018-11-23 20:13:33', '2018-11-23 19:13:33', 1),
(406, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Moy Hernán Zamarrip ha sido rechazado', '-', '7', '2018-11-23 20:13:33', '2018-11-23 19:13:33', 1),
(407, 2, '¡Candidato postulado!', '', '¡Has postulado a Gabo Sansón Rodea en la vacante vacante prueba', '', '1', '2018-11-26 15:12:31', '2018-11-26 14:12:31', 1),
(408, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Nuevo Nuevo Nuevo ha sido rechazado', '-', '7', '2018-11-26 15:12:31', '2018-11-26 14:12:31', 1),
(409, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado User Jdjdja Ndnz ha sido rechazado', '-', '7', '2018-11-26 15:12:31', '2018-11-26 14:12:31', 1),
(410, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado OctavioJorge Cruzzano Cruzalez ha sido rechazado', '-', '7', '2018-11-26 15:12:31', '2018-11-26 14:12:31', 1),
(411, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Hilda Arvizu  Flores ha sido rechazado', '-', '7', '2018-11-26 15:12:31', '2018-11-26 14:12:31', 1),
(412, 2, '¡Postulado rechazado!', '', '¡Lo sentimos! Tu postulado Lizet Blue chompi ha sido rechazado', '-', '7', '2018-11-26 15:12:31', '2018-11-26 14:12:31', 1),
(413, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante Aliquam esse omn', '', '1', '2018-11-26 15:12:31', '2018-11-26 14:12:31', 1),
(414, 1, '¡Candidato postulado!', '', '¡Han postulado a Lizet Blue chompi en la vacante Aliquam esse omn', '', '1', '2018-11-23 21:31:40', '2018-11-23 21:31:40', 0),
(415, 2, '¡Candidato postulado!', '', '¡Has postulado a Lizet Blue chompi en la vacante Aliquam esse omn', '', '1', '2018-11-26 15:12:31', '2018-11-26 14:12:31', 1),
(416, 1, '¡Candidato postulado!', '', '¡Han postulado a Lizet Blue chompi en la vacante Aliquam esse omn', '', '1', '2018-11-23 21:57:21', '2018-11-23 21:57:21', 0),
(417, 41, '¡Candidato postulado!', '', '¡Has postulado a Juana De Arco Pérez  en la vacante Aliquam esse omn', '', '1', '2018-11-27 16:09:36', '2018-11-27 15:09:36', 1),
(418, 1, '¡Candidato postulado!', '', '¡Han postulado a Juana De Arco Pérez  en la vacante Aliquam esse omn', '', '1', '2018-11-27 15:08:54', '2018-11-27 15:08:54', 0),
(419, 41, '¡Candidato postulado!', '', '¡Has postulado a Juana De Arco Pérez  en la vacante Aliquam esse omn', '', '1', '2018-11-27 16:09:36', '2018-11-27 15:09:36', 1),
(420, 1, '¡Candidato postulado!', '', '¡Han postulado a Juana De Arco Pérez  en la vacante Aliquam esse omn', '', '1', '2018-11-27 15:08:57', '2018-11-27 15:08:57', 0),
(421, 41, '¡Candidato postulado!', '', '¡Has postulado a Juana De Arco Pérez  en la vacante Aliquam esse omn', '', '1', '2018-11-27 16:09:36', '2018-11-27 15:09:36', 1),
(422, 1, '¡Candidato postulado!', '', '¡Han postulado a Juana De Arco Pérez  en la vacante Aliquam esse omn', '', '1', '2018-11-27 15:08:58', '2018-11-27 15:08:58', 0),
(423, 41, '¡Candidato postulado!', '', '¡Has postulado a Jorge Manzano Gonzalez en la vacante Becario', '', '1', '2018-11-27 16:09:36', '2018-11-27 15:09:36', 1),
(424, 1, '¡Candidato postulado!', '', '¡Han postulado a Jorge Manzano Gonzalez en la vacante Becario', '', '1', '2018-11-27 15:09:08', '2018-11-27 15:09:08', 0),
(425, 41, '¡Candidato postulado!', '', '¡Has postulado a Jorge Manzano Gonzalez en la vacante Becario', '', '1', '2018-11-27 16:09:36', '2018-11-27 15:09:36', 1),
(426, 1, '¡Candidato postulado!', '', '¡Han postulado a Jorge Manzano Gonzalez en la vacante Becario', '', '1', '2018-11-27 15:09:30', '2018-11-27 15:09:30', 0),
(427, 41, '¡Candidato postulado!', '', '¡Has postulado a Rodrigo  Valles Pérez  en la vacante DISEÑADOR GRAFICO', '', '1', '2018-11-27 16:40:14', '2018-11-27 15:40:14', 1),
(428, 1, '¡Candidato postulado!', '', '¡Han postulado a Rodrigo  Valles Pérez  en la vacante DISEÑADOR GRAFICO', '', '1', '2018-11-27 15:40:06', '2018-11-27 15:40:06', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `available_banks`
--

DROP TABLE IF EXISTS `available_banks`;
CREATE TABLE `available_banks` (
  `id` bigint(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `available_banks`
--

INSERT INTO `available_banks` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Banorte', '2018-09-12 13:51:05', '2018-09-12 13:51:05'),
(2, 'BBVA Bancomer', '2018-09-12 13:56:49', '2018-09-12 13:56:49'),
(3, 'Banamex', '2018-09-12 13:56:49', '2018-09-12 13:56:49'),
(4, 'Santander', '2018-09-12 13:56:49', '2018-09-12 13:56:49'),
(5, 'HSBC', '2018-09-12 13:56:50', '2018-09-12 13:56:50'),
(6, 'Inbursa wallmart', '2018-09-12 13:56:50', '2018-09-12 13:56:50'),
(7, 'Scotiabank', '2018-09-12 13:56:50', '2018-09-12 13:56:50'),
(8, 'Interacciones', '2018-09-12 13:56:50', '2018-09-12 13:56:50'),
(9, 'Banco Azteca', '2018-09-12 13:56:50', '2018-09-12 13:56:50'),
(10, 'Banregio', '2018-09-12 13:56:50', '2018-09-12 13:56:50'),
(11, 'Multiva', '2018-09-12 13:56:50', '2018-09-12 13:56:50'),
(12, 'Mifel', '2018-09-12 13:56:50', '2018-09-12 13:56:50'),
(13, 'Compartamos', '2018-09-12 13:56:50', '2018-09-12 13:56:50'),
(15, 'Ve por mas', '2018-09-12 13:56:51', '2018-09-12 13:56:51'),
(16, 'Invex', '2018-09-12 13:56:51', '2018-09-12 13:56:51'),
(17, 'Bancoppel', '2018-09-12 13:56:51', '2018-09-12 13:56:51'),
(18, 'Banco ahorro Famsa', '2018-09-12 13:56:51', '2018-09-12 13:56:51'),
(20, 'American Express', '2018-09-12 13:56:51', '2018-09-12 13:56:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs` (
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `blogs`
--

INSERT INTO `blogs` (`url`, `created_at`, `updated_at`) VALUES
('https://damsablog.com/', '2018-06-27 21:32:25', '2018-06-27 21:32:25');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `Buscar_recomendados`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `Buscar_recomendados`;
CREATE TABLE `Buscar_recomendados` (
`token` varchar(191)
,`user_id` varchar(191)
,`candidate_id` int(10) unsigned
,`vacancy_id` int(10) unsigned
,`full_name` text
,`vacancy_name` varchar(191)
,`company_name` varchar(191)
,`name` varchar(191)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `Buscar_usuarios_recomendados_vacante`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `Buscar_usuarios_recomendados_vacante`;
CREATE TABLE `Buscar_usuarios_recomendados_vacante` (
`candidate_id` int(10) unsigned
,`vacancy_id` int(10) unsigned
,`full_name` text
,`vacancy_name` varchar(191)
,`name` varchar(191)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidates`
--

DROP TABLE IF EXISTS `candidates`;
CREATE TABLE `candidates` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seeking_work` tinyint(1) NOT NULL,
  `profile_picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `names` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maternal_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paternal_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `house_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `curp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nss` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `birthday_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `candidates`
--

INSERT INTO `candidates` (`id`, `user_id`, `seeking_work`, `profile_picture`, `curriculum`, `names`, `maternal_name`, `paternal_name`, `country`, `state`, `city`, `district`, `street`, `house_number`, `home_phone`, `mobile_phone`, `email`, `gender`, `curp`, `rfc`, `nss`, `created_at`, `updated_at`, `birthday_date`) VALUES
(1, '2', 1, 'candidate_profile_pictures/D8af6Kaf23WvauPhVJzwYpcEKe6FvQrPjzx3iUZJ.jpeg', 'curriculums/ntDWhX4YYNhGPDAAbVjGTz5Z9EJUNDYY9pj7WLdZ.docx', 'OctavioJorge', 'Cruzalez', 'Cruzzano', 'Mexico', 'Jalisco', 'Guadalajara', 'jardines del volcan', 'isala caracuaro', '123', '1331193313', '1233333333', 'octaviovcruz@hotmail.com', 'Hombre', '123HIJBGGYJHCGGHJB', 'centrodeensen', '12356998559', '2018-09-18 20:51:37', '2018-11-02 20:14:33', '1990-11-02'),
(2, '4', 1, 'candidate_profile_pictures/srb6mcrzMjHL8Kk2AVcc7wdlr7LUQ7X5KupVULhs.png', NULL, 'Sandy', 'Ramirez', 'Magallanes', 'Mexico', 'Jalisco', 'Zapopan', 'Colli', 'Bzbz', '2440', '', '9767667676', 'hdhdj@jdjd.com', 'Mujer', 'JZJDJJDJDJDJDJJDJD', '', '', '2018-09-19 18:17:02', '2018-09-29 04:41:50', NULL),
(28, '2', 1, NULL, 'null', 'Test', 'Q', 'Q', 'Mexico', 'Distrito Federal', 'Tlalpan', 'H', 'H', '7', '', '6597979676', 'gg@jdj.com', 'Hombre', 'YDHDJDJXJJXJXJDJJD', '', '', '2018-09-28 21:03:21', '2018-09-28 21:20:32', NULL),
(30, '25', 1, NULL, 'curriculums/3NDRTEoAoBrnEhfC51hMoj3NaqgzMEkYORGQVS4y.png', 'alsd', 'sas', 'sas', 'Mexico', 'Jalisco', 'Bolaños', 'pasew', 'dasd', '123', '1234567890', '1234567890', 'fredo287@outlook.com', 'Hombre', 'LOAA960514HBCZRL02', 'LOAA96051402', '12345678910', '2018-09-29 01:56:27', '2018-09-29 01:57:16', NULL),
(31, '32', 1, 'candidate_profile_pictures/0RBcmv4R5Sk6VukHCA1dPEiThaof2QW4jzFDxg6j.jpeg', 'curriculums/94UBeJE7ZdSTJUQIEbi6eDHBt9uGqflrnoL8QHGp.pdf', 'Mario Alberto', 'Venegas ', 'Cuevas', 'Mexico', 'Jalisco', 'Guadalajara', 'Colonia ', 'Calle ', '11', '', '3331111255', 'mario@oomovil.com', 'Hombre', 'CUVM7501235Y478888', 'CUVM7501235Y', '11211111122', '2018-10-02 02:37:03', '2018-10-02 02:43:18', NULL),
(32, '35', 1, 'candidate_profile_pictures/MyjKICPaBW4h3Jj8xVqvAvhFHamEwD89HDHjhRYU.png', NULL, 'Graciela', 'Lopez', 'Ortega', 'Mexico', 'Jalisco', 'Guadalajara', 'Colinia', 'Crusantemos', '454', '1234567890', '1234567890', 'a', 'Mujer', '111111111111111111', '111111111111', '11111111111', '2018-10-02 23:45:02', '2018-10-25 00:57:47', '2018-10-26'),
(33, '33', 0, 'candidate_profile_pictures/M8ClXHr1YT9hzNyGZcb8LZgVa0TbDg0pr5bLjOBJ.png', 'curriculums/Dkw8eHV0AeA7F4qeviCCppV8iLwYKZ1fpjdPpBsD.pdf', 'José Alberto', 'Baltazar', 'Gaspar', 'Mexico', 'Jalisco', 'Guadalajara', 'Chulavista', 'Pendiente', 'pendiente', '', '3310811223', 'albertohopy126@gmail.com', 'Hombre', 'GABA970609HJCSLL09', 'GABA970609QVA', '21169765134', '2018-10-02 23:54:55', '2018-10-02 23:57:24', NULL),
(34, '2', 1, NULL, 'curriculums/NQYmjBIxYSUzGHEzlOmchdT20f6VURUFf3eSHMyI.pdf', 'mario', 'ghola', 'Hola', 'Mexico', 'Jalisco', 'Guadalajara', 'dfghjndg', 'dfhjdf', '12gh', '2121123121', '6456456456', 'midoriyanome@gmail.com', 'Hombre', 'JDJDJFJDKDKDJDKDKD', '', '', '2018-10-03 22:40:16', '2018-10-29 19:45:01', '2018-10-01'),
(36, '21', 1, NULL, NULL, 'Alejandra ', 'Andrade ', 'Rosas', 'Mexico', 'Jalisco', 'Ocotlán', 'Torrecillas ', 'Hshdjdjdv', '110', '3929250538', '3921360335', 'alero24aa@hotmail.com', 'Mujer', 'JAJSOWJWBQP0987654123', 'ASDEQOPK8910', 'HANDLEQWERT', '2018-10-04 23:54:09', '2018-10-04 23:54:09', NULL),
(37, '41', 1, NULL, 'curriculums/9X2SUKWJOdktNjomptzrNyyVybprTTNkr4vcsG2B.pdf', 'José ', 'Navarro ', 'Villa ', 'Mexico', 'Jalisco', 'Zapopan', 'Lomaz', 'La Mancha ', '50', '3300885522', '3366998855', 'jose@hotmail.com', 'Hombre', 'AAQQSSWWDDEEFFRRGG', 'AAQQSSWWDDEE', '12345678901', '2018-10-09 21:47:34', '2018-10-19 20:00:00', '2018-10-17'),
(38, '41', 0, NULL, 'curriculums/Dkw8eHV0AeA7F4qeviCCppV8iLwYKZ1fpjdPpBsD.pdf', 'María', 'Martínez ', 'Gallegos', 'Mexico', 'Colima', 'Comala', 'Villas', 'Mar Egeo', '30', '3322558800', '3366998855', 'maria@hotmail.com', 'Hombre', 'MMAARRIIAALLMMAADD', 'MMAARRIIAALL', '12345678902', '2018-10-09 21:51:45', '2018-10-19 19:30:16', '2000-09-05'),
(39, '41', 1, NULL, 'curriculums/Dkw8eHV0AeA7F4qeviCCppV8iLwYKZ1fpjdPpBsD.pdf', 'Juana', 'Pérez ', 'De Arco', 'Mexico', 'Jalisco', 'La Barca', 'Valles', 'Benito ', '30', '', '3311225588', 'juanadearco@hotmail.com', 'Mujer', 'JUANADEARCOPEREZZZ', 'JUANADEARCOPE', '12345678904', '2018-10-10 22:31:21', '2018-10-10 22:31:21', NULL),
(40, '41', 1, NULL, 'curriculums/bFzTfuEy90qq6GWpqOsZLCSxY4WCfLEeuqxkTVst.pdf', 'Rodrigo ', 'Pérez ', 'Valles', 'Mexico', 'Nuevo León', 'Apodaca', 'Paraíso ', 'Prado', '13', '', '3322556688', 'rodrigo@hotmail.com', 'Hombre', 'RROODDRRIIGGOOOOOO', '', '', '2018-10-11 18:39:00', '2018-10-19 19:29:15', '1990-07-06'),
(41, '41', 0, NULL, 'curriculums/FgYNZRPQwDmtpQvYWxxU5B1GUqYbBXSP6nNhzN95.pdf', 'Georgina', 'Meza', 'Arias', 'Mexico', 'Jalisco', 'Guadalajara', 'Tuzania', 'Adolfo', '10', '', '3311552244', 'georgina@hotmail.com', 'Mujer', 'AAAAAAAAAAAAAAAAAA', '', '', '2018-10-15 22:09:16', '2018-10-31 20:21:23', '1998-10-31'),
(42, '2', 0, 'candidate_profile_pictures/VMVoK7QTEMgy6KeV404FRY10QCUWO7Jy6onZKJtA.png', NULL, 'Octavio ', 'Zjjs', 'Backend', 'Mexico', 'Chihuahua', 'Buenaventura', 'Jsjd', 'Hsjjj', '8', '3334343463', '6764663131', 'uudud@kdjd.co', 'Mujer', 'JSJDJDJDJDJJDJDKDK', '', '', '2018-10-15 22:35:17', '2018-11-10 00:02:21', '2018-10-10'),
(44, '2', 1, NULL, NULL, 'User', 'Ndnz', 'Jdjdja', 'Mexico', 'Baja California Sur', 'Comondú', 'Jxjz', 'Jzjz', 'zbzjjd', '8784545454', '6434343464664', 'jskd@jzjz.com', 'Hombre', 'NnZBANSNNSNZBZBZBZ', '', '', '2018-10-16 18:30:55', '2018-10-16 18:32:47', NULL),
(45, '2', 1, NULL, NULL, 'Moy', 'Zamarrip', 'Hernán', 'Mexico', 'Aguascalientes', 'Asientos', 'Hdhdhjr', 'Hhehruf', 'jdjd66', '', '4554515154', 'hdhhfufd@difiir.doif', 'Hombre', 'HDJDJD7737UUJDJJDB', '', '', '2018-10-17 20:56:23', '2018-10-29 21:46:39', '2006-10-29'),
(46, '2', 1, 'candidate_profile_pictures/BZaV03KMpFiPwxGealExuAetNf8gcyznXwIrrPTN.jpeg', 'curriculums/LvNLpGkOmsv7PcLwbIWBQB61B4yEzO5QpFgxw8FF.pdf', 'Nuevo', 'Nuevo', 'Nuevo', 'Mexico', 'Aguascalientes', 'Asientos', 'Hdhdjdjd', 'Djdjfjf', 'jdjdjd', '', '848555555555', 'nfkdkd@dkdujd.kdkdid', 'Hombre', 'ZJJDJDJJJ677567727', '', '', '2018-10-17 21:47:40', '2018-10-17 21:58:36', '2016-10-17'),
(47, '2', 1, NULL, NULL, 'Hilda', 'Flores', 'Arvizu ', 'Mexico', 'Baja California Sur', 'La Paz', 'Florida ', 'Guadalajara ', '1233', '3333124585', '5524155255', 'hilda@gmail.com', 'Mujer', 'CAHEHEDGGQ25667889', '2gwdg3764gdvv', '35556666688', '2018-10-17 22:03:06', '2018-10-30 20:11:06', '1997-02-27'),
(48, '41', 1, NULL, NULL, 'Jorge', 'Gonzalez', 'Manzano', 'Test Activities', '01', '2014', '12', '2018', '123', '123', '123', '123', 'Hombre', '123', '123', '123', '2018-10-19 18:18:35', '2018-10-19 18:18:35', '1995-05-08'),
(53, '2', 1, 'candidate_profile_pictures/J4o6C15balYMYWMjEvIRYelnHC6zZgOJVEtaCHLP.jpeg', NULL, 'Lizet', 'chompi', 'Blue', 'Mexico', 'Jalisco', 'Guadalajara', 'jardines alcalde', 'isla mujeres', '2324', '3313312313', '6633333333', 'lizi-blue@hotmail.com', 'Hombre', 'bucl930121asdfre03', 'bucl930121asd', '', '2018-10-20 02:26:20', '2018-10-29 19:44:07', '2018-10-01'),
(55, '69', 1, NULL, NULL, 'Juan Enrique', 'González', 'González', 'Mexico', 'Jalisco', 'Guadalajara', 'Oblatos ', 'Hzldhldj', '280', '3312548708', '3314355271', 'jegg_07@hotmail.com', 'Hombre', 'GOGJ910614HJCMNN03', 'GOGJ910614FN0', '75119116236', '2018-10-22 18:32:01', '2018-10-22 18:36:07', '2017-10-11'),
(56, '69', 1, NULL, NULL, 'Alejandra', 'Andrade', 'Rosas', 'Mexico', 'Jalisco', 'Ocotlán', 'Torrecillas ', 'Tomad saldaña ', '110', '3929250538', '3921360335', 'alero24a@hotmail.com', 'Mujer', 'JJJJJJJSOEILLPPPOR', 'BDBDKDKDKSKDK', '95643487673', '2018-10-22 18:45:59', '2018-10-22 18:45:59', '2018-05-24'),
(57, '35', 1, NULL, NULL, 'Ninfa', 'Ortega', 'Niño', 'Mexico', 'Distrito Federal', 'La Magdalena Contreras', 'Colonis', 'Calle', '2', '', '1234567890', 'n', 'Mujer', '1234556789KNNGTRFD', '', '', '2018-10-22 22:51:46', '2018-10-22 22:51:46', '2018-10-25'),
(58, '2', 1, NULL, NULL, 'Test', 'Kdjx', 'Manzano', 'Mexico', 'Chiapas', 'Altamirano', 'Nzndj', 'Jsjdbd', 'bzbzb', '', '5454575455454', 'jjd@jdjd.com', 'Hombre', 'JDJDJDJDJJDJDJXKDK', '', '', '2018-10-29 22:08:31', '2018-10-29 22:08:31', '2018-10-29'),
(59, '2', 0, NULL, NULL, 'Gabo', 'Rodea', 'Sansón', 'Mexico', 'Chiapas', 'Amatenango de la Frontera', 'Valles', 'Girasol', '56', '', '3333333333', 'gasr@correo.com', 'Hombre', 'GJVCFUKBCFYJBHHXFT', '', '', '2018-11-02 20:19:25', '2018-11-02 20:19:25', '2000-11-02'),
(60, '63', 1, 'candidate_profile_pictures/2NDZ4zGgigfKbYuHNKxZHWTuBvSXULAgX5PQoXrD.jpeg', NULL, 'Manzano', 'DE CV', 'DE CV', 'Mexico', 'Jalisco', 'Guadalajara', 'hgjvbk', 'Colli urbano  Volcan de fuego ', '7272', '3334586668', '3334586668', 'iugkSA@GMAIL.COM', 'Hombre', 'mang191219jnnkkdhf', 'eab9407041x0', '123', '2018-11-05 06:54:13', '2018-11-05 06:54:13', '1995-12-19'),
(61, '35', 1, NULL, NULL, 'Ninfa', 'Ortega', 'Niño', 'Mexico', 'Durango', 'General Simón Bolívar', 'Jardin de los crisantemos', 'Vallarta', '2828', '', '3311771791', 'ninfa_sys@hotmail.com', 'Hombre', '123456789012345678', '', '', '2018-11-05 21:04:31', '2018-11-05 21:04:31', '2018-11-04'),
(62, '73', 1, NULL, NULL, 'Kathya ', 'Partida', 'Mercado ', 'Mexico', 'Sonora', 'Cajeme', 'Las brisas', 'Bahía concepción ', '2235', '', '6441182918', 'disenoweb@damsa.com.mx', 'Hombre', 'WORKWIFBEIWNDIWNDL', 'ISBXISNDKXNLD', '31846854394', '2018-11-14 03:59:05', '2018-11-14 03:59:05', '1992-11-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidate_language`
--

DROP TABLE IF EXISTS `candidate_language`;
CREATE TABLE `candidate_language` (
  `id` int(10) UNSIGNED NOT NULL,
  `candidate_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage_known` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certification_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `candidate_language`
--

INSERT INTO `candidate_language` (`id`, `candidate_id`, `language_id`, `percentage_known`, `certification_type`, `created_at`, `updated_at`) VALUES
(18, '5', '11', '5', 'Rerum deserunt earum dolor qui quis esse sed', '2018-09-26 20:07:48', '2018-09-26 20:07:48'),
(19, '6', '9', '7', 'In mollit qui odio perspiciatis voluptas do', '2018-09-26 20:23:15', '2018-09-26 20:23:15'),
(20, '7', '9', '7', 'In mollit qui odio perspiciatis voluptas do', '2018-09-26 20:24:06', '2018-09-26 20:24:06'),
(21, '9', '7', '62', 'Omnis possimus ut culpa maxime dolorem aute laborum do est doloribus laborum sed exercitationem ab magni maxime illo', '2018-09-26 21:00:01', '2018-09-26 21:00:01'),
(22, '10', '8', '27', 'Dolorum ad in excepteur nisi perferendis aliquip facilis consectetur veritatis', '2018-09-26 22:43:40', '2018-09-26 22:43:40'),
(23, '11', '8', '27', 'Dolorum ad in excepteur nisi perferendis aliquip facilis consectetur veritatis', '2018-09-26 22:46:44', '2018-09-26 22:46:44'),
(24, '12', '8', '27', 'Dolorum ad in excepteur nisi perferendis aliquip facilis consectetur veritatis', '2018-09-26 22:47:18', '2018-09-26 22:47:18'),
(31, '13', '10', '35', 'Laboris cupiditate ut necessitatibus quis natus a minus minus dolorem quisquam qui eum dolore cum', '2018-09-27 00:06:46', '2018-09-27 00:06:46'),
(32, '14', '8', '53', 'Consequatur Iure quo a facere ab quasi proident rerum facilis dolorum sunt illo numquam consequat', '2018-09-27 00:26:42', '2018-09-27 00:26:42'),
(33, '15', '8', '53', 'Consequatur Iure quo a facere ab quasi proident rerum facilis dolorum sunt illo numquam consequat', '2018-09-27 00:27:04', '2018-09-27 00:27:04'),
(34, '16', '11', '100', 'Earum dicta ipsam neque enim minus debitis sit sequi repellendus Nihil mollitia sit velit quia et non adipisci possimus', '2018-09-27 00:30:51', '2018-09-27 00:30:51'),
(35, '17', '11', '100', 'Earum dicta ipsam neque enim minus debitis sit sequi repellendus Nihil mollitia sit velit quia et non adipisci possimus', '2018-09-27 00:33:11', '2018-09-27 00:33:11'),
(36, '18', '11', '100', 'Earum dicta ipsam neque enim minus debitis sit sequi repellendus Nihil mollitia sit velit quia et non adipisci possimus', '2018-09-27 00:46:49', '2018-09-27 00:46:49'),
(37, '19', '10', '50', 'Et voluptatem et aut est in aut quae ea minus sit', '2018-09-27 00:50:02', '2018-09-27 00:50:02'),
(38, '20', '7', '90', 'Ut esse ab quo et eaque voluptate praesentium qui voluptatem distinctio Voluptas enim est consectetur labore', '2018-09-27 01:30:21', '2018-09-27 01:30:21'),
(39, '21', '9', '9', 'Qui occaecat dolores fugit fugiat ut voluptatem temporibus commodo molestias eaque quia', '2018-09-27 01:32:44', '2018-09-27 01:32:44'),
(40, '22', '7', '59', 'Sint velit veritatis anim eiusmod autem consequatur incididunt alias iusto', '2018-09-27 01:55:38', '2018-09-27 01:55:38'),
(76, '28', '8', '82', 'None', '2018-09-28 21:20:32', '2018-09-28 21:20:32'),
(81, '30', '8', '73', 'asdasdada', '2018-09-29 01:57:16', '2018-09-29 01:57:16'),
(84, '2', '7', '60', 'Jj', '2018-09-29 04:41:50', '2018-09-29 04:41:50'),
(92, '33', '7', '100', 'Ninguna', '2018-10-02 23:57:24', '2018-10-02 23:57:24'),
(94, '31', '8', '50', 'diploma', '2018-10-03 22:44:42', '2018-10-03 22:44:42'),
(100, '36', '7', '50', 'certificado ', '2018-10-04 23:54:09', '2018-10-04 23:54:09'),
(105, '39', '10', '47', 'Ninguna', '2018-10-10 22:31:21', '2018-10-10 22:31:21'),
(135, '44', '9', '50', '', '2018-10-16 18:32:47', '2018-10-16 18:32:47'),
(158, '38', '8', '46', 'Ninguna ', '2018-10-19 19:30:16', '2018-10-19 19:30:16'),
(159, '40', '9', '99', '', '2018-10-19 19:54:49', '2018-10-19 19:54:49'),
(160, '37', '8', '46', 'Ninguna ', '2018-10-19 20:00:00', '2018-10-19 20:00:00'),
(166, '56', '7', '100', 'Nativo', '2018-10-22 18:45:59', '2018-10-22 18:45:59'),
(167, '56', '8', '40', 'En curso ', '2018-10-22 18:45:59', '2018-10-22 18:45:59'),
(168, '55', '7', '100', 'Nativo ', '2018-10-22 19:13:49', '2018-10-22 19:13:49'),
(172, '32', '7', '0', 'A', '2018-10-25 01:00:19', '2018-10-25 01:00:19'),
(186, '41', '8', '19', '', '2018-10-31 20:21:23', '2018-10-31 20:21:23'),
(187, '59', '8', '50', 'Proulex', '2018-11-02 20:19:27', '2018-11-02 20:19:27'),
(194, '53', '9', '50', '', '2018-11-09 22:51:48', '2018-11-09 22:51:48'),
(195, '53', '11', '50', '', '2018-11-09 22:51:48', '2018-11-09 22:51:48'),
(196, '42', '7', '64', 'Zjjjjjj', '2018-11-10 00:02:21', '2018-11-10 00:02:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidate_software`
--

DROP TABLE IF EXISTS `candidate_software`;
CREATE TABLE `candidate_software` (
  `id` int(10) UNSIGNED NOT NULL,
  `candidate_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage_known` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certification_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `candidate_software`
--

INSERT INTO `candidate_software` (`id`, `candidate_id`, `software_id`, `percentage_known`, `certification_type`, `created_at`, `updated_at`) VALUES
(10, '5', '1', '46', 'Aut voluptatem ratione odio ipsum reiciendis quis', '2018-09-26 20:07:48', '2018-09-26 20:07:48'),
(11, '6', '7', '90', 'Quos omnis non delectus perspiciatis labore elit ex occaecat id aut aut error praesentium ut incidunt', '2018-09-26 20:23:15', '2018-09-26 20:23:15'),
(12, '7', '7', '90', 'Quos omnis non delectus perspiciatis labore elit ex occaecat id aut aut error praesentium ut incidunt', '2018-09-26 20:24:06', '2018-09-26 20:24:06'),
(13, '8', '1', '80', 'TOEFL', '2018-09-26 20:43:54', '2018-09-26 20:43:54'),
(14, '9', '7', '32', 'Eligendi nihil qui aliquam tenetur ut perspiciatis cupidatat nobis voluptatibus dolorum dolores molestiae dicta quia odio nostrud earum nulla', '2018-09-26 21:00:01', '2018-09-26 21:00:01'),
(15, '10', '1', '29', 'Neque omnis lorem dolorem cumque quasi ea nisi ducimus corrupti et qui suscipit est enim possimus iste nihil sed est', '2018-09-26 22:43:40', '2018-09-26 22:43:40'),
(16, '11', '1', '29', 'Neque omnis lorem dolorem cumque quasi ea nisi ducimus corrupti et qui suscipit est enim possimus iste nihil sed est', '2018-09-26 22:46:44', '2018-09-26 22:46:44'),
(17, '12', '1', '29', 'Neque omnis lorem dolorem cumque quasi ea nisi ducimus corrupti et qui suscipit est enim possimus iste nihil sed est', '2018-09-26 22:47:18', '2018-09-26 22:47:18'),
(20, '13', '7', '57', 'Ullam architecto quisquam error ad unde beatae commodo corrupti sit qui eos', '2018-09-27 00:06:46', '2018-09-27 00:06:46'),
(21, '14', '1', '88', 'Pariatur Aut assumenda modi blanditiis omnis aliquam similique ducimus aliquam dolore incidunt quae totam velit labore dolor officia ea unde', '2018-09-27 00:26:42', '2018-09-27 00:26:42'),
(22, '15', '1', '88', 'Pariatur Aut assumenda modi blanditiis omnis aliquam similique ducimus aliquam dolore incidunt quae totam velit labore dolor officia ea unde', '2018-09-27 00:27:04', '2018-09-27 00:27:04'),
(23, '16', '7', '67', 'Eveniet quis voluptatem sit rem vero aut non voluptates temporibus vel non nulla', '2018-09-27 00:30:51', '2018-09-27 00:30:51'),
(24, '17', '7', '67', 'Eveniet quis voluptatem sit rem vero aut non voluptates temporibus vel non nulla', '2018-09-27 00:33:11', '2018-09-27 00:33:11'),
(25, '18', '7', '67', 'Eveniet quis voluptatem sit rem vero aut non voluptates temporibus vel non nulla', '2018-09-27 00:46:49', '2018-09-27 00:46:49'),
(26, '19', '7', '26', 'Officia sed fugiat ullamco adipisicing sit eveniet', '2018-09-27 00:50:02', '2018-09-27 00:50:02'),
(27, '20', '7', '86', 'Nisi dolore vel enim dolorem dicta iure elit numquam assumenda', '2018-09-27 01:30:21', '2018-09-27 01:30:21'),
(28, '21', '7', '92', 'Laudantium porro ut veritatis enim dicta blanditiis cupiditate eius harum eaque autem quisquam voluptatum a incidunt veniam', '2018-09-27 01:32:44', '2018-09-27 01:32:44'),
(29, '22', '1', '22', 'Autem est dolore do pariatur Magnam fuga', '2018-09-27 01:55:38', '2018-09-27 01:55:38'),
(44, '28', '1', '66', 'Hdhd', '2018-09-28 21:20:32', '2018-09-28 21:20:32'),
(51, '30', '1', '27', 'asdada', '2018-09-29 01:57:16', '2018-09-29 01:57:16'),
(53, '2', '1', '52', 'Bb', '2018-09-29 04:41:50', '2018-09-29 04:41:50'),
(61, '33', '7', '51', 'Excel', '2018-10-02 23:57:24', '2018-10-02 23:57:24'),
(63, '31', '7', '76', 'diploma ', '2018-10-03 22:44:42', '2018-10-03 22:44:42'),
(67, '36', '1', '100', 'certificado ', '2018-10-04 23:54:09', '2018-10-04 23:54:09'),
(72, '39', '7', '84', 'Ninguna', '2018-10-10 22:31:21', '2018-10-10 22:31:21'),
(110, '48', '1', '80', 'TOEFL', '2018-10-19 18:18:35', '2018-10-19 18:18:35'),
(114, '38', '1', '72', 'Ninguna ', '2018-10-19 19:30:16', '2018-10-19 19:30:16'),
(121, '56', '1', '12', 'En vurso ', '2018-10-22 18:45:59', '2018-10-22 18:45:59'),
(122, '55', '1', '0', 'Ufjg', '2018-10-22 19:13:49', '2018-10-22 19:13:49'),
(126, '57', '1', '60', 'Eee', '2018-10-22 23:02:00', '2018-10-22 23:02:00'),
(130, '32', '1', '45', 'A', '2018-10-25 01:00:19', '2018-10-25 01:00:19'),
(146, '41', '7', '24', '', '2018-10-31 20:21:23', '2018-10-31 20:21:23'),
(148, '59', '7', '60', 'Software gurú', '2018-11-02 20:19:27', '2018-11-02 20:19:27'),
(151, '1', '1', '80', 'TOEFL', '2018-11-09 21:49:02', '2018-11-09 21:49:02'),
(156, '53', '1', '50', '', '2018-11-09 22:51:48', '2018-11-09 22:51:48'),
(157, '53', '7', '63', '', '2018-11-09 22:51:48', '2018-11-09 22:51:48'),
(158, '42', '1', '55', 'D', '2018-11-10 00:02:21', '2018-11-10 00:02:21'),
(159, '62', '7', '100', '', '2018-11-14 03:59:05', '2018-11-14 03:59:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidate_vacancy`
--

DROP TABLE IF EXISTS `candidate_vacancy`;
CREATE TABLE `candidate_vacancy` (
  `id` int(10) UNSIGNED NOT NULL,
  `candidate_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date_postulate` datetime DEFAULT NULL,
  `date_cv_sent` datetime DEFAULT NULL,
  `date_interview` datetime DEFAULT NULL,
  `date_psychometric` datetime DEFAULT NULL,
  `date_hired` datetime DEFAULT NULL,
  `Active` tinyint(4) DEFAULT '1',
  `reason_cv_sent` text COLLATE utf8mb4_unicode_ci,
  `reason_interview` text COLLATE utf8mb4_unicode_ci,
  `reason_psychometric` text COLLATE utf8mb4_unicode_ci,
  `reason_hired` text COLLATE utf8mb4_unicode_ci,
  `date_rejected` datetime DEFAULT NULL,
  `reason_rejected` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `candidate_vacancy`
--

INSERT INTO `candidate_vacancy` (`id`, `candidate_id`, `vacancy_id`, `status`, `created_at`, `updated_at`, `date_postulate`, `date_cv_sent`, `date_interview`, `date_psychometric`, `date_hired`, `Active`, `reason_cv_sent`, `reason_interview`, `reason_psychometric`, `reason_hired`, `date_rejected`, `reason_rejected`) VALUES
(36, '1', '27', 'Contratado', '2018-10-05 23:56:40', '2018-10-08 18:19:00', '2018-10-05 15:56:40', '2018-10-08 10:16:32', '2018-10-08 10:16:41', '2018-10-08 10:16:49', '2018-10-08 10:18:59', 0, 'Tu curriculum fue visto', 'en entrevista', 'en psicmetricos', 'Contrado empieza mañana', NULL, NULL),
(37, '28', '27', 'Vacante cubierta', '2018-10-05 23:57:25', '2018-10-05 23:57:25', '2018-10-05 15:57:25', NULL, NULL, NULL, '2018-10-08 10:18:59', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(38, '1', '29', 'Contratado', '2018-10-05 23:59:16', '2018-10-09 21:12:49', '2018-10-05 15:59:16', NULL, NULL, NULL, '2018-10-09 13:12:49', 0, NULL, NULL, NULL, 'Contratado Felicidades !!', NULL, NULL),
(39, '105', '66', 'Postulado', '2018-10-06 00:09:35', '2018-10-06 00:09:35', '2018-10-05 16:09:35', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(40, '1', '26', 'Contratado', '2018-10-06 00:10:16', '2018-10-10 19:39:41', '2018-10-05 16:10:16', NULL, NULL, NULL, '2018-10-10 11:39:40', 0, NULL, NULL, NULL, 'gg', NULL, NULL),
(41, '34', '26', 'Vacante cubierta', '2018-10-06 00:10:38', '2018-10-06 00:10:38', '2018-10-05 16:10:38', NULL, NULL, NULL, '2018-10-10 11:39:40', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(42, '28', '26', 'Vacante cubierta', '2018-10-06 00:11:24', '2018-10-06 00:11:24', '2018-10-05 16:11:24', NULL, NULL, NULL, '2018-10-10 11:39:40', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(43, '33', '37', 'Psicométrico', '2018-10-06 02:52:35', '2018-11-06 03:46:26', '2018-10-05 18:52:35', '2018-10-08 10:57:16', '2018-10-08 10:57:56', '2018-11-05 17:46:25', NULL, 1, '¡Alguien ha visto tu Curriculum! Estamos haciendo las valoraciones necesarias para gestionar tu curriculum. Sigue al pendiente de los resultados.', 'Se ha programado una entrevista para la vacante. Pronto te estaremos informando del siguiente proceso.', 'Lo mejor está por venir. Necesitamos hacer las últimas evaluaciones, antes de tomar la decisión final.', NULL, NULL, NULL),
(44, '1', '32', 'Contratado', '2018-10-09 18:14:02', '2018-10-16 20:51:23', '2018-10-09 10:14:02', '2018-10-16 12:07:11', NULL, NULL, '2018-10-16 12:51:23', 0, 'Visto', NULL, NULL, 'felcidades', NULL, NULL),
(45, '1', '28', 'Contratado', '2018-10-10 23:26:47', '2018-10-16 20:58:33', '2018-10-10 15:26:47', NULL, NULL, NULL, '2018-10-16 12:58:32', 0, NULL, NULL, NULL, 'c', NULL, NULL),
(46, '28', '28', 'Vacante cubierta', '2018-10-10 23:28:04', '2018-10-16 05:33:57', '2018-10-10 15:28:04', '2018-10-15 20:25:07', '2018-10-15 20:26:16', '2018-10-15 20:27:35', '2018-10-16 12:58:32', 0, 'cv visto', 'cv visto', 'cv visto', NULL, '2018-10-15 21:33:57', 'cv visto'),
(47, '36', '19', 'Contratado', '2018-10-11 00:38:05', '2018-10-18 23:04:08', '2018-10-10 16:38:05', NULL, NULL, NULL, '2018-10-18 15:04:06', 0, NULL, NULL, NULL, 'g', NULL, NULL),
(51, '36', '22', 'Vacante cubierta', '2018-10-11 00:38:32', '2018-10-11 00:38:32', '2018-10-10 16:38:32', NULL, NULL, NULL, '2018-10-18 15:08:47', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(53, '1', '22', 'Vacante cubierta', '2018-10-11 00:47:07', '2018-10-18 21:19:38', '2018-10-10 16:47:07', NULL, '2018-10-18 13:19:38', NULL, '2018-10-18 15:08:47', 0, NULL, 'en entrevista', NULL, NULL, NULL, NULL),
(54, '38', '22', 'Vacante cubierta', '2018-10-15 18:43:02', '2018-10-15 18:43:02', '2018-10-15 10:43:02', NULL, NULL, NULL, '2018-10-18 15:08:47', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(55, '28', '40', 'Vacante cubierta', '2018-10-16 05:36:08', '2018-10-16 05:36:59', '2018-10-15 21:36:08', '2018-10-15 21:36:59', NULL, NULL, '2018-10-22 11:45:56', 0, 'cv', NULL, NULL, NULL, NULL, NULL),
(56, '41', '34', 'Vacante cubierta', '2018-10-16 19:04:50', '2018-10-16 19:04:50', '2018-10-16 11:04:50', NULL, NULL, NULL, '2018-10-29 14:52:57', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(57, '37', '34', 'Vacante cubierta', '2018-10-16 19:05:17', '2018-10-16 19:05:17', '2018-10-16 11:05:17', NULL, NULL, NULL, '2018-10-29 14:52:57', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(58, '41', '23', 'Vacante cubierta', '2018-10-16 19:07:31', '2018-10-16 19:07:31', '2018-10-16 11:07:31', NULL, NULL, NULL, '2018-11-14 11:29:28', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(59, '42', '66', 'Postulado', '2018-10-16 19:26:17', '2018-10-16 19:26:17', '2018-10-16 11:26:17', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(60, '40', '25', 'Rechazado', '2018-10-16 19:27:10', '2018-11-06 23:30:18', '2018-10-16 11:27:10', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:30:18', 'sdv'),
(61, '38', '24', 'Vacante cubierta', '2018-10-16 19:30:03', '2018-10-16 19:30:03', '2018-10-16 11:30:03', NULL, NULL, NULL, '2018-10-29 15:51:27', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(62, '37', '20', 'Vacante cubierta', '2018-10-16 19:30:48', '2018-10-16 19:30:48', '2018-10-16 11:30:48', NULL, NULL, NULL, '2018-10-18 15:07:33', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(63, '28', '22', 'Vacante cubierta', '2018-10-17 00:28:54', '2018-10-17 00:28:54', '2018-10-16 16:28:54', NULL, NULL, NULL, '2018-10-18 15:08:47', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(64, '47', '40', 'Vacante cubierta', '2018-10-17 22:20:43', '2018-10-17 22:20:43', '2018-10-17 14:20:43', NULL, NULL, NULL, '2018-10-22 11:45:56', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(65, '1', '34', 'Contratado', '2018-10-17 22:40:02', '2018-10-29 22:53:04', '2018-10-17 14:40:02', NULL, '2018-10-29 14:50:43', NULL, '2018-10-29 14:52:57', 0, NULL, 'sdv', NULL, 'contratado', NULL, NULL),
(68, '28', '42', 'Vacante cubierta', '2018-10-18 22:55:29', '2018-10-18 22:55:29', '2018-10-18 14:55:29', NULL, NULL, NULL, '2018-10-24 17:21:56', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(69, '1', '42', 'Vacante cubierta', '2018-10-18 22:55:43', '2018-10-18 22:55:43', '2018-10-18 14:55:43', NULL, NULL, NULL, '2018-10-24 17:21:56', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(70, '36', '20', 'Vacante cubierta', '2018-10-18 23:08:32', '2018-10-19 00:00:05', '2018-10-18 15:08:32', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-18 16:00:05', 'ssss'),
(74, '36', '35', 'Postulado', '2018-10-19 20:04:17', '2018-10-19 20:04:17', '2018-10-19 12:04:17', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(76, '36', '34', 'Vacante cubierta', '2018-10-19 20:06:27', '2018-10-19 20:06:27', '2018-10-19 12:06:27', NULL, NULL, NULL, '2018-10-29 14:52:57', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(78, '1', '40', 'Vacante cubierta', '2018-10-20 05:05:12', '2018-10-20 05:05:12', '2018-10-19 21:05:12', NULL, NULL, NULL, '2018-10-22 11:45:56', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(79, '56', '26', 'Postulado', '2018-10-22 18:54:21', '2018-10-22 18:54:21', '2018-10-22 10:54:21', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(80, '55', '29', 'Postulado', '2018-10-22 18:56:56', '2018-10-22 18:56:56', '2018-10-22 10:56:56', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(81, '32', '42', 'Contratado', '2018-10-22 22:40:49', '2018-10-25 01:21:57', '2018-10-22 14:40:49', NULL, NULL, NULL, '2018-10-24 17:21:56', 0, NULL, NULL, NULL, 'kahnfk', NULL, NULL),
(83, '47', '34', 'Vacante cubierta', '2018-10-26 20:02:40', '2018-10-26 20:02:40', '2018-10-26 12:02:40', NULL, NULL, NULL, '2018-10-29 14:52:57', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(84, '45', '20', 'Rechazado', '2018-10-29 22:20:30', '2018-10-30 19:32:35', '2018-10-29 14:20:30', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-30 11:32:35', '1'),
(85, '53', '24', 'Contratado', '2018-10-29 23:48:17', '2018-10-29 23:51:27', '2018-10-29 15:48:17', '2018-10-29 15:50:36', '2018-10-29 15:51:02', '2018-10-29 15:51:17', '2018-10-29 15:51:27', 0, 'visto', 'entrevista', 'psico', 'ok', NULL, NULL),
(86, '34', '24', 'Vacante cubierta', '2018-10-29 23:48:26', '2018-10-29 23:48:26', '2018-10-29 15:48:26', NULL, NULL, NULL, '2018-10-29 15:51:27', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(87, '53', '34', 'Rechazado', '2018-10-30 01:54:04', '2018-10-30 19:32:44', '2018-10-29 17:54:04', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-30 11:32:44', '2'),
(88, '1', '33', 'Rechazado', '2018-10-30 19:14:12', '2018-10-30 19:32:56', '2018-10-30 11:14:12', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-30 11:32:56', '3'),
(89, '47', '30', 'Rechazado', '2018-10-30 19:26:54', '2018-10-30 19:33:05', '2018-10-30 11:26:54', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-30 11:33:05', '4'),
(90, '46', '30', 'Rechazado', '2018-10-30 19:27:52', '2018-10-30 19:33:15', '2018-10-30 11:27:52', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-30 11:33:15', '5'),
(91, '58', '30', 'Rechazado', '2018-10-30 19:29:54', '2018-10-30 19:33:27', '2018-10-30 11:29:54', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-30 11:33:27', '6'),
(92, '28', '30', 'Rechazado', '2018-10-30 19:31:22', '2018-10-30 19:33:48', '2018-10-30 11:31:22', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-30 11:33:48', '6'),
(93, '34', '30', 'Rechazado', '2018-10-30 19:31:36', '2018-10-30 19:34:01', '2018-10-30 11:31:36', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-30 11:34:00', '7'),
(94, '44', '30', 'Rechazado', '2018-10-30 19:31:54', '2018-10-30 19:34:15', '2018-10-30 11:31:54', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-10-30 11:34:15', '8'),
(95, '47', '31', 'Rechazado', '2018-10-30 19:34:28', '2018-11-06 23:28:39', '2018-10-30 11:34:28', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:28:39', '123'),
(96, '53', '31', 'Rechazado', '2018-10-30 19:34:39', '2018-11-06 23:28:31', '2018-10-30 11:34:39', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:28:31', '123'),
(97, '53', '35', 'Rechazado', '2018-10-30 19:35:51', '2018-11-01 19:54:58', '2018-10-30 11:35:51', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-01 11:54:56', 'No  paso las pruebas bla bla bla,No  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla blaNo  paso las pruebas bla bla bla'),
(98, '47', '35', 'Rechazado', '2018-10-30 19:36:05', '2018-11-01 19:32:01', '2018-10-30 11:36:05', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-01 11:32:01', 'Rechazado por falta de xp'),
(99, '42', '35', 'Rechazado', '2018-10-30 19:36:12', '2018-11-06 23:30:00', '2018-10-30 11:36:12', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:30:00', 'rgd'),
(100, '46', '35', 'Rechazado', '2018-10-30 19:37:44', '2018-11-06 23:21:10', '2018-10-30 11:37:44', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:21:10', 'no'),
(101, '1', '31', 'Rechazado', '2018-10-30 19:37:53', '2018-11-06 23:28:12', '2018-10-30 11:37:53', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:28:12', '123'),
(102, '34', '31', 'Rechazado', '2018-10-30 19:38:12', '2018-11-06 23:31:10', '2018-10-30 11:38:12', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:31:10', 'erg'),
(103, '45', '30', 'Rechazado', '2018-10-30 19:46:20', '2018-11-06 23:28:20', '2018-10-30 11:46:20', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:28:20', '123'),
(104, '45', '36', 'Rechazado', '2018-10-30 19:46:23', '2018-11-06 23:30:42', '2018-10-30 11:46:23', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:30:42', 'erg'),
(105, '53', '36', 'Rechazado', '2018-10-30 19:46:29', '2018-11-06 23:30:30', '2018-10-30 11:46:29', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:30:26', 'wgr'),
(106, '1', '43', 'Rechazado', '2018-10-30 19:47:28', '2018-11-06 23:28:54', '2018-10-30 11:47:28', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:28:54', '123'),
(107, '32', '20', 'Vacante cubierta', '2018-11-05 22:46:17', '2018-11-05 22:47:33', '2018-11-05 12:46:17', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-05 12:47:33', 'test'),
(108, '32', '23', 'Vacante cubierta', '2018-11-05 22:51:54', '2018-11-05 22:52:55', '2018-11-05 12:51:54', NULL, NULL, NULL, '2018-11-14 11:29:28', 0, NULL, NULL, NULL, NULL, '2018-11-05 12:52:55', 'test'),
(109, '60', '23', 'Vacante cubierta', '2018-11-06 19:49:43', '2018-11-06 19:49:43', '2018-11-06 09:49:43', NULL, NULL, NULL, '2018-11-14 11:29:28', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(110, '59', '25', 'Rechazado', '2018-11-06 22:24:03', '2018-11-06 23:29:50', '2018-11-06 12:24:03', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:29:50', 'no'),
(111, '44', '33', 'Rechazado', '2018-11-06 22:31:19', '2018-11-06 23:30:08', '2018-11-06 12:31:19', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:30:08', 'rgt'),
(112, '28', '23', 'Vacante cubierta', '2018-11-06 23:02:23', '2018-11-06 23:30:51', '2018-11-06 13:02:23', NULL, NULL, NULL, '2018-11-14 11:29:28', 0, NULL, NULL, NULL, NULL, '2018-11-06 13:30:51', 'erg'),
(113, '58', '46', 'Rechazado', '2018-11-06 23:16:49', '2018-11-06 23:31:01', '2018-11-06 13:16:49', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 13:31:01', 'erg'),
(114, '53', '46', 'Rechazado', '2018-11-06 23:31:48', '2018-11-07 00:11:50', '2018-11-06 13:31:48', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:11:50', 'n/a'),
(115, '1', '30', 'Rechazado', '2018-11-06 23:38:00', '2018-11-07 00:12:26', '2018-11-06 13:38:00', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:12:26', 'n/a'),
(116, '46', '46', 'Rechazado', '2018-11-06 23:42:12', '2018-11-07 00:12:41', '2018-11-06 13:42:12', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:12:41', 'n/a'),
(117, '47', '46', 'Rechazado', '2018-11-06 23:46:23', '2018-11-07 00:13:45', '2018-11-06 13:46:23', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:13:45', 'n/a'),
(118, '34', '46', 'Rechazado', '2018-11-06 23:48:36', '2018-11-07 00:13:33', '2018-11-06 13:48:36', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:13:33', 'n/a'),
(119, '45', '33', 'Rechazado', '2018-11-06 23:50:29', '2018-11-07 00:12:50', '2018-11-06 13:50:29', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:12:49', 'n/a'),
(120, '59', '46', 'Rechazado', '2018-11-06 23:52:28', '2018-11-07 00:13:53', '2018-11-06 13:52:28', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:13:53', 'n/a'),
(121, '28', '46', 'Rechazado', '2018-11-06 23:55:27', '2018-11-07 00:12:57', '2018-11-06 13:55:27', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:12:56', 'n/a'),
(122, '44', '46', 'Rechazado', '2018-11-07 00:05:34', '2018-11-07 00:13:14', '2018-11-06 14:05:34', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:13:14', 'n/a'),
(123, '58', '40', 'Rechazado', '2018-11-07 00:05:50', '2018-11-07 00:13:26', '2018-11-06 14:05:50', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:13:25', 'n/a'),
(124, '53', '40', 'Rechazado', '2018-11-07 00:11:57', '2018-11-07 00:14:04', '2018-11-06 14:11:57', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:14:04', 'n/a'),
(125, '1', '46', 'Rechazado', '2018-11-07 00:14:17', '2018-11-07 00:24:09', '2018-11-06 14:14:17', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:24:09', 'asd'),
(126, '45', '46', 'Rechazado', '2018-11-07 00:14:54', '2018-11-07 00:24:18', '2018-11-06 14:14:54', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-06 14:24:18', 'asd'),
(127, '46', '46', 'Rechazado', '2018-11-07 00:28:16', '2018-11-23 21:15:18', '2018-11-06 14:28:16', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-23 17:15:17', '-'),
(128, '44', '46', 'Rechazado', '2018-11-07 00:34:23', '2018-11-23 21:15:32', '2018-11-06 14:34:23', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-23 17:15:31', '-'),
(129, '1', '46', 'Rechazado', '2018-11-07 00:37:45', '2018-11-23 21:15:40', '2018-11-06 14:37:45', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-23 17:15:40', '-'),
(130, '47', '46', 'Rechazado', '2018-11-07 00:38:26', '2018-11-23 21:15:50', '2018-11-06 14:38:26', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-23 17:15:50', '-'),
(131, '47', '46', 'Rechazado', '2018-11-07 00:38:29', '2018-11-14 20:41:14', '2018-11-06 14:38:29', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-14 10:41:14', 'hu'),
(132, '53', '46', 'Rechazado', '2018-11-07 00:40:07', '2018-11-23 21:15:58', '2018-11-06 14:40:07', '2018-11-14 10:41:38', NULL, NULL, NULL, 0, 'gh', NULL, NULL, NULL, '2018-11-23 17:15:58', '-'),
(133, '53', '46', 'Rechazado', '2018-11-07 00:40:08', '2018-11-14 20:40:57', '2018-11-06 14:40:08', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-14 10:40:57', 'gh'),
(134, '28', '46', 'Postulado', '2018-11-07 00:43:20', '2018-11-07 00:43:20', '2018-11-06 14:43:20', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(135, '59', '46', 'Rechazado', '2018-11-07 00:45:04', '2018-11-14 21:14:56', '2018-11-06 14:45:04', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-14 11:14:56', 'bh'),
(136, '34', '46', 'Postulado', '2018-11-07 00:47:50', '2018-11-07 00:47:50', '2018-11-06 14:47:50', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(137, '58', '46', 'Postulado', '2018-11-07 00:52:44', '2018-11-07 00:52:44', '2018-11-06 14:52:44', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(138, '32', '47', 'Vacante cubierta', '2018-11-14 03:49:32', '2018-11-14 03:50:31', '2018-11-13 17:49:32', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-13 17:50:31', 'test'),
(139, '62', '35', 'Postulado', '2018-11-14 04:00:28', '2018-11-14 04:00:28', '2018-11-13 18:00:28', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(140, '59', '22', 'Vacante cubierta', '2018-11-14 21:24:29', '2018-11-14 21:25:44', '2018-11-14 11:24:29', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-14 11:25:44', 'felicidades'),
(141, '45', '23', 'Contratado', '2018-11-14 21:28:53', '2018-11-14 21:29:33', '2018-11-14 11:28:53', '2018-11-14 11:29:16', '2018-11-14 11:29:22', '2018-11-14 11:29:25', '2018-11-14 11:29:28', 0, 'ok', 'ok', 'ok', 'ok', NULL, NULL),
(142, '59', '49', 'Contratado', '2018-11-14 21:33:57', '2018-11-14 21:48:27', '2018-11-14 11:33:57', NULL, NULL, NULL, '2018-11-14 11:48:27', 0, NULL, NULL, NULL, 'cvcx', NULL, NULL),
(143, '59', '49', 'Postulado', '2018-11-14 21:48:38', '2018-11-14 21:48:38', '2018-11-14 11:48:38', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(144, '45', '48', 'Vacante cubierta', '2018-11-15 01:25:37', '2018-11-15 01:25:37', '2018-11-14 15:25:37', NULL, NULL, NULL, '2018-11-14 19:01:23', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(145, '61', '48', 'Contratado', '2018-11-15 04:58:57', '2018-11-15 05:01:23', '2018-11-14 18:58:57', NULL, NULL, NULL, '2018-11-14 19:01:23', 0, NULL, NULL, NULL, 'test', NULL, NULL),
(146, '32', '47', 'Postulado', '2018-11-21 00:49:53', '2018-11-21 00:49:53', '2018-11-20 14:49:53', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(147, '57', '37', 'Postulado', '2018-11-21 22:56:15', '2018-11-21 22:56:15', '2018-11-21 12:56:15', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(148, '57', '37', 'Postulado', '2018-11-21 22:56:16', '2018-11-21 22:56:16', '2018-11-21 12:56:16', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(149, '41', '43', 'Postulado', '2018-11-23 21:01:56', '2018-11-23 21:01:56', '2018-11-23 11:01:56', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(150, '38', '46', 'Postulado', '2018-11-23 21:02:24', '2018-11-23 21:02:24', '2018-11-23 11:02:24', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(151, '37', '47', 'Postulado', '2018-11-23 22:54:34', '2018-11-23 22:54:34', '2018-11-23 12:54:34', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(152, '45', '40', 'Rechazado', '2018-11-24 00:03:07', '2018-11-23 18:09:23', '2018-11-23 14:03:07', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-11-23 14:09:23', '-'),
(153, '59', '46', 'Postulado', '2018-11-23 20:35:52', '2018-11-23 20:35:52', '2018-11-23 16:35:52', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(154, '53', '47', 'Postulado', '2018-11-23 21:31:40', '2018-11-23 21:31:40', '2018-11-23 17:31:40', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(155, '53', '47', 'Postulado', '2018-11-23 21:57:21', '2018-11-23 21:57:21', '2018-11-23 17:57:21', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(156, '39', '47', 'Postulado', '2018-11-27 15:08:54', '2018-11-27 15:08:54', '2018-11-27 11:08:54', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(157, '39', '47', 'Postulado', '2018-11-27 15:08:57', '2018-11-27 15:08:57', '2018-11-27 11:08:57', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(158, '39', '47', 'Postulado', '2018-11-27 15:08:58', '2018-11-27 15:08:58', '2018-11-27 11:08:58', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(159, '48', '43', 'Postulado', '2018-11-27 15:09:08', '2018-11-27 15:09:08', '2018-11-27 11:09:08', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(160, '48', '43', 'Postulado', '2018-11-27 15:09:30', '2018-11-27 15:09:30', '2018-11-27 11:09:30', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(161, '40', '35', 'Postulado', '2018-11-27 15:40:05', '2018-11-27 15:40:05', '2018-11-27 11:40:05', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `icon_url`, `created_at`, `updated_at`) VALUES
(31, 'Almacén/Logística', 'https://porvenirapps.com/prohunter/storage/app/category_icons/WIirGSjcgUsrtDGa1HwzGpbmSSF7pRCd6kDi7quK.png', '2018-09-27 22:08:20', '2018-10-18 22:05:03'),
(33, 'Administración/Oficina', 'https://porvenirapps.com/prohunter/storage/app/category_icons/AEU9wIsn81m21KrSLW0NfnAl04s16CbQ4Cv4N7Rr.jpeg', '2018-09-28 00:35:26', '2018-09-28 00:35:26'),
(34, 'Atención a Clientes', 'https://porvenirapps.com/prohunter/storage/app/category_icons/EGyvMqZCcYttTcH9cI1PdMCqhWPc3cWWFgeClPwh.jpeg', '2018-09-28 00:35:45', '2018-09-28 00:35:45'),
(35, 'Compras/Comercio Exterior', 'https://porvenirapps.com/prohunter/storage/app/category_icons/yb9X0LUpqW1lChJOFFe4U9L5e4IZa0zPTdIL0poA.jpeg', '2018-09-28 00:36:05', '2018-09-28 00:36:05'),
(36, 'Construcción/Obra', 'https://porvenirapps.com/prohunter/storage/app/category_icons/ATUv9vFnTBRDPQdhTEz2q3PV0RDVWFRZ6O3GUoZT.jpeg', '2018-09-28 00:36:36', '2018-09-28 00:36:36'),
(37, 'Contabilidad/Finanzas', 'https://porvenirapps.com/prohunter/storage/app/category_icons/VIlNcgJfYWNhROd1dMp8mWeZrVJ8dthM6N0N3vGZ.jpeg', '2018-09-28 00:36:57', '2018-09-28 00:36:57'),
(38, 'Dirección/Gerencia', 'https://porvenirapps.com/prohunter/storage/app/category_icons/JViUZH1mOFzRjh4TV1WfdVdogBsFGUdndz59Ga6a.jpeg', '2018-09-28 00:37:18', '2018-09-28 00:37:18'),
(39, 'Hostelería/Turismo', 'https://porvenirapps.com/prohunter/storage/app/category_icons/fPjfLfqmSzUKXbEVIFnPmajhyEQBNEaHiyt7oPLH.jpeg', '2018-09-28 00:37:50', '2018-09-28 00:37:50'),
(40, 'Informática/Telecomunicaciones', 'https://porvenirapps.com/prohunter/storage/app/category_icons/jZXIhX7KW2L6pB2l7TNxUgztZMznDULVwRSYcYqt.jpeg', '2018-09-28 00:38:07', '2018-09-28 00:38:07'),
(41, 'Ingeniería', 'https://porvenirapps.com/prohunter/storage/app/category_icons/rnTkLtSSiOdwAum99kGpKmvKRJRWCQeaTF05ZQWX.jpeg', '2018-09-28 00:38:21', '2018-09-28 00:38:21'),
(42, 'Investigación y Calidad', 'https://porvenirapps.com/prohunter/storage/app/category_icons/9lon30tci68L6hw1TC6S17A7yYz1XtVk2uA6BP8u.jpeg', '2018-09-28 00:38:33', '2018-09-28 00:38:33'),
(43, 'Legal/Asesoría', 'https://porvenirapps.com/prohunter/storage/app/category_icons/3WKAnaJNdB5U7xhBh3ITNwYaIfYwr0YRxohYy6kz.jpeg', '2018-09-28 00:38:49', '2018-09-28 00:38:49'),
(44, 'Mantenimiento y Reparaciones Técnicas', 'https://porvenirapps.com/prohunter/storage/app/category_icons/TnwXLIdlkfuVgo2y8FfJVzwO8shrCHbRBdPGOgp6.jpeg', '2018-09-28 00:39:02', '2018-09-28 00:39:02'),
(45, 'Medicina/Salud', 'https://porvenirapps.com/prohunter/storage/app/category_icons/eclbhDMNgfNCQy1mnVTm8sNWgJTlyy0IgrwFDr33.jpeg', '2018-09-28 00:39:24', '2018-09-28 00:39:24'),
(46, 'Producción/Operarios/Manufactura', 'https://porvenirapps.com/prohunter/storage/app/category_icons/IvCGOzMIxZRLEvojdCG0TTFOa8L5bWZLEwP7jfkn.jpeg', '2018-09-28 00:41:34', '2018-09-28 00:41:34'),
(47, 'Recursos Humanos', 'https://porvenirapps.com/prohunter/storage/app/category_icons/LGOQJ0DB7DWnAneqm8COqd8cKpK6DiXpQpon8eGX.jpeg', '2018-09-28 00:41:47', '2018-09-28 00:41:47'),
(48, 'Servicios Generales, Aseo y Seguridad', 'https://porvenirapps.com/prohunter/storage/app/category_icons/Ovsxgz5oO6QwZf7CzOSLOgPfZPxConQ9EMcY0js7.jpeg', '2018-09-28 00:42:08', '2018-09-28 00:42:08'),
(49, 'Transporte', 'https://porvenirapps.com/prohunter/storage/app/category_icons/vwIVbhUJbRzlPO531o4aBPC94TqHIcYJWvzG0qGA.jpeg', '2018-09-28 00:42:17', '2018-09-28 00:42:17'),
(50, 'Ventas', 'https://porvenirapps.com/prohunter/storage/app/category_icons/d1201AS8WFg6ugv7DWGbF7dowNwHvadlTF34XhgO.jpeg', '2018-09-28 00:42:27', '2018-09-28 00:42:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category_preference`
--

DROP TABLE IF EXISTS `category_preference`;
CREATE TABLE `category_preference` (
  `id` int(10) UNSIGNED NOT NULL,
  `preference_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `category_preference`
--

INSERT INTO `category_preference` (`id`, `preference_id`, `category_id`, `created_at`, `updated_at`) VALUES
(125, '15', '41', '2018-09-28 19:53:48', '2018-09-28 19:53:48'),
(126, '15', '44', '2018-09-28 19:53:48', '2018-09-28 19:53:48'),
(127, '3', '33', '2018-09-28 19:56:33', '2018-09-28 19:56:33'),
(231, '12', '31', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(232, '12', '33', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(233, '12', '34', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(234, '12', '35', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(235, '12', '36', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(236, '12', '37', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(237, '12', '38', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(238, '12', '39', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(239, '12', '40', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(240, '12', '41', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(241, '12', '42', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(242, '12', '43', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(243, '12', '44', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(244, '12', '45', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(245, '12', '46', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(246, '12', '47', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(247, '12', '48', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(248, '12', '49', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(249, '12', '50', '2018-09-29 00:39:23', '2018-09-29 00:39:23'),
(250, '11', '34', '2018-09-29 00:42:56', '2018-09-29 00:42:56'),
(270, '16', '31', '2018-10-02 02:03:31', '2018-10-02 02:03:31'),
(335, '8', '31', '2018-10-02 19:36:26', '2018-10-02 19:36:26'),
(336, '8', '33', '2018-10-02 19:36:26', '2018-10-02 19:36:26'),
(355, '18', '31', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(356, '18', '33', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(357, '18', '34', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(358, '18', '35', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(359, '18', '36', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(360, '18', '37', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(361, '18', '38', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(362, '18', '39', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(363, '18', '40', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(364, '18', '41', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(365, '18', '42', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(366, '18', '43', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(367, '18', '44', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(368, '18', '45', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(369, '18', '46', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(370, '18', '47', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(371, '18', '48', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(372, '18', '49', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(373, '18', '50', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(374, '19', '31', '2018-10-02 23:19:24', '2018-10-02 23:19:24'),
(375, '19', '33', '2018-10-02 23:19:24', '2018-10-02 23:19:24'),
(475, '17', '31', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(476, '17', '33', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(477, '17', '34', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(478, '17', '35', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(479, '17', '36', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(480, '17', '37', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(481, '17', '38', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(482, '17', '39', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(483, '17', '40', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(484, '17', '41', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(485, '17', '42', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(486, '17', '43', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(487, '17', '44', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(488, '17', '45', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(489, '17', '46', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(490, '17', '47', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(491, '17', '48', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(492, '17', '49', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(493, '17', '50', '2018-10-03 22:09:13', '2018-10-03 22:09:13'),
(526, '21', '34', '2018-10-04 01:13:29', '2018-10-04 01:13:29'),
(527, '22', '31', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(528, '22', '33', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(529, '22', '34', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(530, '22', '35', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(531, '22', '36', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(532, '22', '37', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(533, '22', '38', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(534, '22', '39', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(535, '22', '40', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(536, '22', '41', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(537, '22', '42', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(538, '22', '43', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(539, '22', '44', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(540, '22', '45', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(541, '22', '46', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(542, '22', '47', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(543, '22', '48', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(544, '22', '49', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(545, '22', '50', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(573, '24', '31', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(574, '24', '33', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(575, '24', '34', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(576, '24', '35', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(577, '24', '36', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(578, '24', '37', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(579, '24', '38', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(580, '24', '39', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(581, '24', '40', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(582, '24', '41', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(583, '24', '42', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(584, '24', '43', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(585, '24', '44', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(586, '24', '45', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(587, '24', '46', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(588, '24', '47', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(589, '24', '48', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(590, '24', '49', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(591, '24', '50', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(597, '25', '33', '2018-10-11 04:25:27', '2018-10-11 04:25:27'),
(598, '25', '37', '2018-10-11 04:25:27', '2018-10-11 04:25:27'),
(599, '25', '40', '2018-10-11 04:25:27', '2018-10-11 04:25:27'),
(600, '25', '41', '2018-10-11 04:25:27', '2018-10-11 04:25:27'),
(601, '25', '43', '2018-10-11 04:25:27', '2018-10-11 04:25:27'),
(602, '25', '47', '2018-10-11 04:25:27', '2018-10-11 04:25:27'),
(603, '25', '50', '2018-10-11 04:25:27', '2018-10-11 04:25:27'),
(642, '26', '40', '2018-10-15 20:13:57', '2018-10-15 20:13:57'),
(643, '26', '41', '2018-10-15 20:13:57', '2018-10-15 20:13:57'),
(889, '27', '40', '2018-10-17 17:39:44', '2018-10-17 17:39:44'),
(890, '27', '44', '2018-10-17 17:39:44', '2018-10-17 17:39:44'),
(910, '28', '37', '2018-10-17 19:47:12', '2018-10-17 19:47:12'),
(997, '29', '46', '2018-10-22 18:02:14', '2018-10-22 18:02:14'),
(1001, '14', '34', '2018-10-22 20:48:53', '2018-10-22 20:48:53'),
(1003, '30', '31', '2018-10-22 23:08:27', '2018-10-22 23:08:27'),
(1007, '32', '36', '2018-10-25 22:36:45', '2018-10-25 22:36:45'),
(1022, '33', '31', '2018-10-29 23:05:45', '2018-10-29 23:05:45'),
(1023, '34', '31', '2018-10-29 23:09:14', '2018-10-29 23:09:14'),
(1024, '34', '36', '2018-10-29 23:09:14', '2018-10-29 23:09:14'),
(1025, '34', '37', '2018-10-29 23:09:14', '2018-10-29 23:09:14'),
(1032, '35', '31', '2018-10-29 23:35:23', '2018-10-29 23:35:23'),
(1155, '36', '31', '2018-10-30 23:05:49', '2018-10-30 23:05:49'),
(1156, '36', '33', '2018-10-30 23:05:49', '2018-10-30 23:05:49'),
(1157, '36', '34', '2018-10-30 23:05:49', '2018-10-30 23:05:49'),
(1158, '36', '35', '2018-10-30 23:05:49', '2018-10-30 23:05:49'),
(1159, '36', '36', '2018-10-30 23:05:49', '2018-10-30 23:05:49'),
(1160, '36', '37', '2018-10-30 23:05:49', '2018-10-30 23:05:49'),
(1423, '7', '31', '2018-10-31 22:44:04', '2018-10-31 22:44:04'),
(1424, '7', '33', '2018-10-31 22:44:04', '2018-10-31 22:44:04'),
(1425, '7', '34', '2018-10-31 22:44:04', '2018-10-31 22:44:04'),
(1426, '7', '35', '2018-10-31 22:44:04', '2018-10-31 22:44:04'),
(1427, '7', '36', '2018-10-31 22:44:04', '2018-10-31 22:44:04'),
(1428, '7', '37', '2018-10-31 22:44:04', '2018-10-31 22:44:04'),
(1429, '7', '38', '2018-10-31 22:44:04', '2018-10-31 22:44:04'),
(1430, '7', '39', '2018-10-31 22:44:04', '2018-10-31 22:44:04'),
(1431, '7', '40', '2018-10-31 22:44:04', '2018-10-31 22:44:04'),
(1471, '37', '41', '2018-11-02 00:29:08', '2018-11-02 00:29:08'),
(1587, '38', '31', '2018-11-05 20:55:15', '2018-11-05 20:55:15'),
(1594, '39', '31', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1595, '39', '33', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1596, '39', '34', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1597, '39', '35', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1598, '39', '36', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1599, '39', '37', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1600, '39', '38', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1601, '39', '39', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1602, '39', '40', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1603, '39', '41', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1604, '39', '42', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1605, '39', '43', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1606, '39', '44', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1607, '39', '45', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1608, '39', '46', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1609, '39', '47', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1610, '39', '48', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1611, '39', '49', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1612, '39', '50', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(1639, '31', '40', '2018-11-14 03:28:51', '2018-11-14 03:28:51'),
(1640, '31', '44', '2018-11-14 03:28:51', '2018-11-14 03:28:51'),
(1641, '5', '31', '2018-11-14 21:41:03', '2018-11-14 21:41:03'),
(1642, '5', '36', '2018-11-14 21:41:03', '2018-11-14 21:41:03'),
(1643, '5', '39', '2018-11-14 21:41:03', '2018-11-14 21:41:03'),
(1644, '40', '36', '2018-11-16 03:26:09', '2018-11-16 03:26:09'),
(1645, '41', '31', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1646, '41', '33', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1647, '41', '34', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1648, '41', '35', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1649, '41', '36', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1650, '41', '37', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1651, '41', '38', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1652, '41', '39', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1653, '41', '40', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1654, '41', '41', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1655, '41', '42', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1656, '41', '43', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1657, '41', '44', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1658, '41', '45', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1659, '41', '46', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1660, '41', '47', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1661, '41', '48', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1662, '41', '49', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1663, '41', '50', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(1683, '42', '31', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1684, '42', '33', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1685, '42', '34', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1686, '42', '35', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1687, '42', '36', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1688, '42', '37', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1689, '42', '38', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1690, '42', '39', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1691, '42', '40', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1692, '42', '41', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1693, '42', '42', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1694, '42', '43', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1695, '42', '44', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1696, '42', '45', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1697, '42', '46', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1698, '42', '47', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1699, '42', '48', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1700, '42', '49', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1701, '42', '50', '2018-11-20 23:23:10', '2018-11-20 23:23:10'),
(1705, '20', '31', '2018-11-21 23:01:54', '2018-11-21 23:01:54'),
(1706, '20', '36', '2018-11-21 23:01:54', '2018-11-21 23:01:54'),
(1707, '20', '39', '2018-11-21 23:01:54', '2018-11-21 23:01:54'),
(1760, '2', '31', '2018-11-26 16:46:30', '2018-11-26 16:46:30'),
(1761, '2', '33', '2018-11-26 16:46:30', '2018-11-26 16:46:30'),
(1762, '2', '35', '2018-11-26 16:46:30', '2018-11-26 16:46:30'),
(1763, '2', '36', '2018-11-26 16:46:30', '2018-11-26 16:46:30'),
(1764, '2', '37', '2018-11-26 16:46:30', '2018-11-26 16:46:30'),
(1765, '2', '48', '2018-11-26 16:46:30', '2018-11-26 16:46:30'),
(1785, '23', '31', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1786, '23', '33', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1787, '23', '34', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1788, '23', '35', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1789, '23', '36', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1790, '23', '37', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1791, '23', '38', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1792, '23', '39', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1793, '23', '40', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1794, '23', '41', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1795, '23', '42', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1796, '23', '43', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1797, '23', '44', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1798, '23', '45', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1799, '23', '46', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1800, '23', '47', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1801, '23', '48', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1802, '23', '49', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1803, '23', '50', '2018-11-26 18:02:36', '2018-11-26 18:02:36'),
(1805, '6', '31', '2018-12-26 04:58:41', '2018-12-26 04:58:41'),
(1806, '6', '34', '2018-12-26 04:58:41', '2018-12-26 04:58:41'),
(1807, '6', '36', '2018-12-26 04:58:41', '2018-12-26 04:58:41'),
(1808, '6', '38', '2018-12-26 04:58:41', '2018-12-26 04:58:41'),
(1809, '6', '39', '2018-12-26 04:58:41', '2018-12-26 04:58:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`id`, `name`, `state_id`, `number`, `created_at`, `updated_at`) VALUES
(2, 'Asientos', 1, 2, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(3, 'Calvillo', 1, 3, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(4, 'Cosío', 1, 4, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(5, 'Jesús María', 1, 5, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(6, 'Pabellón de Arteaga', 1, 6, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(7, 'Rincón de Romos', 1, 7, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(8, 'San José de Gracia', 1, 8, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(9, 'Tepezalá', 1, 9, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(10, 'El Llano', 1, 10, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(11, 'San Francisco de los Romo', 1, 11, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(12, 'Ensenada', 2, 1, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(13, 'Mexicali', 2, 2, '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(14, 'Tecate', 2, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(15, 'Tijuana', 2, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(16, 'Playas de Rosarito', 2, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(17, 'Comondú', 3, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(18, 'Mulegé', 3, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(19, 'La Paz', 3, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(20, 'Los Cabos', 3, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(21, 'Loreto', 3, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(22, 'Calkiní', 4, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(23, 'Campeche', 4, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(24, 'Carmen', 4, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(25, 'Champotón', 4, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(26, 'Hecelchakán', 4, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(27, 'Hopelchén', 4, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(28, 'Palizada', 4, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(29, 'Tenabo', 4, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(30, 'Escárcega', 4, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(31, 'Calakmul', 4, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(32, 'Candelaria', 4, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(33, 'Abasolo', 5, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(34, 'Acuña', 5, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(35, 'Allende', 5, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(36, 'Arteaga', 5, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(37, 'Candela', 5, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(38, 'Castaños', 5, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(39, 'Cuatro Ciénegas', 5, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(40, 'Escobedo', 5, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(41, 'Francisco I. Madero', 5, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(42, 'Frontera', 5, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(43, 'General Cepeda', 5, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(44, 'Guerrero', 5, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(45, 'Hidalgo', 5, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(46, 'Jiménez', 5, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(47, 'Juárez', 5, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(48, 'Lamadrid', 5, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(49, 'Matamoros', 5, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(51, 'Morelos', 5, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(52, 'Múzquiz', 5, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(53, 'Nadadores', 5, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(54, 'Nava', 5, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(55, 'Ocampo', 5, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(56, 'Parras', 5, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(57, 'Piedras Negras', 5, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(58, 'Progreso', 5, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(59, 'Ramos Arizpe', 5, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(60, 'Sabinas', 5, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(61, 'Sacramento', 5, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(62, 'Saltillo', 5, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(63, 'San Buenaventura', 5, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(64, 'San Juan de Sabinas', 5, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(65, 'San Pedro', 5, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(66, 'Sierra Mojada', 5, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(67, 'Torreón', 5, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(68, 'Viesca', 5, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(69, 'Villa Unión', 5, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(70, 'Zaragoza', 5, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(71, 'Armería', 6, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(72, 'Colima', 6, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(73, 'Comala', 6, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(74, 'Coquimatlán', 6, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(75, 'Cuauhtémoc', 6, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(76, 'Ixtlahuacán', 6, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(77, 'Manzanillo', 6, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(78, 'Minatitlán', 6, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(79, 'Tecomán', 6, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(80, 'Villa de Álvarez', 6, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(81, 'Acacoyagua', 7, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(82, 'Acala', 7, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(83, 'Acapetahua', 7, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(84, 'Altamirano', 7, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(85, 'Amatán', 7, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(86, 'Amatenango de la Frontera', 7, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(87, 'Amatenango del Valle', 7, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(88, 'Angel Albino Corzo', 7, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(89, 'Arriaga', 7, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(90, 'Bejucal de Ocampo', 7, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(91, 'Bella Vista', 7, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(92, 'Berriozábal', 7, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(93, 'Bochil', 7, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(94, 'El Bosque', 7, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(95, 'Cacahoatán', 7, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(96, 'Catazajá', 7, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(97, 'Cintalapa', 7, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(98, 'Coapilla', 7, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(99, 'Comitán de Domínguez', 7, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(100, 'La Concordia', 7, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(101, 'Copainalá', 7, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(102, 'Chalchihuitán', 7, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(103, 'Chamula', 7, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(104, 'Chanal', 7, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(105, 'Chapultenango', 7, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(106, 'Chenalhó', 7, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(107, 'Chiapa de Corzo', 7, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(108, 'Chiapilla', 7, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(109, 'Chicoasén', 7, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(110, 'Chicomuselo', 7, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(111, 'Chilón', 7, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(112, 'Escuintla', 7, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(113, 'Francisco León', 7, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(114, 'Frontera Comalapa', 7, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(115, 'Frontera Hidalgo', 7, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(116, 'La Grandeza', 7, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(117, 'Huehuetán', 7, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(118, 'Huixtán', 7, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(119, 'Huitiupán', 7, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(120, 'Huixtla', 7, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(121, 'La Independencia', 7, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(122, 'Ixhuatán', 7, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(123, 'Ixtacomitán', 7, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(124, 'Ixtapa', 7, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(125, 'Ixtapangajoya', 7, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(126, 'Jiquipilas', 7, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(127, 'Jitotol', 7, 47, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(128, 'Juárez', 7, 48, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(129, 'Larráinzar', 7, 49, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(130, 'La Libertad', 7, 50, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(131, 'Mapastepec', 7, 51, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(132, 'Las Margaritas', 7, 52, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(133, 'Mazapa de Madero', 7, 53, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(134, 'Mazatán', 7, 54, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(135, 'Metapa', 7, 55, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(136, 'Mitontic', 7, 56, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(137, 'Motozintla', 7, 57, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(138, 'Nicolás Ruíz', 7, 58, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(139, 'Ocosingo', 7, 59, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(140, 'Ocotepec', 7, 60, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(141, 'Ocozocoautla de Espinosa', 7, 61, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(142, 'Ostuacán', 7, 62, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(143, 'Osumacinta', 7, 63, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(144, 'Oxchuc', 7, 64, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(145, 'Palenque', 7, 65, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(146, 'Pantelhó', 7, 66, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(147, 'Pantepec', 7, 67, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(148, 'Pichucalco', 7, 68, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(149, 'Pijijiapan', 7, 69, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(150, 'El Porvenir', 7, 70, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(151, 'Villa Comaltitlán', 7, 71, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(152, 'Pueblo Nuevo Solistahuacán', 7, 72, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(153, 'Rayón', 7, 73, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(154, 'Reforma', 7, 74, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(155, 'Las Rosas', 7, 75, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(156, 'Sabanilla', 7, 76, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(157, 'Salto de Agua', 7, 77, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(158, 'San Cristóbal de las Casas', 7, 78, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(159, 'San Fernando', 7, 79, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(160, 'Siltepec', 7, 80, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(161, 'Simojovel', 7, 81, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(162, 'Sitalá', 7, 82, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(163, 'Socoltenango', 7, 83, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(164, 'Solosuchiapa', 7, 84, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(165, 'Soyaló', 7, 85, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(166, 'Suchiapa', 7, 86, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(167, 'Suchiate', 7, 87, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(168, 'Sunuapa', 7, 88, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(169, 'Tapachula', 7, 89, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(170, 'Tapalapa', 7, 90, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(171, 'Tapilula', 7, 91, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(172, 'Tecpatán', 7, 92, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(173, 'Tenejapa', 7, 93, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(174, 'Teopisca', 7, 94, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(175, 'Tila', 7, 96, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(176, 'Tonalá', 7, 97, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(177, 'Totolapa', 7, 98, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(178, 'La Trinitaria', 7, 99, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(179, 'Tumbalá', 7, 100, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(180, 'Tuxtla Gutiérrez', 7, 101, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(181, 'Tuxtla Chico', 7, 102, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(182, 'Tuzantán', 7, 103, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(183, 'Tzimol', 7, 104, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(184, 'Unión Juárez', 7, 105, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(185, 'Venustiano Carranza', 7, 106, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(186, 'Villa Corzo', 7, 107, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(187, 'Villaflores', 7, 108, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(188, 'Yajalón', 7, 109, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(189, 'San Lucas', 7, 110, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(190, 'Zinacantán', 7, 111, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(191, 'San Juan Cancuc', 7, 112, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(192, 'Aldama', 7, 113, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(193, 'Benemérito de las Américas', 7, 114, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(194, 'Maravilla Tenejapa', 7, 115, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(195, 'Marqués de Comillas', 7, 116, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(196, 'Montecristo de Guerrero', 7, 117, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(197, 'San Andrés Duraznal', 7, 118, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(198, 'Santiago el Pinar', 7, 119, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(199, 'Ahumada', 8, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(200, 'Aldama', 8, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(201, 'Allende', 8, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(202, 'Aquiles Serdán', 8, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(203, 'Ascensión', 8, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(204, 'Bachíniva', 8, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(205, 'Balleza', 8, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(206, 'Batopilas', 8, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(207, 'Bocoyna', 8, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(208, 'Buenaventura', 8, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(209, 'Camargo', 8, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(210, 'Carichí', 8, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(211, 'Casas Grandes', 8, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(212, 'Coronado', 8, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(213, 'Coyame del Sotol', 8, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(214, 'La Cruz', 8, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(215, 'Cuauhtémoc', 8, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(216, 'Cusihuiriachi', 8, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(217, 'Chihuahua', 8, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(218, 'Chínipas', 8, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(219, 'Delicias', 8, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(220, 'Dr. Belisario Domínguez', 8, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(221, 'Galeana', 8, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(222, 'Santa Isabel', 8, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(223, 'Gómez Farías', 8, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(224, 'Gran Morelos', 8, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(225, 'Guachochi', 8, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(226, 'Guadalupe', 8, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(227, 'Guadalupe y Calvo', 8, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(228, 'Guazapares', 8, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(229, 'Guerrero', 8, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(230, 'Hidalgo del Parral', 8, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(231, 'Huejotitán', 8, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(232, 'Ignacio Zaragoza', 8, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(233, 'Janos', 8, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(234, 'Jiménez', 8, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(235, 'Juárez', 8, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(236, 'Julimes', 8, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(237, 'López', 8, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(238, 'Madera', 8, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(239, 'Maguarichi', 8, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(240, 'Manuel Benavides', 8, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(241, 'Matachí', 8, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(242, 'Matamoros', 8, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(243, 'Meoqui', 8, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(244, 'Morelos', 8, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(245, 'Moris', 8, 47, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(246, 'Namiquipa', 8, 48, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(247, 'Nonoava', 8, 49, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(248, 'Nuevo Casas Grandes', 8, 50, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(249, 'Ocampo', 8, 51, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(250, 'Ojinaga', 8, 52, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(251, 'Praxedis G. Guerrero', 8, 53, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(252, 'Riva Palacio', 8, 54, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(253, 'Rosales', 8, 55, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(254, 'Rosario', 8, 56, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(255, 'San Francisco de Borja', 8, 57, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(256, 'San Francisco de Conchos', 8, 58, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(257, 'San Francisco del Oro', 8, 59, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(258, 'Santa Bárbara', 8, 60, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(259, 'Satevó', 8, 61, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(260, 'Saucillo', 8, 62, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(261, 'Temósachic', 8, 63, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(262, 'El Tule', 8, 64, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(263, 'Urique', 8, 65, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(264, 'Uruachi', 8, 66, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(265, 'Valle de Zaragoza', 8, 67, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(266, 'Azcapotzalco', 9, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(267, 'Coyoacán', 9, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(268, 'Cuajimalpa de Morelos', 9, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(269, 'Gustavo A. Madero', 9, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(270, 'Iztacalco', 9, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(271, 'Iztapalapa', 9, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(272, 'La Magdalena Contreras', 9, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(273, 'Milpa Alta', 9, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(274, 'Álvaro Obregón', 9, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(275, 'Tláhuac', 9, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(276, 'Tlalpan', 9, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(277, 'Xochimilco', 9, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(278, 'Benito Juárez', 9, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(279, 'Cuauhtémoc', 9, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(280, 'Miguel Hidalgo', 9, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(281, 'Venustiano Carranza', 9, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(282, 'Canatlán', 10, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(283, 'Canelas', 10, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(284, 'Coneto de Comonfort', 10, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(285, 'Cuencamé', 10, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(286, 'Durango', 10, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(287, 'General Simón Bolívar', 10, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(288, 'Gómez Palacio', 10, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(289, 'Guadalupe Victoria', 10, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(290, 'Guanaceví', 10, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(291, 'Hidalgo', 10, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(292, 'Indé', 10, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(293, 'Lerdo', 10, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(294, 'Mapimí', 10, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(295, 'Mezquital', 10, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(296, 'Nazas', 10, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(297, 'Nombre de Dios', 10, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(298, 'Ocampo', 10, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(299, 'El Oro', 10, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(300, 'Otáez', 10, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(301, 'Pánuco de Coronado', 10, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(302, 'Peñón Blanco', 10, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(303, 'Poanas', 10, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(304, 'Pueblo Nuevo', 10, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(305, 'Rodeo', 10, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(306, 'San Bernardo', 10, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(307, 'San Dimas', 10, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(308, 'San Juan de Guadalupe', 10, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(309, 'San Juan del Río', 10, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(310, 'San Luis del Cordero', 10, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(311, 'San Pedro del Gallo', 10, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(312, 'Santa Clara', 10, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(313, 'Santiago Papasquiaro', 10, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(314, 'Súchil', 10, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(315, 'Tamazula', 10, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(316, 'Tepehuanes', 10, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(317, 'Tlahualilo', 10, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(318, 'Topia', 10, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(319, 'Vicente Guerrero', 10, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(320, 'Nuevo Ideal', 10, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(321, 'Abasolo', 11, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(322, 'Acámbaro', 11, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(323, 'San Miguel de Allende', 11, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(324, 'Apaseo el Alto', 11, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(325, 'Apaseo el Grande', 11, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(326, 'Atarjea', 11, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(327, 'Celaya', 11, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(328, 'Manuel Doblado', 11, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(329, 'Comonfort', 11, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(330, 'Coroneo', 11, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(331, 'Cortazar', 11, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(332, 'Cuerámaro', 11, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(333, 'Doctor Mora', 11, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(334, 'Dolores Hidalgo Cuna de la Independencia Nacional', 11, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(335, 'Guanajuato', 11, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(336, 'Huanímaro', 11, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(337, 'Irapuato', 11, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(338, 'Jaral del Progreso', 11, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(339, 'Jerécuaro', 11, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(340, 'León', 11, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(341, 'Moroleón', 11, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(342, 'Ocampo', 11, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(343, 'Pénjamo', 11, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(344, 'Pueblo Nuevo', 11, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(345, 'Purísima del Rincón', 11, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(346, 'Romita', 11, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(347, 'Salamanca', 11, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(348, 'Salvatierra', 11, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(349, 'San Diego de la Unión', 11, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(350, 'San Felipe', 11, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(351, 'San Francisco del Rincón', 11, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(352, 'San José Iturbide', 11, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(353, 'San Luis de la Paz', 11, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(354, 'Santa Catarina', 11, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(355, 'Santa Cruz de Juventino Rosas', 11, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(356, 'Santiago Maravatío', 11, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(357, 'Silao de la Victoria', 11, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(358, 'Tarandacuao', 11, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(359, 'Tarimoro', 11, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(360, 'Tierra Blanca', 11, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(361, 'Uriangato', 11, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(362, 'Valle de Santiago', 11, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(363, 'Victoria', 11, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(364, 'Villagrán', 11, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(365, 'Xichú', 11, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(366, 'Yuriria', 11, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(367, 'Acapulco de Juárez', 12, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(368, 'Ahuacuotzingo', 12, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(369, 'Ajuchitlán del Progreso', 12, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(370, 'Alcozauca de Guerrero', 12, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(371, 'Alpoyeca', 12, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(372, 'Apaxtla', 12, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(373, 'Arcelia', 12, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(374, 'Atenango del Río', 12, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(375, 'Atlamajalcingo del Monte', 12, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(376, 'Atlixtac', 12, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(377, 'Atoyac de Álvarez', 12, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(378, 'Ayutla de los Libres', 12, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(379, 'Azoyú', 12, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(380, 'Benito Juárez', 12, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(381, 'Buenavista de Cuéllar', 12, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(382, 'Coahuayutla de José María Izazaga', 12, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(383, 'Cocula', 12, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(384, 'Copala', 12, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(385, 'Copalillo', 12, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(386, 'Copanatoyac', 12, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(387, 'Coyuca de Benítez', 12, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(388, 'Coyuca de Catalán', 12, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(389, 'Cuajinicuilapa', 12, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(390, 'Cualác', 12, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(391, 'Cuautepec', 12, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(392, 'Cuetzala del Progreso', 12, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(393, 'Cutzamala de Pinzón', 12, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(394, 'Chilapa de Álvarez', 12, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(395, 'Chilpancingo de los Bravo', 12, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(396, 'Florencio Villarreal', 12, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(397, 'General Canuto A. Neri', 12, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(398, 'General Heliodoro Castillo', 12, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(399, 'Huamuxtitlán', 12, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(400, 'Huitzuco de los Figueroa', 12, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(401, 'Iguala de la Independencia', 12, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(402, 'Igualapa', 12, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(403, 'Ixcateopan de Cuauhtémoc', 12, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(404, 'Zihuatanejo de Azueta', 12, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(405, 'Juan R. Escudero', 12, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(406, 'Leonardo Bravo', 12, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(407, 'Malinaltepec', 12, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(408, 'Mártir de Cuilapan', 12, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(409, 'Metlatónoc', 12, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(410, 'Mochitlán', 12, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(411, 'Olinalá', 12, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(412, 'Ometepec', 12, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(413, 'Pedro Ascencio Alquisiras', 12, 47, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(414, 'Petatlán', 12, 48, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(415, 'Pilcaya', 12, 49, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(416, 'Pungarabato', 12, 50, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(417, 'Quechultenango', 12, 51, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(418, 'San Luis Acatlán', 12, 52, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(419, 'San Marcos', 12, 53, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(420, 'San Miguel Totolapan', 12, 54, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(421, 'Taxco de Alarcón', 12, 55, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(422, 'Tecoanapa', 12, 56, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(423, 'Técpan de Galeana', 12, 57, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(424, 'Teloloapan', 12, 58, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(425, 'Tepecoacuilco de Trujano', 12, 59, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(426, 'Tetipac', 12, 60, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(427, 'Tixtla de Guerrero', 12, 61, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(428, 'Tlacoachistlahuaca', 12, 62, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(429, 'Tlacoapa', 12, 63, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(430, 'Tlalchapa', 12, 64, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(431, 'Tlalixtaquilla de Maldonado', 12, 65, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(432, 'Tlapa de Comonfort', 12, 66, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(433, 'Tlapehuala', 12, 67, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(434, 'La Unión de Isidoro Montes de Oca', 12, 68, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(435, 'Xalpatláhuac', 12, 69, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(436, 'Xochihuehuetlán', 12, 70, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(437, 'Xochistlahuaca', 12, 71, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(438, 'Zapotitlán Tablas', 12, 72, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(439, 'Zirándaro', 12, 73, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(440, 'Zitlala', 12, 74, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(441, 'Eduardo Neri', 12, 75, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(442, 'Acatepec', 12, 76, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(443, 'Marquelia', 12, 77, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(444, 'Cochoapa el Grande', 12, 78, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(445, 'José Joaquín de Herrera', 12, 79, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(446, 'Juchitán', 12, 80, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(447, 'Iliatenco', 12, 81, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(448, 'Acatlán', 13, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(449, 'Acaxochitlán', 13, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(450, 'Actopan', 13, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(451, 'Agua Blanca de Iturbide', 13, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(452, 'Ajacuba', 13, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(453, 'Alfajayucan', 13, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(454, 'Almoloya', 13, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(455, 'Apan', 13, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(456, 'El Arenal', 13, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(457, 'Atitalaquia', 13, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(458, 'Atlapexco', 13, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(459, 'Atotonilco el Grande', 13, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(460, 'Atotonilco de Tula', 13, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(461, 'Calnali', 13, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(462, 'Cardonal', 13, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(463, 'Cuautepec de Hinojosa', 13, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(464, 'Chapantongo', 13, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(465, 'Chapulhuacán', 13, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(466, 'Chilcuautla', 13, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(467, 'Eloxochitlán', 13, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(468, 'Emiliano Zapata', 13, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(469, 'Epazoyucan', 13, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(470, 'Francisco I. Madero', 13, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(471, 'Huasca de Ocampo', 13, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(472, 'Huautla', 13, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(473, 'Huazalingo', 13, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(474, 'Huehuetla', 13, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(475, 'Huejutla de Reyes', 13, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(476, 'Huichapan', 13, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(477, 'Ixmiquilpan', 13, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(478, 'Jacala de Ledezma', 13, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(479, 'Jaltocán', 13, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(480, 'Juárez Hidalgo', 13, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(481, 'Lolotla', 13, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(482, 'Metepec', 13, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(483, 'San Agustín Metzquititlán', 13, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(484, 'Metztitlán', 13, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(485, 'Mineral del Chico', 13, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(486, 'Mineral del Monte', 13, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(487, 'La Misión', 13, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(488, 'Mixquiahuala de Juárez', 13, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(489, 'Molango de Escamilla', 13, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(490, 'Nicolás Flores', 13, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(491, 'Nopala de Villagrán', 13, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(492, 'Omitlán de Juárez', 13, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(493, 'San Felipe Orizatlán', 13, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(494, 'Pacula', 13, 47, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(495, 'Pachuca de Soto', 13, 48, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(496, 'Pisaflores', 13, 49, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(497, 'Progreso de Obregón', 13, 50, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(498, 'Mineral de la Reforma', 13, 51, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(499, 'San Agustín Tlaxiaca', 13, 52, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(500, 'San Bartolo Tutotepec', 13, 53, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(501, 'San Salvador', 13, 54, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(502, 'Santiago de Anaya', 13, 55, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(503, 'Santiago Tulantepec de Lugo Guerrero', 13, 56, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(504, 'Singuilucan', 13, 57, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(505, 'Tasquillo', 13, 58, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(506, 'Tecozautla', 13, 59, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(507, 'Tenango de Doria', 13, 60, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(508, 'Tepeapulco', 13, 61, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(509, 'Tepehuacán de Guerrero', 13, 62, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(510, 'Tepeji del Río de Ocampo', 13, 63, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(511, 'Tepetitlán', 13, 64, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(512, 'Tetepango', 13, 65, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(513, 'Villa de Tezontepec', 13, 66, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(514, 'Tezontepec de Aldama', 13, 67, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(515, 'Tianguistengo', 13, 68, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(516, 'Tizayuca', 13, 69, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(517, 'Tlahuelilpan', 13, 70, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(518, 'Tlahuiltepa', 13, 71, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(519, 'Tlanalapa', 13, 72, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(520, 'Tlanchinol', 13, 73, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(521, 'Tlaxcoapan', 13, 74, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(522, 'Tolcayuca', 13, 75, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(523, 'Tula de Allende', 13, 76, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(524, 'Tulancingo de Bravo', 13, 77, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(525, 'Xochiatipan', 13, 78, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(526, 'Xochicoatlán', 13, 79, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(527, 'Yahualica', 13, 80, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(528, 'Zacualtipán de Ángeles', 13, 81, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(529, 'Zapotlán de Juárez', 13, 82, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(530, 'Zempoala', 13, 83, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(531, 'Zimapán', 13, 84, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(532, 'Acatic', 14, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(533, 'Acatlán de Juárez', 14, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(534, 'Ahualulco de Mercado', 14, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(535, 'Amacueca', 14, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(536, 'Amatitán', 14, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(537, 'Ameca', 14, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(538, 'San Juanito de Escobedo', 14, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(539, 'Arandas', 14, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(540, 'El Arenal', 14, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(541, 'Atemajac de Brizuela', 14, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(542, 'Atengo', 14, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(543, 'Atenguillo', 14, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(544, 'Atotonilco el Alto', 14, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(545, 'Atoyac', 14, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(546, 'Autlán de Navarro', 14, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(547, 'Ayotlán', 14, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(548, 'Ayutla', 14, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(549, 'La Barca', 14, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(550, 'Bolaños', 14, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(551, 'Cabo Corrientes', 14, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(552, 'Casimiro Castillo', 14, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(553, 'Cihuatlán', 14, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(554, 'Zapotlán el Grande', 14, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(555, 'Cocula', 14, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(556, 'Colotlán', 14, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(557, 'Concepción de Buenos Aires', 14, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(558, 'Cuautitlán de García Barragán', 14, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(559, 'Cuautla', 14, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(560, 'Cuquío', 14, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(561, 'Chapala', 14, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(562, 'Chimaltitán', 14, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(563, 'Chiquilistlán', 14, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(564, 'Degollado', 14, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(565, 'Ejutla', 14, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(566, 'Encarnación de Díaz', 14, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(567, 'Etzatlán', 14, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(568, 'El Grullo', 14, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(569, 'Guachinango', 14, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(570, 'Guadalajara', 14, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(571, 'Hostotipaquillo', 14, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(572, 'Huejúcar', 14, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(573, 'Huejuquilla el Alto', 14, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(574, 'La Huerta', 14, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(575, 'Ixtlahuacán de los Membrillos', 14, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(576, 'Ixtlahuacán del Río', 14, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(577, 'Jalostotitlán', 14, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(578, 'Jamay', 14, 47, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(579, 'Jesús María', 14, 48, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(580, 'Jilotlán de los Dolores', 14, 49, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(581, 'Jocotepec', 14, 50, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(582, 'Juanacatlán', 14, 51, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(583, 'Juchitlán', 14, 52, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(584, 'Lagos de Moreno', 14, 53, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(585, 'El Limón', 14, 54, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(586, 'Magdalena', 14, 55, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(587, 'Santa María del Oro', 14, 56, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(588, 'La Manzanilla de la Paz', 14, 57, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(589, 'Mascota', 14, 58, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(590, 'Mazamitla', 14, 59, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(591, 'Mexticacán', 14, 60, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(592, 'Mezquitic', 14, 61, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(593, 'Mixtlán', 14, 62, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(594, 'Ocotlán', 14, 63, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(595, 'Ojuelos de Jalisco', 14, 64, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(596, 'Pihuamo', 14, 65, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(597, 'Poncitlán', 14, 66, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(598, 'Puerto Vallarta', 14, 67, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(599, 'Villa Purificación', 14, 68, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(600, 'Quitupan', 14, 69, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(601, 'El Salto', 14, 70, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(602, 'San Cristóbal de la Barranca', 14, 71, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(603, 'San Diego de Alejandría', 14, 72, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(604, 'San Juan de los Lagos', 14, 73, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(605, 'San Julián', 14, 74, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(606, 'San Marcos', 14, 75, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(607, 'San Martín de Bolaños', 14, 76, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(608, 'San Martín Hidalgo', 14, 77, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(609, 'San Miguel el Alto', 14, 78, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(610, 'Gómez Farías', 14, 79, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(611, 'San Sebastián del Oeste', 14, 80, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(612, 'Santa María de los Ángeles', 14, 81, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(613, 'Sayula', 14, 82, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(614, 'Tala', 14, 83, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(615, 'Talpa de Allende', 14, 84, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(616, 'Tamazula de Gordiano', 14, 85, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(617, 'Tapalpa', 14, 86, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(618, 'Tecalitlán', 14, 87, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(619, 'Tecolotlán', 14, 88, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(620, 'Techaluta de Montenegro', 14, 89, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(621, 'Tenamaxtlán', 14, 90, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(622, 'Teocaltiche', 14, 91, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(623, 'Teocuitatlán de Corona', 14, 92, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(624, 'Tepatitlán de Morelos', 14, 93, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(625, 'Tequila', 14, 94, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(626, 'Teuchitlán', 14, 95, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(627, 'Tizapán el Alto', 14, 96, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(628, 'Tlajomulco de Zúñiga', 14, 97, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(629, 'San Pedro Tlaquepaque', 14, 98, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(630, 'Tolimán', 14, 99, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(631, 'Tomatlán', 14, 100, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(632, 'Tonalá', 14, 101, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(633, 'Tonaya', 14, 102, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(634, 'Tonila', 14, 103, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(635, 'Totatiche', 14, 104, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(636, 'Tototlán', 14, 105, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(637, 'Tuxcacuesco', 14, 106, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(638, 'Tuxcueca', 14, 107, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(639, 'Tuxpan', 14, 108, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(640, 'Unión de San Antonio', 14, 109, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(641, 'Unión de Tula', 14, 110, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(642, 'Valle de Guadalupe', 14, 111, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(643, 'Valle de Juárez', 14, 112, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(644, 'San Gabriel', 14, 113, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(645, 'Villa Corona', 14, 114, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(646, 'Villa Guerrero', 14, 115, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(647, 'Villa Hidalgo', 14, 116, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(648, 'Cañadas de Obregón', 14, 117, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(649, 'Yahualica de González Gallo', 14, 118, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(650, 'Zacoalco de Torres', 14, 119, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(651, 'Zapopan', 14, 120, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(652, 'Zapotiltic', 14, 121, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(653, 'Zapotitlán de Vadillo', 14, 122, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(654, 'Zapotlán del Rey', 14, 123, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(655, 'Zapotlanejo', 14, 124, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(656, 'San Ignacio Cerro Gordo', 14, 125, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(657, 'Acambay de Ruíz Castañeda', 15, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(658, 'Acolman', 15, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(659, 'Aculco', 15, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(660, 'Almoloya de Alquisiras', 15, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(661, 'Almoloya de Juárez', 15, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(662, 'Almoloya del Río', 15, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(663, 'Amanalco', 15, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(664, 'Amatepec', 15, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(665, 'Amecameca', 15, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(666, 'Apaxco', 15, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(667, 'Atenco', 15, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(668, 'Atizapán', 15, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(669, 'Atizapán de Zaragoza', 15, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(670, 'Atlacomulco', 15, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(671, 'Atlautla', 15, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(672, 'Axapusco', 15, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(673, 'Ayapango', 15, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24');
INSERT INTO `cities` (`id`, `name`, `state_id`, `number`, `created_at`, `updated_at`) VALUES
(674, 'Calimaya', 15, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(675, 'Capulhuac', 15, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(676, 'Coacalco de Berriozábal', 15, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(677, 'Coatepec Harinas', 15, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(678, 'Cocotitlán', 15, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(679, 'Coyotepec', 15, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(680, 'Cuautitlán', 15, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(681, 'Chalco', 15, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(682, 'Chapa de Mota', 15, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(683, 'Chapultepec', 15, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(684, 'Chiautla', 15, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(685, 'Chicoloapan', 15, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(686, 'Chiconcuac', 15, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(687, 'Chimalhuacán', 15, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(688, 'Donato Guerra', 15, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(689, 'Ecatepec de Morelos', 15, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(690, 'Ecatzingo', 15, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(691, 'Huehuetoca', 15, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(692, 'Hueypoxtla', 15, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(693, 'Huixquilucan', 15, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(694, 'Isidro Fabela', 15, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(695, 'Ixtapaluca', 15, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(696, 'Ixtapan de la Sal', 15, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(697, 'Ixtapan del Oro', 15, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(698, 'Ixtlahuaca', 15, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(699, 'Xalatlaco', 15, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(700, 'Jaltenco', 15, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(701, 'Jilotepec', 15, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(702, 'Jilotzingo', 15, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(703, 'Jiquipilco', 15, 47, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(704, 'Jocotitlán', 15, 48, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(705, 'Joquicingo', 15, 49, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(706, 'Juchitepec', 15, 50, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(707, 'Lerma', 15, 51, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(708, 'Malinalco', 15, 52, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(709, 'Melchor Ocampo', 15, 53, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(710, 'Metepec', 15, 54, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(711, 'Mexicaltzingo', 15, 55, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(712, 'Morelos', 15, 56, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(713, 'Naucalpan de Juárez', 15, 57, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(714, 'Nezahualcóyotl', 15, 58, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(715, 'Nextlalpan', 15, 59, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(716, 'Nicolás Romero', 15, 60, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(717, 'Nopaltepec', 15, 61, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(718, 'Ocoyoacac', 15, 62, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(719, 'Ocuilan', 15, 63, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(720, 'El Oro', 15, 64, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(721, 'Otumba', 15, 65, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(722, 'Otzoloapan', 15, 66, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(723, 'Otzolotepec', 15, 67, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(724, 'Ozumba', 15, 68, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(725, 'Papalotla', 15, 69, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(726, 'La Paz', 15, 70, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(727, 'Polotitlán', 15, 71, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(728, 'Rayón', 15, 72, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(729, 'San Antonio la Isla', 15, 73, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(730, 'San Felipe del Progreso', 15, 74, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(731, 'San Martín de las Pirámides', 15, 75, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(732, 'San Mateo Atenco', 15, 76, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(733, 'San Simón de Guerrero', 15, 77, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(734, 'Santo Tomás', 15, 78, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(735, 'Soyaniquilpan de Juárez', 15, 79, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(736, 'Sultepec', 15, 80, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(737, 'Tecámac', 15, 81, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(738, 'Tejupilco', 15, 82, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(739, 'Temamatla', 15, 83, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(740, 'Temascalapa', 15, 84, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(741, 'Temascalcingo', 15, 85, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(742, 'Temascaltepec', 15, 86, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(743, 'Temoaya', 15, 87, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(744, 'Tenancingo', 15, 88, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(745, 'Tenango del Aire', 15, 89, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(746, 'Tenango del Valle', 15, 90, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(747, 'Teoloyucan', 15, 91, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(748, 'Teotihuacán', 15, 92, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(749, 'Tepetlaoxtoc', 15, 93, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(750, 'Tepetlixpa', 15, 94, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(751, 'Tepotzotlán', 15, 95, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(752, 'Tequixquiac', 15, 96, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(753, 'Texcaltitlán', 15, 97, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(754, 'Texcalyacac', 15, 98, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(755, 'Texcoco', 15, 99, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(756, 'Tezoyuca', 15, 100, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(757, 'Tianguistenco', 15, 101, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(758, 'Timilpan', 15, 102, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(759, 'Tlalmanalco', 15, 103, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(760, 'Tlalnepantla de Baz', 15, 104, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(761, 'Tlatlaya', 15, 105, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(762, 'Toluca', 15, 106, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(763, 'Tonatico', 15, 107, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(764, 'Tultepec', 15, 108, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(765, 'Tultitlán', 15, 109, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(766, 'Valle de Bravo', 15, 110, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(767, 'Villa de Allende', 15, 111, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(768, 'Villa del Carbón', 15, 112, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(769, 'Villa Guerrero', 15, 113, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(770, 'Villa Victoria', 15, 114, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(771, 'Xonacatlán', 15, 115, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(772, 'Zacazonapan', 15, 116, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(773, 'Zacualpan', 15, 117, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(774, 'Zinacantepec', 15, 118, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(775, 'Zumpahuacán', 15, 119, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(776, 'Zumpango', 15, 120, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(777, 'Cuautitlán Izcalli', 15, 121, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(778, 'Valle de Chalco Solidaridad', 15, 122, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(779, 'Luvianos', 15, 123, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(780, 'San José del Rincón', 15, 124, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(781, 'Tonanitla', 15, 125, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(782, 'Acuitzio', 16, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(783, 'Aguililla', 16, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(784, 'Álvaro Obregón', 16, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(785, 'Angamacutiro', 16, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(786, 'Angangueo', 16, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(787, 'Apatzingán', 16, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(788, 'Aporo', 16, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(789, 'Aquila', 16, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(790, 'Ario', 16, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(791, 'Arteaga', 16, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(792, 'Briseñas', 16, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(793, 'Buenavista', 16, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(794, 'Carácuaro', 16, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(795, 'Coahuayana', 16, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(796, 'Coalcomán de Vázquez Pallares', 16, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(797, 'Coeneo', 16, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(798, 'Contepec', 16, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(799, 'Copándaro', 16, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(800, 'Cotija', 16, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(801, 'Cuitzeo', 16, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(802, 'Charapan', 16, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(803, 'Charo', 16, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(804, 'Chavinda', 16, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(805, 'Cherán', 16, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(806, 'Chilchota', 16, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(807, 'Chinicuila', 16, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(808, 'Chucándiro', 16, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(809, 'Churintzio', 16, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(810, 'Churumuco', 16, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(811, 'Ecuandureo', 16, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(812, 'Epitacio Huerta', 16, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(813, 'Erongarícuaro', 16, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(814, 'Gabriel Zamora', 16, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(815, 'Hidalgo', 16, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(816, 'La Huacana', 16, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(817, 'Huandacareo', 16, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(818, 'Huaniqueo', 16, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(819, 'Huetamo', 16, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(820, 'Huiramba', 16, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(821, 'Indaparapeo', 16, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(822, 'Irimbo', 16, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(823, 'Ixtlán', 16, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(824, 'Jacona', 16, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(825, 'Jiménez', 16, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(826, 'Jiquilpan', 16, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(827, 'Juárez', 16, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(828, 'Jungapeo', 16, 47, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(829, 'Lagunillas', 16, 48, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(830, 'Madero', 16, 49, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(831, 'Maravatío', 16, 50, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(832, 'Marcos Castellanos', 16, 51, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(833, 'Lázaro Cárdenas', 16, 52, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(834, 'Morelia', 16, 53, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(835, 'Morelos', 16, 54, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(836, 'Múgica', 16, 55, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(837, 'Nahuatzen', 16, 56, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(838, 'Nocupétaro', 16, 57, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(839, 'Nuevo Parangaricutiro', 16, 58, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(840, 'Nuevo Urecho', 16, 59, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(841, 'Numarán', 16, 60, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(842, 'Ocampo', 16, 61, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(843, 'Pajacuarán', 16, 62, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(844, 'Panindícuaro', 16, 63, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(845, 'Parácuaro', 16, 64, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(846, 'Paracho', 16, 65, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(847, 'Pátzcuaro', 16, 66, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(848, 'Penjamillo', 16, 67, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(849, 'Peribán', 16, 68, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(850, 'La Piedad', 16, 69, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(851, 'Purépero', 16, 70, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(852, 'Puruándiro', 16, 71, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(853, 'Queréndaro', 16, 72, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(854, 'Quiroga', 16, 73, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(855, 'Cojumatlán de Régules', 16, 74, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(856, 'Los Reyes', 16, 75, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(857, 'Sahuayo', 16, 76, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(858, 'San Lucas', 16, 77, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(859, 'Santa Ana Maya', 16, 78, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(860, 'Salvador Escalante', 16, 79, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(861, 'Senguio', 16, 80, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(862, 'Susupuato', 16, 81, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(863, 'Tacámbaro', 16, 82, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(864, 'Tancítaro', 16, 83, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(865, 'Tangamandapio', 16, 84, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(866, 'Tangancícuaro', 16, 85, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(867, 'Tanhuato', 16, 86, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(868, 'Taretan', 16, 87, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(869, 'Tarímbaro', 16, 88, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(870, 'Tepalcatepec', 16, 89, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(871, 'Tingambato', 16, 90, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(872, 'Tingüindín', 16, 91, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(873, 'Tiquicheo de Nicolás Romero', 16, 92, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(874, 'Tlalpujahua', 16, 93, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(875, 'Tlazazalca', 16, 94, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(876, 'Tocumbo', 16, 95, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(877, 'Tumbiscatío', 16, 96, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(878, 'Turicato', 16, 97, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(879, 'Tuxpan', 16, 98, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(880, 'Tuzantla', 16, 99, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(881, 'Tzintzuntzan', 16, 100, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(882, 'Tzitzio', 16, 101, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(883, 'Uruapan', 16, 102, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(884, 'Venustiano Carranza', 16, 103, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(885, 'Villamar', 16, 104, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(886, 'Vista Hermosa', 16, 105, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(887, 'Yurécuaro', 16, 106, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(888, 'Zacapu', 16, 107, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(889, 'Zamora', 16, 108, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(890, 'Zináparo', 16, 109, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(891, 'Zinapécuaro', 16, 110, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(892, 'Ziracuaretiro', 16, 111, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(893, 'Zitácuaro', 16, 112, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(894, 'José Sixto Verduzco', 16, 113, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(895, 'Amacuzac', 17, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(896, 'Atlatlahucan', 17, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(897, 'Axochiapan', 17, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(898, 'Ayala', 17, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(899, 'Coatlán del Río', 17, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(900, 'Cuautla', 17, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(901, 'Cuernavaca', 17, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(902, 'Emiliano Zapata', 17, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(903, 'Huitzilac', 17, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(904, 'Jantetelco', 17, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(905, 'Jiutepec', 17, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(906, 'Jojutla', 17, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(907, 'Jonacatepec', 17, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(908, 'Mazatepec', 17, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(909, 'Miacatlán', 17, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(910, 'Ocuituco', 17, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(911, 'Puente de Ixtla', 17, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(912, 'Temixco', 17, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(913, 'Tepalcingo', 17, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(914, 'Tepoztlán', 17, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(915, 'Tetecala', 17, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(916, 'Tetela del Volcán', 17, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(917, 'Tlalnepantla', 17, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(918, 'Tlaltizapán de Zapata', 17, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(919, 'Tlaquiltenango', 17, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(920, 'Tlayacapan', 17, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(921, 'Totolapan', 17, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(922, 'Xochitepec', 17, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(923, 'Yautepec', 17, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(924, 'Yecapixtla', 17, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(925, 'Zacatepec', 17, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(926, 'Zacualpan de Amilpas', 17, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(927, 'Temoac', 17, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(928, 'Acaponeta', 18, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(929, 'Ahuacatlán', 18, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(930, 'Amatlán de Cañas', 18, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(931, 'Compostela', 18, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(932, 'Huajicori', 18, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(933, 'Ixtlán del Río', 18, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(934, 'Jala', 18, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(935, 'Xalisco', 18, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(936, 'Del Nayar', 18, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(937, 'Rosamorada', 18, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(938, 'Ruíz', 18, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(939, 'San Blas', 18, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(940, 'San Pedro Lagunillas', 18, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(941, 'Santa María del Oro', 18, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(942, 'Santiago Ixcuintla', 18, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(943, 'Tecuala', 18, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(944, 'Tepic', 18, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(945, 'Tuxpan', 18, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(946, 'La Yesca', 18, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(947, 'Bahía de Banderas', 18, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(948, 'Abasolo', 19, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(949, 'Agualeguas', 19, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(950, 'Los Aldamas', 19, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(951, 'Allende', 19, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(952, 'Anáhuac', 19, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(953, 'Apodaca', 19, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(954, 'Aramberri', 19, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(955, 'Bustamante', 19, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(956, 'Cadereyta Jiménez', 19, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(957, 'El Carmen', 19, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(958, 'Cerralvo', 19, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(959, 'Ciénega de Flores', 19, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(960, 'China', 19, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(961, 'Doctor Arroyo', 19, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(962, 'Doctor Coss', 19, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(963, 'Doctor González', 19, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(964, 'Galeana', 19, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(965, 'García', 19, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(966, 'San Pedro Garza García', 19, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(967, 'General Bravo', 19, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(968, 'General Escobedo', 19, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(969, 'General Terán', 19, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(970, 'General Treviño', 19, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(971, 'General Zaragoza', 19, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(972, 'General Zuazua', 19, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(973, 'Guadalupe', 19, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(974, 'Los Herreras', 19, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(975, 'Higueras', 19, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(976, 'Hualahuises', 19, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(977, 'Iturbide', 19, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(978, 'Juárez', 19, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(979, 'Lampazos de Naranjo', 19, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(980, 'Linares', 19, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(981, 'Marín', 19, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(982, 'Melchor Ocampo', 19, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(983, 'Mier y Noriega', 19, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(984, 'Mina', 19, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(985, 'Montemorelos', 19, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(986, 'Monterrey', 19, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(987, 'Parás', 19, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(988, 'Pesquería', 19, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(989, 'Los Ramones', 19, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(990, 'Rayones', 19, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(991, 'Sabinas Hidalgo', 19, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(992, 'Salinas Victoria', 19, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(993, 'San Nicolás de los Garza', 19, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(994, 'Hidalgo', 19, 47, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(995, 'Santa Catarina', 19, 48, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(996, 'Santiago', 19, 49, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(997, 'Vallecillo', 19, 50, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(998, 'Villaldama', 19, 51, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(999, 'Abejones', 20, 1, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1000, 'Acatlán de Pérez Figueroa', 20, 2, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1001, 'Asunción Cacalotepec', 20, 3, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1002, 'Asunción Cuyotepeji', 20, 4, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1003, 'Asunción Ixtaltepec', 20, 5, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1004, 'Asunción Nochixtlán', 20, 6, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1005, 'Asunción Ocotlán', 20, 7, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1006, 'Asunción Tlacolulita', 20, 8, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1007, 'Ayotzintepec', 20, 9, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1008, 'El Barrio de la Soledad', 20, 10, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1009, 'Calihualá', 20, 11, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1010, 'Candelaria Loxicha', 20, 12, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1011, 'Ciénega de Zimatlán', 20, 13, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1012, 'Ciudad Ixtepec', 20, 14, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1013, 'Coatecas Altas', 20, 15, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1014, 'Coicoyán de las Flores', 20, 16, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1015, 'La Compañía', 20, 17, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1016, 'Concepción Buenavista', 20, 18, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1017, 'Concepción Pápalo', 20, 19, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1018, 'Constancia del Rosario', 20, 20, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1019, 'Cosolapa', 20, 21, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1020, 'Cosoltepec', 20, 22, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1021, 'Cuilápam de Guerrero', 20, 23, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1022, 'Cuyamecalco Villa de Zaragoza', 20, 24, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1023, 'Chahuites', 20, 25, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1024, 'Chalcatongo de Hidalgo', 20, 26, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1025, 'Chiquihuitlán de Benito Juárez', 20, 27, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1026, 'Heroica Ciudad de Ejutla de Crespo', 20, 28, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1027, 'Eloxochitlán de Flores Magón', 20, 29, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1028, 'El Espinal', 20, 30, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1029, 'Tamazulápam del Espíritu Santo', 20, 31, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1030, 'Fresnillo de Trujano', 20, 32, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1031, 'Guadalupe Etla', 20, 33, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1032, 'Guadalupe de Ramírez', 20, 34, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1033, 'Guelatao de Juárez', 20, 35, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1034, 'Guevea de Humboldt', 20, 36, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1035, 'Mesones Hidalgo', 20, 37, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1036, 'Villa Hidalgo', 20, 38, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1037, 'Heroica Ciudad de Huajuapan de León', 20, 39, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1038, 'Huautepec', 20, 40, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1039, 'Huautla de Jiménez', 20, 41, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1040, 'Ixtlán de Juárez', 20, 42, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1041, 'Heroica Ciudad de Juchitán de Zaragoza', 20, 43, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1042, 'Loma Bonita', 20, 44, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1043, 'Magdalena Apasco', 20, 45, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1044, 'Magdalena Jaltepec', 20, 46, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1045, 'Santa Magdalena Jicotlán', 20, 47, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1046, 'Magdalena Mixtepec', 20, 48, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1047, 'Magdalena Ocotlán', 20, 49, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1048, 'Magdalena Peñasco', 20, 50, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1049, 'Magdalena Teitipac', 20, 51, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1050, 'Magdalena Tequisistlán', 20, 52, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1051, 'Magdalena Tlacotepec', 20, 53, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1052, 'Magdalena Zahuatlán', 20, 54, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1053, 'Mariscala de Juárez', 20, 55, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1054, 'Mártires de Tacubaya', 20, 56, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1055, 'Matías Romero Avendaño', 20, 57, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1056, 'Mazatlán Villa de Flores', 20, 58, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1057, 'Miahuatlán de Porfirio Díaz', 20, 59, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1058, 'Mixistlán de la Reforma', 20, 60, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1059, 'Monjas', 20, 61, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1060, 'Natividad', 20, 62, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1061, 'Nazareno Etla', 20, 63, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1062, 'Nejapa de Madero', 20, 64, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1063, 'Ixpantepec Nieves', 20, 65, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1064, 'Santiago Niltepec', 20, 66, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1065, 'Oaxaca de Juárez', 20, 67, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1066, 'Ocotlán de Morelos', 20, 68, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1067, 'La Pe', 20, 69, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1068, 'Pinotepa de Don Luis', 20, 70, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1069, 'Pluma Hidalgo', 20, 71, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1070, 'San José del Progreso', 20, 72, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1071, 'Putla Villa de Guerrero', 20, 73, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1072, 'Santa Catarina Quioquitani', 20, 74, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1073, 'Reforma de Pineda', 20, 75, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1074, 'La Reforma', 20, 76, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1075, 'Reyes Etla', 20, 77, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1076, 'Rojas de Cuauhtémoc', 20, 78, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1077, 'Salina Cruz', 20, 79, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1078, 'San Agustín Amatengo', 20, 80, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1079, 'San Agustín Atenango', 20, 81, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1080, 'San Agustín Chayuco', 20, 82, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1081, 'San Agustín de las Juntas', 20, 83, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1082, 'San Agustín Etla', 20, 84, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1083, 'San Agustín Loxicha', 20, 85, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1084, 'San Agustín Tlacotepec', 20, 86, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1085, 'San Agustín Yatareni', 20, 87, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1086, 'San Andrés Cabecera Nueva', 20, 88, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1087, 'San Andrés Dinicuiti', 20, 89, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1088, 'San Andrés Huaxpaltepec', 20, 90, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1089, 'San Andrés Huayápam', 20, 91, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1090, 'San Andrés Ixtlahuaca', 20, 92, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1091, 'San Andrés Lagunas', 20, 93, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1092, 'San Andrés Nuxiño', 20, 94, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1093, 'San Andrés Paxtlán', 20, 95, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1094, 'San Andrés Sinaxtla', 20, 96, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1095, 'San Andrés Solaga', 20, 97, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1096, 'San Andrés Teotilálpam', 20, 98, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1097, 'San Andrés Tepetlapa', 20, 99, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1098, 'San Andrés Yaá', 20, 100, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1099, 'San Andrés Zabache', 20, 101, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1100, 'San Andrés Zautla', 20, 102, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1101, 'San Antonino Castillo Velasco', 20, 103, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1102, 'San Antonino el Alto', 20, 104, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1103, 'San Antonino Monte Verde', 20, 105, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1104, 'San Antonio Acutla', 20, 106, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1105, 'San Antonio de la Cal', 20, 107, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1106, 'San Antonio Huitepec', 20, 108, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1107, 'San Antonio Nanahuatípam', 20, 109, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1108, 'San Antonio Sinicahua', 20, 110, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1109, 'San Antonio Tepetlapa', 20, 111, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1110, 'San Baltazar Chichicápam', 20, 112, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1111, 'San Baltazar Loxicha', 20, 113, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1112, 'San Baltazar Yatzachi el Bajo', 20, 114, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1113, 'San Bartolo Coyotepec', 20, 115, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1114, 'San Bartolomé Ayautla', 20, 116, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1115, 'San Bartolomé Loxicha', 20, 117, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1116, 'San Bartolomé Quialana', 20, 118, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1117, 'San Bartolomé Yucuañe', 20, 119, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1118, 'San Bartolomé Zoogocho', 20, 120, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1119, 'San Bartolo Soyaltepec', 20, 121, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1120, 'San Bartolo Yautepec', 20, 122, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1121, 'San Bernardo Mixtepec', 20, 123, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1122, 'San Blas Atempa', 20, 124, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1123, 'San Carlos Yautepec', 20, 125, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1124, 'San Cristóbal Amatlán', 20, 126, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1125, 'San Cristóbal Amoltepec', 20, 127, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1126, 'San Cristóbal Lachirioag', 20, 128, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1127, 'San Cristóbal Suchixtlahuaca', 20, 129, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1128, 'San Dionisio del Mar', 20, 130, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1129, 'San Dionisio Ocotepec', 20, 131, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1130, 'San Dionisio Ocotlán', 20, 132, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1131, 'San Esteban Atatlahuca', 20, 133, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1132, 'San Felipe Jalapa de Díaz', 20, 134, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1133, 'San Felipe Tejalápam', 20, 135, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1134, 'San Felipe Usila', 20, 136, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1135, 'San Francisco Cahuacuá', 20, 137, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1136, 'San Francisco Cajonos', 20, 138, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1137, 'San Francisco Chapulapa', 20, 139, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1138, 'San Francisco Chindúa', 20, 140, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1139, 'San Francisco del Mar', 20, 141, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1140, 'San Francisco Huehuetlán', 20, 142, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1141, 'San Francisco Ixhuatán', 20, 143, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1142, 'San Francisco Jaltepetongo', 20, 144, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1143, 'San Francisco Lachigoló', 20, 145, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1144, 'San Francisco Logueche', 20, 146, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1145, 'San Francisco Nuxaño', 20, 147, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1146, 'San Francisco Ozolotepec', 20, 148, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1147, 'San Francisco Sola', 20, 149, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1148, 'San Francisco Telixtlahuaca', 20, 150, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1149, 'San Francisco Teopan', 20, 151, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1150, 'San Francisco Tlapancingo', 20, 152, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1151, 'San Gabriel Mixtepec', 20, 153, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1152, 'San Ildefonso Amatlán', 20, 154, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1153, 'San Ildefonso Sola', 20, 155, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1154, 'San Ildefonso Villa Alta', 20, 156, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1155, 'San Jacinto Amilpas', 20, 157, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1156, 'San Jacinto Tlacotepec', 20, 158, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1157, 'San Jerónimo Coatlán', 20, 159, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1158, 'San Jerónimo Silacayoapilla', 20, 160, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1159, 'San Jerónimo Sosola', 20, 161, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1160, 'San Jerónimo Taviche', 20, 162, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1161, 'San Jerónimo Tecóatl', 20, 163, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1162, 'San Jorge Nuchita', 20, 164, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1163, 'San José Ayuquila', 20, 165, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1164, 'San José Chiltepec', 20, 166, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1165, 'San José del Peñasco', 20, 167, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1166, 'San José Estancia Grande', 20, 168, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1167, 'San José Independencia', 20, 169, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1168, 'San José Lachiguiri', 20, 170, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1169, 'San José Tenango', 20, 171, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1170, 'San Juan Achiutla', 20, 172, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1171, 'San Juan Atepec', 20, 173, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1172, 'Ánimas Trujano', 20, 174, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1173, 'San Juan Bautista Atatlahuca', 20, 175, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1174, 'San Juan Bautista Coixtlahuaca', 20, 176, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1175, 'San Juan Bautista Cuicatlán', 20, 177, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1176, 'San Juan Bautista Guelache', 20, 178, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1177, 'San Juan Bautista Jayacatlán', 20, 179, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1178, 'San Juan Bautista Lo de Soto', 20, 180, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1179, 'San Juan Bautista Suchitepec', 20, 181, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1180, 'San Juan Bautista Tlacoatzintepec', 20, 182, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1181, 'San Juan Bautista Tlachichilco', 20, 183, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1182, 'San Juan Bautista Tuxtepec', 20, 184, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1183, 'San Juan Cacahuatepec', 20, 185, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1184, 'San Juan Cieneguilla', 20, 186, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1185, 'San Juan Coatzóspam', 20, 187, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1186, 'San Juan Colorado', 20, 188, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1187, 'San Juan Comaltepec', 20, 189, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1188, 'San Juan Cotzocón', 20, 190, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1189, 'San Juan Chicomezúchil', 20, 191, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1190, 'San Juan Chilateca', 20, 192, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1191, 'San Juan del Estado', 20, 193, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1192, 'San Juan del Río', 20, 194, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1193, 'San Juan Diuxi', 20, 195, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1194, 'San Juan Evangelista Analco', 20, 196, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1195, 'San Juan Guelavía', 20, 197, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1196, 'San Juan Guichicovi', 20, 198, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1197, 'San Juan Ihualtepec', 20, 199, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1198, 'San Juan Juquila Mixes', 20, 200, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1199, 'San Juan Juquila Vijanos', 20, 201, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1200, 'San Juan Lachao', 20, 202, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1201, 'San Juan Lachigalla', 20, 203, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1202, 'San Juan Lajarcia', 20, 204, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1203, 'San Juan Lalana', 20, 205, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1204, 'San Juan de los Cués', 20, 206, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1205, 'San Juan Mazatlán', 20, 207, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1206, 'San Juan Mixtepec', 20, 208, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1207, 'San Juan Mixtepec', 20, 209, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1208, 'San Juan Ñumí', 20, 210, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1209, 'San Juan Ozolotepec', 20, 211, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1210, 'San Juan Petlapa', 20, 212, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1211, 'San Juan Quiahije', 20, 213, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1212, 'San Juan Quiotepec', 20, 214, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1213, 'San Juan Sayultepec', 20, 215, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1214, 'San Juan Tabaá', 20, 216, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1215, 'San Juan Tamazola', 20, 217, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1216, 'San Juan Teita', 20, 218, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1217, 'San Juan Teitipac', 20, 219, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1218, 'San Juan Tepeuxila', 20, 220, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1219, 'San Juan Teposcolula', 20, 221, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1220, 'San Juan Yaeé', 20, 222, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1221, 'San Juan Yatzona', 20, 223, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1222, 'San Juan Yucuita', 20, 224, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1223, 'San Lorenzo', 20, 225, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1224, 'San Lorenzo Albarradas', 20, 226, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1225, 'San Lorenzo Cacaotepec', 20, 227, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1226, 'San Lorenzo Cuaunecuiltitla', 20, 228, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1227, 'San Lorenzo Texmelúcan', 20, 229, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1228, 'San Lorenzo Victoria', 20, 230, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1229, 'San Lucas Camotlán', 20, 231, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1230, 'San Lucas Ojitlán', 20, 232, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1231, 'San Lucas Quiaviní', 20, 233, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1232, 'San Lucas Zoquiápam', 20, 234, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1233, 'San Luis Amatlán', 20, 235, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1234, 'San Marcial Ozolotepec', 20, 236, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1235, 'San Marcos Arteaga', 20, 237, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1236, 'San Martín de los Cansecos', 20, 238, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1237, 'San Martín Huamelúlpam', 20, 239, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1238, 'San Martín Itunyoso', 20, 240, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1239, 'San Martín Lachilá', 20, 241, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1240, 'San Martín Peras', 20, 242, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1241, 'San Martín Tilcajete', 20, 243, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1242, 'San Martín Toxpalan', 20, 244, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1243, 'San Martín Zacatepec', 20, 245, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1244, 'San Mateo Cajonos', 20, 246, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1245, 'Capulálpam de Méndez', 20, 247, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1246, 'San Mateo del Mar', 20, 248, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1247, 'San Mateo Yoloxochitlán', 20, 249, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1248, 'San Mateo Etlatongo', 20, 250, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1249, 'San Mateo Nejápam', 20, 251, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1250, 'San Mateo Peñasco', 20, 252, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1251, 'San Mateo Piñas', 20, 253, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1252, 'San Mateo Río Hondo', 20, 254, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1253, 'San Mateo Sindihui', 20, 255, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1254, 'San Mateo Tlapiltepec', 20, 256, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1255, 'San Melchor Betaza', 20, 257, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1256, 'San Miguel Achiutla', 20, 258, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1257, 'San Miguel Ahuehuetitlán', 20, 259, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1258, 'San Miguel Aloápam', 20, 260, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1259, 'San Miguel Amatitlán', 20, 261, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1260, 'San Miguel Amatlán', 20, 262, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1261, 'San Miguel Coatlán', 20, 263, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1262, 'San Miguel Chicahua', 20, 264, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1263, 'San Miguel Chimalapa', 20, 265, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1264, 'San Miguel del Puerto', 20, 266, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1265, 'San Miguel del Río', 20, 267, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1266, 'San Miguel Ejutla', 20, 268, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1267, 'San Miguel el Grande', 20, 269, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1268, 'San Miguel Huautla', 20, 270, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1269, 'San Miguel Mixtepec', 20, 271, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1270, 'San Miguel Panixtlahuaca', 20, 272, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1271, 'San Miguel Peras', 20, 273, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1272, 'San Miguel Piedras', 20, 274, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1273, 'San Miguel Quetzaltepec', 20, 275, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1274, 'San Miguel Santa Flor', 20, 276, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1275, 'Villa Sola de Vega', 20, 277, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1276, 'San Miguel Soyaltepec', 20, 278, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1277, 'San Miguel Suchixtepec', 20, 279, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1278, 'Villa Talea de Castro', 20, 280, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1279, 'San Miguel Tecomatlán', 20, 281, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1280, 'San Miguel Tenango', 20, 282, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1281, 'San Miguel Tequixtepec', 20, 283, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1282, 'San Miguel Tilquiápam', 20, 284, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1283, 'San Miguel Tlacamama', 20, 285, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1284, 'San Miguel Tlacotepec', 20, 286, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1285, 'San Miguel Tulancingo', 20, 287, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1286, 'San Miguel Yotao', 20, 288, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1287, 'San Nicolás', 20, 289, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1288, 'San Nicolás Hidalgo', 20, 290, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1289, 'San Pablo Coatlán', 20, 291, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1290, 'San Pablo Cuatro Venados', 20, 292, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1291, 'San Pablo Etla', 20, 293, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1292, 'San Pablo Huitzo', 20, 294, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1293, 'San Pablo Huixtepec', 20, 295, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1294, 'San Pablo Macuiltianguis', 20, 296, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1295, 'San Pablo Tijaltepec', 20, 297, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1296, 'San Pablo Villa de Mitla', 20, 298, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1297, 'San Pablo Yaganiza', 20, 299, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1298, 'San Pedro Amuzgos', 20, 300, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1299, 'San Pedro Apóstol', 20, 301, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1300, 'San Pedro Atoyac', 20, 302, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1301, 'San Pedro Cajonos', 20, 303, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1302, 'San Pedro Coxcaltepec Cántaros', 20, 304, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1303, 'San Pedro Comitancillo', 20, 305, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1304, 'San Pedro el Alto', 20, 306, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1305, 'San Pedro Huamelula', 20, 307, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1306, 'San Pedro Huilotepec', 20, 308, '2018-06-27 21:32:24', '2018-06-27 21:32:24');
INSERT INTO `cities` (`id`, `name`, `state_id`, `number`, `created_at`, `updated_at`) VALUES
(1307, 'San Pedro Ixcatlán', 20, 309, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1308, 'San Pedro Ixtlahuaca', 20, 310, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1309, 'San Pedro Jaltepetongo', 20, 311, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1310, 'San Pedro Jicayán', 20, 312, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1311, 'San Pedro Jocotipac', 20, 313, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1312, 'San Pedro Juchatengo', 20, 314, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1313, 'San Pedro Mártir', 20, 315, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1314, 'San Pedro Mártir Quiechapa', 20, 316, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1315, 'San Pedro Mártir Yucuxaco', 20, 317, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1316, 'San Pedro Mixtepec', 20, 318, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1317, 'San Pedro Mixtepec', 20, 319, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1318, 'San Pedro Molinos', 20, 320, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1319, 'San Pedro Nopala', 20, 321, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1320, 'San Pedro Ocopetatillo', 20, 322, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1321, 'San Pedro Ocotepec', 20, 323, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1322, 'San Pedro Pochutla', 20, 324, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1323, 'San Pedro Quiatoni', 20, 325, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1324, 'San Pedro Sochiápam', 20, 326, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1325, 'San Pedro Tapanatepec', 20, 327, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1326, 'San Pedro Taviche', 20, 328, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1327, 'San Pedro Teozacoalco', 20, 329, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1328, 'San Pedro Teutila', 20, 330, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1329, 'San Pedro Tidaá', 20, 331, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1330, 'San Pedro Topiltepec', 20, 332, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1331, 'San Pedro Totolápam', 20, 333, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1332, 'Villa de Tututepec de Melchor Ocampo', 20, 334, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1333, 'San Pedro Yaneri', 20, 335, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1334, 'San Pedro Yólox', 20, 336, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1335, 'San Pedro y San Pablo Ayutla', 20, 337, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1336, 'Villa de Etla', 20, 338, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1337, 'San Pedro y San Pablo Teposcolula', 20, 339, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1338, 'San Pedro y San Pablo Tequixtepec', 20, 340, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1339, 'San Pedro Yucunama', 20, 341, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1340, 'San Raymundo Jalpan', 20, 342, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1341, 'San Sebastián Abasolo', 20, 343, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1342, 'San Sebastián Coatlán', 20, 344, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1343, 'San Sebastián Ixcapa', 20, 345, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1344, 'San Sebastián Nicananduta', 20, 346, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1345, 'San Sebastián Río Hondo', 20, 347, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1346, 'San Sebastián Tecomaxtlahuaca', 20, 348, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1347, 'San Sebastián Teitipac', 20, 349, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1348, 'San Sebastián Tutla', 20, 350, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1349, 'San Simón Almolongas', 20, 351, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1350, 'San Simón Zahuatlán', 20, 352, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1351, 'Santa Ana', 20, 353, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1352, 'Santa Ana Ateixtlahuaca', 20, 354, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1353, 'Santa Ana Cuauhtémoc', 20, 355, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1354, 'Santa Ana del Valle', 20, 356, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1355, 'Santa Ana Tavela', 20, 357, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1356, 'Santa Ana Tlapacoyan', 20, 358, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1357, 'Santa Ana Yareni', 20, 359, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1358, 'Santa Ana Zegache', 20, 360, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1359, 'Santa Catalina Quierí', 20, 361, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1360, 'Santa Catarina Cuixtla', 20, 362, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1361, 'Santa Catarina Ixtepeji', 20, 363, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1362, 'Santa Catarina Juquila', 20, 364, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1363, 'Santa Catarina Lachatao', 20, 365, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1364, 'Santa Catarina Loxicha', 20, 366, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1365, 'Santa Catarina Mechoacán', 20, 367, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1366, 'Santa Catarina Minas', 20, 368, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1367, 'Santa Catarina Quiané', 20, 369, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1368, 'Santa Catarina Tayata', 20, 370, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1369, 'Santa Catarina Ticuá', 20, 371, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1370, 'Santa Catarina Yosonotú', 20, 372, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1371, 'Santa Catarina Zapoquila', 20, 373, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1372, 'Santa Cruz Acatepec', 20, 374, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1373, 'Santa Cruz Amilpas', 20, 375, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1374, 'Santa Cruz de Bravo', 20, 376, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1375, 'Santa Cruz Itundujia', 20, 377, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1376, 'Santa Cruz Mixtepec', 20, 378, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1377, 'Santa Cruz Nundaco', 20, 379, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1378, 'Santa Cruz Papalutla', 20, 380, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1379, 'Santa Cruz Tacache de Mina', 20, 381, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1380, 'Santa Cruz Tacahua', 20, 382, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1381, 'Santa Cruz Tayata', 20, 383, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1382, 'Santa Cruz Xitla', 20, 384, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1383, 'Santa Cruz Xoxocotlán', 20, 385, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1384, 'Santa Cruz Zenzontepec', 20, 386, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1385, 'Santa Gertrudis', 20, 387, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1386, 'Santa Inés del Monte', 20, 388, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1387, 'Santa Inés Yatzeche', 20, 389, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1388, 'Santa Lucía del Camino', 20, 390, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1389, 'Santa Lucía Miahuatlán', 20, 391, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1390, 'Santa Lucía Monteverde', 20, 392, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1391, 'Santa Lucía Ocotlán', 20, 393, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1392, 'Santa María Alotepec', 20, 394, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1393, 'Santa María Apazco', 20, 395, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1394, 'Santa María la Asunción', 20, 396, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1395, 'Heroica Ciudad de Tlaxiaco', 20, 397, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1396, 'Ayoquezco de Aldama', 20, 398, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1397, 'Santa María Atzompa', 20, 399, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1398, 'Santa María Camotlán', 20, 400, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1399, 'Santa María Colotepec', 20, 401, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1400, 'Santa María Cortijo', 20, 402, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1401, 'Santa María Coyotepec', 20, 403, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1402, 'Santa María Chachoápam', 20, 404, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1403, 'Villa de Chilapa de Díaz', 20, 405, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1404, 'Santa María Chilchotla', 20, 406, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1405, 'Santa María Chimalapa', 20, 407, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1406, 'Santa María del Rosario', 20, 408, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1407, 'Santa María del Tule', 20, 409, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1408, 'Santa María Ecatepec', 20, 410, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1409, 'Santa María Guelacé', 20, 411, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1410, 'Santa María Guienagati', 20, 412, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1411, 'Santa María Huatulco', 20, 413, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1412, 'Santa María Huazolotitlán', 20, 414, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1413, 'Santa María Ipalapa', 20, 415, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1414, 'Santa María Ixcatlán', 20, 416, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1415, 'Santa María Jacatepec', 20, 417, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1416, 'Santa María Jalapa del Marqués', 20, 418, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1417, 'Santa María Jaltianguis', 20, 419, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1418, 'Santa María Lachixío', 20, 420, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1419, 'Santa María Mixtequilla', 20, 421, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1420, 'Santa María Nativitas', 20, 422, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1421, 'Santa María Nduayaco', 20, 423, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1422, 'Santa María Ozolotepec', 20, 424, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1423, 'Santa María Pápalo', 20, 425, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1424, 'Santa María Peñoles', 20, 426, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1425, 'Santa María Petapa', 20, 427, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1426, 'Santa María Quiegolani', 20, 428, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1427, 'Santa María Sola', 20, 429, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1428, 'Santa María Tataltepec', 20, 430, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1429, 'Santa María Tecomavaca', 20, 431, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1430, 'Santa María Temaxcalapa', 20, 432, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1431, 'Santa María Temaxcaltepec', 20, 433, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1432, 'Santa María Teopoxco', 20, 434, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1433, 'Santa María Tepantlali', 20, 435, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1434, 'Santa María Texcatitlán', 20, 436, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1435, 'Santa María Tlahuitoltepec', 20, 437, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1436, 'Santa María Tlalixtac', 20, 438, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1437, 'Santa María Tonameca', 20, 439, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1438, 'Santa María Totolapilla', 20, 440, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1439, 'Santa María Xadani', 20, 441, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1440, 'Santa María Yalina', 20, 442, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1441, 'Santa María Yavesía', 20, 443, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1442, 'Santa María Yolotepec', 20, 444, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1443, 'Santa María Yosoyúa', 20, 445, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1444, 'Santa María Yucuhiti', 20, 446, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1445, 'Santa María Zacatepec', 20, 447, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1446, 'Santa María Zaniza', 20, 448, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1447, 'Santa María Zoquitlán', 20, 449, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1448, 'Santiago Amoltepec', 20, 450, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1449, 'Santiago Apoala', 20, 451, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1450, 'Santiago Apóstol', 20, 452, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1451, 'Santiago Astata', 20, 453, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1452, 'Santiago Atitlán', 20, 454, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1453, 'Santiago Ayuquililla', 20, 455, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1454, 'Santiago Cacaloxtepec', 20, 456, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1455, 'Santiago Camotlán', 20, 457, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1456, 'Santiago Comaltepec', 20, 458, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1457, 'Santiago Chazumba', 20, 459, '2018-06-27 21:32:24', '2018-06-27 21:32:24'),
(1458, 'Santiago Choápam', 20, 460, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1459, 'Santiago del Río', 20, 461, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1460, 'Santiago Huajolotitlán', 20, 462, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1461, 'Santiago Huauclilla', 20, 463, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1462, 'Santiago Ihuitlán Plumas', 20, 464, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1463, 'Santiago Ixcuintepec', 20, 465, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1464, 'Santiago Ixtayutla', 20, 466, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1465, 'Santiago Jamiltepec', 20, 467, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1466, 'Santiago Jocotepec', 20, 468, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1467, 'Santiago Juxtlahuaca', 20, 469, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1468, 'Santiago Lachiguiri', 20, 470, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1469, 'Santiago Lalopa', 20, 471, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1470, 'Santiago Laollaga', 20, 472, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1471, 'Santiago Laxopa', 20, 473, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1472, 'Santiago Llano Grande', 20, 474, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1473, 'Santiago Matatlán', 20, 475, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1474, 'Santiago Miltepec', 20, 476, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1475, 'Santiago Minas', 20, 477, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1476, 'Santiago Nacaltepec', 20, 478, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1477, 'Santiago Nejapilla', 20, 479, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1478, 'Santiago Nundiche', 20, 480, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1479, 'Santiago Nuyoó', 20, 481, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1480, 'Santiago Pinotepa Nacional', 20, 482, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1481, 'Santiago Suchilquitongo', 20, 483, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1482, 'Santiago Tamazola', 20, 484, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1483, 'Santiago Tapextla', 20, 485, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1484, 'Villa Tejúpam de la Unión', 20, 486, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1485, 'Santiago Tenango', 20, 487, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1486, 'Santiago Tepetlapa', 20, 488, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1487, 'Santiago Tetepec', 20, 489, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1488, 'Santiago Texcalcingo', 20, 490, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1489, 'Santiago Textitlán', 20, 491, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1490, 'Santiago Tilantongo', 20, 492, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1491, 'Santiago Tillo', 20, 493, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1492, 'Santiago Tlazoyaltepec', 20, 494, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1493, 'Santiago Xanica', 20, 495, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1494, 'Santiago Xiacuí', 20, 496, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1495, 'Santiago Yaitepec', 20, 497, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1496, 'Santiago Yaveo', 20, 498, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1497, 'Santiago Yolomécatl', 20, 499, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1498, 'Santiago Yosondúa', 20, 500, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1499, 'Santiago Yucuyachi', 20, 501, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1500, 'Santiago Zacatepec', 20, 502, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1501, 'Santiago Zoochila', 20, 503, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1502, 'Nuevo Zoquiápam', 20, 504, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1503, 'Santo Domingo Ingenio', 20, 505, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1504, 'Santo Domingo Albarradas', 20, 506, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1505, 'Santo Domingo Armenta', 20, 507, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1506, 'Santo Domingo Chihuitán', 20, 508, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1507, 'Santo Domingo de Morelos', 20, 509, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1508, 'Santo Domingo Ixcatlán', 20, 510, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1509, 'Santo Domingo Nuxaá', 20, 511, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1510, 'Santo Domingo Ozolotepec', 20, 512, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1511, 'Santo Domingo Petapa', 20, 513, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1512, 'Santo Domingo Roayaga', 20, 514, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1513, 'Santo Domingo Tehuantepec', 20, 515, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1514, 'Santo Domingo Teojomulco', 20, 516, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1515, 'Santo Domingo Tepuxtepec', 20, 517, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1516, 'Santo Domingo Tlatayápam', 20, 518, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1517, 'Santo Domingo Tomaltepec', 20, 519, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1518, 'Santo Domingo Tonalá', 20, 520, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1519, 'Santo Domingo Tonaltepec', 20, 521, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1520, 'Santo Domingo Xagacía', 20, 522, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1521, 'Santo Domingo Yanhuitlán', 20, 523, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1522, 'Santo Domingo Yodohino', 20, 524, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1523, 'Santo Domingo Zanatepec', 20, 525, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1524, 'Santos Reyes Nopala', 20, 526, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1525, 'Santos Reyes Pápalo', 20, 527, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1526, 'Santos Reyes Tepejillo', 20, 528, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1527, 'Santos Reyes Yucuná', 20, 529, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1528, 'Santo Tomás Jalieza', 20, 530, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1529, 'Santo Tomás Mazaltepec', 20, 531, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1530, 'Santo Tomás Ocotepec', 20, 532, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1531, 'Santo Tomás Tamazulapan', 20, 533, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1532, 'San Vicente Coatlán', 20, 534, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1533, 'San Vicente Lachixío', 20, 535, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1534, 'San Vicente Nuñú', 20, 536, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1535, 'Silacayoápam', 20, 537, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1536, 'Sitio de Xitlapehua', 20, 538, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1537, 'Soledad Etla', 20, 539, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1538, 'Villa de Tamazulápam del Progreso', 20, 540, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1539, 'Tanetze de Zaragoza', 20, 541, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1540, 'Taniche', 20, 542, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1541, 'Tataltepec de Valdés', 20, 543, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1542, 'Teococuilco de Marcos Pérez', 20, 544, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1543, 'Teotitlán de Flores Magón', 20, 545, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1544, 'Teotitlán del Valle', 20, 546, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1545, 'Teotongo', 20, 547, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1546, 'Tepelmeme Villa de Morelos', 20, 548, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1547, 'Heroica Villa Tezoatlán de Segura y Luna. Cuna de la Independencia de Oaxaca', 20, 549, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1548, 'San Jerónimo Tlacochahuaya', 20, 550, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1549, 'Tlacolula de Matamoros', 20, 551, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1550, 'Tlacotepec Plumas', 20, 552, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1551, 'Tlalixtac de Cabrera', 20, 553, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1552, 'Totontepec Villa de Morelos', 20, 554, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1553, 'Trinidad Zaachila', 20, 555, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1554, 'La Trinidad Vista Hermosa', 20, 556, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1555, 'Unión Hidalgo', 20, 557, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1556, 'Valerio Trujano', 20, 558, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1557, 'San Juan Bautista Valle Nacional', 20, 559, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1558, 'Villa Díaz Ordaz', 20, 560, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1559, 'Yaxe', 20, 561, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1560, 'Magdalena Yodocono de Porfirio Díaz', 20, 562, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1561, 'Yogana', 20, 563, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1562, 'Yutanduchi de Guerrero', 20, 564, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1563, 'Villa de Zaachila', 20, 565, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1564, 'San Mateo Yucutindoo', 20, 566, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1565, 'Zapotitlán Lagunas', 20, 567, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1566, 'Zapotitlán Palmas', 20, 568, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1567, 'Santa Inés de Zaragoza', 20, 569, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1568, 'Zimatlán de Álvarez', 20, 570, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1569, 'Acajete', 21, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1570, 'Acateno', 21, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1571, 'Acatlán', 21, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1572, 'Acatzingo', 21, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1573, 'Acteopan', 21, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1574, 'Ahuacatlán', 21, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1575, 'Ahuatlán', 21, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1576, 'Ahuazotepec', 21, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1577, 'Ahuehuetitla', 21, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1578, 'Ajalpan', 21, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1579, 'Albino Zertuche', 21, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1580, 'Aljojuca', 21, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1581, 'Altepexi', 21, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1582, 'Amixtlán', 21, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1583, 'Amozoc', 21, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1584, 'Aquixtla', 21, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1585, 'Atempan', 21, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1586, 'Atexcal', 21, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1587, 'Atlixco', 21, 19, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1588, 'Atoyatempan', 21, 20, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1589, 'Atzala', 21, 21, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1590, 'Atzitzihuacán', 21, 22, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1591, 'Atzitzintla', 21, 23, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1592, 'Axutla', 21, 24, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1593, 'Ayotoxco de Guerrero', 21, 25, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1594, 'Calpan', 21, 26, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1595, 'Caltepec', 21, 27, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1596, 'Camocuautla', 21, 28, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1597, 'Caxhuacan', 21, 29, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1598, 'Coatepec', 21, 30, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1599, 'Coatzingo', 21, 31, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1600, 'Cohetzala', 21, 32, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1601, 'Cohuecan', 21, 33, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1602, 'Coronango', 21, 34, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1603, 'Coxcatlán', 21, 35, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1604, 'Coyomeapan', 21, 36, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1605, 'Coyotepec', 21, 37, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1606, 'Cuapiaxtla de Madero', 21, 38, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1607, 'Cuautempan', 21, 39, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1608, 'Cuautinchán', 21, 40, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1609, 'Cuautlancingo', 21, 41, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1610, 'Cuayuca de Andrade', 21, 42, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1611, 'Cuetzalan del Progreso', 21, 43, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1612, 'Cuyoaco', 21, 44, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1613, 'Chalchicomula de Sesma', 21, 45, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1614, 'Chapulco', 21, 46, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1615, 'Chiautla', 21, 47, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1616, 'Chiautzingo', 21, 48, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1617, 'Chiconcuautla', 21, 49, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1618, 'Chichiquila', 21, 50, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1619, 'Chietla', 21, 51, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1620, 'Chigmecatitlán', 21, 52, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1621, 'Chignahuapan', 21, 53, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1622, 'Chignautla', 21, 54, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1623, 'Chila', 21, 55, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1624, 'Chila de la Sal', 21, 56, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1625, 'Honey', 21, 57, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1626, 'Chilchotla', 21, 58, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1627, 'Chinantla', 21, 59, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1628, 'Domingo Arenas', 21, 60, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1629, 'Eloxochitlán', 21, 61, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1630, 'Epatlán', 21, 62, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1631, 'Esperanza', 21, 63, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1632, 'Francisco Z. Mena', 21, 64, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1633, 'General Felipe Ángeles', 21, 65, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1634, 'Guadalupe', 21, 66, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1635, 'Guadalupe Victoria', 21, 67, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1636, 'Hermenegildo Galeana', 21, 68, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1637, 'Huaquechula', 21, 69, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1638, 'Huatlatlauca', 21, 70, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1639, 'Huauchinango', 21, 71, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1640, 'Huehuetla', 21, 72, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1641, 'Huehuetlán el Chico', 21, 73, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1642, 'Huejotzingo', 21, 74, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1643, 'Hueyapan', 21, 75, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1644, 'Hueytamalco', 21, 76, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1645, 'Hueytlalpan', 21, 77, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1646, 'Huitzilan de Serdán', 21, 78, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1647, 'Huitziltepec', 21, 79, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1648, 'Atlequizayan', 21, 80, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1649, 'Ixcamilpa de Guerrero', 21, 81, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1650, 'Ixcaquixtla', 21, 82, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1651, 'Ixtacamaxtitlán', 21, 83, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1652, 'Ixtepec', 21, 84, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1653, 'Izúcar de Matamoros', 21, 85, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1654, 'Jalpan', 21, 86, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1655, 'Jolalpan', 21, 87, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1656, 'Jonotla', 21, 88, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1657, 'Jopala', 21, 89, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1658, 'Juan C. Bonilla', 21, 90, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1659, 'Juan Galindo', 21, 91, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1660, 'Juan N. Méndez', 21, 92, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1661, 'Lafragua', 21, 93, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1662, 'Libres', 21, 94, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1663, 'La Magdalena Tlatlauquitepec', 21, 95, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1664, 'Mazapiltepec de Juárez', 21, 96, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1665, 'Mixtla', 21, 97, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1666, 'Molcaxac', 21, 98, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1667, 'Cañada Morelos', 21, 99, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1668, 'Naupan', 21, 100, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1669, 'Nauzontla', 21, 101, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1670, 'Nealtican', 21, 102, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1671, 'Nicolás Bravo', 21, 103, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1672, 'Nopalucan', 21, 104, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1673, 'Ocotepec', 21, 105, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1674, 'Ocoyucan', 21, 106, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1675, 'Olintla', 21, 107, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1676, 'Oriental', 21, 108, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1677, 'Pahuatlán', 21, 109, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1678, 'Palmar de Bravo', 21, 110, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1679, 'Pantepec', 21, 111, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1680, 'Petlalcingo', 21, 112, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1681, 'Piaxtla', 21, 113, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1682, 'Puebla', 21, 114, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1683, 'Quecholac', 21, 115, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1684, 'Quimixtlán', 21, 116, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1685, 'Rafael Lara Grajales', 21, 117, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1686, 'Los Reyes de Juárez', 21, 118, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1687, 'San Andrés Cholula', 21, 119, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1688, 'San Antonio Cañada', 21, 120, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1689, 'San Diego la Mesa Tochimiltzingo', 21, 121, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1690, 'San Felipe Teotlalcingo', 21, 122, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1691, 'San Felipe Tepatlán', 21, 123, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1692, 'San Gabriel Chilac', 21, 124, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1693, 'San Gregorio Atzompa', 21, 125, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1694, 'San Jerónimo Tecuanipan', 21, 126, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1695, 'San Jerónimo Xayacatlán', 21, 127, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1696, 'San José Chiapa', 21, 128, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1697, 'San José Miahuatlán', 21, 129, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1698, 'San Juan Atenco', 21, 130, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1699, 'San Juan Atzompa', 21, 131, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1700, 'San Martín Texmelucan', 21, 132, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1701, 'San Martín Totoltepec', 21, 133, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1702, 'San Matías Tlalancaleca', 21, 134, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1703, 'San Miguel Ixitlán', 21, 135, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1704, 'San Miguel Xoxtla', 21, 136, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1705, 'San Nicolás Buenos Aires', 21, 137, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1706, 'San Nicolás de los Ranchos', 21, 138, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1707, 'San Pablo Anicano', 21, 139, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1708, 'San Pedro Cholula', 21, 140, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1709, 'San Pedro Yeloixtlahuaca', 21, 141, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1710, 'San Salvador el Seco', 21, 142, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1711, 'San Salvador el Verde', 21, 143, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1712, 'San Salvador Huixcolotla', 21, 144, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1713, 'San Sebastián Tlacotepec', 21, 145, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1714, 'Santa Catarina Tlaltempan', 21, 146, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1715, 'Santa Inés Ahuatempan', 21, 147, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1716, 'Santa Isabel Cholula', 21, 148, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1717, 'Santiago Miahuatlán', 21, 149, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1718, 'Huehuetlán el Grande', 21, 150, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1719, 'Santo Tomás Hueyotlipan', 21, 151, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1720, 'Soltepec', 21, 152, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1721, 'Tecali de Herrera', 21, 153, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1722, 'Tecamachalco', 21, 154, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1723, 'Tecomatlán', 21, 155, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1724, 'Tehuacán', 21, 156, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1725, 'Tehuitzingo', 21, 157, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1726, 'Tenampulco', 21, 158, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1727, 'Teopantlán', 21, 159, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1728, 'Teotlalco', 21, 160, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1729, 'Tepanco de López', 21, 161, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1730, 'Tepango de Rodríguez', 21, 162, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1731, 'Tepatlaxco de Hidalgo', 21, 163, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1732, 'Tepeaca', 21, 164, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1733, 'Tepemaxalco', 21, 165, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1734, 'Tepeojuma', 21, 166, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1735, 'Tepetzintla', 21, 167, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1736, 'Tepexco', 21, 168, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1737, 'Tepexi de Rodríguez', 21, 169, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1738, 'Tepeyahualco', 21, 170, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1739, 'Tepeyahualco de Cuauhtémoc', 21, 171, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1740, 'Tetela de Ocampo', 21, 172, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1741, 'Teteles de Avila Castillo', 21, 173, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1742, 'Teziutlán', 21, 174, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1743, 'Tianguismanalco', 21, 175, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1744, 'Tilapa', 21, 176, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1745, 'Tlacotepec de Benito Juárez', 21, 177, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1746, 'Tlacuilotepec', 21, 178, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1747, 'Tlachichuca', 21, 179, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1748, 'Tlahuapan', 21, 180, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1749, 'Tlaltenango', 21, 181, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1750, 'Tlanepantla', 21, 182, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1751, 'Tlaola', 21, 183, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1752, 'Tlapacoya', 21, 184, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1753, 'Tlapanalá', 21, 185, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1754, 'Tlatlauquitepec', 21, 186, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1755, 'Tlaxco', 21, 187, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1756, 'Tochimilco', 21, 188, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1757, 'Tochtepec', 21, 189, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1758, 'Totoltepec de Guerrero', 21, 190, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1759, 'Tulcingo', 21, 191, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1760, 'Tuzamapan de Galeana', 21, 192, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1761, 'Tzicatlacoyan', 21, 193, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1762, 'Venustiano Carranza', 21, 194, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1763, 'Vicente Guerrero', 21, 195, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1764, 'Xayacatlán de Bravo', 21, 196, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1765, 'Xicotepec', 21, 197, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1766, 'Xicotlán', 21, 198, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1767, 'Xiutetelco', 21, 199, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1768, 'Xochiapulco', 21, 200, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1769, 'Xochiltepec', 21, 201, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1770, 'Xochitlán de Vicente Suárez', 21, 202, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1771, 'Xochitlán Todos Santos', 21, 203, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1772, 'Yaonáhuac', 21, 204, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1773, 'Yehualtepec', 21, 205, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1774, 'Zacapala', 21, 206, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1775, 'Zacapoaxtla', 21, 207, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1776, 'Zacatlán', 21, 208, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1777, 'Zapotitlán', 21, 209, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1778, 'Zapotitlán de Méndez', 21, 210, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1779, 'Zaragoza', 21, 211, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1780, 'Zautla', 21, 212, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1781, 'Zihuateutla', 21, 213, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1782, 'Zinacatepec', 21, 214, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1783, 'Zongozotla', 21, 215, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1784, 'Zoquiapan', 21, 216, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1785, 'Zoquitlán', 21, 217, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1786, 'Amealco de Bonfil', 22, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1787, 'Pinal de Amoles', 22, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1788, 'Arroyo Seco', 22, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1789, 'Cadereyta de Montes', 22, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1790, 'Colón', 22, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1791, 'Corregidora', 22, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1792, 'Ezequiel Montes', 22, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1793, 'Huimilpan', 22, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1794, 'Jalpan de Serra', 22, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1795, 'Landa de Matamoros', 22, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1796, 'El Marqués', 22, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1797, 'Pedro Escobedo', 22, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1798, 'Peñamiller', 22, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1799, 'Querétaro', 22, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1800, 'San Joaquín', 22, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1801, 'San Juan del Río', 22, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1802, 'Tequisquiapan', 22, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1803, 'Tolimán', 22, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1804, 'Cozumel', 23, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1805, 'Felipe Carrillo Puerto', 23, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1806, 'Isla Mujeres', 23, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1807, 'Othón P. Blanco', 23, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1808, 'Benito Juárez', 23, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1809, 'José María Morelos', 23, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1810, 'Lázaro Cárdenas', 23, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1811, 'Solidaridad', 23, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1812, 'Tulum', 23, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1813, 'Bacalar', 23, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1814, 'Ahualulco', 24, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1815, 'Alaquines', 24, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1816, 'Aquismón', 24, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1817, 'Armadillo de los Infante', 24, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1818, 'Cárdenas', 24, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1819, 'Catorce', 24, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1820, 'Cedral', 24, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1821, 'Cerritos', 24, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1822, 'Cerro de San Pedro', 24, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1823, 'Ciudad del Maíz', 24, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1824, 'Ciudad Fernández', 24, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1825, 'Tancanhuitz', 24, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1826, 'Ciudad Valles', 24, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1827, 'Coxcatlán', 24, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1828, 'Charcas', 24, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1829, 'Ebano', 24, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1830, 'Guadalcázar', 24, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1831, 'Huehuetlán', 24, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1832, 'Lagunillas', 24, 19, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1833, 'Matehuala', 24, 20, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1834, 'Mexquitic de Carmona', 24, 21, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1835, 'Moctezuma', 24, 22, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1836, 'Rayón', 24, 23, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1837, 'Rioverde', 24, 24, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1838, 'Salinas', 24, 25, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1839, 'San Antonio', 24, 26, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1840, 'San Ciro de Acosta', 24, 27, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1841, 'San Luis Potosí', 24, 28, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1842, 'San Martín Chalchicuautla', 24, 29, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1843, 'San Nicolás Tolentino', 24, 30, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1844, 'Santa Catarina', 24, 31, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1845, 'Santa María del Río', 24, 32, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1846, 'Santo Domingo', 24, 33, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1847, 'San Vicente Tancuayalab', 24, 34, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1848, 'Soledad de Graciano Sánchez', 24, 35, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1849, 'Tamasopo', 24, 36, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1850, 'Tamazunchale', 24, 37, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1851, 'Tampacán', 24, 38, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1852, 'Tampamolón Corona', 24, 39, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1853, 'Tamuín', 24, 40, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1854, 'Tanlajás', 24, 41, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1855, 'Tanquián de Escobedo', 24, 42, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1856, 'Tierra Nueva', 24, 43, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1857, 'Vanegas', 24, 44, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1858, 'Venado', 24, 45, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1859, 'Villa de Arriaga', 24, 46, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1860, 'Villa de Guadalupe', 24, 47, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1861, 'Villa de la Paz', 24, 48, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1862, 'Villa de Ramos', 24, 49, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1863, 'Villa de Reyes', 24, 50, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1864, 'Villa Hidalgo', 24, 51, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1865, 'Villa Juárez', 24, 52, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1866, 'Axtla de Terrazas', 24, 53, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1867, 'Xilitla', 24, 54, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1868, 'Zaragoza', 24, 55, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1869, 'Villa de Arista', 24, 56, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1870, 'Matlapa', 24, 57, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1871, 'El Naranjo', 24, 58, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1872, 'Ahome', 25, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1873, 'Angostura', 25, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1874, 'Badiraguato', 25, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1875, 'Concordia', 25, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1876, 'Cosalá', 25, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1877, 'Culiacán', 25, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1878, 'Choix', 25, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1879, 'Elota', 25, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1880, 'Escuinapa', 25, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1881, 'El Fuerte', 25, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1882, 'Guasave', 25, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1883, 'Mazatlán', 25, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1884, 'Mocorito', 25, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1885, 'Rosario', 25, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1886, 'Salvador Alvarado', 25, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1887, 'San Ignacio', 25, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1888, 'Sinaloa', 25, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1889, 'Navolato', 25, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1890, 'Aconchi', 26, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1891, 'Agua Prieta', 26, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1892, 'Alamos', 26, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1893, 'Altar', 26, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1894, 'Arivechi', 26, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1895, 'Arizpe', 26, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1896, 'Atil', 26, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1897, 'Bacadéhuachi', 26, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1898, 'Bacanora', 26, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1899, 'Bacerac', 26, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1900, 'Bacoachi', 26, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1901, 'Bácum', 26, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1902, 'Banámichi', 26, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1903, 'Baviácora', 26, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1904, 'Bavispe', 26, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1905, 'Benjamín Hill', 26, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1906, 'Caborca', 26, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1907, 'Cajeme', 26, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1908, 'Cananea', 26, 19, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1909, 'Carbó', 26, 20, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1910, 'La Colorada', 26, 21, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1911, 'Cucurpe', 26, 22, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1912, 'Cumpas', 26, 23, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1913, 'Divisaderos', 26, 24, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1914, 'Empalme', 26, 25, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1915, 'Etchojoa', 26, 26, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1916, 'Fronteras', 26, 27, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1917, 'Granados', 26, 28, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1918, 'Guaymas', 26, 29, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1919, 'Hermosillo', 26, 30, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1920, 'Huachinera', 26, 31, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1921, 'Huásabas', 26, 32, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1922, 'Huatabampo', 26, 33, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1923, 'Huépac', 26, 34, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1924, 'Imuris', 26, 35, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1925, 'Magdalena', 26, 36, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1926, 'Mazatán', 26, 37, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1927, 'Moctezuma', 26, 38, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1928, 'Naco', 26, 39, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1929, 'Nácori Chico', 26, 40, '2018-06-27 21:32:25', '2018-06-27 21:32:25');
INSERT INTO `cities` (`id`, `name`, `state_id`, `number`, `created_at`, `updated_at`) VALUES
(1930, 'Nacozari de García', 26, 41, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1931, 'Navojoa', 26, 42, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1932, 'Nogales', 26, 43, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1933, 'Onavas', 26, 44, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1934, 'Opodepe', 26, 45, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1935, 'Oquitoa', 26, 46, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1936, 'Pitiquito', 26, 47, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1937, 'Puerto Peñasco', 26, 48, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1938, 'Quiriego', 26, 49, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1939, 'Rayón', 26, 50, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1940, 'Rosario', 26, 51, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1941, 'Sahuaripa', 26, 52, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1942, 'San Felipe de Jesús', 26, 53, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1943, 'San Javier', 26, 54, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1944, 'San Luis Río Colorado', 26, 55, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1945, 'San Miguel de Horcasitas', 26, 56, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1946, 'San Pedro de la Cueva', 26, 57, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1947, 'Santa Ana', 26, 58, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1948, 'Santa Cruz', 26, 59, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1949, 'Sáric', 26, 60, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1950, 'Soyopa', 26, 61, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1951, 'Suaqui Grande', 26, 62, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1952, 'Tepache', 26, 63, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1953, 'Trincheras', 26, 64, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1954, 'Tubutama', 26, 65, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1955, 'Ures', 26, 66, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1956, 'Villa Hidalgo', 26, 67, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1957, 'Villa Pesqueira', 26, 68, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1958, 'Yécora', 26, 69, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1959, 'General Plutarco Elías Calles', 26, 70, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1960, 'Benito Juárez', 26, 71, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1961, 'San Ignacio Río Muerto', 26, 72, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1962, 'Balancán', 27, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1963, 'Cárdenas', 27, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1964, 'Centla', 27, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1965, 'Centro', 27, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1966, 'Comalcalco', 27, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1967, 'Cunduacán', 27, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1968, 'Emiliano Zapata', 27, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1969, 'Huimanguillo', 27, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1970, 'Jalapa', 27, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1971, 'Jalpa de Méndez', 27, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1972, 'Jonuta', 27, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1973, 'Macuspana', 27, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1974, 'Nacajuca', 27, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1975, 'Paraíso', 27, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1976, 'Tacotalpa', 27, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1977, 'Teapa', 27, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1978, 'Tenosique', 27, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1979, 'Abasolo', 28, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1980, 'Aldama', 28, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1981, 'Altamira', 28, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1982, 'Antiguo Morelos', 28, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1983, 'Burgos', 28, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1984, 'Bustamante', 28, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1985, 'Camargo', 28, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1986, 'Casas', 28, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1987, 'Ciudad Madero', 28, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1988, 'Cruillas', 28, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1989, 'Gómez Farías', 28, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1990, 'González', 28, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1991, 'Güémez', 28, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1992, 'Guerrero', 28, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1993, 'Gustavo Díaz Ordaz', 28, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1994, 'Hidalgo', 28, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1995, 'Jaumave', 28, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1996, 'Jiménez', 28, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1997, 'Llera', 28, 19, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1998, 'Mainero', 28, 20, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(1999, 'El Mante', 28, 21, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2000, 'Matamoros', 28, 22, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2001, 'Méndez', 28, 23, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2002, 'Mier', 28, 24, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2003, 'Miguel Alemán', 28, 25, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2004, 'Miquihuana', 28, 26, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2005, 'Nuevo Laredo', 28, 27, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2006, 'Nuevo Morelos', 28, 28, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2007, 'Ocampo', 28, 29, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2008, 'Padilla', 28, 30, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2009, 'Palmillas', 28, 31, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2010, 'Reynosa', 28, 32, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2011, 'Río Bravo', 28, 33, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2012, 'San Carlos', 28, 34, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2013, 'San Fernando', 28, 35, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2014, 'San Nicolás', 28, 36, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2015, 'Soto la Marina', 28, 37, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2016, 'Tampico', 28, 38, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2017, 'Tula', 28, 39, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2018, 'Valle Hermoso', 28, 40, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2019, 'Victoria', 28, 41, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2020, 'Villagrán', 28, 42, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2021, 'Xicoténcatl', 28, 43, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2022, 'Amaxac de Guerrero', 29, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2023, 'Apetatitlán de Antonio Carvajal', 29, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2024, 'Atlangatepec', 29, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2025, 'Atltzayanca', 29, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2026, 'Apizaco', 29, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2027, 'Calpulalpan', 29, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2028, 'El Carmen Tequexquitla', 29, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2029, 'Cuapiaxtla', 29, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2030, 'Cuaxomulco', 29, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2031, 'Chiautempan', 29, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2032, 'Muñoz de Domingo Arenas', 29, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2033, 'Españita', 29, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2034, 'Huamantla', 29, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2035, 'Hueyotlipan', 29, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2036, 'Ixtacuixtla de Mariano Matamoros', 29, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2037, 'Ixtenco', 29, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2038, 'Mazatecochco de José María Morelos', 29, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2039, 'Contla de Juan Cuamatzi', 29, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2040, 'Tepetitla de Lardizábal', 29, 19, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2041, 'Sanctórum de Lázaro Cárdenas', 29, 20, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2042, 'Nanacamilpa de Mariano Arista', 29, 21, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2043, 'Acuamanala de Miguel Hidalgo', 29, 22, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2044, 'Natívitas', 29, 23, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2045, 'Panotla', 29, 24, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2046, 'San Pablo del Monte', 29, 25, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2047, 'Santa Cruz Tlaxcala', 29, 26, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2048, 'Tenancingo', 29, 27, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2049, 'Teolocholco', 29, 28, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2050, 'Tepeyanco', 29, 29, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2051, 'Terrenate', 29, 30, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2052, 'Tetla de la Solidaridad', 29, 31, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2053, 'Tetlatlahuca', 29, 32, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2054, 'Tlaxcala', 29, 33, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2055, 'Tlaxco', 29, 34, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2056, 'Tocatlán', 29, 35, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2057, 'Totolac', 29, 36, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2058, 'Ziltlaltépec de Trinidad Sánchez Santos', 29, 37, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2059, 'Tzompantepec', 29, 38, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2060, 'Xaloztoc', 29, 39, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2061, 'Xaltocan', 29, 40, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2062, 'Papalotla de Xicohténcatl', 29, 41, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2063, 'Xicohtzinco', 29, 42, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2064, 'Yauhquemehcan', 29, 43, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2065, 'Zacatelco', 29, 44, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2066, 'Benito Juárez', 29, 45, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2067, 'Emiliano Zapata', 29, 46, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2068, 'Lázaro Cárdenas', 29, 47, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2069, 'La Magdalena Tlaltelulco', 29, 48, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2070, 'San Damián Texóloc', 29, 49, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2071, 'San Francisco Tetlanohcan', 29, 50, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2072, 'San Jerónimo Zacualpan', 29, 51, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2073, 'San José Teacalco', 29, 52, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2074, 'San Juan Huactzinco', 29, 53, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2075, 'San Lorenzo Axocomanitla', 29, 54, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2076, 'San Lucas Tecopilco', 29, 55, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2077, 'Santa Ana Nopalucan', 29, 56, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2078, 'Santa Apolonia Teacalco', 29, 57, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2079, 'Santa Catarina Ayometla', 29, 58, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2080, 'Santa Cruz Quilehtla', 29, 59, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2081, 'Santa Isabel Xiloxoxtla', 29, 60, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2082, 'Acajete', 30, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2083, 'Acatlán', 30, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2084, 'Acayucan', 30, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2085, 'Actopan', 30, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2086, 'Acula', 30, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2087, 'Acultzingo', 30, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2088, 'Camarón de Tejeda', 30, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2089, 'Alpatláhuac', 30, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2090, 'Alto Lucero de Gutiérrez Barrios', 30, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2091, 'Altotonga', 30, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2092, 'Alvarado', 30, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2093, 'Amatitlán', 30, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2094, 'Naranjos Amatlán', 30, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2095, 'Amatlán de los Reyes', 30, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2096, 'Angel R. Cabada', 30, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2097, 'La Antigua', 30, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2098, 'Apazapan', 30, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2099, 'Aquila', 30, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2100, 'Astacinga', 30, 19, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2101, 'Atlahuilco', 30, 20, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2102, 'Atoyac', 30, 21, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2103, 'Atzacan', 30, 22, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2104, 'Atzalan', 30, 23, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2105, 'Tlaltetela', 30, 24, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2106, 'Ayahualulco', 30, 25, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2107, 'Banderilla', 30, 26, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2108, 'Benito Juárez', 30, 27, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2109, 'Boca del Río', 30, 28, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2110, 'Calcahualco', 30, 29, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2111, 'Camerino Z. Mendoza', 30, 30, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2112, 'Carrillo Puerto', 30, 31, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2113, 'Catemaco', 30, 32, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2114, 'Cazones de Herrera', 30, 33, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2115, 'Cerro Azul', 30, 34, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2116, 'Citlaltépetl', 30, 35, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2117, 'Coacoatzintla', 30, 36, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2118, 'Coahuitlán', 30, 37, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2119, 'Coatepec', 30, 38, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2120, 'Coatzacoalcos', 30, 39, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2121, 'Coatzintla', 30, 40, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2122, 'Coetzala', 30, 41, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2123, 'Colipa', 30, 42, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2124, 'Comapa', 30, 43, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2125, 'Córdoba', 30, 44, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2126, 'Cosamaloapan de Carpio', 30, 45, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2127, 'Cosautlán de Carvajal', 30, 46, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2128, 'Coscomatepec', 30, 47, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2129, 'Cosoleacaque', 30, 48, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2130, 'Cotaxtla', 30, 49, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2131, 'Coxquihui', 30, 50, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2132, 'Coyutla', 30, 51, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2133, 'Cuichapa', 30, 52, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2134, 'Cuitláhuac', 30, 53, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2135, 'Chacaltianguis', 30, 54, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2136, 'Chalma', 30, 55, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2137, 'Chiconamel', 30, 56, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2138, 'Chiconquiaco', 30, 57, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2139, 'Chicontepec', 30, 58, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2140, 'Chinameca', 30, 59, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2141, 'Chinampa de Gorostiza', 30, 60, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2142, 'Las Choapas', 30, 61, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2143, 'Chocamán', 30, 62, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2144, 'Chontla', 30, 63, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2145, 'Chumatlán', 30, 64, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2146, 'Emiliano Zapata', 30, 65, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2147, 'Espinal', 30, 66, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2148, 'Filomeno Mata', 30, 67, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2149, 'Fortín', 30, 68, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2150, 'Gutiérrez Zamora', 30, 69, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2151, 'Hidalgotitlán', 30, 70, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2152, 'Huatusco', 30, 71, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2153, 'Huayacocotla', 30, 72, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2154, 'Hueyapan de Ocampo', 30, 73, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2155, 'Huiloapan de Cuauhtémoc', 30, 74, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2156, 'Ignacio de la Llave', 30, 75, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2157, 'Ilamatlán', 30, 76, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2158, 'Isla', 30, 77, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2159, 'Ixcatepec', 30, 78, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2160, 'Ixhuacán de los Reyes', 30, 79, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2161, 'Ixhuatlán del Café', 30, 80, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2162, 'Ixhuatlancillo', 30, 81, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2163, 'Ixhuatlán del Sureste', 30, 82, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2164, 'Ixhuatlán de Madero', 30, 83, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2165, 'Ixmatlahuacan', 30, 84, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2166, 'Ixtaczoquitlán', 30, 85, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2167, 'Jalacingo', 30, 86, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2168, 'Xalapa', 30, 87, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2169, 'Jalcomulco', 30, 88, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2170, 'Jáltipan', 30, 89, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2171, 'Jamapa', 30, 90, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2172, 'Jesús Carranza', 30, 91, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2173, 'Xico', 30, 92, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2174, 'Jilotepec', 30, 93, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2175, 'Juan Rodríguez Clara', 30, 94, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2176, 'Juchique de Ferrer', 30, 95, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2177, 'Landero y Coss', 30, 96, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2178, 'Lerdo de Tejada', 30, 97, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2179, 'Magdalena', 30, 98, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2180, 'Maltrata', 30, 99, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2181, 'Manlio Fabio Altamirano', 30, 100, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2182, 'Mariano Escobedo', 30, 101, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2183, 'Martínez de la Torre', 30, 102, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2184, 'Mecatlán', 30, 103, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2185, 'Mecayapan', 30, 104, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2186, 'Medellín de Bravo', 30, 105, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2187, 'Miahuatlán', 30, 106, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2188, 'Las Minas', 30, 107, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2189, 'Minatitlán', 30, 108, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2190, 'Misantla', 30, 109, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2191, 'Mixtla de Altamirano', 30, 110, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2192, 'Moloacán', 30, 111, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2193, 'Naolinco', 30, 112, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2194, 'Naranjal', 30, 113, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2195, 'Nautla', 30, 114, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2196, 'Nogales', 30, 115, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2197, 'Oluta', 30, 116, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2198, 'Omealca', 30, 117, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2199, 'Orizaba', 30, 118, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2200, 'Otatitlán', 30, 119, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2201, 'Oteapan', 30, 120, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2202, 'Ozuluama de Mascareñas', 30, 121, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2203, 'Pajapan', 30, 122, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2204, 'Pánuco', 30, 123, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2205, 'Papantla', 30, 124, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2206, 'Paso del Macho', 30, 125, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2207, 'Paso de Ovejas', 30, 126, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2208, 'La Perla', 30, 127, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2209, 'Perote', 30, 128, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2210, 'Platón Sánchez', 30, 129, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2211, 'Playa Vicente', 30, 130, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2212, 'Poza Rica de Hidalgo', 30, 131, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2213, 'Las Vigas de Ramírez', 30, 132, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2214, 'Pueblo Viejo', 30, 133, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2215, 'Puente Nacional', 30, 134, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2216, 'Rafael Delgado', 30, 135, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2217, 'Rafael Lucio', 30, 136, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2218, 'Los Reyes', 30, 137, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2219, 'Río Blanco', 30, 138, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2220, 'Saltabarranca', 30, 139, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2221, 'San Andrés Tenejapan', 30, 140, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2222, 'San Andrés Tuxtla', 30, 141, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2223, 'San Juan Evangelista', 30, 142, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2224, 'Santiago Tuxtla', 30, 143, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2225, 'Sayula de Alemán', 30, 144, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2226, 'Soconusco', 30, 145, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2227, 'Sochiapa', 30, 146, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2228, 'Soledad Atzompa', 30, 147, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2229, 'Soledad de Doblado', 30, 148, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2230, 'Soteapan', 30, 149, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2231, 'Tamalín', 30, 150, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2232, 'Tamiahua', 30, 151, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2233, 'Tampico Alto', 30, 152, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2234, 'Tancoco', 30, 153, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2235, 'Tantima', 30, 154, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2236, 'Tantoyuca', 30, 155, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2237, 'Tatatila', 30, 156, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2238, 'Castillo de Teayo', 30, 157, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2239, 'Tecolutla', 30, 158, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2240, 'Tehuipango', 30, 159, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2241, 'Álamo Temapache', 30, 160, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2242, 'Tempoal', 30, 161, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2243, 'Tenampa', 30, 162, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2244, 'Tenochtitlán', 30, 163, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2245, 'Teocelo', 30, 164, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2246, 'Tepatlaxco', 30, 165, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2247, 'Tepetlán', 30, 166, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2248, 'Tepetzintla', 30, 167, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2249, 'Tequila', 30, 168, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2250, 'José Azueta', 30, 169, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2251, 'Texcatepec', 30, 170, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2252, 'Texhuacán', 30, 171, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2253, 'Texistepec', 30, 172, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2254, 'Tezonapa', 30, 173, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2255, 'Tierra Blanca', 30, 174, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2256, 'Tihuatlán', 30, 175, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2257, 'Tlacojalpan', 30, 176, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2258, 'Tlacolulan', 30, 177, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2259, 'Tlacotalpan', 30, 178, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2260, 'Tlacotepec de Mejía', 30, 179, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2261, 'Tlachichilco', 30, 180, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2262, 'Tlalixcoyan', 30, 181, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2263, 'Tlalnelhuayocan', 30, 182, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2264, 'Tlapacoyan', 30, 183, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2265, 'Tlaquilpa', 30, 184, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2266, 'Tlilapan', 30, 185, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2267, 'Tomatlán', 30, 186, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2268, 'Tonayán', 30, 187, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2269, 'Totutla', 30, 188, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2270, 'Tuxpan', 30, 189, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2271, 'Tuxtilla', 30, 190, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2272, 'Ursulo Galván', 30, 191, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2273, 'Vega de Alatorre', 30, 192, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2274, 'Veracruz', 30, 193, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2275, 'Villa Aldama', 30, 194, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2276, 'Xoxocotla', 30, 195, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2277, 'Yanga', 30, 196, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2278, 'Yecuatla', 30, 197, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2279, 'Zacualpan', 30, 198, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2280, 'Zaragoza', 30, 199, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2281, 'Zentla', 30, 200, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2282, 'Zongolica', 30, 201, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2283, 'Zontecomatlán de López y Fuentes', 30, 202, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2284, 'Zozocolco de Hidalgo', 30, 203, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2285, 'Agua Dulce', 30, 204, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2286, 'El Higo', 30, 205, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2287, 'Nanchital de Lázaro Cárdenas del Río', 30, 206, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2288, 'Tres Valles', 30, 207, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2289, 'Carlos A. Carrillo', 30, 208, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2290, 'Tatahuicapan de Juárez', 30, 209, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2291, 'Uxpanapa', 30, 210, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2292, 'San Rafael', 30, 211, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2293, 'Santiago Sochiapan', 30, 212, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2294, 'Abalá', 31, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2295, 'Acanceh', 31, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2296, 'Akil', 31, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2297, 'Baca', 31, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2298, 'Bokobá', 31, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2299, 'Buctzotz', 31, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2300, 'Cacalchén', 31, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2301, 'Calotmul', 31, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2302, 'Cansahcab', 31, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2303, 'Cantamayec', 31, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2304, 'Celestún', 31, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2305, 'Cenotillo', 31, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2306, 'Conkal', 31, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2307, 'Cuncunul', 31, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2308, 'Cuzamá', 31, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2309, 'Chacsinkín', 31, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2310, 'Chankom', 31, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2311, 'Chapab', 31, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2312, 'Chemax', 31, 19, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2313, 'Chicxulub Pueblo', 31, 20, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2314, 'Chichimilá', 31, 21, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2315, 'Chikindzonot', 31, 22, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2316, 'Chocholá', 31, 23, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2317, 'Chumayel', 31, 24, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2318, 'Dzán', 31, 25, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2319, 'Dzemul', 31, 26, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2320, 'Dzidzantún', 31, 27, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2321, 'Dzilam de Bravo', 31, 28, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2322, 'Dzilam González', 31, 29, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2323, 'Dzitás', 31, 30, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2324, 'Dzoncauich', 31, 31, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2325, 'Espita', 31, 32, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2326, 'Halachó', 31, 33, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2327, 'Hocabá', 31, 34, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2328, 'Hoctún', 31, 35, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2329, 'Homún', 31, 36, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2330, 'Huhí', 31, 37, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2331, 'Hunucmá', 31, 38, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2332, 'Ixil', 31, 39, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2333, 'Izamal', 31, 40, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2334, 'Kanasín', 31, 41, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2335, 'Kantunil', 31, 42, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2336, 'Kaua', 31, 43, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2337, 'Kinchil', 31, 44, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2338, 'Kopomá', 31, 45, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2339, 'Mama', 31, 46, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2340, 'Maní', 31, 47, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2341, 'Maxcanú', 31, 48, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2342, 'Mayapán', 31, 49, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2343, 'Mérida', 31, 50, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2344, 'Mocochá', 31, 51, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2345, 'Motul', 31, 52, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2346, 'Muna', 31, 53, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2347, 'Muxupip', 31, 54, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2348, 'Opichén', 31, 55, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2349, 'Oxkutzcab', 31, 56, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2350, 'Panabá', 31, 57, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2351, 'Peto', 31, 58, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2352, 'Progreso', 31, 59, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2353, 'Quintana Roo', 31, 60, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2354, 'Río Lagartos', 31, 61, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2355, 'Sacalum', 31, 62, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2356, 'Samahil', 31, 63, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2357, 'Sanahcat', 31, 64, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2358, 'San Felipe', 31, 65, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2359, 'Santa Elena', 31, 66, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2360, 'Seyé', 31, 67, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2361, 'Sinanché', 31, 68, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2362, 'Sotuta', 31, 69, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2363, 'Sucilá', 31, 70, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2364, 'Sudzal', 31, 71, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2365, 'Suma', 31, 72, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2366, 'Tahdziú', 31, 73, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2367, 'Tahmek', 31, 74, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2368, 'Teabo', 31, 75, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2369, 'Tecoh', 31, 76, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2370, 'Tekal de Venegas', 31, 77, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2371, 'Tekantó', 31, 78, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2372, 'Tekax', 31, 79, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2373, 'Tekit', 31, 80, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2374, 'Tekom', 31, 81, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2375, 'Telchac Pueblo', 31, 82, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2376, 'Telchac Puerto', 31, 83, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2377, 'Temax', 31, 84, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2378, 'Temozón', 31, 85, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2379, 'Tepakán', 31, 86, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2380, 'Tetiz', 31, 87, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2381, 'Teya', 31, 88, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2382, 'Ticul', 31, 89, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2383, 'Timucuy', 31, 90, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2384, 'Tinum', 31, 91, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2385, 'Tixcacalcupul', 31, 92, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2386, 'Tixkokob', 31, 93, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2387, 'Tixmehuac', 31, 94, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2388, 'Tixpéhual', 31, 95, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2389, 'Tizimín', 31, 96, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2390, 'Tunkás', 31, 97, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2391, 'Tzucacab', 31, 98, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2392, 'Uayma', 31, 99, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2393, 'Ucú', 31, 100, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2394, 'Umán', 31, 101, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2395, 'Valladolid', 31, 102, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2396, 'Xocchel', 31, 103, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2397, 'Yaxcabá', 31, 104, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2398, 'Yaxkukul', 31, 105, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2399, 'Yobaín', 31, 106, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2400, 'Apozol', 32, 1, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2401, 'Apulco', 32, 2, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2402, 'Atolinga', 32, 3, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2403, 'Benito Juárez', 32, 4, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2404, 'Calera', 32, 5, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2405, 'Cañitas de Felipe Pescador', 32, 6, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2406, 'Concepción del Oro', 32, 7, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2407, 'Cuauhtémoc', 32, 8, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2408, 'Chalchihuites', 32, 9, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2409, 'Fresnillo', 32, 10, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2410, 'Trinidad García de la Cadena', 32, 11, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2411, 'Genaro Codina', 32, 12, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2412, 'General Enrique Estrada', 32, 13, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2413, 'General Francisco R. Murguía', 32, 14, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2414, 'El Plateado de Joaquín Amaro', 32, 15, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2415, 'General Pánfilo Natera', 32, 16, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2416, 'Guadalupe', 32, 17, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2417, 'Huanusco', 32, 18, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2418, 'Jalpa', 32, 19, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2419, 'Jerez', 32, 20, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2420, 'Jiménez del Teul', 32, 21, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2421, 'Juan Aldama', 32, 22, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2422, 'Juchipila', 32, 23, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2423, 'Loreto', 32, 24, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2424, 'Luis Moya', 32, 25, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2425, 'Mazapil', 32, 26, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2426, 'Melchor Ocampo', 32, 27, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2427, 'Mezquital del Oro', 32, 28, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2428, 'Miguel Auza', 32, 29, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2429, 'Momax', 32, 30, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2430, 'Monte Escobedo', 32, 31, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2431, 'Morelos', 32, 32, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2432, 'Moyahua de Estrada', 32, 33, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2433, 'Nochistlán de Mejía', 32, 34, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2434, 'Noria de Ángeles', 32, 35, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2435, 'Ojocaliente', 32, 36, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2436, 'Pánuco', 32, 37, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2437, 'Pinos', 32, 38, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2438, 'Río Grande', 32, 39, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2439, 'Sain Alto', 32, 40, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2440, 'El Salvador', 32, 41, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2441, 'Sombrerete', 32, 42, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2442, 'Susticacán', 32, 43, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2443, 'Tabasco', 32, 44, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2444, 'Tepechitlán', 32, 45, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2445, 'Tepetongo', 32, 46, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2446, 'Teúl de González Ortega', 32, 47, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2447, 'Tlaltenango de Sánchez Román', 32, 48, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2448, 'Valparaíso', 32, 49, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2449, 'Vetagrande', 32, 50, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2450, 'Villa de Cos', 32, 51, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2451, 'Villa García', 32, 52, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2452, 'Villa González Ortega', 32, 53, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2453, 'Villa Hidalgo', 32, 54, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2454, 'Villanueva', 32, 55, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2455, 'Zacatecas', 32, 56, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2456, 'Trancoso', 32, 57, '2018-06-27 21:32:25', '2018-06-27 21:32:25'),
(2457, 'Santa María de la Paz', 32, 58, '2018-06-27 21:32:25', '2018-06-27 21:32:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `name`, `short_name`, `created_at`, `updated_at`) VALUES
(1, 'Mexico', 'MX', '2018-06-27 21:32:23', '2018-06-27 21:32:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `education_data`
--

DROP TABLE IF EXISTS `education_data`;
CREATE TABLE `education_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `candidate_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `universidad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_month` int(11) NOT NULL,
  `start_year` int(11) NOT NULL,
  `end_month` int(11) NOT NULL,
  `end_year` int(11) NOT NULL,
  `academic_level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `degree_obtained` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `additional_info` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `education_data`
--

INSERT INTO `education_data` (`id`, `candidate_id`, `universidad`, `start_month`, `start_year`, `end_month`, `end_year`, `academic_level`, `degree_obtained`, `additional_info`, `created_at`, `updated_at`) VALUES
(14, '5', 'Quos in voluptatibus dolores est suscipit nostrum tempore sed odit', 11, 2014, 2, 1964, 'Aute est fugiat adipisci laborum fugiat soluta nulla tempore nisi amet ea libero sunt laborum', 'Anim rem ipsam deleniti nihil non', 'Ut iure voluptate et laborum Rerum quasi harum velit quas excepturi repellendus', '2018-09-26 20:07:48', '2018-09-26 20:07:48'),
(15, '6', 'Vitae consectetur non tempora labore sed veritatis est omnis ullam accusamus libero aut eos', 11, 1973, 9, 1988, 'Exercitation esse magna omnis temporibus', 'Fugiat non autem porro voluptas cupidatat iste corrupti in at accusantium voluptas eveniet aliquip reiciendis nulla in incidunt eum', 'Cupiditate cillum perferendis omnis labore ea assumenda nobis enim enim sed ut elit a quasi', '2018-09-26 20:23:15', '2018-09-26 20:23:15'),
(16, '7', 'Vitae consectetur non tempora labore sed veritatis est omnis ullam accusamus libero aut eos', 11, 1973, 9, 1988, 'Exercitation esse magna omnis temporibus', 'Fugiat non autem porro voluptas cupidatat iste corrupti in at accusantium voluptas eveniet aliquip reiciendis nulla in incidunt eum', 'Cupiditate cillum perferendis omnis labore ea assumenda nobis enim enim sed ut elit a quasi', '2018-09-26 20:24:06', '2018-09-26 20:24:06'),
(17, '8', 'UTJ', 1, 2014, 6, 2018, 'Licenciatura', 'Ing en Computacion', 'Info', '2018-09-26 20:43:54', '2018-09-26 20:43:54'),
(18, '8', 'UTJ', 1, 2014, 6, 2018, 'Licenciatura', 'Ing en Computacion', 'Info', '2018-09-26 20:43:54', '2018-09-26 20:43:54'),
(19, '9', 'Est dolor enim voluptate sunt dolor nulla amet id consectetur amet nisi pariatur Sed molestiae', 7, 1962, 3, 2015, 'Quam magnam inventore id nulla', 'Qui voluptatem rerum quaerat consequatur voluptatem est recusandae Dolores voluptatum sed aliqua', 'Exercitationem exercitation laudantium duis architecto cillum eius dignissimos laudantium mollitia pariatur Non repudiandae', '2018-09-26 21:00:01', '2018-09-26 21:00:01'),
(20, '10', 'Sint est voluptatem eum ab officia dolore repudiandae mollitia facere dolorum eiusmod laboris sed excepturi ab qui', 1, 2008, 7, 1985, 'Rem dolore qui aliqua Eius aut aut ut distinctio', 'Pariatur Irure consequat Ullamco molestias beatae', 'Mollitia et amet suscipit sint eveniet sit impedit voluptatem quia commodo alias laborum ullam enim aliquam', '2018-09-26 22:43:40', '2018-09-26 22:43:40'),
(21, '11', 'Sint est voluptatem eum ab officia dolore repudiandae mollitia facere dolorum eiusmod laboris sed excepturi ab qui', 1, 2008, 7, 1985, 'Rem dolore qui aliqua Eius aut aut ut distinctio', 'Pariatur Irure consequat Ullamco molestias beatae', 'Mollitia et amet suscipit sint eveniet sit impedit voluptatem quia commodo alias laborum ullam enim aliquam', '2018-09-26 22:46:44', '2018-09-26 22:46:44'),
(22, '12', 'Sint est voluptatem eum ab officia dolore repudiandae mollitia facere dolorum eiusmod laboris sed excepturi ab qui', 1, 2008, 7, 1985, 'Rem dolore qui aliqua Eius aut aut ut distinctio', 'Pariatur Irure consequat Ullamco molestias beatae', 'Mollitia et amet suscipit sint eveniet sit impedit voluptatem quia commodo alias laborum ullam enim aliquam', '2018-09-26 22:47:18', '2018-09-26 22:47:18'),
(27, '13', 'Quas at veniam porro eius qui enim', 9, 2003, 7, 1975, 'Officia blanditiis commodo alias recusandae Voluptatem Sapiente repellendus Magna numquam suscipit laboriosam ex voluptate voluptatem do magnam molestiae', 'Nostrud commodo blanditiis voluptatem mollit est omnis pariatur Dignissimos adipisicing at deserunt est nihil perspiciatis nulla pariatur Aliquam velit cumque', 'Voluptas mollitia id aperiam ducimus sapiente voluptatem Eiusmod fugiat ut', '2018-09-27 00:06:46', '2018-09-27 00:06:46'),
(28, '14', 'Facere adipisci mollitia nemo minus est adipisci voluptas et incidunt nulla sunt nemo sequi est aute', 3, 1994, 5, 1993, 'Eaque corporis alias sapiente at', 'Quo voluptate consequatur Laudantium accusantium nulla aliqua Ad incidunt quis', 'Totam animi aut est labore ut optio aliquip iusto nostrud', '2018-09-27 00:26:42', '2018-09-27 00:26:42'),
(29, '15', 'Facere adipisci mollitia nemo minus est adipisci voluptas et incidunt nulla sunt nemo sequi est aute', 3, 1994, 5, 1993, 'Eaque corporis alias sapiente at', 'Quo voluptate consequatur Laudantium accusantium nulla aliqua Ad incidunt quis', 'Totam animi aut est labore ut optio aliquip iusto nostrud', '2018-09-27 00:27:04', '2018-09-27 00:27:04'),
(30, '16', 'Repellendus Adipisci sit sunt iusto nulla maiores', 10, 2006, 12, 2002, 'Quia esse temporibus qui optio irure odit rem suscipit minus', 'Consequuntur ex aute qui quaerat itaque adipisci ut inventore consectetur architecto quaerat dolores dolores sunt', 'Necessitatibus cumque sit nulla accusamus dolor cupidatat at tempora sunt iure quasi rerum ipsum omnis voluptas', '2018-09-27 00:30:51', '2018-09-27 00:30:51'),
(31, '17', 'Repellendus Adipisci sit sunt iusto nulla maiores', 10, 2006, 12, 2002, 'Quia esse temporibus qui optio irure odit rem suscipit minus', 'Consequuntur ex aute qui quaerat itaque adipisci ut inventore consectetur architecto quaerat dolores dolores sunt', 'Necessitatibus cumque sit nulla accusamus dolor cupidatat at tempora sunt iure quasi rerum ipsum omnis voluptas', '2018-09-27 00:33:11', '2018-09-27 00:33:11'),
(32, '18', 'Repellendus Adipisci sit sunt iusto nulla maiores', 10, 2006, 12, 2002, 'Quia esse temporibus qui optio irure odit rem suscipit minus', 'Consequuntur ex aute qui quaerat itaque adipisci ut inventore consectetur architecto quaerat dolores dolores sunt', 'Necessitatibus cumque sit nulla accusamus dolor cupidatat at tempora sunt iure quasi rerum ipsum omnis voluptas', '2018-09-27 00:46:49', '2018-09-27 00:46:49'),
(33, '19', 'Non reiciendis qui architecto dolor qui sit qui nemo perspiciatis eius et maiores architecto sed autem rerum consequat Qui', 10, 1984, 1, 1991, 'Cumque aut voluptas officia aut duis iste ea reprehenderit qui ex', 'Distinctio Pariatur Et sit sapiente ad anim', 'Magni excepturi tenetur sit hic qui aut consequatur magni omnis ea debitis molestias voluptate rem excepteur', '2018-09-27 00:50:02', '2018-09-27 00:50:02'),
(34, '20', 'Et nesciunt id quo excepteur sed voluptatem consequatur Labore rerum', 3, 1973, 11, 1994, 'Voluptas tempor tempore quas delectus necessitatibus sequi cupidatat a consequat Voluptatum dicta qui non laudantium numquam ullamco', 'Anim autem officia eius tempora modi ut et rerum', 'Sint vitae voluptatem voluptate magna et consequatur Incidunt ducimus earum et nulla laudantium in dolorem vel cum velit sint ea', '2018-09-27 01:30:21', '2018-09-27 01:30:21'),
(35, '21', 'Dignissimos irure aute non aspernatur animi id ipsam enim facilis qui hic', 5, 1987, 5, 1990, 'Consectetur tempore velit deserunt quae duis aut autem', 'Dolore debitis et esse veniam nisi', 'Sunt do voluptatem officia ut doloribus dolor tempor facere maxime id eveniet consequatur Amet ut praesentium enim dolorem', '2018-09-27 01:32:44', '2018-09-27 01:32:44'),
(36, '22', 'Omnis quibusdam eius doloremque consequat Minima nostrum quia nesciunt natus illo quidem voluptatibus et', 6, 1989, 11, 1991, 'Consequuntur delectus labore facilis esse voluptatem Quasi eveniet', 'Iusto sapiente dolor vel possimus eum velit unde voluptatem expedita rerum eum dolores praesentium pariatur Enim nisi', 'Illo atque anim magni vitae at qui sed velit sit ratione sit dolorem', '2018-09-27 01:55:38', '2018-09-27 01:55:38'),
(65, '28', 'Utj', 7, 2012, 7, 2012, 'Jdjd', 'Ufuf', 'Njdjd', '2018-09-28 21:20:32', '2018-09-28 21:20:32'),
(69, '30', 'asdasd', 1, 2017, 1, 2018, 'dasda', 'asda', 'asdasd', '2018-09-29 01:57:16', '2018-09-29 01:57:16'),
(71, '2', 'Jdjzj', 7, 2012, 8, 2011, 'Ndndnd', 'Nzbdb', '', '2018-09-29 04:41:50', '2018-09-29 04:41:50'),
(79, '33', 'CUCSH', 1, 2018, 12, 2018, 'Estudiante', 'Estudiante', 'Inicia estudios Enero 2018-concluye Diciembre 2018', '2018-10-02 23:57:24', '2018-10-02 23:57:24'),
(81, '31', 'Iteso', 1, 2013, 1, 2017, 'Licenciatura ', 'Ingeniero ', 'Jsjskdk', '2018-10-03 22:44:42', '2018-10-03 22:44:42'),
(85, '36', 'Tecnológico de Ocotlán ', 5, 2009, 1, 2018, 'Ingeniería ', 'Sistemas computacionales ', 'J', '2018-10-04 23:54:09', '2018-10-04 23:54:09'),
(90, '39', 'Universidad de Francia', 5, 1995, 7, 2001, 'Licenciatura ', 'Leyes ', '', '2018-10-10 22:31:21', '2018-10-10 22:31:21'),
(124, '48', 'UTJ', 1, 2014, 6, 2018, 'Licenciatura', 'Ing en Computacion', 'Info', '2018-10-19 18:18:35', '2018-10-19 18:18:35'),
(125, '48', 'UTJ', 1, 2014, 6, 2018, 'Licenciatura', 'Ing en Computacion', 'Info', '2018-10-19 18:18:35', '2018-10-19 18:18:35'),
(130, '38', 'Universidad de Guadalajara ', 6, 2006, 6, 2010, 'Ingeniería ', 'Mecatronica', '', '2018-10-19 19:30:16', '2018-10-19 19:30:16'),
(143, '56', 'Tecnologico de ocotlan ', 8, 2009, 12, 2014, 'Ingenieria ', 'Ingeniero en sistemas computacionales', 'Hdjdhs', '2018-10-22 18:45:59', '2018-10-22 18:45:59'),
(144, '55', 'Udg', 8, 2009, 4, 2017, 'Lincencistura ', 'Linenciado en comunición', 'Jfbd', '2018-10-22 19:13:49', '2018-10-22 19:13:49'),
(148, '32', 'Universidad autonoma de mexico', 7, 2010, 5, 2012, 'Licenciatura', 'Ingeniería en sistemas computacionales computacionales', '', '2018-10-25 01:00:19', '2018-10-25 01:00:19'),
(190, '34', 'Centro de enseñanza técnica industrial ', 9, 2011, 9, 2015, 'Grado académico ', 'Ingeniería ', 'Información adicional ', '2018-10-29 19:45:01', '2018-10-29 19:45:01'),
(196, '45', 'Universidad / instituto ', 5, 2012, 3, 2017, 'Grado academico', 'Título obtenido ', 'Información adicional ', '2018-10-29 21:46:39', '2018-10-29 21:46:39'),
(198, '47', 'Colegio Lomas', 3, 2015, 2, 2017, 'Ingeniero', 'University ', '', '2018-10-30 20:11:06', '2018-10-30 20:11:06'),
(201, '41', 'Universidad autónoma de Guadalajara ', 7, 2012, 7, 2016, '', '', '', '2018-10-31 20:21:23', '2018-10-31 20:21:23'),
(204, '59', 'Ceti', 8, 2016, 7, 2018, 'Grado de estudio', 'Titulo obtenido', 'Información adicional', '2018-11-02 20:19:25', '2018-11-02 20:19:25'),
(205, '60', '123', 3, 2003, 2, 2018, '', '123', '', '2018-11-05 06:54:43', '2018-11-05 06:54:43'),
(206, '61', 'Universidad autónoma de Guadalajara', 6, 2015, 6, 2018, '', '', '', '2018-11-05 21:04:31', '2018-11-05 21:04:31'),
(208, '1', 'UTJ', 1, 2014, 6, 2018, 'Ing en Computacion', 'Licenciatura', 'Info', '2018-11-09 21:49:02', '2018-11-09 21:49:02'),
(209, '1', 'UTJ', 1, 2014, 6, 2018, 'Ing en Computacion', 'Licenciatura', 'Info', '2018-11-09 21:49:02', '2018-11-09 21:49:02'),
(212, '53', 'Centro de enseñanza técnica industrial ', 1, 2017, 1, 2018, 'Ingeniería', 'ISC', 'Kotlinlover', '2018-11-09 22:51:48', '2018-11-09 22:51:48'),
(213, '42', 'Cbetis', 7, 2009, 8, 2012, 'Jdud', 'Kdjdjd', '', '2018-11-10 00:02:21', '2018-11-10 00:02:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(13, '¿Quienes somos?', 'Somos una empresa dedicada a brindarte una solución eficaz a las demandas del mercado...', '2018-08-16 18:29:01', '2018-08-16 18:29:01'),
(14, '¿Como agrego un candidato?', 'Para agregar a un nuevo candidato en el apartado de Mis candidatos dar click en el boton + que aparece en la esquina inferior deracha de la pantalla...', '2018-08-16 18:30:38', '2018-08-16 18:30:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `firebase`
--

DROP TABLE IF EXISTS `firebase`;
CREATE TABLE `firebase` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `firebase`
--

INSERT INTO `firebase` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, '1', 'fpeb43hrIEE:APA91bEiKr4BIbqtjkiu9FC0_r-kkM6_QaTZTA-Oz3xqoWtjCHBKxUeUxcT3WTazhXWArM4-Z3SlVZhssyC91FzgTo0nTZ6EFoPguaO6Sgbjh3xyLqUfHPNgtgo3bDjJNK_BbN0lsMZK', '2018-09-18 20:38:23', '2018-12-04 22:25:21'),
(2, '2', 'dH7DUtZlALU:APA91bE3mwr9WDj9XtJcQKVQbtkTO0PtDz6V_FFBRbMisS6e8jqAFVZdYKyAcSAQChxKFWvst5ejNzARDdTBBOoZL-F_9yZxcnteOUi-ilX4OLn4bky9q1Fqjpw_vNGVv-iUKl2O9jAN', '2018-09-18 20:40:52', '2018-12-12 22:36:45'),
(3, '4', 'czN2WJ8KOi0:APA91bFrNo7M757WjPTY4J2HtdgcmS-CuxKmKorM3h2dVntSw8wX_cdMtvVkiQSUcf89gwTNWvpfNre1hdgGG5Ok1lnMGXs-FFHoaoCnxW9oT5APuGk0Iu6RlRvLlutjFw5fKnz5pVZy', '2018-09-18 23:24:05', '2018-09-29 04:41:13'),
(4, '7', 'ejaUCxx6U8U:APA91bG1IQmfN1tUe1C_UH7AKRVK4dPKtKYuFs-FV4IejdO1EAUviFV7SdcZM-pFEQ7pr6m6qoshO35299upi0HFl5zEpkrs71b6V9lWWTuVfBpC4VvYAX9JMnBvfqbMXkdRZPs8sk6q', '2018-09-19 19:22:02', '2018-09-19 19:22:02'),
(5, '12', 'c7IsEUFrbX0:APA91bHAOC2PACVCqVufiowDIzWzFLoLqgKOf_SN_RukuP6icQz55iRrJKlGy1CbTC4AgqHLKKTGNT2bASj8zzHDt8ADhZj_JpTR8Gb0qyiqYpcIUAelrusectjyDk9LcXSeqMDoB9fk', '2018-09-19 22:40:43', '2018-09-19 22:40:43'),
(6, '10', 'e3qlmsHQy9E:APA91bFTLtPQs6p7hMB_vZcZZx4nS5Yr2lbexLpcdp6DcGMQf6va1XFr7aT6g4iLaUjAeZRLD07ud4PVGW0BhjY1Z0ITZRC0ZOISp92YdmPTZO1ajERIcdfVYNM2AKHkeiAelvepQXEs', '2018-09-24 20:11:35', '2018-10-04 19:07:16'),
(7, '11', 'f4v_B8L9MjU:APA91bFn_1gWgYLxjebc-GzdbOnu2F3Fxj4FvoAv2cwnxbNbq1KDa-VPnOllcnuF5NMSQgRn6mWfCUvaZJrNuU3OHztBlFpnJJ3cg8xAvbLjBVtSKQDdyZMDUw9oV7IUWVvoEfcx85wV', '2018-09-24 21:31:06', '2018-09-24 21:31:06'),
(8, '16', 'ejaUCxx6U8U:APA91bG1IQmfN1tUe1C_UH7AKRVK4dPKtKYuFs-FV4IejdO1EAUviFV7SdcZM-pFEQ7pr6m6qoshO35299upi0HFl5zEpkrs71b6V9lWWTuVfBpC4VvYAX9JMnBvfqbMXkdRZPs8sk6q', '2018-09-26 20:02:21', '2018-09-26 20:02:21'),
(9, '17', 'f0FvTIsRT6o:APA91bHUDXRmzZ91cUUvjNZC-GBcz9-yTGXmI6ULcEIvScUCOP94DMCWi5CFAqmkCGzHTsRA5oMgMTduIsJYM7H52sxJRnliOhm7GuvosMyg2bZBt3HnOj9Fo9PeRwaqOKaLWn6UrTNa', '2018-09-26 20:42:12', '2018-09-26 20:42:12'),
(10, '9', 'eCrgtH9uVeo:APA91bFk6pxCB_Xs31VWGIokaby7_yXm1m9d1FAtERxLyARSgqwEb_VCIIEYy3DV0qYU9WfFOREiqFfqzbqpmhDFisBvyk1SCIdXeLJqDDx1OqR6HKuUZXYLVzxu14GQHnfRnFuEFjO5', '2018-09-27 16:57:04', '2018-09-27 16:57:04'),
(11, '18', 'fthXJjZDl2M:APA91bF9owT7t4MldEKgC4mdjkkZdKrf_Xvq-cq5NLmCOBqgZ8aW-QM45KQhLv-wnhpdrVFGSQ_zs5ChzEw-dsx0B5qSZ-GMML7LvgGNM-8ZWMQnzmNMzbJjSfjVb-KsMMc5s1VDJgRW', '2018-09-27 20:22:52', '2018-09-27 20:22:52'),
(12, '20', 'fAAwMDyCAJU:APA91bEA0UBbEfu6umHsB2o4Cm5H7msWFZBJ6vkbl67SpV0lvtgrFhR0Xnj2sx0UWeJjCnULVf-FaCkgE9fvrUbiDL97SgMBSBiSsoNupmVJLOwNaPT42FOtsEY2wOyvAOGZWU22GDb_', '2018-09-27 20:53:11', '2018-09-27 20:53:11'),
(13, '21', 'e3qlmsHQy9E:APA91bFTLtPQs6p7hMB_vZcZZx4nS5Yr2lbexLpcdp6DcGMQf6va1XFr7aT6g4iLaUjAeZRLD07ud4PVGW0BhjY1Z0ITZRC0ZOISp92YdmPTZO1ajERIcdfVYNM2AKHkeiAelvepQXEs', '2018-09-28 03:03:01', '2018-10-24 04:26:26'),
(14, '22', 'ejaUCxx6U8U:APA91bG1IQmfN1tUe1C_UH7AKRVK4dPKtKYuFs-FV4IejdO1EAUviFV7SdcZM-pFEQ7pr6m6qoshO35299upi0HFl5zEpkrs71b6V9lWWTuVfBpC4VvYAX9JMnBvfqbMXkdRZPs8sk6q', '2018-09-28 18:42:36', '2018-09-28 18:42:36'),
(15, '26', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-09-29 01:49:08', '2018-09-29 01:49:08'),
(16, '25', 'd2b40Z3J37c:APA91bFxGQcDDQ9VFBBuXm4iB9LEhjqASnQ23saDUo8tw6VHW9GhtshT9FyPojTxREH76VStdMNh-VQ4lHI2guWU8irkb1fZLon94IWKi-r2t9j7qokK-hJnBwIwYF7OHebamfyHFgE-', '2018-09-29 02:21:37', '2018-09-29 02:21:37'),
(17, '28', 'eiMvvQXYrgw:APA91bHZy1ibhQrT1EQqTthrtuRv6obaARZTlmwKtNhbPJuNY-VmeUAPNZhKHDA3pwfP5cH1Q2-RlnVpk5WCsc7DZwNyEVmpD4NCs5e1flRJXJWYG-BbWRYNuceBF9EUYwrVP-LYq9Rk', '2018-10-01 20:38:23', '2018-10-01 20:38:23'),
(18, '30', 'coisoU_T28k:APA91bGxgzqGIVXibbU7_mPLQhSYWjYIJwU-biNqyBt8JOCiXYN8D_vpfGTMnWrAekX8UmT7a2pgs60L83MTKwYsMD1fPJChFhEnt8SmD2o_XfxHd460Jf9sEcdTpbyjkJoxqKO5b3kX', '2018-10-02 01:27:48', '2018-10-02 01:27:48'),
(19, '31', 'fJGDxdyiLCc:APA91bGcedPOqnRhyRfn-6Bx3mNEg3-9n_THxuyKAaUn9SEUQFo3yCH-_V7v4syp5s94s3VQwD-Gc8nH-OriV485jNoAinMnMxOlDtnJ1goOMjGh0nLuiZ2jo32QC_LtX6WijprPRFQz', '2018-10-02 01:55:07', '2018-10-20 18:52:04'),
(20, '32', 'cdCypp2xXz0:APA91bHS7dxzigSzGnNm3K6eslzYqy3QA1ouhD1LBA_G6zyJYSWwXUmK9YVTfmibDo9n2YHeUZKMeeuxU6B0Udh-d1rZmAM-p2uIBWr336OPuIO0ax0syve_TkeHI7sIzk8D49jp7UQE', '2018-10-02 02:29:48', '2018-10-02 02:29:48'),
(21, '33', 'eWc5IzYgZDs:APA91bEPKYeGbP2yWgKQIsISesjVhyYUpou6fAS4stUhGHdGuP7QtBlhuftnL-Ss01eVF3H3hvY9EWO3Q1GxNSoYI3-5oorRYhk3RSbNHmZTmeg4hmDxVT1KbamS-yEe27MLSGVggmBe', '2018-10-02 21:40:31', '2018-10-02 21:40:31'),
(22, '34', 'eZM3hXN8SH8:APA91bG2pKKHot1IwlDtNHsjqb7c0unBrZDHfX_gv2UdyEXmM7IqWxB90rp0gYfEYGyZUuWFMNJK3YG-S3DJ9pICsXxVpBK2LBzPA8o-3KF6Zz09Iun6c_FAhfS8z0rYm87pl58c_Ed5', '2018-10-02 23:17:46', '2018-10-02 23:17:46'),
(23, '35', 'dk5YA4ThT2A:APA91bF996Vz4qVA9I-OKuP_1ib0BZgHjNswcc0beak97h4nrP3gZx_Sv21r7hItHmZJ_Aqb8kjwQTbYGgn-v8Yt0xoikhoqJ44ojFYQnSl2ho617iyG_1cmk-ojHXFAuifFyYITKhax', '2018-10-02 23:23:40', '2018-11-05 20:57:45'),
(24, '38', 'fpetFzcH7yw:APA91bGHtB-AJoaz1507gdxCTZ5WEue8EBsZqfLIu5OgjWilO6LZHaIPetrQpjbSy_Bae1aJVqoUP38jKfyUM9TL0GQ5ZfcsO9-e_p4-hOPXDZ8V-w2X3WEFDYjWphxhQkaYT0pvlp2G', '2018-10-03 19:06:31', '2018-10-03 19:06:31'),
(25, '13', 'dOZjZQtCLIE:APA91bHmMU3ZtSkvlnl0_mE1dhnQvhnrSTp2AffYW3aPrvYFAhsVw3RgWTunN5CpxZ20r2kHTlZn5wIHT5ksMbSKbQe1v2Sh5titiqaDAihd4GOAQzoGyox6vCyVgDTW1sY0ZtnXsOb8', '2018-10-03 22:56:12', '2018-10-03 22:56:12'),
(26, '40', 'd0MnlW1M1bo:APA91bEvP2K6sMN7W7083MEkpf2EI1GGRlAyB19jFVtYHeHuN6U5Wrys6KPKVGbJA0a_1C8zi3l2wZSffHmDQZ9cFme_VPCYPs4hZu3b08ZyJesv_HHjHp9tm9sbZQmmcBSVcs0THj_7', '2018-10-04 02:40:58', '2018-10-04 02:40:58'),
(27, '41', 'fH7S9KLJCqM:APA91bFD3FN3yxu9DXrWGENiH0iHBOJ_j9dsqVBN4oKDCe3xTCGWYdmgvxLRGvOq7ftEH4RLNo3mqwt83zMyykZaSVgUVrUSDNSZdufGfpSRUh_hv0XkfWy3Lb25iBDnfa26cinCrmvW', '2018-10-08 22:15:24', '2018-11-26 17:32:09'),
(28, '42', 'cH9LelPFK4k:APA91bGBZyWmDtj9_LBHuKCk_V5nzzYtLiK3NxDaJDgk5zmlEo0BeFAKZ4nhHjW0ZmLhxTwt-ZmXxFIzoPlhw3d3HWo_iKC8gviMstGLpOkcptyxvwjZGsB9u5MdmWw0p_WEmNn0BPeR', '2018-10-10 02:29:41', '2018-10-10 02:29:41'),
(29, '43', 'd7Sc2wwVumY:APA91bHV2cWRqIPs9ommg_ihbHW1hO5axM7BheOZQ9da5hpV69bfy_loUuP0W4sHdLJ_eNe4o8o1mqBO92Ka1LAJXIiWPL81AZMp52Nf7MxuAWw_ae8RAa58nUJdmI7nXbfgk7LcYP86', '2018-10-11 04:24:46', '2018-10-11 04:24:46'),
(30, '46', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-10-12 18:05:46', '2018-10-12 18:05:46'),
(31, '47', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-10-12 18:21:05', '2018-10-12 18:21:05'),
(32, '48', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-10-12 20:36:40', '2018-10-12 20:36:40'),
(33, '50', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-10-12 20:51:03', '2018-10-12 20:51:03'),
(34, '60', 'cN8JgI22xaQ:APA91bGxTuavTEG7zJcxI0GgqALXQEL-tCSP_MCBwE5NPpXipLjT-pvYyhofrN6NpS6nHytCg2MERoHqg_4rI6sZ8_tF2oARNTmlPBaMflzhR-hpLEGXrJyE8nAx9mIQBXtHnmuW1qWd', '2018-10-15 20:13:44', '2018-10-15 20:13:44'),
(35, '63', 'dH7DUtZlALU:APA91bE3mwr9WDj9XtJcQKVQbtkTO0PtDz6V_FFBRbMisS6e8jqAFVZdYKyAcSAQChxKFWvst5ejNzARDdTBBOoZL-F_9yZxcnteOUi-ilX4OLn4bky9q1Fqjpw_vNGVv-iUKl2O9jAN', '2018-10-17 17:39:28', '2018-11-06 01:20:48'),
(36, '69', 'cTXMoJQvdCM:APA91bFCpQWrmWzf7sZ9_ZYB81T4_wL36fFa_NM2-BnmdW6cvZGt1NW8YXnGlgZ5u7gC_xmgu5cBUfYKagxbVIVcCy_eXQfeItTGE5N5XJKJip3w0s4_KxXnqXEOvUDKhMgvNr_iBAuL', '2018-10-22 18:00:31', '2018-10-22 18:00:31'),
(37, '70', 'cAoqAyq8I_s:APA91bEGWdwVbl7Kujq1bCRTj_f6XARpqq-7L35vqds29_xzZ16qYu7lfHpT3I0hNYW6l7GzpKvhNSyv5VH-1Q-jcd8vK__CYUjAQiKyZRGzYLlTYnu-MG4xNsI_RLb_T9hluP8dqmEA', '2018-10-22 23:08:03', '2018-10-22 23:08:03'),
(38, '24', 'd-yMG0OJMis:APA91bHkLs94y6QGt7zm4IBRPimQrXrSGtaT0LWRCzq5W48NhBrCGNNkvezibxLNrF_oo54BOfG5533MUduHl1hijX_viz_uEa9r6RoY6vBrZlW45s2LTFN58QmGj9lcYIk5teM915vo', '2018-10-25 21:22:28', '2018-11-02 00:29:04'),
(39, '74', 'c4tDHzvOJ1s:APA91bEiyImstpQSe_DWaDbXk2o_J1lyvxwk40tR5Pep13B5pNJoFRkiyIcDg8sUt3SdrS4ht3Nd9wh7UOW8YfewTnM21mjraKUsaKzqTVugicaivC1fcnZ0XzUrSsUq7OH0SPGzyJLa', '2018-10-25 21:24:09', '2018-10-27 04:20:34'),
(40, '75', 'cvhi_qVnd5s:APA91bEYZAq0n37HhImFvM3YOUbAr_DcvbEe3jXKlvdT4F4dPwbCPfyAhKtaLolmAsOc98exHVUYdz20SPd4kdJyeDRL4dt5R2ROcBM6aCIRxQEcV7Tz5bCKOCIwNzOs8H8Fq3cXGCdJ', '2018-10-25 22:36:33', '2018-10-25 22:36:33'),
(41, '76', 'fH7S9KLJCqM:APA91bFD3FN3yxu9DXrWGENiH0iHBOJ_j9dsqVBN4oKDCe3xTCGWYdmgvxLRGvOq7ftEH4RLNo3mqwt83zMyykZaSVgUVrUSDNSZdufGfpSRUh_hv0XkfWy3Lb25iBDnfa26cinCrmvW', '2018-10-29 23:03:19', '2018-10-29 23:03:19'),
(42, '77', 'fH7S9KLJCqM:APA91bFD3FN3yxu9DXrWGENiH0iHBOJ_j9dsqVBN4oKDCe3xTCGWYdmgvxLRGvOq7ftEH4RLNo3mqwt83zMyykZaSVgUVrUSDNSZdufGfpSRUh_hv0XkfWy3Lb25iBDnfa26cinCrmvW', '2018-10-29 23:09:10', '2018-10-29 23:09:10'),
(43, '78', 'f3g5d1NnkQM:APA91bG8QSTaBcFoIUxtRaYafylG_vBY5kzo40YBkELjNOyENoHeNECfwR33rPTziks9ha0nBZo-VQK5PIR72VnJV4-gOxEW2l5CEEebtjz-2ZSsEGMZz0y78ggQeTQbCLkNhBbc0fWU', '2018-10-29 23:35:19', '2018-10-29 23:35:19'),
(44, '80', 'fH7S9KLJCqM:APA91bFD3FN3yxu9DXrWGENiH0iHBOJ_j9dsqVBN4oKDCe3xTCGWYdmgvxLRGvOq7ftEH4RLNo3mqwt83zMyykZaSVgUVrUSDNSZdufGfpSRUh_hv0XkfWy3Lb25iBDnfa26cinCrmvW', '2018-10-30 23:02:15', '2018-10-30 23:02:15'),
(45, '81', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-10-30 23:06:56', '2018-10-30 23:06:56'),
(46, '82', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-10-30 23:09:02', '2018-10-30 23:09:02'),
(47, '83', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-10-30 23:10:00', '2018-10-30 23:10:00'),
(48, '84', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-10-30 23:10:57', '2018-10-30 23:10:57'),
(49, '86', 'fsJ7bj_27hM:APA91bGV-WDPOFeEpRfbMLMPZ1OpZbpqP7BdYlBsrLO_SdN8aN16dQbAPystjnZAhjkvlcABASPS2OPzZZqslBYen4auLk6nkuWAdBNpb8APOg3CCQ_TyOA0GRKi_2gkmsdKMT0jf3sG', '2018-10-30 23:54:41', '2018-10-30 23:54:41'),
(50, '88', 'fzaMISKq5Kw:APA91bGkxyxvAQiBJi7aF29QpccWh0gI1vcnYaQcC17GgTIoYxD_RiK6xVP0t4zpiKpjQWRVrGowjgK-gaWnU7hVeV1d5nsiNM2PLEQUvWAcExkIBrXEf6aDyJgY_hiQ3Ys7UVVSo7xq', '2018-10-31 20:01:08', '2018-11-29 16:49:29'),
(51, '89', 'dk5YA4ThT2A:APA91bF996Vz4qVA9I-OKuP_1ib0BZgHjNswcc0beak97h4nrP3gZx_Sv21r7hItHmZJ_Aqb8kjwQTbYGgn-v8Yt0xoikhoqJ44ojFYQnSl2ho617iyG_1cmk-ojHXFAuifFyYITKhax', '2018-11-01 22:43:31', '2018-11-05 20:55:07'),
(52, '92', 'difEACRFIJU:APA91bGzhwhCmaBHI0GXGB15BymCaMjHZa0x-xpx4TZunKWArLJN3jM0GeEQD3EZ1gyx8RNG-_0B3cZrJN5z4mzToGKmk7DpVgyudSxBUOi3-nfaiMvXRlcWzH0Aen5B-K4iOF0B7Gw0', '2018-11-08 02:15:00', '2018-11-08 02:15:00'),
(53, '15', 'f9xD6p7zxts:APA91bEN_azPXL95O4NMRgSJzNIxpHwIFeMOwevRdvGJ6dnz6f3FYiKIUSPPeyiNN4Ql0W-2TkURAPEITEcD86aEzqSkS9EMC8jS21S5YCTxLhcY__bTzCk0jL4DCbdn9LR884Q3NJrj', '2018-11-08 07:26:16', '2018-11-08 07:26:16'),
(54, '15', 'f9xD6p7zxts:APA91bEN_azPXL95O4NMRgSJzNIxpHwIFeMOwevRdvGJ6dnz6f3FYiKIUSPPeyiNN4Ql0W-2TkURAPEITEcD86aEzqSkS9EMC8jS21S5YCTxLhcY__bTzCk0jL4DCbdn9LR884Q3NJrj', '2018-11-08 07:26:16', '2018-11-08 07:26:16'),
(55, '73', 'dqWKBGeX6L4:APA91bGCYAYoHaZNrb-cUzoqdkNxgLhSZtbL_3yi7_9Pbih1ed3zumdJjRajYg-K1qzK9pl9cbqA43T2a_1TUKkqTemEiDGijf1oExra1Sbxts92WvWRC3Hc_r3OFR4Yt7ApZR7PlbDk', '2018-11-14 04:00:17', '2018-11-14 04:00:17'),
(56, '94', 'dk5YA4ThT2A:APA91bF996Vz4qVA9I-OKuP_1ib0BZgHjNswcc0beak97h4nrP3gZx_Sv21r7hItHmZJ_Aqb8kjwQTbYGgn-v8Yt0xoikhoqJ44ojFYQnSl2ho617iyG_1cmk-ojHXFAuifFyYITKhax', '2018-11-16 03:22:35', '2018-11-16 03:22:35'),
(57, '95', 'dOZjZQtCLIE:APA91bHmMU3ZtSkvlnl0_mE1dhnQvhnrSTp2AffYW3aPrvYFAhsVw3RgWTunN5CpxZ20r2kHTlZn5wIHT5ksMbSKbQe1v2Sh5titiqaDAihd4GOAQzoGyox6vCyVgDTW1sY0ZtnXsOb8', '2018-11-20 20:57:09', '2018-11-20 20:57:09'),
(58, '96', 'fH7S9KLJCqM:APA91bFD3FN3yxu9DXrWGENiH0iHBOJ_j9dsqVBN4oKDCe3xTCGWYdmgvxLRGvOq7ftEH4RLNo3mqwt83zMyykZaSVgUVrUSDNSZdufGfpSRUh_hv0XkfWy3Lb25iBDnfa26cinCrmvW', '2018-11-20 23:03:03', '2018-11-22 23:16:16');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `Grafica_Preferencias_genero`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `Grafica_Preferencias_genero`;
CREATE TABLE `Grafica_Preferencias_genero` (
`Masculino` bigint(21)
,`Femenino` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intro_sliders`
--

DROP TABLE IF EXISTS `intro_sliders`;
CREATE TABLE `intro_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci,
  `urlImagen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `languages`
--

INSERT INTO `languages` (`id`, `name`, `created_at`, `updated_at`) VALUES
(7, 'Español', '2018-08-02 20:58:18', '2018-08-02 20:58:18'),
(8, 'Ingles', '2018-08-02 20:58:27', '2018-08-02 20:58:27'),
(9, 'Ruso', '2018-08-02 20:58:32', '2018-08-02 20:58:32'),
(10, 'Aleman', '2018-08-02 20:58:35', '2018-08-02 20:58:35'),
(11, 'portugués', '2018-08-02 20:58:57', '2018-08-02 20:58:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(564, '2018_06_26_214844_create_aviso_privacidad_table', 1),
(638, '2018_06_11_175825_create_intro_sliders_table', 2),
(639, '2018_06_11_185127_create_users_table', 2),
(640, '2018_06_18_164750_create_categories_table', 2),
(641, '2018_06_18_172327_create_table_preferences', 2),
(642, '2018_06_18_183218_category_preference', 2),
(643, '2018_06_18_212930_create_vacancies_table', 2),
(644, '2018_06_19_212421_create_user_vacancy_table', 2),
(645, '2018_06_20_193214_create_candidates_table', 2),
(646, '2018_06_20_200444_create_education_data_table', 2),
(647, '2018_06_20_200938_create_languages_table', 2),
(648, '2018_06_20_201146_create_software_table', 2),
(649, '2018_06_20_201547_create_work_experience_table', 2),
(650, '2018_06_20_212556_create_candidate_language_table', 2),
(651, '2018_06_20_215308_create_candidate_software_table', 2),
(652, '2018_06_23_214734_create_candidate_vacancy_table', 2),
(653, '2018_06_23_220647_create_user_candidate_table', 2),
(654, '2018_06_23_234709_create_firebase_table', 2),
(655, '2018_06_24_002156_create_faq_table', 2),
(656, '2018_06_24_161812_create_notices_table', 2),
(657, '2018_06_26_161602_create_countries_table', 2),
(658, '2018_06_26_161617_create_cities_table', 2),
(659, '2018_06_26_161630_create_states_table', 2),
(660, '2018_06_26_213229_create_blog_table', 2),
(661, '2018_06_26_214913_create_privacy_agreement_table', 2),
(662, '2018_06_27_171858_create_about_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modules_admin`
--

DROP TABLE IF EXISTS `modules_admin`;
CREATE TABLE `modules_admin` (
  `id` bigint(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modules_admin`
--

INSERT INTO `modules_admin` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Administradores', '2018-10-30 17:01:23', '2018-10-30 17:01:23'),
(2, 'Usuarios APP', '2018-10-30 17:41:09', '2018-10-30 17:03:36'),
(3, 'Categorías', '2018-10-30 17:03:36', '2018-10-30 17:03:36'),
(4, 'Vacantes', '2018-10-30 17:03:36', '2018-10-30 17:03:36'),
(5, 'Postulados', '2018-10-30 17:03:37', '2018-10-30 17:03:37'),
(6, 'Notificaciones', '2018-10-30 17:43:42', '2018-10-30 17:03:37'),
(7, 'Intro APP', '2018-10-30 17:44:57', '2018-10-30 17:05:18'),
(8, 'Zona geográfica', '2018-11-01 20:23:55', '2018-10-30 17:05:18'),
(9, 'Preguntas Frecuentes', '2018-10-30 17:44:54', '2018-10-30 17:05:18'),
(10, 'Idiomas', '2018-10-30 17:44:53', '2018-10-30 17:05:18'),
(12, 'Software Categorías', '2018-10-30 17:44:53', '2018-10-30 17:05:18'),
(13, 'Blog y Aviso de privacidad', '2018-10-30 17:44:52', '2018-10-30 17:05:18'),
(14, 'Acerca de APP', '2018-10-30 17:44:51', '2018-10-30 17:05:18'),
(15, 'Bancos', '2018-10-31 15:19:10', '2018-10-31 15:19:10'),
(16, 'Bancos', '2018-10-31 18:14:50', '2018-10-31 18:14:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notices`
--

DROP TABLE IF EXISTS `notices`;
CREATE TABLE `notices` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notices`
--

INSERT INTO `notices` (`id`, `titulo`, `contenido`, `imagen`, `created_at`, `updated_at`) VALUES
(10, 'test', 'esto es una prueba', 'https://porvenirapps.com/prohunter/storage/app/Img_Avisos/vHeOx1OdlJeYU06Vsezsd9zZPGom6v3089aqZEbY.png', '2018-11-05 22:58:59', '2018-11-05 22:58:59'),
(11, 'Prueba', 'Interna', 'https://porvenirapps.com/prohunter/storage/app/Img_Avisos/M47i4SKVAqXZffaZ2v2uMNMqSJ84jTlo0rRQJ0Wn.png', '2018-11-06 04:05:11', '2018-11-06 04:05:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_history`
--

DROP TABLE IF EXISTS `payment_history`;
CREATE TABLE `payment_history` (
  `id` bigint(20) NOT NULL,
  `payment_id` bigint(20) NOT NULL,
  `missing` float NOT NULL,
  `paid_percentage` varchar(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `payment_history`
--

INSERT INTO `payment_history` (`id`, `payment_id`, `missing`, `paid_percentage`, `created_at`, `updated_at`) VALUES
(22, 12, 4932, '0%', '2018-10-10 16:36:24', '2018-10-09 21:12:49'),
(23, 12, 2469, '50%', '2018-10-10 16:36:24', '2018-10-09 21:13:48'),
(24, 13, 3000, '0%', '2018-10-10 16:33:46', '2018-10-10 19:39:40'),
(25, 13, 1500, '50%', '2018-10-10 20:21:05', '2018-10-10 20:21:05'),
(26, 14, 5.6, '0%', '2018-10-16 20:51:23', '2018-10-16 20:51:23'),
(27, 15, 6000, '0%', '2018-10-16 20:58:32', '2018-10-16 20:58:32'),
(28, 16, 6000, '0%', '2018-10-18 23:04:06', '2018-10-18 23:04:06'),
(29, 17, 4500, '0%', '2018-10-18 23:07:33', '2018-10-18 23:07:33'),
(30, 18, 6366.4, '0%', '2018-10-18 23:08:47', '2018-10-18 23:08:47'),
(31, 16, 3000, '50%', '2018-10-18 23:57:35', '2018-10-18 23:57:35'),
(32, 17, 2250, '50%', '2018-10-18 23:57:45', '2018-10-18 23:57:45'),
(33, 18, 3183.2, '50%', '2018-10-18 23:57:49', '2018-10-18 23:57:49'),
(34, 12, 2466, '100%', '2018-10-19 22:54:27', '2018-10-19 22:54:27'),
(35, 13, 1500, '100%', '2018-10-19 22:57:34', '2018-10-19 22:57:34'),
(36, 19, 1470, '0%', '2018-10-22 19:45:56', '2018-10-22 19:45:56'),
(37, 20, 0.11, '0%', '2018-10-25 01:21:57', '2018-10-25 01:21:57'),
(38, 21, 5600, '0%', '2018-10-29 22:52:57', '2018-10-29 22:52:57'),
(39, 22, 3858.4, '0%', '2018-10-29 23:51:27', '2018-10-29 23:51:27'),
(40, 14, 2.8, '100%', '2018-11-09 00:43:40', '2018-11-09 00:43:40'),
(41, 15, 3000, '50%', '2018-11-09 00:43:51', '2018-11-09 00:43:51'),
(42, 23, 3500, '0%', '2018-11-14 21:29:28', '2018-11-14 21:29:28'),
(43, 24, 391.5, '0%', '2018-11-14 21:48:27', '2018-11-14 21:48:27'),
(44, 25, 558.72, '0%', '2018-11-15 05:01:23', '2018-11-15 05:01:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_users`
--

DROP TABLE IF EXISTS `payment_users`;
CREATE TABLE `payment_users` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `postulate_id` bigint(20) NOT NULL,
  `total_to_pay` float NOT NULL,
  `paid_percentage` varchar(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `payment_users`
--

INSERT INTO `payment_users` (`id`, `user_id`, `postulate_id`, `total_to_pay`, `paid_percentage`, `created_at`, `updated_at`) VALUES
(12, 2, 38, 4932, '100%', '2018-10-19 18:54:27', '2018-10-19 22:54:27'),
(13, 2, 40, 3000, '100%', '2018-10-19 18:57:34', '2018-10-19 22:57:34'),
(14, 2, 44, 5.6, '100%', '2018-11-08 19:43:40', '2018-11-09 00:43:40'),
(15, 2, 45, 6000, '50%', '2018-11-08 19:43:51', '2018-11-09 00:43:51'),
(16, 21, 47, 6000, '50%', '2018-10-18 19:57:35', '2018-10-18 23:57:35'),
(17, 21, 48, 4500, '50%', '2018-10-18 19:57:45', '2018-10-18 23:57:45'),
(18, 21, 50, 6366.4, '50%', '2018-10-18 19:57:49', '2018-10-18 23:57:49'),
(19, 21, 67, 1470, '0%', '2018-10-22 19:45:56', '2018-10-22 19:45:56'),
(20, 35, 81, 0.11, '0%', '2018-10-25 01:21:57', '2018-10-25 01:21:57'),
(21, 2, 65, 5600, '0%', '2018-10-29 22:52:57', '2018-10-29 22:52:57'),
(22, 2, 85, 3858.4, '0%', '2018-10-29 23:51:27', '2018-10-29 23:51:27'),
(23, 2, 141, 3500, '0%', '2018-11-14 21:29:28', '2018-11-14 21:29:28'),
(24, 2, 142, 391.5, '0%', '2018-11-14 21:48:27', '2018-11-14 21:48:27'),
(25, 35, 145, 558.72, '0%', '2018-11-15 05:01:23', '2018-11-15 05:01:23');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `postulados_rechazados`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `postulados_rechazados`;
CREATE TABLE `postulados_rechazados` (
`id` int(10) unsigned
,`vacante_id` int(10) unsigned
,`correo` varchar(191)
,`candidato` text
,`vacancy_name` varchar(191)
,`company_name` varchar(191)
,`status` varchar(191)
,`reason_rejected` text
,`date_rejected` datetime
,`Created_by` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `Postulados_vs_contratados_30_dias`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `Postulados_vs_contratados_30_dias`;
CREATE TABLE `Postulados_vs_contratados_30_dias` (
`Total_postulados` bigint(21)
,`Total_contratados` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences`
--

DROP TABLE IF EXISTS `preferences`;
CREATE TABLE `preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `preferences`
--

INSERT INTO `preferences` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '1', '2018-09-18 20:38:26', '2018-09-18 20:38:26'),
(2, '2', '2018-09-18 20:41:26', '2018-09-18 20:41:26'),
(3, '4', '2018-09-18 23:24:09', '2018-09-18 23:24:09'),
(4, '7', '2018-09-19 19:22:07', '2018-09-19 19:22:07'),
(5, '9', '2018-09-19 20:28:08', '2018-09-19 20:28:08'),
(6, '10', '2018-09-19 20:30:16', '2018-09-19 20:30:16'),
(7, '11', '2018-09-19 20:48:49', '2018-09-19 20:48:49'),
(8, '12', '2018-09-19 20:49:43', '2018-09-19 20:49:43'),
(9, '15', '2018-09-24 06:35:47', '2018-09-24 06:35:47'),
(10, '16', '2018-09-26 20:02:27', '2018-09-26 20:02:27'),
(11, '17', '2018-09-26 20:42:16', '2018-09-26 20:42:16'),
(12, '18', '2018-09-27 20:23:41', '2018-09-27 20:23:41'),
(13, '20', '2018-09-27 20:53:28', '2018-09-27 20:53:28'),
(14, '21', '2018-09-28 02:15:58', '2018-09-28 02:15:58'),
(15, '22', '2018-09-28 19:53:48', '2018-09-28 19:53:48'),
(16, '31', '2018-10-02 01:59:23', '2018-10-02 01:59:23'),
(17, '32', '2018-10-02 02:26:50', '2018-10-02 02:26:50'),
(18, '33', '2018-10-02 21:40:51', '2018-10-02 21:40:51'),
(19, '34', '2018-10-02 23:19:24', '2018-10-02 23:19:24'),
(20, '35', '2018-10-02 23:23:50', '2018-10-02 23:23:50'),
(21, '38', '2018-10-03 19:06:34', '2018-10-03 19:06:34'),
(22, '40', '2018-10-04 02:41:20', '2018-10-04 02:41:20'),
(23, '41', '2018-10-08 22:21:41', '2018-10-08 22:21:41'),
(24, '42', '2018-10-10 02:30:34', '2018-10-10 02:30:34'),
(25, '43', '2018-10-11 04:25:27', '2018-10-11 04:25:27'),
(26, '60', '2018-10-15 20:13:57', '2018-10-15 20:13:57'),
(27, '63', '2018-10-17 17:39:44', '2018-10-17 17:39:44'),
(28, '64', '2018-10-17 19:47:12', '2018-10-17 19:47:12'),
(29, '69', '2018-10-22 18:00:35', '2018-10-22 18:00:35'),
(30, '70', '2018-10-22 23:08:27', '2018-10-22 23:08:27'),
(31, '73', '2018-10-25 01:35:12', '2018-10-25 01:35:12'),
(32, '75', '2018-10-25 22:36:45', '2018-10-25 22:36:45'),
(33, '76', '2018-10-29 23:05:45', '2018-10-29 23:05:45'),
(34, '77', '2018-10-29 23:09:14', '2018-10-29 23:09:14'),
(35, '78', '2018-10-29 23:35:23', '2018-10-29 23:35:23'),
(36, '80', '2018-10-30 23:05:49', '2018-10-30 23:05:49'),
(37, '24', '2018-11-02 00:29:08', '2018-11-02 00:29:08'),
(38, '89', '2018-11-05 20:55:15', '2018-11-05 20:55:15'),
(39, '92', '2018-11-08 02:15:39', '2018-11-08 02:15:39'),
(40, '94', '2018-11-16 03:26:09', '2018-11-16 03:26:09'),
(41, '95', '2018-11-20 20:56:13', '2018-11-20 20:56:13'),
(42, '96', '2018-11-20 23:04:00', '2018-11-20 23:04:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privacy_agreement`
--

DROP TABLE IF EXISTS `privacy_agreement`;
CREATE TABLE `privacy_agreement` (
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `privacy_agreement`
--

INSERT INTO `privacy_agreement` (`text`, `created_at`, `updated_at`) VALUES
('&lt;div style=&quot;text-align: center;&quot;&gt;&lt;div&gt;&lt;span style=&quot;text-align: left;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;De acuerdo a lo Previsto en la “Ley Federal de Protección de Datos Personales”, declara Desarrollos Móviles y Multimedia S.A de C.V, ser una empresa legalmente constituida de conformidad con las leyes mexicanas, con domicilio fiscal y sus oficinas centrales en López Mateos Sur 2077 Local Z-5, Colonia Jardines de Plaza del Sol, CP 44510 Guadalajara, Jalisco, México; y como responsable del tratamiento de sus datos personales, hace de su conocimiento que la información de nuestros clientes es tratada de forma estrictamente confidencial por lo que al proporcionar sus datos personales, tales como:&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&amp;nbsp;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-7&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;1.&lt;/span&gt;&amp;nbsp;Nombre Completo.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-10&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;2.&lt;/span&gt;&amp;nbsp;Dirección.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-13&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;3.&lt;/span&gt;&amp;nbsp;Registro Federal de Contribuyentes.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-16&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;4.&amp;nbsp;&lt;/span&gt;Teléfonos Oficina y móviles.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-19&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;5.&lt;/span&gt;&amp;nbsp;Correo Electrónico.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&amp;nbsp;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Finalidades y uso de datos personales:&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-25&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;1.&lt;/span&gt;&amp;nbsp;Campañas de Publicidad.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-28&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;2.&lt;/span&gt;&amp;nbsp;Proveer servicios y productos solicitados.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-31&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;3.&lt;/span&gt;&amp;nbsp;Notificación de nuevos servicios o productos, o cambios en los mismos.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-34&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;4.&lt;/span&gt;&amp;nbsp;Actualización de la Base de Datos.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-37&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;5.&lt;/span&gt;&amp;nbsp;Evaluación de nuestra calidad de servicio.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span id=&quot;u7256-40&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;6.&lt;/span&gt;&amp;nbsp;Cumplimiento a las obligaciones contraídas.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&amp;nbsp;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Medidas para guardar la confidencialidad y Manejo de los Datos Personales:&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Para prevenir el acceso no autorizado a sus datos personales y con el fin de asegurar que la información sea utilizada para los fines establecidos en este aviso de privacidad, hemos establecido diversos procedimientos con la finalidad de evitar el uso o divulgación no autorizados de sus datos, permitiéndonos tratarlos debidamente.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&amp;nbsp;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Derechos de Acceso, Rectificación, Oposición y Cancelación:&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Consultores en Sistemas Informáticos de R.H S.A de C.V cuenta con políticas y procedimientos que contemplan medidas de seguridad y controles para proteger los Datos Personales, especialmente los Datos Personales Sensibles. Por lo anterior los Datos Personales incluyendo los Datos Personales Sensibles proporcionados en forma voluntaria ya sea en forma física, electrónica o por cualquier otro medio son tratados y guardados de manera confidencial a través de medios tecnológicos y de procedimientos internos de protección. Los Titulares de los Datos Personales de Consultores en Sistemas Informáticos de R.H S.A de C.V están obligados a proporcionar Datos Personales precisos, claros y completos al momento de que se lleve a cabo su recopilación.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&amp;nbsp;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Transferencia de Datos Personales:&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Todos sus datos personales son tratados de acuerdo a la legislación aplicable y vigente en el país, por ello le informamos que usted tiene en todo momento los derechos (ARCO) de acceder, rectificar, cancelar u oponerse al tratamiento que le damos a sus datos personales; derecho que podrá hacer valer a través del Área de Privacidad encargada de la seguridad de datos personales en el Teléfono (33) 30 30 71 04, o por medio de su correo electrónico: scarranza@oomovil.com. A través de estos canales usted podrá actualizar sus datos y especificar el medio por el cual desea recibir información, ya que en caso de no contar con esta especificación de su parte, Consultores en Sistemas Informáticos de R.H S.A de C.V, establecerá libremente el canal que considere pertinente para enviarle información. Modificación al Aviso de Privacidad, Legislación y Jurisdicción:&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&amp;nbsp;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Cualquier modificación a este aviso de privacidad podrá consultarla en&amp;nbsp;&lt;span id=&quot;u7256-60&quot; style=&quot;color: rgb(193, 39, 45);&quot;&gt;www.oomovil.com&lt;/span&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&amp;nbsp;&lt;/div&gt;&lt;/div&gt;', '2018-06-27 21:32:25', '2018-06-27 21:32:25');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `reporte_candidatos_a_apagar`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `reporte_candidatos_a_apagar`;
CREATE TABLE `reporte_candidatos_a_apagar` (
`id_usuario` varchar(191)
,`Usuario_APP` varchar(191)
,`fecha_cumpleaños` date
,`RFC` varchar(30)
,`cuenta_clabe` varchar(50)
,`Nombre_vacante` varchar(191)
,`Candidato` text
,`Total_a_pagar` float
,`Porcentage_a_pagar` varchar(5)
,`faltante` double
,`fecha` timestamp
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `software`
--

DROP TABLE IF EXISTS `software`;
CREATE TABLE `software` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `software`
--

INSERT INTO `software` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Adobe Illustrator', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(7, 'mysql', '2018-08-02 15:38:46', '2018-08-02 15:38:46'),
(9, 'a', '2018-10-18 18:27:28', '2018-10-18 18:27:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `software_categories`
--

DROP TABLE IF EXISTS `software_categories`;
CREATE TABLE `software_categories` (
  `id` bigint(20) NOT NULL,
  `id_categoria` bigint(20) NOT NULL,
  `id_software` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `software_categories`
--

INSERT INTO `software_categories` (`id`, `id_categoria`, `id_software`) VALUES
(1, 26, 1),
(4, 26, 7),
(6, 31, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `states`
--

INSERT INTO `states` (`id`, `name`, `short_name`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'Aguascalientes', 'Ags.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(2, 'Baja California', 'BC', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(3, 'Baja California Sur', 'BCS', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(4, 'Campeche', 'Camp.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(5, 'Coahuila de Zaragoza', 'Coah.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(6, 'Colima', 'Col.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(7, 'Chiapas', 'Chis.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(8, 'Chihuahua', 'Chih.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(9, 'Distrito Federal', 'DF', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(10, 'Durango', 'Dgo.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(11, 'Guanajuato', 'Gto.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(12, 'Guerrero', 'Gro.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(13, 'Hidalgo', 'Hgo.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(14, 'Jalisco', 'Jal.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(15, 'México', 'Mex.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(16, 'Michoacán de Ocampo', 'Mich.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(17, 'Morelos', 'Mor.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(18, 'Nayarit', 'Nay.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(19, 'Nuevo León', 'NL', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(20, 'Oaxaca', 'Oax.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(21, 'Puebla', 'Pue.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(22, 'Querétaro', 'Qro.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(23, 'Quintana Roo', 'Q. Roo', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(24, 'San Luis Potosí', 'SLP', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(25, 'Sinaloa', 'Sin.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(26, 'Sonora', 'Son.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(27, 'Tabasco', 'Tab.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(28, 'Tamaulipas', 'Tamps.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(29, 'Tlaxcala', 'Tlax.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(30, 'Veracruz de Ignacio de la Llave', 'Ver.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(31, 'Yucatán', 'Yuc.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23'),
(32, 'Zacatecas', 'Zac.', '1', '2018-06-27 21:32:23', '2018-06-27 21:32:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_candidate`
--

DROP TABLE IF EXISTS `status_candidate`;
CREATE TABLE `status_candidate` (
  `id` bigint(20) NOT NULL,
  `name_status` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `status_candidate`
--

INSERT INTO `status_candidate` (`id`, `name_status`, `created_at`, `updated_at`) VALUES
(1, 'Postulado', '2018-08-13 14:25:44', '2018-08-13 14:25:44'),
(2, 'CV Visto', '2018-08-13 14:25:44', '2018-08-13 14:25:44'),
(3, 'En entrevista', '2018-08-13 14:25:44', '2018-08-13 14:25:44'),
(4, 'Psicométrico', '2018-08-13 14:25:44', '2018-08-13 14:25:44'),
(5, 'Contratado', '2018-08-13 14:25:44', '2018-08-13 14:25:44'),
(6, 'Vacante cubierta', '2018-08-17 17:58:58', '2018-08-17 17:58:58'),
(7, 'Rechazado', '2018-08-17 17:58:59', '2018-08-17 17:58:59');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `tabla_postulados`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `tabla_postulados`;
CREATE TABLE `tabla_postulados` (
`id` int(10) unsigned
,`id_candidate` int(10) unsigned
,`Usuario` varchar(191)
,`names` varchar(191)
,`paternal_name` varchar(191)
,`maternal_name` varchar(191)
,`Compañia` varchar(191)
,`Vacancy` varchar(191)
,`vacancy_id` varchar(191)
,`status` varchar(191)
,`created_by` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contrasena` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_dispositivo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen_perfil` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fecha_cumpleaños` date DEFAULT NULL,
  `lugar_nacimiento` text COLLATE utf8mb4_unicode_ci,
  `RFC` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre`, `correo`, `contrasena`, `celular`, `info_dispositivo`, `imagen_perfil`, `tipo`, `created_at`, `updated_at`, `fecha_cumpleaños`, `lugar_nacimiento`, `RFC`) VALUES
(1, 'arosas', 'admin@oomovil.com', '$2y$10$P74.MPEbxPSUS7fVz6YI0./513jBopebyRkNTQJ3Y0sXNXX.v04c.', '3314598239', 'Web', 'imagenes_perfil/RdKkWX9WiIzaG2gKvnquUBua6rPPF4Oz7LBzfSn3.jpeg', 'Root', '2018-09-18 20:38:23', '2018-10-31 20:55:56', NULL, NULL, NULL),
(2, 'Diego Vargas Joel', 'yeyo@correo.com', '$2y$10$IXtd2pSXVWrH.s2Z5cMPyupFWZYniQ7WvklfyUnBmBBXhgXNfz9Me', '3314598223', 'iPhone 7 - BuildVersion: 7', 'imagenes_perfil/uyHhVtMo8ipIgvsO5T07eRi93mFhjGMdGcSkyONA.png', 'normal', '2018-09-18 20:40:52', '2018-11-26 16:46:28', '2018-05-09', 'Guadalajara, Jalisco', 'ervdfvdfvdfvd'),
(9, 'Meli Arceo', 'gZgqLsPSuTQbYNKkveB2QaPCsOE2@prohunter.com', '$2y$10$vhQ0/yWKhjX8ovOFJAl7yuuWHzvW1ldjiv9rTBNorT80Dn38lLIBG', NULL, 'iPhone X - BuildVersion: 2', 'imagenes_perfil/tbn0Aev1WZAC4F7zIXjwG9fWVl14gQsRjuidhBpV.jpeg', 'facebook', '2018-09-19 20:27:56', '2018-09-19 20:27:56', NULL, NULL, NULL),
(10, 'Alee Andrade', 'G0syFOf7x7T2KU38KUIyhCJPSkJ2@prohunter.com', '$2y$10$mzScGqUQjH3RaFlH/EKYP.DS.CDC7MdUyXQs3NayBn.2akmhxxb6e', NULL, 'iPhone 7 Plus - BuildVersion: 8', 'imagenes_perfil/iiyXf0zVEO2B9wuuwd6bNd0hGL0Zlf6qGkgwoYrj.jpeg', 'facebook', '2018-09-19 20:30:10', '2018-12-26 04:58:26', NULL, NULL, NULL),
(11, 'Alan Castro', 'alan.castro@oomovil.com', '$2y$10$P8/Tye2IDLpopLZ5HNLlHOmtg1ePMyX8izQsC.qwIrH8jWez/G4C.', '4991008189', 'iPhone 8 - BuildVersion: 8', NULL, 'normal', '2018-09-19 20:48:38', '2018-11-27 14:13:34', NULL, NULL, NULL),
(12, 'Alejandra Rosas', 'alirosas91@hotmail.com', '$2y$10$Mos32PVtkrWe.rytlUWzb.XhL0cysmeru9IAY.Yx0d/bO5Ouc13TW', '3921360335', 'iPhone 7 Plus - BuildVersion: 2', NULL, 'normal', '2018-09-19 20:49:40', '2018-09-19 21:02:39', '1991-05-24', 'Ocotlán', 'ROAA910524U18'),
(13, 'Moy Hdez', 'uw7kyDZencXBKty59zJd85IvP4Q2@prohunter.com', '$2y$10$.y1F9VJh0Ao1qFuXunaL/.d6zZbU0EB9Ep.7StY8xxxNSbGf5RQ7.', '4991008189', 'iPhone 7 - BuildVersion: 1', 'imagenes_perfil/vdyElXrkmxDVETwPMmjtsjzxiqMgYRnkjKX3SHEE.jpeg', 'facebook', '2018-09-19 20:49:59', '2018-09-19 20:55:43', '2018-09-19', 'Jsjaja', 'ROAA910524U18'),
(14, 'Francisco', 'ciscogonzalezquintero@gmail.com', '$2y$10$wieKzD1I3VxVWZBGVfY1secrFNohl3LvN03hqIkpT7LU4pf1FEMXG', '3313421637', '5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36 OPR/55.0.2994.61', NULL, 'normal', '2018-09-20 18:52:03', '2018-09-20 18:52:03', NULL, NULL, NULL),
(15, 'Lucy Hernandez', 'DSTKBm39pVWVDHpVJn3jPsSaww02@prohunter.com', '$2y$10$8WjWIa7srGxzPoPrX68A4.32kQ3QgrJaH6o.ub9oeKxDKES6I/38u', NULL, 'iPhone 7 - BuildVersion: 2', 'imagenes_perfil/Q3IFxpnA8bFKL5z2opflDoh40CkUTcSKd2Qy0Mu6.jpeg', 'google', '2018-09-24 06:35:37', '2018-09-24 06:35:37', NULL, NULL, NULL),
(17, 'Carlos Rizo', 'qPydrc4k4WandROTsa9L88ZQwPG2@google.com', '$2y$10$Uqbt3xcJsKvp9p1V640OF.z10CQP8b7VDhHJQqFPDJAebanzZn2ly', '3331665763', 'samsung-SM-G950F', 'imagenes_perfil/elZWPxIJkXUYZ1duWZmjknGA7KLnJh3KRAqD1m8H.png', 'google', '2018-09-26 20:42:11', '2018-09-27 23:16:19', NULL, NULL, NULL),
(18, 'Mary Díaz', 'YBv03Ut7m6Oq3JJdRUkeWCleAKJ2@google.com', '$2y$10$zr5IR6LgJfmS8OnSBE7h7OEV5bEm3n.uySUHII1t/KPPXrKp4X6h6', NULL, 'lge-LG-X220', 'imagenes_perfil/luAPuXfVOCNQs45Jkbuk3LTvB3grIJ3Oep9QzXuv.png', 'google', '2018-09-27 20:22:52', '2018-09-27 20:22:52', NULL, NULL, NULL),
(20, 'Lorena Mendoza', 'WHuFQFm22ZfRTqKHoaVnHX1fEgu2@google.com', '$2y$10$wBY.unocrhKgbyNiX3K4jusu7rdd0Qc9c24HPg0aEBvfXCuMroe3C', '3310823163', 'lge-LG-X220', 'imagenes_perfil/XkPZSABYgXAFjTsSO8xcEQwKohPLiJOwzgOyxJOW.png', 'google', '2018-09-27 20:53:10', '2018-09-27 21:08:29', NULL, NULL, NULL),
(21, 'Alejandra', 'alero24a@hotmail.com', '$2y$10$zIxb3jZy20L1CUOrP0L69OVVCI4/o4b/bIz4qWPsPPPNxrNWiIBvq', '3921360335', 'lge-LG-H221', 'imagenes_perfil/7kLmkMBcyWkTByTCJXMkGcRj3AvJqEChJiXRk5by.jpeg', 'normal', '2018-09-28 02:15:54', '2018-10-23 02:08:29', '1991-05-24', 'Ocotlán', 'ROAA910524U18'),
(24, 'Mario Cuevas', '5hOGKuJOukPOy0nk0hHnG6kUWv62@facebook.com', '$2y$10$mVX5zqiWxYGpT30WlvJXxuF/E3td0Bv.0IxwKdlffnzy/JjHpf2my', '0000000000', 'HUAWEI-HUAWEI CUN-L03', NULL, 'facebook', '2018-09-28 21:51:36', '2018-11-02 00:29:04', NULL, NULL, NULL),
(25, 'Alfredo Lozoya Orozco', 'dp5PTGVl1AYMQa2CLsAJKvuq1Lk1@facebook.com', '$2y$10$nE2UpsXKMEgpLTyVJxHd8.QzBmHYrSwAvpy7454x5a3vo/5Z13lAe', '3335898797', '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'imagenes_perfil/s9erDZpuXd8s58enliGLVizFHLQMZyNhBbPbIWBY.jpeg', 'facebook', '2018-09-28 22:07:49', '2018-09-28 22:13:28', '1997-05-14', 'xxxxxxxxxxxxxxxx', 'xxxxxxxxxxxxx'),
(27, 'Alee Andrade', 'G0syFOf7x7T2KU38KUIyhCJPSkJ2@facebook.com', '$2y$10$3FfqkHIiLgIxqxGIMX/tkebj4cUwf0pc/jEp6McxXineUHusYSGEe', '0000000000', '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', NULL, 'facebook', '2018-10-01 19:56:26', '2018-10-18 23:16:26', NULL, NULL, NULL),
(28, 'Midori Yanome', 'y8NXs1qqfuhdYMoIs42NQPzXZaW2@facebook.com', '$2y$10$Vns3k0gItKQYzomfK4gcLO2e71Ujt2zKJYg87w4saevyJFs57wRSW', '- -', '5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', NULL, 'facebook', '2018-10-01 20:38:13', '2018-10-02 19:34:44', NULL, '- -', '- -'),
(29, 'Mario Cuevas', 'FGSMCmwS1TOLpGWFqNgy9YFOxoo2@facebook.com', '$2y$10$rBMrBSDJd/bS/bgGFQEumuvZxq1ib56j8Gcp1e1/ASRE/lmSzjhte', '0000000000', '5.0 (Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; CMNTDFJS; Zoom 3.6.0; rv:11.0) like Gecko', 'imagenes_perfil/DG72O2Mdafgo17ntk0l1tvcl5Ss0RTXHJRZjlpf3.jpeg', 'facebook', '2018-10-01 23:37:15', '2018-10-25 02:32:15', NULL, NULL, NULL),
(30, 'Meli Arceo', 'gZgqLsPSuTQbYNKkveB2QaPCsOE2@facebook.com', '$2y$10$LPzdC1zuMyQFRbGcm596kuaLoF8Xwswonx85pUxFBXRxU3OkH3aJe', '0000000000', '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', NULL, 'facebook', '2018-10-02 01:27:20', '2018-10-02 01:27:20', NULL, NULL, NULL),
(31, 'Alan Castro Melgoza', 'DHxu5qNqNRhjejdUVfy2ypMn0zp2@facebook.com', '$2y$10$NwOP9Hn9yd5Jt/.im/Te4O2e1A1n2iuM07nhpWJPHwE6QLqhDxeUy', '- -', '5.0 (Linux; Android 7.0; Moto G (5) Build/NPPS25.137-93-14) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', NULL, 'facebook', '2018-10-02 01:55:00', '2018-10-20 18:51:51', NULL, '- -', '- -'),
(32, 'Mario Cuevas', 'FGSMCmwS1TOLpGWFqNgy9YFOxoo2@prohunter.com', '$2y$10$trg3Ka1DYJVD7VoCS.qWq.mSSNWGr8IbNfncrEvZDdYMsc6ZLHEc.', NULL, 'iPhone 6s - BuildVersion: 2', 'imagenes_perfil/Y3S8wkccYUo5raRAmZiwxUQSX5fdDKdZfAVVelN7.jpeg', 'facebook', '2018-10-02 02:25:57', '2018-10-02 02:25:57', NULL, NULL, NULL),
(33, 'Alejandra Fregoso', 'afregoso@damsa.com.mx', '$2y$10$oKJUE/aTE6L.1zASx9xGh.iGbdp6yjjPCP2H7gqPTHNmyM3M/4ffi', NULL, 'HUAWEI-HUAWEI VNS-L23', NULL, 'normal', '2018-10-02 21:40:31', '2018-10-02 21:40:31', NULL, NULL, NULL),
(34, 'ninfan', 'nninodamsa.com.mx', '$2y$10$qoXL4z4PYaBF.fIUHC3eru93b4OOrtqb1SeJ3CU7/8dcuUwx2CF8a', NULL, 'lge-LG-M700', NULL, 'normal', '2018-10-02 23:17:45', '2018-10-02 23:17:45', NULL, NULL, NULL),
(35, 'Ninfa Graciela Ortega', 'Nfh6wRZXnsW3kU3Xy0FyHp6CtTK2@facebook.com', '$2y$10$zfnL7kUSpnwn7Jlg76X2J.k5O.Vyj6VWfe/HXEyItRLmo6YnhJjom', '3311771791', 'lge-LG-M700', 'imagenes_perfil/Au4LV0gWSzozciIAblvGGa7gggGiYVZvOKAM5mPS.png', 'facebook', '2018-10-02 23:23:39', '2018-11-16 03:26:28', '2018-11-01', 'tamaulipas', '1234567890'),
(38, 'Elizabeth Pérez', 'dlqDK5L4juhZ6hp2NBLFDjT6Pco1@facebook.com', '$2y$10$yoPEy6btJRc7W6/CLRJhsO.wt9KuFQNhTOXI4mbTuJF/8aTma/x7W', NULL, 'samsung-SM-J510MN', 'imagenes_perfil/sGQ9PxeOhy98ck0RlRfAcjzlrdkxaE6cVY8Qnbh9.png', 'facebook', '2018-10-03 19:06:31', '2018-10-03 19:06:31', NULL, NULL, NULL),
(40, 'Leslie Pineda', '9wYLPmkAQXZnROnB4iR73orASKr1@facebook.com', '$2y$10$8BjUt/MfpQY4U6Z.Q4guROv5Zb6FPHUhSLIWsVwIZRqNMb44BdtpW', '3313258832', 'motorola-Moto C', 'imagenes_perfil/b9zvkRsDSZuK2dWx6nBQnUJzvTM2OxYOQUXwjknO.png', 'facebook', '2018-10-04 02:40:58', '2018-10-18 22:26:44', NULL, NULL, NULL),
(41, 'Gabriel', 'gabriel@oomovil.com', '$2y$10$rwwGYJRV6LaFmgduDg3VreLgxU.1/J205anWXba5M1Gorhl2CVDtK', '3322115544', 'HUAWEI-EML-L09', NULL, 'normal', '2018-10-08 22:15:23', '2018-11-26 17:32:09', '1995-05-08', 'Zapopan', 'AADDEEFRGTHYJ'),
(42, 'Christina Garcia', '1vusCrTMOnTcxPKLjX4JUPlLhAH2@facebook.com', '$2y$10$2umXY8E5UfbCajUdMu3jzOD6aT5oPeBjXul0DIBCM3WVvhAcIZ0Gy', NULL, 'motorola-Moto C', 'imagenes_perfil/DoFyUwVm38Jy3rh82iaQt57Oym3RdE36PWjN7syp.png', 'facebook', '2018-10-10 02:29:41', '2018-10-10 02:29:41', NULL, NULL, NULL),
(43, 'Martin Marabel', 'fe6UVBdRAVMVpDFrhJrn4Ycprpr2@facebook.com', '$2y$10$lZUgWsPB9REFIer60L4jaOGmH5jUd7LHemGCKCg9Vn7TLzdgD6C3W', NULL, 'motorola-Moto C', 'imagenes_perfil/tN1qNqO0Tkwp9fp2dvWDE50lUUD7HmS23WmUPBdm.png', 'facebook', '2018-10-11 04:24:46', '2018-10-11 04:24:46', NULL, NULL, NULL),
(61, 'Daniela Diaz', 'M30KsQxT9peKFJuNlgCs4Zj5k4Y2@facebook.com', '$2y$10$YG680lZy5oymW59LgUPyh.2jOQi1HdaHc8NfR217zgHUn/Wvnsmlq', '0000000000', '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'imagenes_perfil/xbw3MKIipw8NEBhX6jdnpF1QJjlvtmODcBBl8ATQ.jpeg', 'facebook', '2018-10-16 00:15:40', '2018-10-16 00:15:40', NULL, NULL, NULL),
(62, 'usuarioapp', 'usuario@app.com', '$2y$10$fvvILynY0lyTUpuBaayZa.cUdjpAcsQmFn1COYlGKeCITpw96NR3G', '1838383838', 'Web', NULL, 'normal', '2018-10-16 01:50:09', '2018-10-30 19:52:06', '1994-02-02', 'Guadalajara', '232323232323'),
(63, 'Jorge Manzano', 'yJ1pBnEQojgbB7sDhI9xpuLm7Fd2@facebook.com', '$2y$10$vtrGflhFvBJKFibz24crgerf.9VUnt5PQJhs8m.QYtfwFLW5MXOdC', NULL, 'samsung-SM-G935T', 'imagenes_perfil/x8OXrWVqSpD9Eqzlybpawc8uxyz4nJPKV4efopMZ.png', 'facebook', '2018-10-17 17:39:25', '2018-11-06 01:20:48', NULL, NULL, NULL),
(64, 'Alan Castro Melgoza', 'DHxu5qNqNRhjejdUVfy2ypMn0zp2@prohunter.com', '$2y$10$uwkmaPPvAmp9qpn85aTxm.bInw8H3VLi/r0U4/fSvW/AuoOCNwn6K', NULL, 'iPhone 5 - BuildVersion: 2', 'imagenes_perfil/K73D7JZSGtGS7b1szkm3s0khobh1epbw1t1Qqez5.jpeg', 'facebook', '2018-10-17 19:47:08', '2018-10-17 19:47:08', NULL, NULL, NULL),
(66, 'Hilda', 'leviz.castro@gmail.com', '$2y$10$FXCPj8hiMLLdBMDoca.yUemZ7xbjcnerOp9UoKgTqXLWvPXkpGCVK', '8656678798', 'Web', NULL, 'Administrador', '2018-10-17 23:14:11', '2018-10-30 18:52:42', NULL, NULL, NULL),
(68, 'Oomovil', 'arosas@damsa.com.mx', '$2y$10$Ghb7FrO5gC/14bc6fr5XQu5M4Ssr1d7d1NHk7LmMiFbKC1425pBpu', '3314598239', 'Web', NULL, 'Administrador', '2018-10-18 21:55:40', '2018-10-31 00:02:35', NULL, NULL, NULL),
(69, 'Juan Enrrique Gonzalez', 'jegg_07@hotmail.com', '$2y$10$kdvGA.O4hA9xjWAa/874reHGw4XHrU83JgA.vjcvfhFAJ5670v9BG', NULL, 'samsung-SM-J700M', NULL, 'normal', '2018-10-22 18:00:30', '2018-10-22 18:00:30', NULL, NULL, NULL),
(70, 'ninfa', 'nin', '$2y$10$A0mLcOTI/QwX3Xf3mqs3HuOvhnSJ4NXTYVTnz.Qk/jb4IGCj/PScC', NULL, 'lge-LG-M700', NULL, 'normal', '2018-10-22 23:08:03', '2018-10-22 23:08:03', NULL, NULL, NULL),
(71, 'ninfan', 'nnino@damsa.com.mx', '$2y$10$x9LJhnUDXz1roMKmgBqwoOpvi6t.G2V2kkrle2Tf64cqotVqKrxNi', '12', '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134', NULL, 'normal', '2018-10-22 23:21:37', '2018-10-22 23:21:37', NULL, NULL, NULL),
(72, 'pepe', 'pepetoro@yopmail.com', '$2y$10$bcV9OkpqrX2TSpjwBmbgRe7No1gtX6GMydylwe31OBTOWD2ML5sEy', '3333333333', '5.0 (X11)', NULL, 'normal', '2018-10-23 22:19:05', '2018-10-23 22:19:05', NULL, NULL, NULL),
(73, 'Alberto Flores', 'to7NhkS56jZF368fKAtio1G6coF2@prohunter.com', '$2y$10$P3YM5RPEWtkpEjO5SvpCc.OQ84tB4BEdjKLLTnynwziQ1ecElVhNC', NULL, 'iPhone 6 - BuildVersion: 5', 'imagenes_perfil/1RlZEJsTcNJmc5LflOSkR4SzRsuWTYzR4uOmG4fX.jpeg', 'facebook', '2018-10-25 01:35:03', '2018-11-14 03:28:08', NULL, NULL, NULL),
(74, 'Jorge Manzano', 'FCBWqMstBifCM1As3CjFZeDmKhH3@google.com', '$2y$10$Ii6SbM56LoUTZ00w//PGCO3cvCcJu6yM3o3BWW2jOb8GVuFp27gK.', '0000000000', '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'imagenes_perfil/juTQtTkD9iqkaeQHBcqujDLOXq2A3LoYpPMzREt6.jpeg', 'google', '2018-10-25 21:22:10', '2018-10-27 04:20:24', NULL, NULL, NULL),
(76, 'Juan Roberto', 'micorreo', '$2y$10$OSSsbI.osBxxjdMV.Wo3.ejWRQDJY3gGku3x.w7Stz3OEO7S8hZ16', NULL, 'HUAWEI-EML-L09', NULL, 'normal', '2018-10-29 23:03:19', '2018-10-29 23:03:19', NULL, NULL, NULL),
(77, 'juan Manuel', 'micorreo@correo.com', '$2y$10$NRJc3BKSTNEdeyGEZ46ymO/LB.d5PH3xHYsAQY6EogAVvt48WkVYW', NULL, 'HUAWEI-EML-L09', NULL, 'normal', '2018-10-29 23:09:09', '2018-10-29 23:09:09', NULL, NULL, NULL),
(78, 'erick', 'erick@correo.com', '$2y$10$Gknk7fxeCPQp/9SWEBzWb.Jg4.8bVwiN.Ocx4jBGiVgowOdwVpJeu', NULL, 'motorola-Moto G (5)', NULL, 'normal', '2018-10-29 23:35:18', '2018-10-29 23:35:18', NULL, NULL, NULL),
(80, 'gato', 'dongato@correo.com', '$2y$10$6p5sW2.qctqgB7EZZRJnJOGTDyxKEauqwy7pry0J.8mZz1IfwPbwG', NULL, 'HUAWEI-EML-L09', NULL, 'normal', '2018-10-30 23:02:15', '2018-10-30 23:02:15', NULL, NULL, NULL),
(88, 'Octavio Villavicencio Cruz', 'FQ6cjEUKmxeTMpYq9kjDNTp3Fo33@facebook.com', '$2y$10$ZqSsy3tAJYOGv68rjrVcxeWHXEXYbUDIVIOjnxTVV4bN7F7W6BEGW', '0000000000', '5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'imagenes_perfil/EF2YZCDqmDbD0RgzjKBtlRQZmzOcG1t24Xq5Ijpg.jpeg', 'facebook', '2018-10-31 20:01:05', '2018-11-29 16:48:10', NULL, NULL, NULL),
(89, 'ale', 'dggh@aaa.com', '$2y$10$RMpTKu6DZP6WTvg1ZuZs8eF/uu2IGf9IX9BJHdEembJ1VjrSVQr9W', NULL, 'lge-LG-M700', NULL, 'normal', '2018-11-01 22:43:31', '2018-11-01 22:43:31', NULL, NULL, NULL),
(90, 'Ninfa', 'nin@aa.com', '$2y$10$br9MVM02OKe5gRitRSPGUOsbU4hI7xSBR51JMAL/cFXbxz4IhaI2m', '1223456789', 'Web', NULL, 'Administrador', '2018-11-05 22:55:39', '2018-11-05 22:55:39', NULL, NULL, NULL),
(91, 'Jorge Manzano', 'jorge@oomovil.com', '$2y$10$js80XfdQ6JZrDCbyyQkHpuyeZCp/UHrNp7Sk3PedAg9yw3/TRO2Ci', '3314598239', 'Web', NULL, 'Root', '2018-11-06 21:32:19', '2018-11-06 21:32:19', NULL, NULL, NULL),
(92, 'A Gandara', 'nH2HN7YHyfO61oLoAbMW5xN6XGX2@google.com', '$2y$10$g4AYk.YWEZX8lT7fMID9oO/8z7sliDlK0r1w16vNc9AjmzL3ssF6y', NULL, 'samsung-SM-G925I', 'imagenes_perfil/UzVtqM0wkELw0YRxAfMrsxANWa98cvcrCwm7kowP.png', 'google', '2018-11-08 02:15:00', '2018-11-08 02:15:00', NULL, NULL, NULL),
(93, 'Kenneth Yates', 'octavio@correo.com', '$2y$10$ZQIzMDnRMW4qp6DxWEyqHuqxZIoIMgO94S8c0IxbHYylt738R3f2K', '13311933137', '5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', NULL, 'normal', '2018-11-14 20:22:12', '2018-11-14 20:22:12', NULL, NULL, NULL),
(94, 'ninfa87', 'ninfa@gmail.com', '$2y$10$DIl2uHP2T5/r6WFkr8LPAuGA6.DIDluTY9izMfsnGtgM/Xr/KpuL6', NULL, 'lge-LG-M700', NULL, 'normal', '2018-11-16 03:22:35', '2018-11-16 03:22:35', NULL, NULL, NULL),
(95, 'Moy', 'moy@moy.com.mx', '$2y$10$UZ5sHBJuYoXVSYqU94iXLO4tTRrPaECq36Ze3m0BX3TV1crLr8.Me', '3331001583', 'iPhone 7 - BuildVersion: 5', NULL, 'normal', '2018-11-20 20:56:10', '2018-11-20 20:56:10', NULL, NULL, NULL),
(96, 'Alejandro', 'sandoval@correo.com.mx', '$2y$10$QT5X3z2.T1Hnqr3jWsn2Leoj8ZlH1u0d7ffFZJ7NfsJiadOpXuQZy', NULL, 'iPhone 7 - BuildVersion: 6', NULL, 'normal', '2018-11-20 23:03:03', '2018-11-21 00:00:45', NULL, NULL, NULL),
(97, 'Lucero', 'daniela@oomovil.com', '$2y$10$ZOWX5pOIt2dpSdf1J8D9wu4Q/idsVFm0pQwNCLKpWqKinv.O40xjC', '3313517684', '5.0 (Linux; Android 5.0.2; LG-D693n Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', NULL, 'normal', '2018-11-29 16:23:45', '2018-11-29 16:23:45', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_bank_data`
--

DROP TABLE IF EXISTS `user_bank_data`;
CREATE TABLE `user_bank_data` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `cuenta_clabe` varchar(50) NOT NULL,
  `banco_destino` varchar(450) NOT NULL,
  `beneficiario` varchar(450) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_bank_data`
--

INSERT INTO `user_bank_data` (`id`, `id_user`, `cuenta_clabe`, `banco_destino`, `beneficiario`, `created_at`, `updated_at`) VALUES
(2, 21, '012320015526091696', 'BBVA Bancomer', 'Alejandra Rosas Andrade', '2018-09-28 02:55:09', '2018-09-28 02:55:09'),
(3, 31, '- -', 'Banorte', '- -', '2018-10-02 02:02:43', '2018-10-02 02:02:43'),
(4, 35, '111222333444555666', 'Banorte', 'A', '2018-10-03 00:02:37', '2018-10-03 00:02:37'),
(5, 50, '- -', 'Banorte', '- -', '2018-10-12 21:27:23', '2018-10-12 21:27:23'),
(7, 2, '123456789012234557', 'Banco Azteca', 'MANZANO ROJO', '2018-10-31 18:28:08', '2018-10-31 22:28:08'),
(8, 41, '556683727282727272', 'Banorte', 'Okjvhchfhfh', '2018-10-16 19:30:38', '2018-10-16 19:30:38'),
(9, 69, '333333333333333333', 'BBVA Bancomer', 'Juan Enrique Gómez González', '2018-10-22 18:54:11', '2018-10-22 18:54:11'),
(10, 63, '685424386868686863', 'Banorte', 'Ychcjcjvjv', '2018-11-06 19:49:37', '2018-11-06 19:49:37'),
(11, 73, '616464646646464964', 'BBVA Bancomer', 'Akeksndl ekenekdk', '2018-11-14 04:00:01', '2018-11-14 04:00:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_rating`
--

DROP TABLE IF EXISTS `user_rating`;
CREATE TABLE `user_rating` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `qualification` float NOT NULL,
  `commentary` text,
  `admin_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_rating`
--

INSERT INTO `user_rating` (`id`, `user_id`, `qualification`, `commentary`, `admin_id`, `created_at`, `updated_at`) VALUES
(1, 122, 3, NULL, 125, '2018-09-08 02:31:00', '2018-09-08 06:31:00'),
(2, 138, 4.9, NULL, 125, '2018-09-03 14:27:09', '2018-09-03 18:27:09'),
(3, 144, 4.5, NULL, 125, '2018-09-03 01:34:51', '2018-09-03 01:34:51'),
(4, 117, 4.5, NULL, 125, '2018-09-03 14:58:35', '2018-09-03 18:58:35'),
(5, 148, 3.4, NULL, 125, '2018-09-04 14:12:31', '2018-09-04 18:12:31'),
(6, 142, 4.7, NULL, 125, '2018-09-04 03:34:18', '2018-09-04 03:34:18'),
(7, 150, 4.7, NULL, 125, '2018-09-05 20:05:38', '2018-09-06 00:05:38'),
(8, 2, 4.8, NULL, 1, '2018-11-14 19:53:01', '2018-11-15 00:53:01'),
(9, 35, 0, NULL, 1, '2018-10-12 19:44:32', '2018-10-12 23:44:32'),
(10, 38, 0, NULL, 1, '2018-10-12 19:52:41', '2018-10-12 23:52:41'),
(11, 89, 0, NULL, 1, '2018-11-05 23:02:02', '2018-11-06 04:02:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_vacancy`
--

DROP TABLE IF EXISTS `user_vacancy`;
CREATE TABLE `user_vacancy` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_vacancy`
--

INSERT INTO `user_vacancy` (`id`, `user_id`, `vacancy_id`, `created_at`, `updated_at`) VALUES
(24, '117', '75', '2018-08-16 20:59:34', '2018-08-16 20:59:34'),
(29, '117', '77', '2018-08-16 21:03:27', '2018-08-16 21:03:27'),
(30, '88', '69', '2018-08-16 21:40:24', '2018-08-16 21:40:24'),
(35, '123', '70', '2018-08-17 03:32:58', '2018-08-17 03:32:58'),
(36, '123', '71', '2018-08-17 03:32:59', '2018-08-17 03:32:59'),
(45, '123', '79', '2018-08-17 19:21:52', '2018-08-17 19:21:52'),
(47, '123', '80', '2018-08-17 19:21:56', '2018-08-17 19:21:56'),
(55, '125', '69', '2018-08-17 20:40:22', '2018-08-17 20:40:22'),
(56, '125', '71', '2018-08-17 20:40:23', '2018-08-17 20:40:23'),
(82, '117', '70', '2018-08-21 00:00:20', '2018-08-21 00:00:20'),
(88, '127', '77', '2018-08-21 20:58:03', '2018-08-21 20:58:03'),
(89, '127', '72', '2018-08-21 20:58:05', '2018-08-21 20:58:05'),
(90, '123', '72', '2018-08-21 20:58:15', '2018-08-21 20:58:15'),
(91, '123', '68', '2018-08-21 20:58:21', '2018-08-21 20:58:21'),
(92, '117', '80', '2018-08-21 20:58:34', '2018-08-21 20:58:34'),
(93, '117', '79', '2018-08-21 20:58:40', '2018-08-21 20:58:40'),
(95, '117', '76', '2018-08-21 23:20:22', '2018-08-21 23:20:22'),
(104, '123', '75', '2018-08-22 20:53:35', '2018-08-22 20:53:35'),
(109, '138', '71', '2018-08-25 02:19:35', '2018-08-25 02:19:35'),
(110, '138', '69', '2018-08-25 02:19:36', '2018-08-25 02:19:36'),
(119, '142', '79', '2018-08-28 18:31:28', '2018-08-28 18:31:28'),
(121, '140', '67', '2018-08-28 19:11:13', '2018-08-28 19:11:13'),
(124, '140', '70', '2018-08-28 19:26:27', '2018-08-28 19:26:27'),
(126, '140', '68', '2018-08-31 00:42:48', '2018-08-31 00:42:48'),
(160, '148', '69', '2018-09-04 01:28:31', '2018-09-04 01:28:31'),
(162, '140', '77', '2018-09-06 01:39:29', '2018-09-06 01:39:29'),
(164, '140', '75', '2018-09-06 19:02:39', '2018-09-06 19:02:39'),
(168, '143', '77', '2018-09-07 04:34:08', '2018-09-07 04:34:08'),
(171, '148', '70', '2018-09-10 19:35:10', '2018-09-10 19:35:10'),
(173, '161', '69', '2018-09-11 20:18:55', '2018-09-11 20:18:55'),
(176, '164', '75', '2018-09-13 02:59:01', '2018-09-13 02:59:01'),
(182, '166', '75', '2018-09-13 20:21:08', '2018-09-13 20:21:08'),
(185, '167', '79', '2018-09-13 20:53:43', '2018-09-13 20:53:43'),
(186, '166', '77', '2018-09-13 21:10:58', '2018-09-13 21:10:58'),
(187, '166', '79', '2018-09-13 21:11:00', '2018-09-13 21:11:00'),
(188, '166', '80', '2018-09-13 21:11:03', '2018-09-13 21:11:03'),
(189, '166', '70', '2018-09-13 21:11:09', '2018-09-13 21:11:09'),
(190, '167', '69', '2018-09-13 21:12:42', '2018-09-13 21:12:42'),
(191, '167', '76', '2018-09-13 21:26:20', '2018-09-13 21:26:20'),
(192, '167', '77', '2018-09-13 21:32:31', '2018-09-13 21:32:31'),
(193, '167', '71', '2018-09-13 21:32:32', '2018-09-13 21:32:32'),
(194, '156', '68', '2018-09-14 20:13:50', '2018-09-14 20:13:50'),
(195, '156', '70', '2018-09-14 20:13:59', '2018-09-14 20:13:59'),
(196, '140', '71', '2018-09-14 20:23:58', '2018-09-14 20:23:58'),
(207, '122', '68', '2018-09-18 20:17:03', '2018-09-18 20:17:03'),
(208, '122', '72', '2018-09-18 20:18:36', '2018-09-18 20:18:36'),
(284, '33', '36', '2018-10-06 04:06:02', '2018-10-06 04:06:02'),
(285, '33', '37', '2018-10-06 04:06:27', '2018-10-06 04:06:27'),
(317, '21', '37', '2018-10-19 20:04:03', '2018-10-19 20:04:03'),
(318, '21', '35', '2018-10-19 20:04:08', '2018-10-19 20:04:08'),
(327, '2', '20', '2018-11-06 01:17:25', '2018-11-06 01:17:25'),
(328, '2', '22', '2018-11-08 22:33:37', '2018-11-08 22:33:37'),
(331, '35', '47', '2018-11-21 00:49:20', '2018-11-21 00:49:20'),
(333, '41', '37', '2018-11-26 15:25:39', '2018-11-26 15:25:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vacancies`
--

DROP TABLE IF EXISTS `vacancies`;
CREATE TABLE `vacancies` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract_profit_percentage` int(11) NOT NULL,
  `company_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Active` tinyint(4) DEFAULT '1',
  `Created_by` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `limit_date` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vacancies`
--

INSERT INTO `vacancies` (`id`, `category_id`, `vacancy_name`, `company_name`, `company_logo`, `country`, `state`, `city`, `schedule_type`, `salary`, `contract_profit_percentage`, `company_info`, `details`, `created_at`, `updated_at`, `Active`, `Created_by`, `limit_date`) VALUES
(19, '49', 'MONITORISTA', 'SUMA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/TJeNxf0bHH1jV75kG399TX28MnwgbidxqCfO3Ixs.png', '1', 'Jalisco', 'Zapopan', '09:00 A 19:00', '8,000', 75, 'EMPRESA: SUMA', 'Monitorista (programar, coordinar y eficientar los trayectos realizados en el transporte de personal) interpretar. Instrucciones de trabajo para conocer e identificar las operaciones y actividades a realizar como parte del proceso de transporte.  Programador de viajes empresariales. Monitoreo y control de autobuses, camiones, camionetas y subcontratistas. Rastreo y monitoreo satelital permanente vía GPS, realizar reportes por tiempos de rutas. Seguimiento a unidades durante todo su recorrido. Análisis de mapas vs. Gps. Reporte de vialidad, bloqueos o reparación/mantenimiento avenidas principales. Realizar llamadas a los operadores para situaciones de riesgo y comunicarlas a todo el equipo. Información actualizada en sistema y base de datos.\r\n\r\nEscolaridad\r\nGrado escolaridad: preparatoria\r\nCarrera: no necesaria\r\nAvance: 100 %\r\n\r\nSoftware\r\nSoftware dominio\r\nOficio ó profesión\r\nOficio experiencia.\r\n\r\nConocimiento de idiomas\r\nIdioma lectura escritura conversación\r\n\r\nHabilidades y competencias:\r\nAlto sentido de urgencia, liderazgo/enfoque a resultados,\r\nNegociación/analítico planeación y organización pro actividad\r\nSolución de problemas\r\n\r\nComentarios:\r\n3 Años de experiencia en el puesto.', '2018-10-05 21:52:54', '2018-10-05 21:54:15', 0, '1', '2018-10-20'),
(20, '34', 'ASESOR DE VENTAS', 'OMNIBUS DE TEQUILA, S.A. DE C.V.', 'https://porvenirapps.com/prohunter/storage/app/company_logos/giJGWjCvsBcJKQpYRU7mUDsAWvszPUkLPViBfJkL.png', '1', 'Nayarit', 'Compostela', '8 Horas diarias', '6,000.00', 75, 'Empresa: OMNIBUS DE TEQUILA, S.A. DE C.V.', 'Venta de boletos transporte , atención a clientes, cortes de caja chica, actitud de servicio, conocimientos básicos en\r\nComputación. Disponibilidad de horario. Vacante para cubrir urgentemente. 1 mujer\r\n\r\nEscolaridad\r\nGrado escolaridad: preparatoria\r\nCarrera: no necesaria\r\nAvance: 10 %.\r\n\r\nSoftware\r\nSoftware dominio\r\nMicrosoft office básico\r\nOficio ó profesión.\r\n\r\nOficio experiencia\r\nVentas ninguna.\r\n\r\nConocimiento de idiomas\r\nIngles básico intermedio.', '2018-10-05 22:06:15', '2018-10-05 22:06:15', 1, '1', '2018-10-20'),
(22, '31', 'OPERADOR DE CNC', 'SITE PLASTICO', 'https://porvenirapps.com/prohunter/storage/app/company_logos/JGcvNw95okIzgedEwqwYiF7hxeCZLOsJyr62RbwZ.jpeg', '1', 'Jalisco', 'San Pedro Tlaquepaque', '08:00 A 17:30 - 3 x 4', '7,958', 80, 'Empresa: SITE PLASTICO', 'Operador de torno. Atender las solicitudes de refacciones para maquinaria, mantenientos correctivos, prepara materiales para proyectos nuevos. Conocimiento en el manejo de maquinaria; perfiladora 3 ejes, torno convencional, taladro de pedestal, taladros mano, interpretación dibujo técnico. Conocimiento en CAD CAM , conocimiento de moldes.\r\n\r\nComentarios:\r\nRolar turnos. Semana de jueves a miércoles, se paga el viernes. Se checa 4 veces al día (entrada, salida de alimentos, entrada de alimentos, salida).\r\n\r\nExperiencia de 3 años mínimo. Experiencia requerida en\r\nMantenimiento maquinaria, moldes inyección, moldes termoformado, maquinados convencionales.', '2018-10-05 22:25:45', '2018-10-05 22:28:48', 1, '1', '2018-10-20'),
(23, '34', 'VENDEDOR DE MOSTRADOR', 'NAFARRATE EQUIPO MEDICO', 'https://porvenirapps.com/prohunter/storage/app/company_logos/pGRtd49rXzwqjwwupKoYVKedeoNJwJmcj99shZHf.jpeg', '1', 'Jalisco', 'Tlajomulco de Zúñiga', '10:00 A 19:00', '5,000.00', 70, 'Empresa: NAFARRATE EQUIPO MEDICO SUCURSAL: NAFARRATE', 'Ventas en mostrador, atención a clientes (personal, telefónico y correo), cotizaciones, procesos administrativos (fondo fijo, actualizar precios), revisar existencias de productos, resurtir mercancía • gusto por las ventas • puntualidad • alto sentido de compromiso • disciplina • paciencia • trabajo en equipo.\r\n\r\nEscolaridad\r\nGrado escolaridad: preparatoria\r\nCarrera: no necesaria\r\nAvance: 100 %.\r\n\r\nSoftware\r\nSoftware dominio\r\nMicrosoft office intermedio.\r\n\r\nOficio ó profesión\r\nVentas minima 1 año\r\nAtencion a clientes minima 1 año.\r\n\r\nHabilidades y competencias:\r\nVentas, gusto por las ventas, atención a cliente • manejo de equipo de Cómputo • paquetería office • Internet • gusto por las ventas •\r\nServicio al cliente • puntualidad • alto sentido de compromiso •Disciplina • paciencia • trabajo en equipo.', '2018-10-05 22:38:55', '2018-10-05 22:38:55', 0, '1', '2018-10-20'),
(24, '34', 'VENDEDOR DE MOSTRADOR', 'LICA SAFETY DEPOT', 'https://porvenirapps.com/prohunter/storage/app/company_logos/JRT2h92QhxGkwWhcFmcDP0sF0mEyzJs8emCzg2z0.jpeg', '1', 'Jalisco', 'Guadalajara', '8:30 A 6:30', '4,823.00', 80, 'Empresa: LICA SAFETY DEPOT', 'Promoción y venta de artículos de seguridad personal en sucursal, atención al cliente, ofrecer producto, labor de venta.\r\n\r\nHORARIO: L - V\r\nDE 8:30 A 6:30 SAB 9 A 2 PM.\r\n\r\nEscolaridad\r\nGrado escolaridad: preparatoria\r\nCarrera: no necesaria\r\nAvance: 100 %.\r\n\r\nSoftware\r\nSoftware dominio\r\nMicrosoft excel basico\r\nMicrosoft office basico\r\nMicrosoft windows basico.\r\n\r\nOficio ó profesión\r\nOficio experiencia\r\nVentas 1 a 11 meses.\r\n\r\nHabilidades y competencias:\r\nGusto por las ventas, amable, Pro-activo / a disponibilidad buena presentación.', '2018-10-05 22:43:58', '2018-10-05 22:43:58', 0, '1', '2018-10-20'),
(25, '34', 'CAJERO', 'AEROPUERTO DE AGUASCALIENTES, S.A. DE C.V.', 'https://porvenirapps.com/prohunter/storage/app/company_logos/MccYqZxrNlyKvzyJGqjs4jLlbCYZ2nlgkECfYXds.jpeg', '1', 'Aguascalientes', 'Calvillo', 'DISPONIBILIDAD DE HORARIO', '5,300.00', 70, 'Empresa: AEROPUERTO DE AGUASCALIENTES, S.A. DE C.V.', 'Trato a clientes, venta de producto, control de inventario, cortes de caja, manejo de efectivo. Apoyo administrativo en cierres de mes.\r\n\r\nEscolaridad\r\nGrado escolaridad: preparatoria\r\nCarrera: no necesaria\r\nAvance: 100 %\r\n\r\nSoftware\r\nSoftware dominio\r\nMicrosoft office basico\r\n\r\nOficio ó profesión\r\nOficio experiencia\r\nCajero 1 a 11 meses\r\n\r\nHabilidades y competencias:\r\nProactvo, dinámico, responsable, honesto, habilidad para los números,puntual, atención a clientes. Los candidatos de preferencia deberán\r\nVivir cerca del aeropuerto o en su defecto, no tener problemas de traslado.', '2018-10-05 22:55:29', '2018-10-05 22:55:29', 1, '1', '2018-10-22'),
(26, '31', 'PROMOTOR', 'INDUSTRIAL WENDY', 'https://porvenirapps.com/prohunter/storage/app/company_logos/HAQbnWsquwxRJ0sIxU1IOmAaBYqJqP33pctrhq8V.jpeg', '1', 'Guanajuato', 'León', '12:00 A 8:00', '4,000.00', 75, 'EMPRESA: INDUSTRIAL WENDY', 'Escolaridad\r\nGrado escolaridad: primaria\r\nCarrera: no necesaria\r\nAvance: 10 %\r\n\r\nSoftware\r\nSoftware dominio\r\n\r\nOficio ó profesión\r\nNinguno ninguna\r\n\r\nHabilidades y competencias:\r\nFacilidad de palabra, amable, proactivo. Experiencia en labor de venta,\r\nNegociación de espacio para exhibición y venta, acomodo de mercancía\r\nEstratégicamente, limpieza del área, abordar clientes\r\n\r\nComentarios:\r\nLabor de venta, negociación de espacio para exhibición y venta,\r\nAcomodo de mercancía estratégicamente, limpieza del área, abordar\r\nClientes potenciales, en tiendas de conveniencia y mueblerías grandes.', '2018-10-05 23:01:43', '2018-10-05 23:01:43', 0, '1', '2018-10-20'),
(27, '46', 'OPERADOR DIE CASTING', 'MARCHESI', 'https://porvenirapps.com/prohunter/storage/app/company_logos/Sm5THkXw8rSbCH6GmUCxXRC2WjjdiFqiU8Z0cmyz.png', '1', 'Jalisco', 'Acatlán de Juárez', 'MIXTO', '6,000.00', 70, 'Empresa: MARCHESI', 'Operar maquina de die casting mover y ajustar parámetros alimentar horno de fundición conocimientos de los riesgos y peligros\r\nDe su área y la maquina a operar llenado de formato de verificación de arranque de turno de maquina a operar llenado de\r\nFormato de liberación de primer pieza en maquina a operar o operación asignada llenar su hoja de producción hora por hora con\r\nCantidades de piezas producidas utiliza el equipo de protección personal de acuerdo a la descripción de su instructivo de\r\nOperación.\r\n\r\nEscolaridad\r\nGrado escolaridad: secundaria\r\nCarrera: no necesaria\r\n\r\nComentarios:\r\nBuena actitud, ganas de superación, honesto, responsable.', '2018-10-05 23:08:48', '2018-10-05 23:08:48', 0, '1', '2018-10-25'),
(28, '34', 'INGENIERO DE VENTAS', 'NATURA RECOVERY', 'https://porvenirapps.com/prohunter/storage/app/company_logos/d8HqOaOqjMUoeHYpHRbpzkJ1bJgQNuv1muymqfWa.png', '1', 'Jalisco', 'Zapopan', '8:00 A 17:30', '8,000.00', 75, 'Empresa: NATURA RECOVERY', 'Funciones a desempeñar: ofrecer los servicios de recolección de materiales reciclables.\r\n\r\nHabilidades y competencias:\r\nProactivo trabajo en equipo responsable puntual sentido de urgencia.\r\n\r\nComentarios:\r\nDisponibilidad para viajar comisiones por venta facturada.', '2018-10-05 23:14:05', '2018-10-05 23:14:05', 0, '1', '2018-10-20'),
(29, '31', 'OP EQPO (MAQUINISTA)', 'LICA SAFETY DEPOT', 'https://porvenirapps.com/prohunter/storage/app/company_logos/dND21lhFoDcHWMS3rB17tpJfOBDrEC6Pbd7qbq6g.png', '1', 'Jalisco', 'Zapopan', '7:00 A 15:00', '6,165.00', 80, 'Empresa: LICA SAFETY DEPOT', 'Operador de maquina, colocar calzado en maquina para que pegue la suela, quitar excesos, armar cajas, pintar, acomodar\r\nCalzado en racks.\r\n\r\nHabilidades y competencias:\r\nDeseable experiencia en fabricación de calzado, operando maquina.\r\nReportes de producción\r\n\r\nComentarios:\r\nSe realiza limpieza diaria antes de finalizar su turno de trabajo.', '2018-10-05 23:25:07', '2018-10-05 23:25:07', 0, '1', '2018-10-22'),
(30, '40', 'TECNICO DE SOPORTE', 'DAMSA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/hmGaPiHDsXbOtHduJl5mATa7nPuxUltMEPKyez0F.jpeg', '1', 'Jalisco', 'Zapopan', '08:00 A 18: 00', '8,620.00', 70, 'Empresa: DAMSA', 'Mantenimiento cableado estructurado, administración de redes wan, lan, wlan, administrar ad, usuarios de dominio,\r\nConfiguración sql instancias, manejar plataformas virtuales en este caso citrix, crear servidores y su administración, corregir\r\nErrores de la paqueteria office, correo y manejo excel, soporte a los usuarios en sitio y remoto, corregir o habilitar extensiones telefónicas, ajustarse a sistema de tickets.\r\n\r\nEscolaridad\r\nGrado escolaridad: licenciatura\r\nCarrera: sistemas computacionales\r\nAvance: 100 %\r\n\r\nSoftware\r\nSoftware dominio\r\nMicrosoft office avanzado\r\n\r\nOficio experiencia\r\nSistemas mínima 1 año.', '2018-10-06 00:47:44', '2018-10-06 00:54:12', 1, '1', '2018-10-25'),
(31, '34', 'RECEPCIONISTA RECLUTAMIENTO', 'DAMSA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/N5LmTSHM5PCRahW7j6k84QVvmxnNzWO2SIyAOF8k.jpeg', '1', 'Jalisco', 'Zapopan', '8:00 A 18:00', '6,712.00', 80, 'Empresa: DAMSA', 'Recibir llamadas telefónicas y canalizar con los colaboradores DAMSA según corresponda. Brindar información a los candidatos y al publico en general. Entregar materiales, documentos, tripticos, folletos a candidatos y publico en general. Publicar letreros e información diversa en paredes y pizarrones. Informar a ejecutivos de reclutamiento y candidatos.\r\n\r\nEscolaridad\r\nGrado escolaridad: técnica\r\nCarrera: no necesaria\r\nAvance: 100 %\r\n\r\nSoftware\r\nSoftware dominio:\r\nMicrosoft office básico.\r\n\r\nOficio experiencia\r\nRecepcionista mínima 1 año.\r\n\r\nHabilidades y competencias:\r\nExperiencia de 6 meses a 1 año en puesto similar, conocimiento y uso de Paqueteria office, buena ortografía deseable inglés básico/intermedio\r\nTrabajo en equipo, facilidad de palabra, actitud de servicio, proactivo, Enfoque a resultados, atención a clientes', '2018-10-06 00:53:43', '2018-10-06 00:53:54', 1, '1', '2018-10-25'),
(32, '33', 'CAPTURISTA DE DATOS', 'DAMSA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/5nX8KzWBTpIJIibG22kL0YNKPiOJAYtAGmwgRt54.jpeg', '1', 'Jalisco', 'Zapopan', '08:00 18: 00', '7,000.00', 80, 'Empresa: DAMSA', 'Funciones administrativas, captura de datos, inventarios, buen manejo de excel, manejo de office. El puesto real es auxiliar\r\nCapturista de activos.\r\n\r\nEscolaridad\r\nGrado escolaridad: técnica.\r\nCarrera: técnico en electrónica.\r\nAvance: 100 %.\r\n\r\nSoftware dominio\r\nMicrosoft excel intermedio.\r\nMicrosoft office intermedio.\r\n\r\nOficio experiencia\r\nAux. Administrativo 1 a 11 meses.\r\n\r\nHabilidades y competencias:\r\nPuntualidad, actitud de servicio, disponibilidad, honestidad, responsabilidad, organización, trabajo en equipo, valores.', '2018-10-06 00:57:58', '2018-10-06 00:57:58', 0, '1', '2018-10-20'),
(33, '37', 'BECARIO EN CONTABILIDAD', 'DAMSA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/3bK6EdQuoI7jQ7RixMRpcRXP0M2GjplZrv2eJZOF.jpeg', '1', 'Jalisco', 'Zapopan', '5 Horas diarias', '2,704.00', 20, 'Empresa: DAMSA', 'Becario destinado a apoyar el área de auditoria contable.\r\n\r\nGrado escolaridad: licenciatura\r\nCarrera: administración de empresas\r\nAvance: 60 %\r\n\r\nSoftware dominio\r\nMicrosoft excel basico\r\n\r\nOficio experiencia\r\nAux. Administrativo ninguna\r\n\r\nHabilidades y competencias:\r\nManejo de excel. Habilidades de análisis cualitativos y cuantitativos', '2018-10-06 01:02:40', '2018-10-06 01:02:40', 1, '1', '2018-10-22'),
(34, '31', 'AUXILIAR DE ACTIVOS', 'DAMSA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/AfIATlujvxsPayjcPuBWrUdmxuzchv0cjaouvB2s.jpeg', '1', 'Jalisco', 'Zapopan', '08:00 A 18: 00 hrs', '7,000.00', 80, 'Empresa: DAMSA', 'Conocimiento en almacenes e inventarios, funciones administrativas, conteos cíclicos de inventarios, carga y descarga de\r\nMobiliario, manejo de equipo de computo, licencia de conducir.\r\n\r\nEscolaridad\r\nGrado escolaridad: tecnica\r\nCarrera: tecnico en electronica\r\nAvance: 100 %\r\n\r\nSoftware dominio\r\nMicrosoft excel intermedio\r\nMicrosoft office intermedio\r\n\r\nOficio experiencia\r\nAux. Administrativo 1 a 11 meses\r\n\r\nHabilidades y competencias:\r\nPuntualidad, actitud de servicio, disponibilidad, honestidad, responsabilidad, organización, trabajo en equipo, valores.', '2018-10-06 01:07:02', '2018-10-06 01:07:02', 0, '1', '2018-10-20'),
(35, '40', 'DISEÑADOR GRAFICO', 'DAMSA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/uF7es5DZT61wh1iiDkFgPEMla0PDr4D5niOiwsJo.jpeg', '1', 'Jalisco', 'Zapopan', '08:00 A 18: 00 hrs', '10,650.00', 80, 'Empresa: Damsa', 'Puesto de diseñador gráfico Jr. Diseño de comunicados para colaboradores y partes interesadas, administración de redes sociales\r\nVinculadas a la empresa y sus trabajadores, vigilar el adecuado uso de logotipos e imagen de DAMSA y proponer actualizaciones e\r\nInnovación en materia de medios digitales.\r\n\r\nGrado escolaridad: licenciatura\r\nCarrera: diseño gráfico\r\nAvance: 100 %\r\n\r\nSoftware dominio\r\nDiseño gráfico 1 a 11 meses.\r\n\r\nConocimiento de idiomas\r\nIdioma lectura escritura conversación.\r\n\r\nHabilidades y competencias:\r\nExperiencia de 6 meses en puesto similar, uso de creative adobe.', '2018-10-06 01:10:57', '2018-10-06 01:11:20', 1, '1', '2018-10-20'),
(36, '34', 'BECARIO', 'DAMSA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/ctPWTZIW9AdInZGreIZ65wvUrfsOgtrCWKouQP2P.jpeg', '1', 'Jalisco', 'Zapopan', '8:00 A­ 13:00 hrs', '3,000', 70, 'Empresa: DAMSA', 'Aplicación de encuestas de satisfacción durante el proceso de reclutamiento.\r\n\r\nEscolaridad: licenciatura \r\nCarrera: administración de empresas\r\nAvance: 60 %.\r\n\r\nSoftware\r\nDominio microsoft excel básico.\r\n \r\nhabilidades y competencias: experiencia en aplicación de encuestas, servicio al cliente, manejo de aparatos electrónicos \'tablet\'. \r\nActitud de servicio, atención al cliente, facilidad de palabra, enfoque en resultados, proactivo, empático.', '2018-10-06 01:28:24', '2018-10-06 02:15:47', 1, '1', '2018-10-20'),
(37, '34', 'BECARIO', 'DAMSA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/m6BLkE8Fkatdi3oTqCB00djy5eVhxhjX9XRPWYBK.jpeg', '1', 'Jalisco', 'Zapopan', '13:00 A­ 18:00 hrs', '3,000.00', 80, 'Empresa: DAMSA', 'Aplicación de encuestas de satisfacción durante el proceso de reclutamiento.\r\n\r\nEscolaridad: licenciatura \r\nCarrera: administración de empresas\r\nAvance: 60 %.\r\n\r\nSoftware\r\nDominio microsoft excel básico.\r\n \r\nhabilidades y competencias: experiencia en aplicación de encuestas, servicio al cliente, manejo de aparatos electrónicos \'tablet\'. \r\nActitud de servicio, atención al cliente, facilidad de palabra, enfoque en resultados, proactivo, empático.', '2018-10-06 02:17:59', '2018-10-06 02:17:59', 1, '1', '2018-10-25'),
(40, '49', 'Vacante de prueba', 'Empresa 123', 'https://porvenirapps.com/prohunter/storage/app/company_logos/lpLkm8Q324Dq0qrPHVeQpj5D9NtCwl7TbVNoB0Gc.jpeg', '1', 'Jalisco', 'Amatitán', '08:00AM -13:00PM', '10,500.00', 14, 'djkdnvkljfdklvjfdklvhsdflkvndfkl', 'snmklnfkldnkldfndlfnl', '2018-10-16 01:14:00', '2018-10-16 01:15:06', 1, '1', '2019-12-12'),
(42, '33', 'aaa', 'aaa', 'https://porvenirapps.com/prohunter/storage/app/company_logos/LWtyaq5Rhiub5zdnkqxnW4dJdPBhnpv0P3UX6N7Z.jpeg', '1', 'Baja California Sur', 'Mulegé', '1 a 2', '1.00', 11, 'qqq', 'qqq', '2018-10-18 22:07:20', '2018-10-18 22:07:20', 0, '1', '2018-10-19'),
(43, '41', 'Becario', 'DAMSA', 'https://porvenirapps.com/prohunter/storage/app/company_logos/0E61bifB8g8M1t3DuQtZGX2ZL0Q8QZsK1qzqzCS4.jpeg', '1', 'México', 'Nezahualcóyotl', '9:00 a.m. 2:00 p.m.', '3,500.00', 90, 'Damsa', 'Becario de diseño', '2018-10-22 18:14:39', '2018-10-22 18:14:39', 1, '1', '2018-10-31'),
(46, '46', 'vacante prueba', 'prueba', 'https://porvenirapps.com/prohunter/storage/app/company_logos/cBGFSyhIiq3Uy5vUl3oBG9KZxENpO5lmrlgcuqfv.jpeg', '1', 'Chiapas', 'Angel Albino Corzo', '8:30 am :  2:30pm', '80,000.00', 4, '5000', '5000', '2018-11-06 21:33:11', '2018-11-06 21:33:11', 1, '91', '2018-11-09'),
(47, '39', 'Aliquam esse omn', 'Vel officia co', 'https://porvenirapps.com/prohunter/storage/app/company_logos/W9TCkF8sJ5B5v2x51y6sA5gJrPNWxCGzUOcx6jVp.jpeg', '1', 'Baja California Sur', 'Loreto', 'Quam velit voluptas nisi deserunt qui ut sequi', '785.00', 40, 'Aut accusantium sit magna alias asperiores delectus nesciunt hic ab non commodo', 'Omnis distinctio Sint qui voluptate vero', '2018-11-08 22:45:04', '2018-11-08 22:46:40', 1, '1', '2003-10-28'),
(48, '35', 'Sit dolor vel accus', 'accusamus e', 'https://porvenirapps.com/prohunter/storage/app/company_logos/llWkSQ3wmmv5j6aa2CgETGtTASpyB9AmDgE2x2M9.jpeg', '1', 'Campeche', 'Hecelchakán', 'Quo id rerum odit repellendus', '582.00', 96, 'Nisi iusto esse unde voluptas occaecat dolore sint', 'Consectetur eos voluptatem Quasi dicta Nam aut rerum reprehenderit velit', '2018-11-08 22:45:27', '2018-11-08 22:46:31', 0, '1', '1972-06-01'),
(49, '37', 'Natus ex ips', 'Sint distinc', 'https://porvenirapps.com/prohunter/storage/app/company_logos/Srsqum5dm3mC3tsoizgAxBLIxNUwW26pfcVwW4gx.jpeg', '1', 'Campeche', 'Hecelchakán', 'Impedit voluptate ea ipsum cum laudantium sint deserunt occaecat dolor voluptatem Officia doloribus sit ipsa vel aspernatur earum quae', '725.00', 54, 'Praesentium consequatur ratione nulla voluptas ut nostrum eum fugiat perferendis delectus nisi culpa earum aliqua Earum', 'Atque consequatur consequatur molestiae duis quis doloribus ex obcaecati rerum consectetur aut nostrud omnis non omnis animi', '2018-11-08 22:45:44', '2018-11-08 22:46:18', 0, '1', '2013-07-14');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vacantes_cubiertas`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vacantes_cubiertas`;
CREATE TABLE `vacantes_cubiertas` (
`id` int(10) unsigned
,`vacante_id` int(10) unsigned
,`Nombre` varchar(191)
,`company_name` varchar(191)
,`vacancy_name` varchar(191)
,`Contratado` text
,`Created_by` varchar(45)
,`vacante_creada_por` varchar(191)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `work_experience`
--

DROP TABLE IF EXISTS `work_experience`;
CREATE TABLE `work_experience` (
  `id` int(10) UNSIGNED NOT NULL,
  `candidate_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activities` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_month` int(11) NOT NULL,
  `start_year` int(11) NOT NULL,
  `end_month` int(11) NOT NULL DEFAULT '-1',
  `end_year` int(11) NOT NULL DEFAULT '-1',
  `work_portfolio_url_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_portfolio_url_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_portfolio_url_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `work_experience`
--

INSERT INTO `work_experience` (`id`, `candidate_id`, `active`, `position`, `company_name`, `role`, `activities`, `start_month`, `start_year`, `end_month`, `end_year`, `work_portfolio_url_1`, `work_portfolio_url_2`, `work_portfolio_url_3`, `created_at`, `updated_at`) VALUES
(10, '5', '1', 'In mollit cum obcaecati sed debitis anim quidem illum officia quam in assumenda incididunt tenetur voluptate', 'Aliquid distinctio Provident facilis ut dolor incidunt aut consequuntur et maxime corporis rem doloribus similique mollitia non et voluptate sunt', 'Impedit tempore vero nihil id officia enim voluptas neque proident cupiditate veniam aspernatur eum corrupti tempor fugit maiores maxime aut', 'Quo voluptatem laudantium deserunt velit deserunt doloremque tempore', 4, 2007, 10, 1967, 'Voluptate autem vel eiusmod nesciunt reprehenderit est', 'Voluptate autem vel eiusmod nesciunt reprehenderit est', 'Voluptate autem vel eiusmod nesciunt reprehenderit est', '2018-09-26 20:07:48', '2018-09-26 20:07:48'),
(11, '6', '1', 'Tempora velit atque cillum temporibus', 'Quia quisquam perspiciatis est sit', 'Elit minima in enim consectetur aliquid adipisicing aut libero architecto unde qui reprehenderit dolor explicabo', 'Ullamco est vel ut reiciendis vero non vitae est sequi reiciendis', 6, 1966, 0, 0, 'Officia sed id sint hic aliqua Quod consequatur voluptas incidunt', 'Officia sed id sint hic aliqua Quod consequatur voluptas incidunt', 'Officia sed id sint hic aliqua Quod consequatur voluptas incidunt', '2018-09-26 20:23:15', '2018-09-26 20:23:15'),
(12, '7', '1', 'Tempora velit atque cillum temporibus', 'Quia quisquam perspiciatis est sit', 'Elit minima in enim consectetur aliquid adipisicing aut libero architecto unde qui reprehenderit dolor explicabo', 'Ullamco est vel ut reiciendis vero non vitae est sequi reiciendis', 6, 1966, 0, 0, 'Officia sed id sint hic aliqua Quod consequatur voluptas incidunt', 'Officia sed id sint hic aliqua Quod consequatur voluptas incidunt', 'Officia sed id sint hic aliqua Quod consequatur voluptas incidunt', '2018-09-26 20:24:06', '2018-09-26 20:24:06'),
(13, '8', '1', 'Position', 'Test company', 'Test Role', 'Test Activities', 1, 2014, 12, 2018, NULL, NULL, NULL, '2018-09-26 20:43:54', '2018-09-26 20:43:54'),
(14, '8', '0', 'Position', 'Test company', 'Test Role', 'Test Activities', 1, 2014, 12, 2018, NULL, NULL, NULL, '2018-09-26 20:43:54', '2018-09-26 20:43:54'),
(15, '9', '1', 'Non voluptates excepteur quisquam odit delectus aut fugiat', 'Id sequi veniam nihil nisi ea aut praesentium sint exercitationem aut', 'Irure esse fugiat laborum Delectus quam dolores delectus placeat quibusdam veniam voluptates qui et quo nostrud esse dolor asperiores', 'Nisi fugiat magna ad eos architecto sunt quis eligendi saepe optio mollitia', 4, 1994, 0, 0, 'Aut aut aut molestiae est', 'Aut aut aut molestiae est', 'Aut aut aut molestiae est', '2018-09-26 21:00:01', '2018-09-26 21:00:01'),
(16, '10', '1', 'Molestiae Nam suscipit odit lorem vitae', 'Et architecto non distinctio Rerum minima', 'Quam non dolor obcaecati do sunt sunt', 'Ex natus laboris qui ad et qui illum qui molestias molestiae commodo aut minima', 3, 1967, 8, 1997, 'Veniam itaque cum velit tempore ut consequat Est nulla quaerat', 'Veniam itaque cum velit tempore ut consequat Est nulla quaerat', 'Veniam itaque cum velit tempore ut consequat Est nulla quaerat', '2018-09-26 22:43:40', '2018-09-26 22:43:40'),
(17, '11', '1', 'Molestiae Nam suscipit odit lorem vitae', 'Et architecto non distinctio Rerum minima', 'Quam non dolor obcaecati do sunt sunt', 'Ex natus laboris qui ad et qui illum qui molestias molestiae commodo aut minima', 3, 1967, 8, 1997, 'Veniam itaque cum velit tempore ut consequat Est nulla quaerat', 'Veniam itaque cum velit tempore ut consequat Est nulla quaerat', 'Veniam itaque cum velit tempore ut consequat Est nulla quaerat', '2018-09-26 22:46:44', '2018-09-26 22:46:44'),
(18, '12', '1', 'Molestiae Nam suscipit odit lorem vitae', 'Et architecto non distinctio Rerum minima', 'Quam non dolor obcaecati do sunt sunt', 'Ex natus laboris qui ad et qui illum qui molestias molestiae commodo aut minima', 3, 1967, 8, 1997, 'Veniam itaque cum velit tempore ut consequat Est nulla quaerat', 'Veniam itaque cum velit tempore ut consequat Est nulla quaerat', 'Veniam itaque cum velit tempore ut consequat Est nulla quaerat', '2018-09-26 22:47:18', '2018-09-26 22:47:18'),
(21, '13', '1', 'Itaque fuga Consectetur molestias quidem natus deleniti aspernatur deserunt possimus nihil', 'Ut quibusdam ea veniam velit quis velit obcaecati illum beatae sed officia qui vel quibusdam adipisci', 'Non ut laboris ad ipsum accusamus optio aut quos aliquam non voluptatem Elit est', 'Hic quia illo dignissimos deserunt nulla accusamus', 4, 2017, 1, 2000, 'Odit modi dolorem rerum non dicta praesentium quod dolore eligendi officiis et', 'Odit modi dolorem rerum non dicta praesentium quod dolore eligendi officiis et', 'Odit modi dolorem rerum non dicta praesentium quod dolore eligendi officiis et', '2018-09-27 00:06:46', '2018-09-27 00:06:46'),
(22, '14', '1', 'Qui aute porro dolor dolor fugiat omnis dolore incidunt anim est eveniet qui possimus culpa aperiam', 'Voluptatibus quidem dolor laboris reiciendis eu', 'Porro et magnam vel esse ea illum sed veniam officiis quaerat reprehenderit mollitia', 'Dolor pariatur Reiciendis culpa magnam voluptatem Neque est vitae repudiandae aut sint exercitationem', 11, 1991, 2, 1974, 'Aliquid labore rerum voluptatum pariatur Vel non et dolorum eius minus sed reprehenderit omnis sed dolor corporis omnis quia', 'Aliquid labore rerum voluptatum pariatur Vel non et dolorum eius minus sed reprehenderit omnis sed dolor corporis omnis quia', 'Aliquid labore rerum voluptatum pariatur Vel non et dolorum eius minus sed reprehenderit omnis sed dolor corporis omnis quia', '2018-09-27 00:26:42', '2018-09-27 00:26:42'),
(23, '15', '1', 'Qui aute porro dolor dolor fugiat omnis dolore incidunt anim est eveniet qui possimus culpa aperiam', 'Voluptatibus quidem dolor laboris reiciendis eu', 'Porro et magnam vel esse ea illum sed veniam officiis quaerat reprehenderit mollitia', 'Dolor pariatur Reiciendis culpa magnam voluptatem Neque est vitae repudiandae aut sint exercitationem', 11, 1991, 2, 1974, 'Aliquid labore rerum voluptatum pariatur Vel non et dolorum eius minus sed reprehenderit omnis sed dolor corporis omnis quia', 'Aliquid labore rerum voluptatum pariatur Vel non et dolorum eius minus sed reprehenderit omnis sed dolor corporis omnis quia', 'Aliquid labore rerum voluptatum pariatur Vel non et dolorum eius minus sed reprehenderit omnis sed dolor corporis omnis quia', '2018-09-27 00:27:04', '2018-09-27 00:27:04'),
(24, '16', '1', 'Duis labore quis eius aliquid culpa sint necessitatibus adipisci', 'Odio sit commodo a qui sed magna quia consequatur mollitia rerum', 'Quibusdam totam perferendis molestiae magni aspernatur laborum quia eu et officia molestiae ad doloribus', 'Rerum atque Nam eos soluta', 6, 1979, 8, 1985, 'Id id eos dolores qui enim consectetur quis ullam voluptate porro sed sed quam quo', 'Id id eos dolores qui enim consectetur quis ullam voluptate porro sed sed quam quo', 'Id id eos dolores qui enim consectetur quis ullam voluptate porro sed sed quam quo', '2018-09-27 00:30:51', '2018-09-27 00:30:51'),
(25, '17', '1', 'Duis labore quis eius aliquid culpa sint necessitatibus adipisci', 'Odio sit commodo a qui sed magna quia consequatur mollitia rerum', 'Quibusdam totam perferendis molestiae magni aspernatur laborum quia eu et officia molestiae ad doloribus', 'Rerum atque Nam eos soluta', 6, 1979, 8, 1985, 'Id id eos dolores qui enim consectetur quis ullam voluptate porro sed sed quam quo', 'Id id eos dolores qui enim consectetur quis ullam voluptate porro sed sed quam quo', 'Id id eos dolores qui enim consectetur quis ullam voluptate porro sed sed quam quo', '2018-09-27 00:33:11', '2018-09-27 00:33:11'),
(26, '18', '1', 'Duis labore quis eius aliquid culpa sint necessitatibus adipisci', 'Odio sit commodo a qui sed magna quia consequatur mollitia rerum', 'Quibusdam totam perferendis molestiae magni aspernatur laborum quia eu et officia molestiae ad doloribus', 'Rerum atque Nam eos soluta', 6, 1979, 8, 1985, 'Id id eos dolores qui enim consectetur quis ullam voluptate porro sed sed quam quo', 'Id id eos dolores qui enim consectetur quis ullam voluptate porro sed sed quam quo', 'Id id eos dolores qui enim consectetur quis ullam voluptate porro sed sed quam quo', '2018-09-27 00:46:49', '2018-09-27 00:46:49'),
(27, '19', '1', 'Reprehenderit eum nulla voluptatem illum aut', 'Asperiores id ea et molestiae eligendi nulla odio elit eum ea repellendus Quo dicta elit', 'Soluta error illum laborum aut qui consectetur dolore ullamco beatae labore eius distinctio Aliquam', 'Dolor hic sed id ex commodi accusamus corporis magnam fugit proident magni nihil facere sed sed nihil suscipit laudantium', 12, 2013, 0, 0, 'Sint est assumenda magnam cillum ullamco', 'Sint est assumenda magnam cillum ullamco', 'Sint est assumenda magnam cillum ullamco', '2018-09-27 00:50:02', '2018-09-27 00:50:02'),
(28, '20', '1', 'Voluptas accusantium aspernatur molestias cumque id illo sit nostrum reiciendis et placeat officia proident', 'Quas sed cupiditate ut at', 'Enim unde excepturi velit in numquam non suscipit et sunt quam pariatur Quod nisi dolor totam nihil dolores', 'Ad veniam nihil incididunt natus autem', 3, 2001, 4, 1976, 'Nihil impedit dolorem aperiam magni quis elit repudiandae ullamco assumenda est tenetur ullam rerum', 'Nihil impedit dolorem aperiam magni quis elit repudiandae ullamco assumenda est tenetur ullam rerum', 'Nihil impedit dolorem aperiam magni quis elit repudiandae ullamco assumenda est tenetur ullam rerum', '2018-09-27 01:30:21', '2018-09-27 01:30:21'),
(29, '21', '1', 'Esse qui doloribus odit esse inventore exercitation elit in quod id sunt quod et culpa aut eaque inventore ut quod', 'Nostrud et voluptate officia numquam quo similique amet ut voluptas voluptate maiores sunt sapiente voluptas', 'Omnis possimus dolore corrupti autem laborum Aut eius reiciendis', 'Proident sed repellendus Ullamco sint velit nemo consequatur Autem alias commodo cumque vero quo ipsam', 10, 1974, 9, 1992, 'Eius tempore culpa incidunt tenetur', 'Eius tempore culpa incidunt tenetur', 'Eius tempore culpa incidunt tenetur', '2018-09-27 01:32:44', '2018-09-27 01:32:44'),
(30, '22', '1', 'Eos adipisci esse sequi ipsam illo officia consequatur corporis culpa nulla vitae voluptate', 'Excepteur qui velit vero sed rerum a omnis', 'Irure sunt inventore quis quis cupiditate doloremque sed', 'Laudantium qui debitis modi omnis molestiae', 1, 2018, 9, 2008, 'Fugiat velit minus accusantium saepe eligendi consequatur incidunt', 'Fugiat velit minus accusantium saepe eligendi consequatur incidunt', 'Fugiat velit minus accusantium saepe eligendi consequatur incidunt', '2018-09-27 01:55:38', '2018-09-27 01:55:38'),
(45, '28', '1', 'Jxjx', 'Jzjz', 'Nzndj', 'Nzndj', 6, 2013, 0, 2018, '', '', '', '2018-09-28 21:20:32', '2018-09-28 21:20:32'),
(49, '30', '1', 'asda', 'asd', 'asd', 'asd', 2, 2016, 0, 2018, 'https://web.whatsapp.com/', 'https://web.whatsapp.com/', 'https://web.whatsapp.com/', '2018-09-29 01:57:16', '2018-09-29 01:57:16'),
(51, '2', '1', 'Bdbd', 'Jsjd', 'Jsjd', 'Jsjd', 7, 2014, 0, 2018, 'jdkd', 'jdkd', 'jdkd', '2018-09-29 04:41:50', '2018-09-29 04:41:50'),
(60, '33', '0', 'Atención a clientes', 'Atento', 'Teleservicios', 'Atención a clientes y portabilidad', 2, 2018, 7, 2018, '', '', '', '2018-10-02 23:57:24', '2018-10-02 23:57:24'),
(61, '33', '0', 'Ayudante de oficina', 'CNR industries de México', 'Importación y exportación', 'Ayudante de oficina, actividades varias', 3, 2017, 1, 2018, '', '', '', '2018-10-02 23:57:24', '2018-10-02 23:57:24'),
(63, '31', '1', 'gerente ', 'empresa', 'giro ', 'Jdjdkdkdkdkdkdkdk', 2, 2014, 1, 2018, NULL, NULL, NULL, '2018-10-03 22:44:42', '2018-10-03 22:44:42'),
(67, '36', '1', 'becario de desarrollo ', 'olor ', 'electronics ', 'Jskdjd\nJsjdjd\nJsjdjd\nJsjdjd is\nJsjdjd\n', 4, 2015, 12, 2015, NULL, NULL, NULL, '2018-10-04 23:54:09', '2018-10-04 23:54:09'),
(72, '39', '1', 'Comandante', 'Compañía de Francia', 'Proveedora ', 'Dirigir tropas', 6, 2002, 4, 2018, '', '', '', '2018-10-10 22:31:21', '2018-10-10 22:31:21'),
(116, '48', '1', 'Position', 'Test company', 'Test Role', 'Test Activities', 1, 2014, 12, 2018, NULL, NULL, NULL, '2018-10-19 18:18:35', '2018-10-19 18:18:35'),
(117, '48', '0', 'Position', 'Test company', 'Test Role', 'Test Activities', 1, 2014, 12, 2018, NULL, NULL, NULL, '2018-10-19 18:18:35', '2018-10-19 18:18:35'),
(127, '38', '1', 'Mantenimiento ', 'Flex', 'Manufactura ', 'Mantenimiento a maquinaria especial', 1, 2011, 0, 0, '', '', '', '2018-10-19 19:30:16', '2018-10-19 19:30:16'),
(129, '40', '0', 'Gerente general ', 'Flex', 'Manufactura ', 'Administrar ', 2, 2016, 9, 2018, '', '', '', '2018-10-19 19:54:49', '2018-10-19 19:54:49'),
(130, '37', '0', 'Gerente ', 'Jabil', 'Manufactura ', 'Administración de la Planta', 1, 2015, 5, 2018, '', '', '', '2018-10-19 20:00:00', '2018-10-19 20:00:00'),
(136, '56', '1', 'Analista de procesos', 'Damsa', 'Industrual ', 'Anlita de sw ', 12, 2017, 0, 0, '', '', '', '2018-10-22 18:45:59', '2018-10-22 18:45:59'),
(137, '55', '1', 'Capturista de datos', 'Damsa', 'Industrial', 'Capturista de fatos ', 10, 2017, 0, 0, '', '', '', '2018-10-22 19:13:49', '2018-10-22 19:13:49'),
(141, '57', '0', 'Lider de program y diseño web', 'Comercializadora de valor agregado', 'Giro', 'Funciones', 6, 2015, 5, 2011, '', '', '', '2018-10-22 23:02:00', '2018-10-22 23:02:00'),
(145, '32', '0', 'A', 'A', 'A', 'A', 6, 2018, 6, 2012, '', '', '', '2018-10-25 01:00:19', '2018-10-25 01:00:19'),
(166, '34', '1', 'Puesto en la empresa Puesto en la empresa Puesto en la empresa', 'Nombre de la empresa Nombre de la empresa Nombre de la empresa', 'Giro de la empresa Giro de la empresa Giro de la empresa', 'Actividades y/o funciones Actividades y/o funciones Actividades y/o funciones Actividades y/o funciones', 3, 2012, 0, 2018, '', '', '', '2018-10-29 19:45:01', '2018-10-29 19:45:01'),
(168, '47', '1', 'Jefa directa', 'lol in', 'tecnología ', 'tecnología ', 8, 2015, 1, 2018, '', '', '', '2018-10-30 20:11:06', '2018-10-30 20:11:06'),
(171, '41', '0', 'Administrador', 'Jabil', 'Manufactura ', 'Administrador', 5, 2013, 5, 2018, '', '', '', '2018-10-31 20:21:23', '2018-10-31 20:21:23'),
(174, '59', '0', 'Programador', 'Oomovil', 'Desarrolladores', 'Android', 9, 2018, 12, 2018, '', '', '', '2018-11-02 20:19:27', '2018-11-02 20:19:27'),
(175, '61', '0', 'Lider de programacion y diseño web', 'Damsa', 'Personal', 'Programacion', 8, 2017, 6, 2012, '', '', '', '2018-11-05 21:04:31', '2018-11-05 21:04:31'),
(178, '53', '0', 'Desarrollo', 'oOMovil', 'Desarrollo', 'Desarrollador', 7, 2016, 8, 2016, '', '', '', '2018-11-09 22:51:48', '2018-11-09 22:51:48'),
(179, '62', '1', 'programador ', 'ITSON', 'educación ', 'Programación ', 9, 2016, 1, 2018, NULL, NULL, NULL, '2018-11-14 03:59:05', '2018-11-14 03:59:05');

-- --------------------------------------------------------

--
-- Estructura para la vista `Buscar_recomendados`
--
DROP TABLE IF EXISTS `Buscar_recomendados`;

CREATE  VIEW `Buscar_recomendados`  AS  select `f`.`token` AS `token`,`can`.`user_id` AS `user_id`,`can`.`id` AS `candidate_id`,`v`.`id` AS `vacancy_id`,concat(`can`.`names`,' ',`can`.`paternal_name`,' ',`can`.`maternal_name`) AS `full_name`,`v`.`vacancy_name` AS `vacancy_name`,`v`.`company_name` AS `company_name`,`s`.`name` AS `name` from ((((((`candidates` `can` join `firebase` `f` on((`can`.`user_id` = `f`.`user_id`))) join `candidate_software` `cs` on((`can`.`id` = `cs`.`candidate_id`))) join `software` `s` on((`cs`.`software_id` = `s`.`id`))) join `software_categories` `sc` on((`s`.`id` = `sc`.`id_software`))) join `categories` `cat` on((`sc`.`id_categoria` = `cat`.`id`))) join `vacancies` `v` on((`cat`.`id` = `v`.`category_id`))) where (`v`.`Active` = 1) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `Buscar_usuarios_recomendados_vacante`
--
DROP TABLE IF EXISTS `Buscar_usuarios_recomendados_vacante`;

CREATE  VIEW `Buscar_usuarios_recomendados_vacante`  AS  select `can`.`id` AS `candidate_id`,`v`.`id` AS `vacancy_id`,concat(`can`.`names`,'',`can`.`paternal_name`,'',`can`.`maternal_name`) AS `full_name`,`v`.`vacancy_name` AS `vacancy_name`,`s`.`name` AS `name` from (((((`candidates` `can` join `candidate_software` `cs` on((`can`.`id` = `cs`.`candidate_id`))) join `software` `s` on((`cs`.`software_id` = `s`.`id`))) join `software_categories` `sc` on((`s`.`id` = `sc`.`id_software`))) join `categories` `cat` on((`sc`.`id_categoria` = `cat`.`id`))) join `vacancies` `v` on((`cat`.`id` = `v`.`category_id`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `Grafica_Preferencias_genero`
--
DROP TABLE IF EXISTS `Grafica_Preferencias_genero`;

CREATE  VIEW `Grafica_Preferencias_genero`  AS  select (select count(0) AS `genero_masculino` from `candidates` where (`candidates`.`gender` = 'Hombre')) AS `Masculino`,(select count(0) AS `genero_femenino` from `candidates` where (`candidates`.`gender` = 'Mujer')) AS `Femenino` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `postulados_rechazados`
--
DROP TABLE IF EXISTS `postulados_rechazados`;

CREATE  VIEW `postulados_rechazados`  AS  select `cv`.`id` AS `id`,`v`.`id` AS `vacante_id`,`u`.`correo` AS `correo`,concat(`c`.`names`,' ',`c`.`paternal_name`,' ',`c`.`maternal_name`) AS `candidato`,`v`.`vacancy_name` AS `vacancy_name`,`v`.`company_name` AS `company_name`,`cv`.`status` AS `status`,`cv`.`reason_rejected` AS `reason_rejected`,`cv`.`date_rejected` AS `date_rejected`,`v`.`Created_by` AS `Created_by` from (((`candidate_vacancy` `cv` join `vacancies` `v` on((`cv`.`vacancy_id` = `v`.`id`))) join `candidates` `c` on((`cv`.`candidate_id` = `c`.`id`))) join `users` `u` on((`c`.`user_id` = `u`.`id`))) where (`cv`.`status` = 'rechazado') ;

-- --------------------------------------------------------

--
-- Estructura para la vista `Postulados_vs_contratados_30_dias`
--
DROP TABLE IF EXISTS `Postulados_vs_contratados_30_dias`;

CREATE  VIEW `Postulados_vs_contratados_30_dias`  AS  select (select count(0) AS `postulados` from `candidate_vacancy` where ((`candidate_vacancy`.`status` <> 'Total_postulados') and (`candidate_vacancy`.`created_at` <> 0) and (curdate() - interval -(30) day))) AS `Total_postulados`,(select count(0) AS `postulados` from `candidate_vacancy` where ((`candidate_vacancy`.`status` = 'Contratado') and (`candidate_vacancy`.`created_at` <> 0) and (curdate() - interval -(30) day))) AS `Total_contratados` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `reporte_candidatos_a_apagar`
--
DROP TABLE IF EXISTS `reporte_candidatos_a_apagar`;

CREATE  VIEW `reporte_candidatos_a_apagar`  AS  select `c`.`user_id` AS `id_usuario`,(select `users`.`nombre` from `users` where (`users`.`id` = `c`.`user_id`)) AS `Usuario_APP`,(select `users`.`fecha_cumpleaños` from `users` where (`users`.`id` = `c`.`user_id`)) AS `fecha_cumpleaños`,(select `users`.`RFC` from `users` where (`users`.`id` = `c`.`user_id`)) AS `RFC`,`ubd`.`cuenta_clabe` AS `cuenta_clabe`,`v`.`vacancy_name` AS `Nombre_vacante`,concat(`c`.`names`,' ',`c`.`maternal_name`,' ',`c`.`paternal_name`) AS `Candidato`,`pu`.`total_to_pay` AS `Total_a_pagar`,`pu`.`paid_percentage` AS `Porcentage_a_pagar`,((`pu`.`total_to_pay` / 100) * (100 - replace(`pu`.`paid_percentage`,'%',''))) AS `faltante`,`pu`.`created_at` AS `fecha` from ((((`payment_users` `pu` left join `candidate_vacancy` `cv` on((`pu`.`postulate_id` = `cv`.`id`))) left join `vacancies` `v` on((`cv`.`vacancy_id` = `v`.`id`))) left join `candidates` `c` on((`cv`.`candidate_id` = `c`.`id`))) left join `user_bank_data` `ubd` on((`ubd`.`id_user` = `c`.`user_id`))) where (`pu`.`paid_percentage` <> '100%') ;

-- --------------------------------------------------------

--
-- Estructura para la vista `tabla_postulados`
--
DROP TABLE IF EXISTS `tabla_postulados`;

CREATE  VIEW `tabla_postulados`  AS  select `cv`.`id` AS `id`,`c`.`id` AS `id_candidate`,(select `users`.`nombre` from `users` where (`users`.`id` = `c`.`user_id`)) AS `Usuario`,`c`.`names` AS `names`,`c`.`paternal_name` AS `paternal_name`,`c`.`maternal_name` AS `maternal_name`,`v`.`company_name` AS `Compañia`,`v`.`vacancy_name` AS `Vacancy`,`cv`.`vacancy_id` AS `vacancy_id`,`cv`.`status` AS `status`,`v`.`Created_by` AS `created_by` from ((`vacancies` `v` join `candidate_vacancy` `cv` on((`v`.`id` = `cv`.`vacancy_id`))) join `candidates` `c` on((`cv`.`candidate_id` = `c`.`id`))) where ((`cv`.`Active` = 1) and (`cv`.`status` <> 'Vacante cubierta')) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vacantes_cubiertas`
--
DROP TABLE IF EXISTS `vacantes_cubiertas`;

CREATE  VIEW `vacantes_cubiertas`  AS  select `cv`.`id` AS `id`,`v`.`id` AS `vacante_id`,`u`.`nombre` AS `Nombre`,`v`.`company_name` AS `company_name`,`v`.`vacancy_name` AS `vacancy_name`,(select (select concat(`cc`.`names`,' ',`cc`.`paternal_name`,' ',`cc`.`maternal_name`) from `candidates` `cc` where (`cc`.`id` = `cvc`.`candidate_id`)) from `candidate_vacancy` `cvc` where ((`cvc`.`status` = 'Contratado') and (`cvc`.`vacancy_id` = `cv`.`vacancy_id`))) AS `Contratado`,`v`.`Created_by` AS `Created_by`,(select `users`.`nombre` from `users` where (`users`.`id` = `v`.`Created_by`)) AS `vacante_creada_por` from (((`candidate_vacancy` `cv` join `vacancies` `v` on((`cv`.`vacancy_id` = `v`.`id`))) join `candidates` `c` on((`cv`.`candidate_id` = `c`.`id`))) join `users` `u` on((`c`.`user_id` = `u`.`id`))) where (`v`.`Active` = 0) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `administrator_modules_assigned`
--
ALTER TABLE `administrator_modules_assigned`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `available_banks`
--
ALTER TABLE `available_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `candidate_language`
--
ALTER TABLE `candidate_language`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `candidate_software`
--
ALTER TABLE `candidate_software`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `candidate_vacancy`
--
ALTER TABLE `candidate_vacancy`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `category_preference`
--
ALTER TABLE `category_preference`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `education_data`
--
ALTER TABLE `education_data`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `firebase`
--
ALTER TABLE `firebase`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `intro_sliders`
--
ALTER TABLE `intro_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modules_admin`
--
ALTER TABLE `modules_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payment_users`
--
ALTER TABLE `payment_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `preferences`
--
ALTER TABLE `preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `software`
--
ALTER TABLE `software`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `software_categories`
--
ALTER TABLE `software_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_candidate`
--
ALTER TABLE `status_candidate`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_correo_unique` (`correo`);

--
-- Indices de la tabla `user_bank_data`
--
ALTER TABLE `user_bank_data`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_rating`
--
ALTER TABLE `user_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_vacancy`
--
ALTER TABLE `user_vacancy`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vacancies`
--
ALTER TABLE `vacancies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `work_experience`
--
ALTER TABLE `work_experience`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `about`
--
ALTER TABLE `about`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `administrator_modules_assigned`
--
ALTER TABLE `administrator_modules_assigned`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=429;

--
-- AUTO_INCREMENT de la tabla `available_banks`
--
ALTER TABLE `available_banks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `candidate_language`
--
ALTER TABLE `candidate_language`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT de la tabla `candidate_software`
--
ALTER TABLE `candidate_software`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT de la tabla `candidate_vacancy`
--
ALTER TABLE `candidate_vacancy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `category_preference`
--
ALTER TABLE `category_preference`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1810;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2458;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `education_data`
--
ALTER TABLE `education_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;

--
-- AUTO_INCREMENT de la tabla `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `firebase`
--
ALTER TABLE `firebase`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `intro_sliders`
--
ALTER TABLE `intro_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=663;

--
-- AUTO_INCREMENT de la tabla `modules_admin`
--
ALTER TABLE `modules_admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `notices`
--
ALTER TABLE `notices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `payment_users`
--
ALTER TABLE `payment_users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `preferences`
--
ALTER TABLE `preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `software`
--
ALTER TABLE `software`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `software_categories`
--
ALTER TABLE `software_categories`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `status_candidate`
--
ALTER TABLE `status_candidate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT de la tabla `user_bank_data`
--
ALTER TABLE `user_bank_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `user_rating`
--
ALTER TABLE `user_rating`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `user_vacancy`
--
ALTER TABLE `user_vacancy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=335;

--
-- AUTO_INCREMENT de la tabla `vacancies`
--
ALTER TABLE `vacancies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `work_experience`
--
ALTER TABLE `work_experience`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
